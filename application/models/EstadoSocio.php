<?php

abstract class EstadoSocio
{
    const Activo = 0;
    const DadoDeBaja = 1;
    const Moroso = 2;
}