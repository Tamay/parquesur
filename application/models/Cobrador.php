<?php

/**
 * Template DataMapper Model
 *
 * Use this basic model as a template for creating new models.
 * It is not recommended that you include this file with your application,
 * especially if you use a Template library (as the classes may collide).
 *
 * To use:
 * 1) Copy this file to the lowercase name of your new model.
 * 2) Find-and-replace (case-sensitive) 'Template' with 'Your_model'
 * 3) Find-and-replace (case-sensitive) 'template' with 'your_model'
 * 4) Find-and-replace (case-sensitive) 'templates' with 'your_models'
 * 5) Edit the file as desired.
 *
 * @license		MIT License
 * @category	Models
 * @author		Phil DeJarnett
 * @link		http://www.overzealous.com
 */

class Cobrador extends DataMapper {
	// Uncomment and edit these two if the class has a model name that
	//  doesn't convert properly using the inflector_helper.
	// var $model = 'template';
	// var $table = 'templates';
	var $table = 'cobradores';

	// You can override the database connections with this option
	// var $db_params = 'db_config_name';

	// --------------------------------------------------------------------
	// Relationships
	//   Configure your relationships below
	// --------------------------------------------------------------------
	// Insert related models that Template can have just one of.
	var $has_one = array('persona');


	// Insert related models that Template can have more than one of.
	var $has_many = array('socio');
	/* Relationship Examples
	 * For normal relationships, simply add the model name to the array:
	 *   $has_one = array('user'); // Template has one User
	 *
	 * For complex relationships, such as having a Creator and Editor for
	 * Template, use this form:
	 *   $has_one = array(
	 *   	'creator' => array(
	 *   		'class' => 'user',
	 *   		'other_field' => 'created_template'
	 *   	)
	 *   );
	 *
	 * Don't forget to add 'created_template' to User, with class set to
	 * 'template', and the other_field set to 'creator'!
	 *
	 */

	// --------------------------------------------------------------------
	// Validation
	//   Add validation requirements, such as 'required', for your fields.
	// --------------------------------------------------------------------

	var $validation = array(
		/*'example' => array(
			// example is required, and cannot be more than 120 characters long.
			'rules' => array('required', 'max_length' => 120),
			'label' => 'Example'
		)*/
	);

	// --------------------------------------------------------------------
	// Default Ordering
	//   Uncomment this to always sort by 'name', then by
	//   id descending (unless overridden)
	// --------------------------------------------------------------------

	// var $default_order_by = array('name', 'id' => 'desc');

	// --------------------------------------------------------------------

	/**
	 * Constructor: calls parent constructor
	 */
    function __construct($id = NULL)
	{
		parent::__construct($id);
    }

	// --------------------------------------------------------------------
	// Post Model Initialisation
	//   Add your own custom initialisation code to the Model
	// The parameter indicates if the current config was loaded from cache or not
	// --------------------------------------------------------------------
	function post_model_init($from_cache = FALSE)
	{
	}

	// --------------------------------------------------------------------
	// Custom Methods
	//   Add your own custom methods here to enhance the model.
	// --------------------------------------------------------------------

	/* Example Custom Method
	function get_open_templates()
	{
		return $this->where('status <>', 'closed')->get();
	}
	*/

	// --------------------------------------------------------------------
	// Custom Validation Rules
	//   Add custom validation rules for this model here.
	// --------------------------------------------------------------------

	/* Example Rule
	function _convert_written_numbers($field, $parameter)
	{
	 	$nums = array('one' => 1, 'two' => 2, 'three' => 3);
	 	if(in_array($this->{$field}, $nums))
		{
			$this->{$field} = $nums[$this->{$field}];
	 	}
	}
	*/
function _importarCobradores($rutaExcel)
    {
        require_once ("Excel/reader.php");
        $data = new Spreadsheet_Excel_Reader();
        $data->setOutputEncoding('CP1251');
        $data->read($rutaExcel);
        error_reporting(E_ALL ^ E_NOTICE);

        //LA SIGUIENTE LÍNEA LEE DE LA HOJA 0, LA CELDA A1
        //echo $data->sheets[0]['cells'][1][1];
        for ($i = 1; $i <= 20; $i++)
        {
		$nroCobrador = $data->sheets[0]['cells'][i][1];
        $nombreCompleto = $data->sheets[0]['cells'][i][2];
        $comision = $data->sheets[0]['cells'][i][3];
        $dni = i;

     	$Persona  = new Persona();
		$dniBusqueda = $Persona->get_by_dni()->dni;
		//Si no existe la persona con ese dni, la creo antes de crear el cobrador
		if($dniBusqueda=='')
        {
        	//Separación de los nombres
        	$nombres = explode(" ", $nombreCompleto);
        	
        	$Persona->nombre = $nombres[1] + ' ' + $nombres[2] + ' ' +$nombres[3];
            $Persona->apellidos = $nombres[0];
                $Persona->dni = $dni;
                $Persona->genero = 1;
                $Persona->correoElectronico = '';
                $Persona->fechaNacimiento = '';
                $Persona->profesion = '';
                $Persona->domicilio = '';
				$Persona->telefonoFijo = '';
				$Persona->telefonoCelular = '';
				$Persona->nacionalidad = '';
				$Persona->estadoCivil = '';
            
				//Guardo la persona
                ($Persona->save());
         		//Creo el cobrador y lo asigno a esa persona
							
				 $Cobrador = new Cobrador();
				 $Cobrador->estadoCobrador = EstadoCobrador::Activo;
                ($Cobrador->save(array($Persona)));
				//Paso el mensaje de exito
                
        }
        else //Si la persona ya existia hay dos posibilidades: o es cobrador o socio
        {
				//Compruebo que no exista ya el cobrador a nombre de esa persona
				 $Cobradores = new Cobrador();
				 $Cobradores -> get();
				 $existe=false;
				 foreach ($Cobradores as $cobra)
				 {
					if ($cobra->persona_id == $Persona->id)
					{
						$existe=true;
						$mensaje = 'yaescobrador';
					}
					 
				 }
				 
			//Obtengo la persona que ya existia y no es cobrador
			if ($existe ==false)
			{
			$Persona->get_by_dni($dni->dni);
			//Creo el cobrador
        	$Cobrador = new Cobrador();
			$Cobrador->estadoCobrador = EstadoCobrador::Activo;
			//Guardo el cobrador con esa persona
			$Cobrador-> Save(array($Persona));
			$mensaje = 'noeracobradorsisocio';
			}
			

        }
		echo $mensaje;
    }
  




}
}

/* End of file template.php */
/* Location: ./application/models/template.php */