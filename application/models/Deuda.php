<?php
/**
 * Template DataMapper Model
 *
 * Use this basic model as a template for creating new models.
 * It is not recommended that you include this file with your application,
 * especially if you use a Template library (as the classes may collide).
 *
 * To use:
 * 1) Copy this file to the lowercase name of your new model.
 * 2) Find-and-replace (case-sensitive) 'Template' with 'Your_model'
 * 3) Find-and-replace (case-sensitive) 'template' with 'your_model'
 * 4) Find-and-replace (case-sensitive) 'templates' with 'your_models'
 * 5) Edit the file as desired.
 *
 * @license		MIT License
 * @category	Models
 * @author		Phil DeJarnett
 * @link		http://www.overzealous.com
 */
class Deuda extends DataMapper {

	// Uncomment and edit these two if the class has a model name that
	//  doesn't convert properly using the inflector_helper.
	// var $model = 'template';
	// var $table = 'templates';
	var $table = 'deudas';

	// You can override the database connections with this option
	// var $db_params = 'db_config_name';

	// --------------------------------------------------------------------
	// Relationships
	//   Configure your relationships below
	// --------------------------------------------------------------------

	// Insert related models that Template can have just one of.
	var $has_one = array('socio');

	// Insert related models that Template can have more than one of.
	var $has_many = array('lineadeuda');

	/* Relationship Examples
	 * For normal relationships, simply add the model name to the array:
	 *   $has_one = array('user'); // Template has one User
	 *
	 * For complex relationships, such as having a Creator and Editor for
	 * Template, use this form:
	 *   $has_one = array(
	 *   	'creator' => array(
	 *   		'class' => 'user',
	 *   		'other_field' => 'created_template'
	 *   	)
	 *   );
	 *
	 * Don't forget to add 'created_template' to User, with class set to
	 * 'template', and the other_field set to 'creator'!
	 *
	 */

	// --------------------------------------------------------------------
	// Validation
	//   Add validation requirements, such as 'required', for your fields.
	// --------------------------------------------------------------------

	var $validation = array(
		/*'example' => array(
			// example is required, and cannot be more than 120 characters long.
			'rules' => array('required', 'max_length' => 120),
			'label' => 'Example'
		)*/
	);

	// --------------------------------------------------------------------
	// Default Ordering
	//   Uncomment this to always sort by 'name', then by
	//   id descending (unless overridden)
	// --------------------------------------------------------------------

	// var $default_order_by = array('name', 'id' => 'desc');

	// --------------------------------------------------------------------

	/**
	 * Constructor: calls parent constructor
	 */
    function __construct($id = NULL)
	{
		parent::__construct($id);
    }


// --------------------------------------------------------------------
	// Post Model Initialisation
	//   Add your own custom initialisation code to the Model
	// The parameter indicates if the current config was loaded from cache or not
	// --------------------------------------------------------------------
	function post_model_init($from_cache = FALSE)
	{
	}

	// --------------------------------------------------------------------
	// Custom Methods
	//   Add your own custom methods here to enhance the model.
	// --------------------------------------------------------------------

	/* Example Custom Method
	function get_open_templates()
	{
		return $this->where('status <>', 'closed')->get();
	}
	*/

	// --------------------------------------------------------------------
	// Custom Validation Rules
	//   Add custom validation rules for this model here.
	// --------------------------------------------------------------------

	/* Example Rule
	function _convert_written_numbers($field, $parameter)
	{
	 	$nums = array('one' => 1, 'two' => 2, 'three' => 3);
	 	if(in_array($this->{$field}, $nums))
		{
			$this->{$field} = $nums[$this->{$field}];
	 	}
	}
	*/

    function Generar($cobrador_id,$mes,$año)
    {
        //Obtengo el cobrador
        $Cobrador = new Cobrador();
        $Cobrador->get_by_id($cobrador_id);
        $idCobrador = $Cobrador->id;

        $Cobro = new Cobro();
        $Cobro->select_min('id');
        $Cobro->get();

        //Busco todos los socios.
        $Socio = new Socio();
        $Socio ->where(array('cobrador_id'=> $idCobrador,'estado'=> EstadoSocio::Activo,'fechaAlta <='=> ($año.'-'.$mes.'-01')));
        $Socio->get();

        //Anio cargado por el admin
        $anioDeuda = $año;
        //Mes cargado por el admin
        $mesDeuda = $mes;
        $LineasDeudas = array();
        $Deudas=array();

        //Creo una nueva deuda
        $consultaMaxID = new Deuda();
        $maximoID = $consultaMaxID->select_max('id')->get()->id;
        $idCorresponde = $maximoID;

        //Para cada uno de ellos hago:
        foreach($Socio as $socioDeudor)
        {

            if(($this->tieneQueGenerar($mesDeuda,$anioDeuda,$socioDeudor->id) and ($socioDeudor->intervaloPago == 1)) OR ($this->debeGenerarDeuda($mes,$año,$socioDeudor->id) AND $socioDeudor->intervaloPago>1))
         {
             $idCorresponde+=1;
            // Monto total de la deuda
            $montoTotal=0;



            $Deuda = new Deuda();
            $Deuda->id =$idCorresponde;
            $Deuda->anioAdeudado =  $anioDeuda;
            $Deuda->mesAdeudado =  $mesDeuda;
            $Deuda->monto=0;
            $Deuda->socio_id=$socioDeudor->id;


            //Para obtener la cuota social entro al tipo de socio
            $idTipoSocio = $socioDeudor->tiposocio_id;
            $TipoSocio = new TipoSocio();
            $TipoSocio->get_by_id($idTipoSocio);



             //--------PARTE DE EMBARCACIONES----------- //
             $embarcacion = new Embarcacion();
             $cantidadEmbarcaciones = $embarcacion->where('socio_id', $socioDeudor->id)->count();
             if($cantidadEmbarcaciones>0)
             {
                 $embarcacion->where('socio_id', $socioDeudor->id);
                 $embarcacion->get();
                 $tipoEmbarcacion = new TipoEmbarcaciones();
                 // Para cada embarcaciones genera las lineas deudas
                 foreach ($embarcacion as $barco)
                 {
                     //Busco el tipo de embarcacion
                     $lineaDeuda = new Lineadeuda();
                     $tipoEmbarcacion->where('id',$barco->tipoembarcacion_id);
                     $tipoEmbarcacion->get();
                     $lineaDeuda->descripcion= $tipoEmbarcacion->descripcion;
                     $lineaDeuda->monto = ($tipoEmbarcacion->monto);

                     //Sumo el monto total para mostrar el total de la deuda
                     $montoTotal=$montoTotal + $lineaDeuda->monto;
                     $lineaDeuda->tipoembarcacion_id = $tipoEmbarcacion->id;
                     //$lineaDeuda->tiposocio_id = 'NULL';
                     //$lineaDeuda->actividad_id = 'NULL';
                     $lineaDeuda->deuda_id = $Deuda->id;
                     array_push($LineasDeudas,$lineaDeuda);
                 }
             }






            /// ----------- PARTE TIPO DE ABONO -------------- ///
            $lineaDeuda = new LineaDeuda();
            $lineaDeuda ->descripcion = $TipoSocio->descripcion;
            $lineaDeuda ->monto = $TipoSocio->monto;
            $lineaDeuda->tiposocio_id = $TipoSocio->id;
            //$lineaDeuda->tipoembarcacion_id = 'NULL';
            //$lineaDeuda->actividad_id = 'NULL';
            $montoTotal=$montoTotal + ($lineaDeuda->monto);

            $lineaDeuda->deuda_id = $Deuda->id;
            array_push($LineasDeudas,$lineaDeuda);







            //--------PARTE DE ACTIVIDADES SOCIO TITULAR-----------
            $inscripcion = new Inscripcion();
            $cantidadInscripciones = $inscripcion->where(array('socio_id' => $socioDeudor->id, 'fechaBaja is null' => null))->count();

            if($cantidadInscripciones>0)
            {
            $inscripcion->where(array('socio_id' => $socioDeudor->id, 'fechaBaja is null' => null));
            $inscripcion->get();
            $deporte = new Deporte();

            foreach ($inscripcion as $inscrip)
            {
                $lineaDeuda = new Lineadeuda();
                //obtengo el deporte de la inscripcion
                $deporte->get_by_id($inscrip->actividad_id);

                $lineaDeuda->descripcion = $deporte->nombre;
                $lineaDeuda->monto =$deporte->monto;
                $montoTotal=$montoTotal + $lineaDeuda->monto;
                $lineaDeuda->actividad_id = $deporte->id;
                $lineaDeuda->deuda_id = $Deuda->id;
                array_push($LineasDeudas,$lineaDeuda);
            }
            }
         $Deuda->monto=$montoTotal;

             // Si son nulas las deudas, es decir monto cero,
             //les pongo como pago el último pago efectuado asi no salen como deudas impagas

             array_push($Deudas,$Deuda);
        }

    }
    $this->guardarDeudas($Deudas);
    $this->guardarLineasDeuda($LineasDeudas,$Deuda);

        $Deudas = new Deuda();
        $Deudas->where('mesAdeudado',$mesDeuda);
        $Deudas->where('anioAdeudado',$anioDeuda);
        $Deudas->where('monto',0);
        $Deudas->update('cobro_id', $Cobro->id);


}





    function guardarDeudas($deudas)
    {
        $utilitarios = new Utils();
        $VectorConDeudas=array();
        $VectorConDeudasNulas=array();
        foreach($deudas as $deuda)
        {
            $arraydeuda=array($deuda->id,$deuda->mesAdeudado,$deuda->anioAdeudado,$deuda->monto,$deuda->socio_id);
            array_push($VectorConDeudas,$arraydeuda);
        }
        ($utilitarios->insert_rows('deudas', array('id', 'mesAdeudado','anioAdeudado','monto','socio_id'), $VectorConDeudas));

    }


    /// GUARDA LAS LINEAS DE DEUDA CORRESPONDIENTE A LA DEUDA PASADA COMO PARÁMETRO
    function guardarLineasDeuda($lineasDeuda)
    {
        $utilitarios = new Utils();
        $LineasTipoSocios = array();
        $LineasActividad = array();
        $LineasEmbarcaciones = array();
        foreach($lineasDeuda as $lineaDeuda)
        {
            if($lineaDeuda->actividad_id<>'')
            {
                $arraylineaDeudaActividad = array($lineaDeuda->descripcion, $lineaDeuda->monto, $lineaDeuda->deuda_id,$lineaDeuda->actividad_id);
                array_push($LineasActividad,$arraylineaDeudaActividad);
            }
            if($lineaDeuda->tiposocio_id<>'')
            {
                $arraylineaDeudaTipoSocio = array($lineaDeuda->descripcion, $lineaDeuda->monto, $lineaDeuda->deuda_id, $lineaDeuda->tiposocio_id);
                array_push($LineasTipoSocios,$arraylineaDeudaTipoSocio);
            }
            if($lineaDeuda->tipoembarcacion_id<>'')
            {
                $arraylineaDeudaEmbarcacion = array($lineaDeuda->descripcion, $lineaDeuda->monto, $lineaDeuda->deuda_id,$lineaDeuda->tipoembarcacion_id);
                array_push($LineasEmbarcaciones,$arraylineaDeudaEmbarcacion);
            }
        }
        $utilitarios->insert_rows('lineasdeuda', array('descripcion', 'monto','deuda_id','tiposocio_id'), $LineasTipoSocios);
        $utilitarios->insert_rows('lineasdeuda', array('descripcion', 'monto','deuda_id','tipoembarcacion_id'), $LineasEmbarcaciones);
        $utilitarios->insert_rows('lineasdeuda', array('descripcion', 'monto','deuda_id','actividad_id'), $LineasActividad);
    }



    //Pregunta si ya tiene una deuda ese mes y año. Si ya tiene no genera.
    public function tieneQueGenerar($mes,$anio,$idSocio)
    {
        $tiene=true;
        $Deudas = new Deuda();
        $Deudas->where(array('mesAdeudado' => $mes, 'anioAdeudado' => $anio, 'socio_id' => $idSocio));
        $cantDeudas= $Deudas->where(array('mesAdeudado' => $mes, 'anioAdeudado' => $anio, 'socio_id' => $idSocio))->count();
        $Deudas->get();
        if ($cantDeudas<>0)
        {
            echo 'El Socio '.$idSocio.' no tiene que generar porque ya tiene deudas';
            echo '<br>';
            $tiene=false;
        }

        return $tiene;
    }



    //Pregunta si le corresponde pagar por el tema del intervalo de pago.
    public function debeGenerarDeuda($mes,$año,$id)
    {
        $valorRetorno=false;
        //Busco el socio
        $socio = new Socio();
        $socio->get_by_id($id);
        //Obtengo los datos de ahora

        $esteAnio = $año;//date("Y");
        $estemes  = $mes;//date("m");
        //Obtengo el intervalo en el q el socio paga
        $intervalo = $socio->intervaloPago;
        //Tengo sus deudas generadas
        $deudasInterval = new Deuda();
        $deudasInterval->where('socio_id', $socio->id);
        $deudasInterval->get();
        $arrayDeudas=$deudasInterval->all_to_array();


        //Busco el ultimo elemento de la lista
        $ultimo=count($arrayDeudas)-1;



        if($ultimo>=-1)
        {

            if($ultimo>=0)
            {
                $anioInicio= $arrayDeudas[$ultimo]['anioAdeudado'];
                $mesInicio = $arrayDeudas[$ultimo]['mesAdeudado'];
                $diferencia = $this->dif($mesInicio, $estemes, $anioInicio, $esteAnio);

                if($diferencia>=$intervalo)
                {
                    $valorRetorno=true;
                }
            }
            else
            {
                $valorRetorno=true;
            }
        }

        return $valorRetorno;

    }

    public function dif($mesInicio, $mesFin, $anioInicio, $anioFin)
    {

        //la fecha inicial debe ser menor a la fecha final

        $inicio =$anioInicio."-".$mesInicio."-01";

        $fin =$anioFin."-".$mesFin."-01";

        $datetime1=new DateTime($inicio);
        $datetime2=new DateTime($fin);

        # obtenemos la diferencia entre las dos fechas
        $interval=$datetime2->diff($datetime1);

        # obtenemos la diferencia en meses
        $intervalMeses=$interval->format("%m");
        # obtenemos la diferencia en años y la multiplicamos por 12 para tener los meses
        $intervalAnos = $interval->format("%y")*12;


        return ($intervalMeses+$intervalAnos);
    }



}
/* End of file template.php */
/* Location: ./application/models/template.php */