<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CCobrador extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<metod_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if (!$this->session->userdata('username')) 
		{
			redirect('CLogin');
		}
		else
		{
				$Usuario = new usuario_model();
				$Usuario->where('username',$this->session->userdata('username'));
				$Usuario->get();
			if ($Usuario->tipoUsuario == 0)
			{
				$data['seccion'] = 'Cobrador';
				$this->load->view('header',$data);
				$this->load->view('menu');
				$this->load->view('cobradores');
			}
			else {redirect('CPermiso');}
	}
}

//Funcion para agregar un cobrador. Se preguinta si la persona ya existe (el dni) y la asigna como cobrador si es asi. Si no, la crea y agrega el nuevo cobrador.
	public function agregarCobrador()
	{
		$mensaje = 'fallo';
     	$Persona  = new Persona();
		$dniBusqueda = $Persona->get_by_dni($this->input->post('dni'))->dni;
		//Si no existe la persona con ese dni, la creo antes de crear el cobrador
		if($dniBusqueda=='')
        {
             	$Persona->nombre = $this->input->post('nombre');
                $Persona->apellidos = $this->input->post('apellidos');
                $Persona->dni = $this->input->post('dni');
                $Persona->genero = $this->input->post('genero');
                $Persona->correoElectronico = $this->input->post('correoElectronico');
                $Persona->fechaNacimiento = $this->input->post('fechaNacimiento');
                $Persona->profesion = $this->input->post('profesion');
                $Persona->domicilio = $this->input->post('domicilio');
				$Persona->telefonoFijo = $this->input->post('telefonoFijo');
				$Persona->telefonoCelular = $this->input->post('telefonoCelular');
				$Persona->nacionalidad = $this->input->post('nacionalidad');
				$Persona->estadoCivil = $this->input->post('estadoCivil');
            
				//Guardo la persona
                ($Persona->save());
         		//Creo el cobrador y lo asigno a esa persona
							
				 $Cobrador = new Cobrador();
				 $Cobrador->comision = $this->input->post('comision');
				 $Cobrador->estadoCobrador = EstadoCobrador::Activo;
                ($Cobrador->save(array($Persona)));
				//Paso el mensaje de exito
				$mensaje = 'exito';
                
        }
        else //Si la persona ya existia hay dos posibilidades: o es cobrador o socio
        {
				//Compruebo que no exista ya el cobrador a nombre de esa persona
				 $Cobradores = new Cobrador();
				 $Cobradores -> get();
				 $existe=false;
				 foreach ($Cobradores as $cobra)
				 {
					if ($cobra->persona_id == $Persona->id)
					{
						$existe=true;
						//$mensaje = 'yaescobrador';
					}
					 
				 }
				 
			//Obtengo la persona que ya existia y no es cobrador
			if ($existe ==false)
			{
			$Persona->get_by_dni($this->input->post('dni'))->dni;
			//Creo el cobrador
        	$Cobrador = new Cobrador();
			$Cobrador->estadoCobrador = EstadoCobrador::Activo;
			//Guardo el cobrador con esa persona
			$Cobrador-> Save(array($Persona));
			$mensaje = 'noeracobradorsisocio';
			}
			

        }
		echo $mensaje;

	}

//Funcion para obtener un cobrador seleccionado de la lista de los cobradores, para editarlo o darlo de baja, o bien ver sus socios a los que tiene que cobrar.
	public function obtenerCobrador()
	{
		$cobrador_id = $this->uri->segment(3);
		$Cobrador = new Cobrador();
		//busco el cobrador por id
		$Cobrador->get_by_id($cobrador_id);
		//busco la persona del cobradorpara sacarle los datos
		$persona_cobrador = new Persona();
		$persona_cobrador->get_by_id($Cobrador->persona_id);
		
		
		//obtengo los datos
		$data = array(
		'idCobrador' =>$Cobrador->id,
		'nombre'=> $persona_cobrador->nombre,
		'apellidos'=> $persona_cobrador->apellidos,
		'dni'=> $persona_cobrador->dni,
		'domicilio'=> $persona_cobrador->domicilio,
		'correoElectronico'=> $persona_cobrador->correoElectronico,
		'fechaNacimiento'=> $persona_cobrador->fechaNacimiento,
		'profesion'=> $persona_cobrador->profesion,
		'genero'=> $persona_cobrador->genero,
		'nacionalidad' => $persona_cobrador->nacionalidad,
		'telefonoFijo' => $persona_cobrador->telefonoFijo,
        'telefonoCelular'=> $persona_cobrador->telefonoCelular,
        'nacionalidad' => $persona_cobrador->nacionalidad,
        'estadoCivil' => $persona_cobrador->estadoCivil,
'comision' => $Cobrador->comision,

		);
		echo json_encode($data);
	}



//Funcion para obtener el listado de todos los cobradores
public function obtenerCobradores()

	{
		$Cobradores = new Cobrador();
		$persona_cobrador = new Persona();
		$buscado = $this->input->post('valorBusqueda');
		$Cobradores->where('estadoCobrador <>', EstadoCobrador::DadoDeBaja);
		$Cobradores->get();
		$resultado = array();
   		$resultado['cobradores'] = array();

		foreach ($Cobradores as $Cobrador)
		{
			$persona_cobrador->get_by_id($Cobrador->persona_id);
			$cobradorCompleto = array();			


			if($buscado=="" || stristr(''.$persona_cobrador->nombre.' '.$persona_cobrador->apellidos,$buscado)!==false
			|| stristr(''.$persona_cobrador->apellidos.' '.$persona_cobrador->nombre,$buscado)!==false)
			{

				$cobradorCompleto['id'] = $Cobrador->id;
   				$cobradorCompleto['nombre'] = $persona_cobrador->nombre;
   				$cobradorCompleto['apellidos'] = $persona_cobrador->apellidos;	
   				$cobradorCompleto['dni'] = $persona_cobrador->dni;
   				$cobradorCompleto['domicilio'] = $persona_cobrador->domicilio;
   				$cobradorCompleto['correoElectronico'] = $persona_cobrador->correoElectronico;
				$cobradorCompleto['nacionalidad'] = $persona_cobrador->nacionalidad;
				$cobradorCompleto['estadoCivil'] = $persona_cobrador->estadoCivil;
   				$cobradorCompleto['telefonoFijo'] = $persona_cobrador->telefonoFijo;
				$cobradorCompleto['telefonoCelular'] = $persona_cobrador->telefonoCelular;
   				array_push($resultado['cobradores'], $cobradorCompleto);
			}
		}
		
		echo json_encode($resultado);
	}


public function EditarCobrador()
{
		$cobrador_id = $this->uri->segment(3);
		//obtengo el cobrador

		echo $cobrador_id;
        $Cobrador = new Cobrador();
        
        $Cobrador->where('id',$cobrador_id);
        //Saco el id de la persona del cobrador
		$Cobrador->update('comision', ($this->input->post('comision')));

		$personaid = $Cobrador->get_by_id($cobrador_id)->persona_id;
		
        //Obtengo la persona        
		$Persona = new Persona();
		//$Persona->get_by_id($personaid);
		$Persona->where('id',$personaid);
		//Actualizo las entradas del cobrador
		$Persona->update(array('nombre'=>$this->input->post('nombre'), 'apellidos'=>$this->input->post('apellidos'),
			'dni'=>$this->input->post('dni'),'genero'=>$this->input->post('genero'),
			'correoElectronico'=>$this->input->post('correoElectronico'),'fechaNacimiento'=>$this->input->post('fechaNacimiento'), 'nacionalidad'=>$this->input->post('nacionalidad'), 'estadoCivil'=>$this->input->post('estadoCivil'), 'telefonoFijo'=>$this->input->post('telefonoFijo'), 'telefonoCelular'=>$this->input->post('telefonoCelular'),
			'profesion'=>$this->input->post('profesion'),'domicilio'=>$this->input->post('domicilio')));

}


//Asigna un estado inactivo a un cobrador
public function DarDeBajaCobrador()
{
$cobrador_id = $this->uri->segment(3);	
$Cobrador = new Cobrador();
$Cobrador->where('id', $cobrador_id)->update('estadoCobrador', EstadoCobrador::DadoDeBaja);
}

//Obtiene los socios que deben ser cobrados por ese cobrador
public function ObtenerSociosVinculados()
    {
		
        $cobradorID = $this->uri->segment(3);
        $Socios = new Socio();	
        $Socios->where('cobrador_id',$cobradorID);
        $Socios->get();
		$resultado = array();
   		$resultado['sociosVinculados'] = array();
		
	    foreach ($Socios as $socio)
		{
			$persona_socio = new Persona();
	 		$persona_socio->get_by_id($socio->persona_id);
			$socioCompleto = array();			

				$socioCompleto['id'] = $socio->id;
   				$socioCompleto['nombre'] = $persona_socio->nombre;
   				$socioCompleto['apellidos'] = $persona_socio->apellidos;	
   				$socioCompleto['dni'] = $persona_socio->dni;
   				$socioCompleto['domicilio'] = $persona_socio->domicilio;



   				array_push($resultado['sociosVinculados'], $socioCompleto);
		}
		

		echo json_encode($resultado);

	}
	
	//Devuelve nombre y apellido de los cobradores para que cuando se da de alta un socio se eliga el cobrador
	public function CobradoresSeleccion()
	{
		$cobradores = new Cobrador();
		$cobradores->where('estadoCobrador <>', EstadoCobrador::DadoDeBaja);
		$cobradores->get();

		$resultado = '<select name="cobrador" id="campoCobrador">';
		foreach ($cobradores as $cobrador)
        {
			$persona_cobrador = new Persona();
			$persona_cobrador ->get_by_id($cobrador->persona_id);
			$resultado = $resultado . '<option value="'.$cobrador->id.'">'.$persona_cobrador->nombre.' '.$persona_cobrador->apellidos.' </option>';
		}
		
		echo $resultado . '</select>';
	}




	public function importarCobradores()
	{
		$CCobrador = new Cobrador();
		$CCobrador->importarCobradores('C:\Users\Fer\Desktop\cobrador.xlsx');
	}
	
	
}
