<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if (!$this->session->userdata('username')) //si esto existe devuelve verdadero
		{
			redirect('CLogin');
		}
		else
		{
				$Usuario = new usuario_model();
				$Usuario->where('username',$this->session->userdata('username'));
				$Usuario->get();
			if ($Usuario->tipoUsuario == 0)
			{
				$data['seccion'] = 'INICIO';
				$this->load->view('header',$data);
				$this->load->view('menu');
				$this->load->view('inicio');
				if($this->_hayquehacerbackup())
				{
					$this->load->view('alertaBackup');
					//$this->hacerBackup();
				}
				
			}
			else {redirect('CPermiso');}
		}
	}


	public function _hayquehacerbackup()
	{
		$valor = false;
		$parametro = new Parametro();    			
					//Obtengo el parametro 3 que es el del backup
					$parametro->get_by_id(3);
					//dia del año de hoy (0 a 365)
					$hoy= date("z");
					//dia del año del ultimo backup (0 a 265)
					$ultimoBackup = date("z",strtotime($parametro->fechaModificacion));
					//diferencia entre ambos
					$diferencia = $hoy - $ultimoBackup;
					//paso una semana sin backup
					if ($diferencia>=7)
					{
						$valor=true;
					}
					return $valor;
	}


	public function hacerBackup()
    {
    	if(!$this->session->userdata('username'))
    	{
    		redirect('CLogin');
    	}
    	else
    	{
    			$Usuario = new usuario_model();
				$Usuario->where('username',$this->session->userdata('username'));
				$Usuario->get();

    		 switch ($Usuario->tipoUsuario) {
    			 case 0:
    			 $parametro = new Parametro();    			
					//Obtengo el parametro 3 que es el del backup
					$parametro->get_by_id(3);
						//guardo que el ultimo backup fue hoy
						$parametro->fechaModificacion =strftime( "%Y-%m-%d", time());
						$parametro->save();

	    				$this->load->dbutil();

				        $prefs = array(     
				                'format'      => 'zip',             
				                'filename'    => 'backup.sql'
				              );


				        $backup =& $this->dbutil->backup($prefs); 

				        $db_name = 'backup-de-'. date("Y-m-d-H-i-s") .'.zip';
				        $save = 'pathtobkfolder/'.$db_name;

				        $this->load->helper('file');
				        write_file($save, $backup); 


				        $this->load->helper('download');
				        force_download($db_name, $backup);				       			

	    				break;
	    			
	    		 case 1:
	    				redirect('CPermiso');
	    				break;
	    		 case 2:
	    				redirect('CPermiso');
	    				break;
	    			
	    			
    		}

    		 
    	}

    }

}
