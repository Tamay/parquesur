<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CReporte extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if (!$this->session->userdata('username')) 
		{
			redirect('CLogin');
		}
		else
		{
				$Usuario = new usuario_model();
				$Usuario->where('username',$this->session->userdata('username'));
				$Usuario->get();
			if ($Usuario->tipoUsuario == 0)
			{
				$data['seccion'] = 'REPORTES';
				//$this->load->view('header',$data);
				//$this->load->view('menu');
				$this->load->view('reportes');

			}
			else {redirect('CPermiso');}
		}



	}


	public function reportesFinancieros()
	{
		$this->load->view('reportesFinancieros');
	}


	public function reportesSocietarios()
	{
		$this->load->view('reportessocietarios');
	}

	public function reportesEmbarcaciones()
	{
		$this->load->view('reportesembarcaciones');
	}






	//socios, recaudacion, socios nuevos, recaudacion por mes, socios activos

	//Devuelve la cantidad total de socios activos
	public function totalSocios()
	{
		//obtengo todos los socios con estado 0, es decir activos.
		$socios = new Socio();
		$valor = $socios->where('estado',0)->count();	
		echo $valor;
		return $valor;
	}

	//Devuelve la cantidad de socios que se registrarion en el corriente año
	public function sociosNuevosAnio()
	{
		$fechaDesde = (date("Y").'-01-01');
		$fechaHasta = (date("Y").'-12-31');

		//Obtengo todos los socios donde la fecha de alta sea = al año corriente
		$socios = new Socio();
		$nuevos=$socios->where(array('fechaAlta >='=> $fechaDesde,'fechaAlta <='=> $fechaHasta,))->count();
		
		echo $nuevos;
		return $nuevos;
	}

	//Devuelve el total de lo recaudado en el ultimo mes
	public function totalRecaudadoUltimoMes()
	{
		
		// En la posición 0 se guarda el año y en la posición 1 se guarda el mes
		$fechaDesde = (date("Y").'-'.date("m").'-01');
		$fechaHasta = (date("Y").'-'.date("m").'-31');

		$fechaDesde = new DateTime($fechaDesde);
		$fechaHasta = new DateTime($fechaHasta);

		$cobros = new Cobro();
		$cobros->get_where(array('fechaCobro <=' => $fechaHasta->format('Y-m-d'), 'fechaCobro >=' => $fechaDesde->format('Y-m-d')));

		$monto = 0;

		foreach ($cobros as $cobro) {
			$monto = $monto + $cobro->importe;
		}

		echo $monto;
		return $monto;

	}


	//Devuelve un vector de dimensión igual a 12 dónde en cada posición se encuentra la recaudación
	// de cada mes.
	public function recaudacionAnual()
	{
		$resultado = array();
		
		for($i=1;$i<=date("m");$i++)
		{

			$principioMes = (new Datetime(date("Y")."-".$i."-01"))->format('Y-m-d');
			$finMes = (new Datetime(date("Y")."-".$i."-28"))->format('Y-m-d');

			$monto = 0;
			
			$Cobros = new Cobro();
			//Obtengo todas los cobros donde se cumplen las condiciones
			$Cobros->get_where(array('fechaCobro >=' => $principioMes, 'fechaCobro <='=>$finMes));			

			foreach ($Cobros as $cobro) 
			{
				$monto = $monto + $cobro->importe;
			}

			array_push($resultado,$monto);
		}
        for($i=date("m");$i<12;$i++)
        {
            $monto=0;
            array_push($resultado,$monto);
        }
		echo json_encode($resultado);
	}




	//Devuelve un vector de dimensión igual a 12 dónde en cada posición se encuentra la recaudación
	// de cada mes.
	public function sociosAnual()
	{
		$resultado = array();

        for($i=1;$i<=date("m");$i++)
		{
			$principioMes = (new Datetime(date("Y")."-".$i."-01"))->format('Y-m-d');
			$finMes = (new Datetime(date("Y")."-".$i."-29"))->format('Y-m-d');
			
			$Socios = new Socio();			
			$cantidad = $Socios->where(array('fechaAlta <=' => $principioMes, '((fechaBaja>'.$finMes.') OR (fechaBaja is null))'=>null))->count();

			array_push($resultado,$cantidad);
		}
        $cantidad=0;
        for($i=date("m");$i<12;$i++)
        {

            array_push($resultado,$cantidad);
        }

		echo json_encode($resultado);
	}



	//Devuelve o recaudado entre 2 fechas.
	public function recaudadoEntre()
	{
		$fechaDesde = $this->input->post('fechaDesde');
		$fechaHasta = $this->input->post('fechaHasta');

		// En la posición 0 se guarda el año y en la posición 1 se guarda el mes
		$fechaDesde = explode("-", $fechaDesde);
		$fechaHasta = explode("-", $fechaHasta);

		$fechaDesde = ($fechaDesde[0].'-'.$fechaDesde[1].'-01');
		$fechaHasta = ($fechaHasta[0].'-'.$fechaHasta[1].'-01');

		$fechaDesde = new DateTime($fechaDesde);
		$fechaHasta = new DateTime($fechaHasta);

		$cobros = new Cobro();
		$cobros->get_where(array('fechaCobro <=' => $fechaHasta->format('Y-m-d'), 'fechaCobro >=' => $fechaDesde->format('Y-m-d')));

		$monto = 0;

		foreach ($cobros as $cobro) {
			$monto = $monto + $cobro->importe;
		}

		echo $monto;
		return $monto;
	}


    public function recaudacionMensual()
    {
        $socios = new Socio();
        $socios->where(array('fechaBaja is NULL' => null))->get();

        $recaudacionMensual = 0;
        $recaudacionMensualxEmbarcaciones = 0;
        $recaudacionMensualxServicios = 0;
        $recaudacionMensualxAbono = 0;
        $cantSocios = 0;
        foreach($socios as $socio)
        {
            $cantSocios+=1;
            $recaudacionMensual += $socio->montoTotal();
            $recaudacionMensualxEmbarcaciones += $socio->cuantoPagaPorEmbarcacion();
            $recaudacionMensualxServicios += $socio->cuantoPagaPorServicios();
            $recaudacionMensualxAbono += $socio->cuantoPagaPorAbono();
        }

        echo '<h3>Detalle de la recaudación mensual</h3>';
        echo '<table>';
        echo '<tr><td align="right">&nbsp;&nbsp;-&nbsp;TOTAL A RECAUDAR POR ABONOS:&nbsp; </td> <td>&nbsp;&nbsp;&nbsp;$&nbsp;'.$recaudacionMensualxAbono.'</td></tr>';
        echo '<tr><td align="right">&nbsp;&nbsp;-&nbsp;TOTAL A RECAUDAR POR EMBARCACIONES:&nbsp;</td> <td>&nbsp;&nbsp;&nbsp;$&nbsp;'.$recaudacionMensualxEmbarcaciones.'</td></tr>';
        echo '<tr><td align="right">&nbsp;&nbsp;-&nbsp;TOTAL A RECAUDAR POR SERVICIOS:&nbsp;</td> <td>&nbsp;&nbsp;&nbsp;$&nbsp;'.$recaudacionMensualxServicios.'</td></tr>';
        echo '<tr><td align="right">TOTAL:&nbsp;</td><td>&nbsp;&nbsp;&nbsp;$&nbsp;'.$recaudacionMensual.'</td></tr>';
        echo '<tr><td colspan=2 align="right">SOBRE UN TOTAL DE '.$cantSocios.' SOCIOS</td></tr>';
        echo '</table>';
    }



	//Devuelve todos los socios activos como un padrón.
	public function sociosActivos()
	{

        $mesesAntiguedad=$this->input->post('mesesAntiguedad');
        $boolInactivos = $this->input->post('inactivos');
        $cantCuotasImpagas = $this->input->post('cuotasImpagas');
		
		$socios = new Socio();
		$persona_socio = new Persona();

        if($boolInactivos!=1)
		{
		$socios->where('estado <>', EstadoSocio::DadoDeBaja);
		}

        if($mesesAntiguedad>0)
        {
            $fechaActual = date('Y-m-j');
            $TieneQueSerMayorA = strtotime ( '-'.$mesesAntiguedad.' month' , strtotime ( $fechaActual ) ) ;
            $TieneQueSerMayorA = date ( 'Y-m-d' , $TieneQueSerMayorA );
          $socios->where('fechaAlta <=', $TieneQueSerMayorA);
        }

		$socios->get();
		$resultado = array();
   		$resultado['data'] = array();

        $i=0;
		foreach ($socios as $socio)
		{

            //echo 'Cantidad de cuotas impagas '.($this->cantidadCuotas($socio->id)).' |' .$i.' |';
            if($this->cantidadCuotas($socio->id)<=$cantCuotasImpagas)
            {
                $i++;
			$persona_socio->get_by_id($socio->persona_id);
			$socioCompleto = array();			

				$socioCompleto['id'] = sprintf('%04d',$socio->id);


				$vectorString= preg_split("/[\s,]+/", $persona_socio->nombre);
	            $stringNombreSocios='';
	            $contadorPalabras=0;
	            foreach($vectorString as $string)
	            {
	            	if($contadorPalabras<1)
	            	{
	            		$stringNombreSocios=$string;
	            	}
	            	else
	            	{
	            		if($contadorPalabras<2)
	            		{
	            			$stringNombreSocios=$stringNombreSocios.' '.substr($string, 0,1).'.';			
	            		}
	            		
	            	}
	            	$contadorPalabras++;
	            }

   				$socioCompleto['nombreCompleto'] = $persona_socio->apellidos.' '.$stringNombreSocios;
   				$socioCompleto['dni'] = $persona_socio->dni;
   				$socioCompleto['domicilio'] = $persona_socio->domicilio;

   				$fechaNacimientoConFormato = new DateTime($persona_socio->fechaNacimiento);
				$fechaNacimiento=$fechaNacimientoConFormato->format('d/m/Y');
   				$socioCompleto['fechaNacimiento'] = $fechaNacimiento;
   				
   				$fechaAltaConFormato = new DateTime($socio->fechaAlta);
				$fechaAlta=$fechaAltaConFormato->format('d/m/Y');
   				$socioCompleto['fechaAlta'] = $fechaAlta;

   				$fechaBaja=$socio->fechaBaja;
   				if($fechaBaja!='')
				{
   				$fechaBajaConFormato = new DateTime($fechaBaja);
				$fechaBaja=$fechaBajaConFormato->format('d/m/Y');
				}
   				$socioCompleto['fechaBaja'] = $fechaBaja;

   				$telefonoFijo=$persona_socio->telefonoFijo;
   				$telefonoCelular=$persona_socio->telefonoCelular;
   				if($telefonoFijo==0)
   				{
   					$telefonoFijo='';
   				}

   				if($telefonoCelular==0)
   				{
   					$telefonoCelular='';
   				}
   				$socioCompleto['telefonos'] = $telefonoFijo.' /<br>'.$telefonoCelular;
   				$tipoDeSocio = new TipoSocio();
	            $tipoDeSocio->get_by_id($socio->tiposocio_id);


	            $vectorString= preg_split("/[\s,]+/", $tipoDeSocio->descripcion);
	            $stringTipoSocios='';
	            foreach($vectorString as $string)
	            {
	            	$stringTipoSocios=$stringTipoSocios.' '.substr($string, 0,3).'.';
	            }
	            $socioCompleto['tipoSocio']= $stringTipoSocios;
	            $socioCompleto['montoServicios'] = $socio->cuantoPagaPorServicios()+$socio->cuantoPagaPorEmbarcacion();
				$socioCompleto['montoAbonos'] = $socio->cuantoPagaPorAbono();	            
                $socioCompleto['montoTotal'] = $socio->montoTotal();


   				array_push($resultado['data'], $socioCompleto);
   			}
		}
		
		echo json_encode($resultado);
	}

	//Devuelve la actividad de altas y bajas entre dos fechas.
	public function altasBajas()
	{
		
		$fechaDesde= $this->input->post('fechaDesde');
		$fechaHasta= $this->input->post('fechaHasta');

		$sociosAlta = new Socio();
		$sociosBaja = new Socio();

		$persona_socioAlta = new Persona();
		$persona_socioBaja = new Persona();

		$sociosAlta->get_where(array('fechaAlta <=' => $fechaHasta, 'fechaAlta >=' => $fechaDesde));
		$sociosBaja->get_where(array('fechaBaja <=' => $fechaHasta, 'fechaBaja >=' => $fechaDesde));
	
		$resultado = array();
   		$resultado['data'] = array();

		foreach ($sociosAlta as $socio)
		{
			$fechaAltaConFormato = new DateTime($socio->fechaAlta);
			$fechaAlta=$fechaAltaConFormato->format('d/m/Y');

			$persona_socioAlta->get_by_id($socio->persona_id);
			$socioCompletoAlta = array();			
				$socioCompletoAlta['id'] = $socio->id;
   				$socioCompletoAlta['nombreCompleto'] = $persona_socioAlta->apellidos.' '.$persona_socioAlta->nombre;
   				$socioCompletoAlta['situacion'] = 'Alta';
   				$socioCompletoAlta['fecha'] = $fechaAlta;
   				$tipoDeSocio = new TipoSocio();
                $tipoDeSocio->get_by_id($socio->tiposocio_id);
                $socioCompletoAlta['tipoSocio']= $tipoDeSocio->descripcion;


   				array_push($resultado['data'], $socioCompletoAlta);
		
		}


		foreach ($sociosBaja as $socio)
		{
			$persona_socioBaja->get_by_id($socio->persona_id);
			$socioCompletoBaja = array();	

			$fechaBajaConFormato = new DateTime($socio->fechaBaja);
			$fechaBaja=$fechaBajaConFormato->format('d/m/Y');

				$socioCompletoBaja['id'] = $socio->id;
                $socioCompletoBaja['nombreCompleto'] = $persona_socioBaja->apellidos.' '.$persona_socioBaja->nombre;
   				$socioCompletoBaja['situacion'] = 'Baja';
   				$socioCompletoBaja['fecha'] = $fechaBaja;
   				$tipoDeSocio = new TipoSocio();
                $tipoDeSocio->get_by_id($socio->tiposocio_id);
                $socioCompletoBaja['tipoSocio']= $tipoDeSocio->descripcion;


   				array_push($resultado['data'], $socioCompletoBaja);
		}
		
		echo json_encode($resultado);
	}
	


	//Devuelve todos los socios que tienen embarcaciones y el importe que estos pagan por la embarcacion. NO CUOTA SOCIAL
	public function sociosConEmbarcaciones()
	{
		$embarcaciones = new Embarcacion();
		$resultado = array();
   		$resultado['data'] = array();

        $socios = new Socio();
        $socios->where('estado',EstadoSocio::Activo)->get();



        foreach ($socios as $socio)
        {
            $socioCompleto=array();
            $cantEmbarcaciones = $embarcaciones->where('socio_id',$socio->id)->count();
            if($cantEmbarcaciones>0)
            {
                $cont=0;
                $stringNombresEmbarcaciones = '';
                $stringTiposEmbarcaciones = '';
                $stringMatriculasEmbarcaciones = '';
                $stringMarcaMotorEmbarcaciones = '';
                $stringObservacionesEmbarcaciones = '';
                $montoTotal=0;
                $embarcaciones->where('socio_id',$socio->id)->get();
                foreach($embarcaciones as $embarcacion)
                {
                    $tipoEmba = new TipoEmbarcaciones();
                    $tipoEmba->get_by_id($embarcacion->tipoembarcacion_id);

                    if($cont==0)
                    {
                        $stringNombresEmbarcaciones = '- '.$embarcacion->nombre;
                        $stringTiposEmbarcaciones = '- '.$tipoEmba->descripcion;
                        $stringMatriculasEmbarcaciones = '- '.$embarcacion->matricula;
                        $stringMarcaMotorEmbarcaciones = '- '.$embarcacion->motorMarca;
                        $stringMotorNumberoEmbarcaciones = '- '.$embarcacion->motorNumero;
                        $stringMotorTipoEmbarcaciones = '- '.$embarcacion->motorTipo;
                        $stringObservacionesEmbarcaciones = '- '.$embarcacion->observaciones;
                    }
                    else
                    {
                        $stringNombresEmbarcaciones = $stringNombresEmbarcaciones.'<br>'.'- '.$embarcacion->nombre;
                        $stringTiposEmbarcaciones = $stringTiposEmbarcaciones.'<br>'.'- '. $tipoEmba->descripcion;
                        $stringMatriculasEmbarcaciones = $stringMatriculasEmbarcaciones.'<br>'.'- '. $embarcacion->matricula;
                        $stringMarcaMotorEmbarcaciones = $stringMarcaMotorEmbarcaciones.'<br>'.'- '.$embarcacion->motorMarca;
                        $stringMotorNumberoEmbarcaciones = $stringMotorNumberoEmbarcaciones.'<br>'.'- '.$embarcacion->motorNumero;
                        $stringMotorTipoEmbarcaciones = $stringMotorTipoEmbarcaciones.'<br>'.'- '.$embarcacion->motorTipo;
                        $stringObservacionesEmbarcaciones =  $stringObservacionesEmbarcaciones.'<br>'.'- '.$embarcacion->observaciones;
                    }


                    $montoTotal+= $tipoEmba->monto;
                    $cont+=1;
                }
                $persona_socio = new Persona();
                $persona_socio->get_by_id($socio->persona_id);
                $socioCompleto['id']=sprintf('%04d',$socio->id);
                $socioCompleto['nombreCompleto']=$persona_socio->apellidos.' '.$persona_socio->nombre;
                $socioCompleto['nombresEmbarcaciones'] = $stringNombresEmbarcaciones;
                $socioCompleto['matriculasEmbarcaciones'] = $stringMatriculasEmbarcaciones;
                $socioCompleto['tiposEmbarcaciones'] = $stringTiposEmbarcaciones;
                $socioCompleto['domicilio']=$persona_socio->domicilio;
                $socioCompleto['DNI']=$persona_socio->dni;

                $telefonoFijo=$persona_socio->telefonoFijo;
   				$telefonoCelular=$persona_socio->telefonoCelular;
                if($telefonoFijo==0)
   				{
   					$telefonoFijo='';
   				}

   				if($telefonoCelular==0)
   				{
   					$telefonoCelular='';
   				}
   				$socioCompleto['telefonos'] = $telefonoFijo.' /<br>'.$telefonoCelular;
                $socioCompleto['monto']=$montoTotal;
                $socioCompleto['Marca']=$stringMarcaMotorEmbarcaciones;
                $socioCompleto['Numero']=$stringMotorNumberoEmbarcaciones;
                $socioCompleto['Tipo']=$stringMotorNumberoEmbarcaciones;
                $socioCompleto['observaciones']=$stringObservacionesEmbarcaciones;
                array_push($resultado['data'], $socioCompleto);
            }

        }


		echo json_encode($resultado);

	}

	//Devuelve lo recaudado entre un rango por cobrador.
	public function recaudadoCompleto()
	{
		$fechaDesde = $this->input->post('fechaDesde');
		$fechaHasta = $this->input->post('fechaHasta');
		$cobrador = $this->input->post('cobrador');
        $todos = $this->input->post('todos');

		// En la posición 0 se guarda el año y en la posición 1 se guarda el mes
		$fechaDesde = explode("-", $fechaDesde);
		$fechaHasta = explode("-", $fechaHasta);

		$fechaDesde = ($fechaDesde[0].'-'.$fechaDesde[1].'-01');
		$fechaHasta = ($fechaHasta[0].'-'.$fechaHasta[1].'-01');

		$fechaDesde = new DateTime($fechaDesde);
		$fechaHasta = new DateTime($fechaHasta);

        $diferencia = $fechaDesde->diff($fechaHasta);
        $intervalMeses = ( $diferencia->y * 12 ) + $diferencia->m;


        echo '<table class="table">';
        echo '<thead>';
        echo '<tr>';
            echo '<th>Mes y Año</th>';
            echo '<th>Cobrado por Abonos</th>';
            echo '<th>Cobrado por Servicios</th>';
            echo '<th>Cobrado por Nautica</th>';
            echo '<th>Cobrado Total</th>';
            echo '<th>Total a cobrar</th>';
        echo '</tr>';
        $TOTALABONOS=0;
        $TOTALEMBARCACIONES=0;
        $TOTALSERVICIOS=0;
        $TOTALTOTAL=0;
        $TOTALACOBRAR=0;


        for($i=1;$i<=$intervalMeses;$i++)
        {
            $fechaHasta = date('Y-m-d',strtotime('+1 month', strtotime($fechaDesde->format('Y-m-d'))));
            $cobros = new Cobro();
            $deudas = new Deuda();
            $deudas->where(array('mesAdeudado' => $fechaDesde->format('m'),'anioAdeudado'=> $fechaDesde->format('Y'),'cobro_id IS NULL' => null))->get();

            if($todos>=1)
            {
                $cobros->get_where(array('fechaCobro <=' => $fechaHasta, 'fechaCobro >=' => $fechaDesde->format('Y-m-d')));
            }
            else
            {
                $cobros->get_where(array('fechaCobro <=' => $fechaHasta, 'fechaCobro >=' => $fechaDesde->format('Y-m-d'),'cobrador_id'=>$cobrador));
            }



            $montoTotal = 0;
            $montoTotalAbonos = 0;
            $montoTotalEmbarcaciones = 0;
            $montoTotalServicios = 0;
            $montoTotalACobrar=0;


        foreach($deudas as $deuda)
            {
                if($todos>=1)
                {
                    $montoTotalACobrar+=$deuda->monto;
                }
                else
                {
                    $Socio = new Socio();
                    $Socio->get_by_id($deuda->socio_id);
                    if($Socio->cobrador_id==$cobrador)
                    {
                        $montoTotalACobrar+=$deuda->monto;
                    }
                }
            }
            

		foreach ($cobros as $cobro) {

			$montoTotal = $montoTotal + $cobro->importe;
            $montoTotalAbonos+=$cobro->cuantoCobroxAbonos();
            $montoTotalEmbarcaciones+=$cobro->cuantoCobroxEmbarcaciones();
            $montoTotalServicios+=$cobro->cuantoCobroxServicios();

		}
            $TOTALACOBRAR+=$montoTotalACobrar;
            $TOTALABONOS+=$montoTotalAbonos;
            $TOTALEMBARCACIONES+=$montoTotalEmbarcaciones;
            $TOTALSERVICIOS+=$montoTotalEmbarcaciones;
            $TOTALTOTAL+=$montoTotal;


            echo '<tr><td>'.$fechaDesde->format('m/Y').'</td><td>'.$montoTotalAbonos.'</td><td>'.$montoTotalServicios.'</td><td>'.$montoTotalEmbarcaciones.'</td><td>'.$montoTotal.'</td><td>'.$montoTotalACobrar.'</td></tr>';
            $fechaDesde = new DateTime(date('Y-m-d',strtotime('+1 month', strtotime($fechaDesde->format('Y-m-d')))));
        }
        echo '<tr><td align="right">TOTAL</td><td>'.$TOTALABONOS.'</td><td>'.$TOTALSERVICIOS.'</td><td>'.$TOTALEMBARCACIONES.'</td><td>'.$TOTALTOTAL.'</td><td>'.$TOTALACOBRAR.'</td></tr>';
		echo '</table>';
	}


	//Devuelve la cantidad de inscriptos a cada deporte y el monto q se deberia recaudar por cada uno de ellos.
	public function informacionActividades()
	{

		$resultado = array();
   		$resultado['data'] = array();

		$Deportes = new Deporte();
		$Deportes->get();

        $cantTotal = 0;
        $montoTotal = 0;
		foreach ($Deportes as $deporte) {
			$informacion = array();
			$informacion['deporte'] = $deporte->nombre;

			$inscripcion = new Inscripcion();
			$inscripcion->where(array('actividad_id'=>$deporte->id,'fechaBaja is null' => null ));
			$inscripcion->get();

			$cantidadDeInscripciones = $inscripcion->where(array('actividad_id'=>$deporte->id,'fechaBaja is null' => null ))->count();
            $informacion['cantidad'] = $cantidadDeInscripciones;
			$informacion['precioIndividual'] = ($deporte->monto);
            $informacion['monto'] = ($deporte->monto)*($cantidadDeInscripciones);

            $montoTotal+=($deporte->monto)*($cantidadDeInscripciones);
            $cantTotal+=$cantidadDeInscripciones;

			array_push($resultado['data'], $informacion);
		}
        $informacion['deporte'] = '<span style="float:right">Total</span>';
        $informacion['cantidad'] = $cantTotal;
        $informacion['precioIndividual'] = '';
        $informacion['monto'] = $montoTotal;

        array_push($resultado['data'], $informacion);


 		 echo json_encode($resultado);

	}

	public function sociosAdherentes()
	{
          $resultado = array();
   		$resultado['data'] = array();

		$sociosAdherentes = new SocioAdherente();
		$sociosAdherentes->get();

		foreach ($sociosAdherentes as $pibe) {
		$informacion= array();

	    $fecha = $pibe->fechaNacimiento;
	    $fecha = str_replace("/","-",$fecha);
	    $fecha = date('Y/m/d',strtotime($fecha));
	    $hoy = date('Y/m/d');
	    $edad = $hoy - $fecha;
	     $informacion['nombre']=$pibe->nombreCompleto;
	     $socio_titular = new Socio();
	     $socio_titular->get_by_id($pibe->socio_id);
	     $persona_socio = new Persona();
	      $persona_socio->get_by_id($socio_titular->persona_id);
          $informacion['nroSocio']= $socio_titular->id;
	     $informacion['apellidos']= $persona_socio->apellidos.' '.$persona_socio->nombre;
	     $informacion['edad'] = $edad;
	     array_push($resultado['data'], $informacion);
	    }
	    echo json_encode($resultado);
	}


public function dif($mesInicio, $mesFin, $anioInicio, $anioFin)
	{
		
		//la fecha inicial debe ser menor a la fecha final
		
		$inicio =$anioInicio."-".$mesInicio."-01";

		$fin =$anioFin."-".$mesFin."-01";

		$datetime1=new DateTime($inicio);
		$datetime2=new DateTime($fin);
		 
		# obtenemos la diferencia entre las dos fechas
		$interval=$datetime2->diff($datetime1);
		 
		# obtenemos la diferencia en meses
		$intervalMeses=$interval->format("%m");
		# obtenemos la diferencia en años y la multiplicamos por 12 para tener los meses
		$intervalAnos = $interval->format("%y")*12;
		 
		
		return ($intervalMeses+$intervalAnos);
	}

	
	public function cantidadCuotas($idSocio)
	{
		$socio = new Socio();
		$socio->get_by_id($idSocio);
		$deudas = new Deuda();
		$valorRetorno=$deudas->where(array('socio_id'=>$socio->id, 'cobro_id is null'=>null))->count();
		return $valorRetorno;

	}

	public function sociosMorosos()
	{		
		$socios = new Socio();
		$socios->get();

        $minCuotas=$this->input->post('minCuotas');
        $maxCuotas=$this->input->post('maxCuotas');

		$resultado = array();
   		$resultado['data'] = array();
   		$meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octurbe","Noviembre","Dicembre");

		foreach ($socios as $socio)
		{
			if($this->cantidadCuotas($socio->id)>=$minCuotas && $this->cantidadCuotas($socio->id)<=$maxCuotas)
			{
				//Instancio las personas
				$persona_socio = new Persona();	
				$persona_socio->get_by_id($socio->persona_id);

				$socioCompleto = array();	

				$socioCompleto['id'] = sprintf('%04d',$socio->id);
		   		$socioCompleto['nombreCompleto'] = $persona_socio->apellidos.' '.$persona_socio->nombre;
		   		$socioCompleto['dni'] = $persona_socio->dni;

		   		$cobrador = new Cobrador();
		   		$cobrador->get_by_id($socio->cobrador_id);

		   		$personaCobrador = new Persona();
		   		$personaCobrador->get_by_id($cobrador->persona_id);
		   		$socioCompleto['cobrador'] = $personaCobrador->nombre.' '.$personaCobrador->apellidos;

		   		$tipoDeSocio = new TipoSocio();
		   		$tipoDeSocio->get_by_id($socio->tiposocio_id);
		   		$socioCompleto['tipoSocio']= $tipoDeSocio->descripcion;


		   		//Obtengo las deudas ya que es moroso
				$deudas = new Deuda();
				$deudas->get_where(array('socio_id' => $socio->id, 'cobro_id is null'=>null));
		   		$deudasImpagas = '';
				foreach($deudas as $deuda)
				{
					$deudasImpagas= $deudasImpagas.$meses[$deuda->mesAdeudado].'('.$deuda->anioAdeudado.')   ';
				}

				$socioCompleto['deudasImpagas'] = $deudasImpagas;


				array_push($resultado['data'], $socioCompleto);
			}
   		}		
		echo json_encode($resultado);
	}

	public function detallesDeudas()
	{
		//Primero le pido mes y año que quiere el reporte
		$mesAdeudado = 1;//$this->input->post('mesAdeudado');
		$anioAdeudado = 2016;//$this->input->post('anioAdeudado');


		//Obtengo todas las deudas que corresponden a ese mes y año
		$deudas = new Deuda();
		$deudas->where(array('mesAdeudado' => $mesAdeudado, 'anioAdeudado' => $anioAdeudado));
		$deudas->get();


		// Defino un vector que va a tener todos las posibles lineas deuda que hay
		$posiblesLineas = array();
		//Lleno ese vector
		foreach ($deudas as $deuda) {
			//obtengo todas las lineas de deudas de la deuda que estoy recorriendo
			$lineas = new LineaDeuda();
			$lineas->where('deuda_id',$deuda->id);
			$lineas->get();

			//lleno el vector de posibilidades si es que no existe ya
			foreach ($lineas as $linea) {
				//si no existe esa descripcion de linea de deuda en el vector
				if (in_array($linea->descripcion, $posiblesLineas, true) == false)
				{
					array_push($posiblesLineas, $linea->descripcion);
				}
			}
		}


		//---------HASTA ACA FUNCIONA, LO DE ABAJO SON PRUEBAS PERO NO ME ANDA-------------

		//Ya tengo todos los posibles valores de lineas deuda en el vector posiblesDeudas.
		//Ahora hago un contador para cada uno y un monto.
		$resultado = array();
		$resultado['data'] = array();
		//Entonces, para cada linea de deuda, me fijo su descripción y cual indice del vector corresponde, le sumo +1 y un monto.

		$valor = count($posiblesLineas);
		$lineas = new LineaDeuda();
		$lineas->get_where(array('deuda_id'=>$deuda->id));
		foreach ($lineas as $linea) {
			$informacion= array();

			for ($i=0; $i<=$valor; $i++) {
				//Si la linea que estoy viendo coincide con la posicion del vector
				if ($linea->descripcion == $posiblesLineas[$i]) {
					$informacion['monto'] = $linea->monto;
					$informacion['cantidad'] = 1;
					$informacion['descripcion'] = $linea->descripcion;
					array_push($resultado['data'], $informacion);


				}

			}

		}

		//echo json_encode($resultado);
		echo json_encode($posiblesLineas);






	}

	public function obtenerEmbarcaciones()

	{
		$embarcaciones = new Embarcacion();
		$socio_embarcacion = new Socio();
		$persona_socio = new Persona();
		$tipoEmbarcaciones = new TipoEmbarcaciones();

		$buscado = $this->input->post('valorBusqueda');
		//	$socios->where('estado <>', EstadoSocio::DadoDeBaja);
		$embarcaciones->get();
		$resultado = array();
		$resultado['data'] = array();
		//$tabla = "";

		foreach ($embarcaciones as $Embarcacion)
		{
			$socio_embarcacion->get_by_id($Embarcacion->socio_id);
			$persona_socio -> get_by_id($socio_embarcacion->persona_id);
			$tipoEmbarcaciones->get_by_id($Embarcacion->tipoembarcacion_id);
			$embarcacionCompleto = array();
			$embarcacionCompleto['Nombre']=$Embarcacion->nombre;
			$embarcacionCompleto['Matricula']=$Embarcacion->matricula;
			$embarcacionCompleto['Marca']=$Embarcacion->motorMarca;
			$embarcacionCompleto['Numero']=$Embarcacion->motorNumero;
			$embarcacionCompleto['Tipo']=$Embarcacion->motorTipo;
			$embarcacionCompleto['ApellidoYNombre']=$persona_socio->apellidos." ".$persona_socio->nombre;
			$embarcacionCompleto['Domicilio']=$persona_socio->domicilio;
			$embarcacionCompleto['DNI']=$persona_socio->dni;

			$telefonoFijo=$persona_socio->telefonoFijo;
   				$telefonoCelular=$persona_socio->telefonoCelular;
   				if($telefonoFijo==0)
   				{
   					$telefonoFijo='';
   				}

   				if($telefonoCelular==0)
   				{
   					$telefonoCelular='';
   				}
			$embarcacionCompleto['TE']=$telefonoFijo.' /<br>'.$telefonoCelular;


		//  	$tabla.='{"Nombre":"'.$Embarcacion->nombre.'","Matricula":"'.$Embarcacion->matricula.'","Marca":"'.$Embarcacion->motorMarca.'","N°":"'.$Embarcacion->motorNumero.'","Tipo":"'.$Embarcacion->motorTipo.'","NombreYApellido":"'.$persona_socio->nombre.'  '.$persona_socio->apellidos.'","Domicilio":"'.$persona_socio->domicilio.'","DNI":"'.$persona_socio->dni.'","TE":"'.$persona_socio->telefonoFijo.'/'.$persona_socio->telefonoCelular.'"},';
			array_push($resultado['data'], $embarcacionCompleto);
		}
		//$tabla =substr($tabla,0, strlen($tabla)-1);
		//echo '{"data":['.$tabla.']}';
		echo json_encode($resultado);
	}



    public function CantidadDeEmbarcaciones()
    {
        $Embarcaciones = new Embarcacion();
        $Embarcaciones->get();
        $cont=0;
        
        foreach ($Embarcaciones as $embarcacion) {
        	$Socio = new Socio();
        	$Socio->get_by_id($embarcacion->socio_id);
        	if($Socio->estado==EstadoSocio::Activo)
        	{
        		$cont+=1;
        	}
        }
        echo ($cont);

    }
}



