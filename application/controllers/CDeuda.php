<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CDeuda extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		if (!$this->session->userdata('username')) 
		{
			redirect('CLogin');
		}
		else
		{
				$Usuario = new usuario_model();
				$Usuario->where('username',$this->session->userdata('username'));
				$Usuario->get();
			if ($Usuario->tipoUsuario == 0)
			{
				$data['seccion'] = 'Deuda';
				$this->load->view('header',$data);
				$this->load->view('menu');
				$this->load->view('deudas');

			}
			else {redirect('CPermiso');}
		}
		
		
	}
	
	 /// GUARDA LAS LINEAS DE DEUDA CORRESPONDIENTE A LA DEUDA PASADA COMO PARÁMETRO
    function _guardarLineasDeuda($lineasDeuda,$Deuda)
    {
        foreach($lineasDeuda as $lineaDeuda)
        {
        	$lineaDeuda->save();
            $lineaDeuda->save(array($Deuda));
        }
    }

    public function tieneQueGenerar($mes,$anio,$idSocio)
    {
    	$tiene=true;
    	$Deudas = new Deuda();
    	$Deudas->where(array('mesAdeudado' => $mes, 'anioAdeudado' => $anio, 'socio_id' => $idSocio));
        $cantDeudas= $Deudas->where(array('mesAdeudado' => $mes, 'anioAdeudado' => $anio, 'socio_id' => $idSocio))->count();
        $Deudas->get();
    	if ($cantDeudas<>0)
    	{
            echo 'No tiene que generar porque ya tiene deudas';
            echo '<br>';
    		$tiene=false;
    	}

    	return $tiene;
    }

	public function Generar()
	{
		$idCobrador=($this->input->post('cobrador'));
        //Anio cargado por el admin
		$anioDeuda = $this->input->post('anio');
		//Mes cargado por el admin
		$mesDeuda = $this->input->post('mes');

        $Deuda = new Deuda();
        $Deuda->Generar($idCobrador,$mesDeuda,$anioDeuda);
    }



   
	public function ObtenerDeudas()
	{
			
		//Obtengo el cobrador
		$Cobrador = new Cobrador();
		$Cobrador->get_by_id($this->input->post('cobrador'));
		
		//Saco los datos de año y mes
		$mesDeuda = $this->input->post('mes');
		$anioDeuda = $this->input->post('anio');


		//Instancio y obtengo tdas las deudas
		$Deuda = new Deuda();
        $Deuda->get_where(array('mesAdeudado' => $mesDeuda, 'anioAdeudado'=>$anioDeuda));


		//Convierto el mes en cadena
		
		$meses = array("","Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");

		$mesCadena = $meses[$mesDeuda];
		
		//Resultado devuelto como vector de preinscripciones
		$resultado = array();
   		$resultado['deudas'] = array();
		
		//Instancio el socio que tiene la deuda
		$socioDeudor = new Socio();
		
		//Instancio la persona para sacarle nombre y apellido del deudor
		$personaSocio = new Persona();
		foreach ($Deuda as $deuda)
		{
			$deudaCompleta = array();
			
			$socioDeudor->get_by_id($deuda->socio_id);

			if($socioDeudor->cobrador_id==$Cobrador->id)
			{
			$personaSocio->get_by_id($socioDeudor->persona_id);

                $cantLetras = (strlen("Apellido y nombre")-strlen($personaSocio->apellidos))-2;


			$deudaCompleta['nrosocio'] = $socioDeudor->id;
			$deudaCompleta['mes'] = $mesCadena.'/'.$anioDeuda;
			$deudaCompleta['nombreCompleto'] = ucfirst(mb_strtolower ($personaSocio->apellidos)).' '.ucfirst(mb_strtolower(substr($personaSocio->nombre,0,$cantLetras).'.'));
			$deudaCompleta['domicilioCobro'] = ucfirst(mb_strtolower ($socioDeudor->domicilioCobro));
			$deudaCompleta['montoTotal'] = $deuda->monto;

            /// Obtengo los distintos tipos de montos
            $lineasDeudas = new Lineadeuda();

            //Obtengo el monto por tipo de abono
            $lineasDeudas->get_where(array('deuda_id'=> $deuda->id,'tiposocio_id is not NULL'=>null));
            $deudaCompleta['montoSocietario'] =$lineasDeudas->monto;

            //Obtengo el monto por servicios
            $lineasDeudas->where('deuda_id',$deuda->id)->group_start()->where('tipoembarcacion_id is not NULL',null)->or_where('actividad_id is not NULL',null)->group_end();
            $lineasDeudas->get();
            $montoServicios = 0;
            foreach($lineasDeudas as $servicio)
            {
                $montoServicios = $montoServicios + $servicio->monto;
            }

                $deudaCompleta['montoServicios'] = $montoServicios;
			
			array_push($resultado['deudas'], $deudaCompleta);
			}
				
		}

        usort($resultado['deudas'], function($a, $b) { return strcasecmp($a['nombreCompleto'],$b['nombreCompleto']); });

    	echo json_encode($resultado);
	}


    function Cupones()
    {
        $Socios = new Socio();

        //Saco los datos de año y mes
        $mesDeuda = $this->input->get('mes');
        $anioDeuda = $this->input->get('anio');

        $Deudas = new Deuda();
        $Deudas->get_where(array('mesAdeudado' => $mesDeuda, 'anioAdeudado'=>$anioDeuda));

        //Obtengo el cobrador
        $Cobrador = new Cobrador();
        $Cobrador->get_by_id($this->input->get('cobrador'));

        $cupones = array();

        foreach ($Deudas as $deuda)
        {

            // Obtengo el socio
            $Socios->get_by_id($deuda->socio_id);

            if($Socios->cobrador_id==$Cobrador->id)
            {

            //Obtengo la persona del socio
            $PersonaSocio = new Persona();
            $PersonaSocio->get_by_id($Socios->persona_id);

            //Obtengo el tipo de socio
            $TipoDeSocios = new TipoSocio();
            $TipoDeSocios->get_by_id($Socios->tiposocio_id);

            //Obtengo las líneas deudas
            $clineaDeuda = new LineaDeuda();
            $lineasdeudas = $clineaDeuda->get_where(array('deuda_id' => $deuda->id));

            // Asigno todos los valores al cupón.
            $cupon['nroSocio']=$Socios->id;
            $cupon['nombre']=$PersonaSocio->nombre;
            $cupon['apellidos']=$PersonaSocio->apellidos;
            $cupon['domicilio']=$PersonaSocio->domicilio;
            $cupon['tipoSocio'] = $TipoDeSocios->descripcion;
            $cupon['mesAdeudado']=$mesDeuda;
            $cupon['añoAdeudado']=$anioDeuda;
            $cupon['monto'] = $deuda->monto;
            $cupon['lineasDeudas']=$lineasdeudas;

            // Inserto el cupón dentro del array cupones
            array_push($cupones, $cupon);
            }
        }


        //Cargo la vista cupones y le envío el array de los cupones
        $data['cupones'] = $cupones;
        $this->load->view('cupones',$data);

    }
}
?>
