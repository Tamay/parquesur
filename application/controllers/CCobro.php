<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class CCobro extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */

	public function index()

	{
		if (!$this->session->userdata('username')) 
		{
			redirect('CLogin');
		}
		else
		{
				$Usuario = new usuario_model();
				$Usuario->where('username',$this->session->userdata('username'));
				$Usuario->get();
			if ($Usuario->tipoUsuario == 0)
			{
				$data['seccion'] = 'COBROS';
				$this->load->view('header',$data);
				$this->load->view('menu');
				$this->load->view('cobros');

			}
			else {redirect('CPermiso');}
		}


		


	}



 


public function Pagar()
    {
		$this->load->helper('date');
	  $deudas = new Deuda();
        $deudas->get();
	  $monto=0;
	  $cobro = new Cobro();
	  
     foreach($deudas as $deuda)
	 {
		 foreach (($this->input->post('deudasId')) as $listaIdDeudas)
		 {
			 if ($deuda->id == $listaIdDeudas)
			 {
				   
				$monto = $monto + $deuda->monto;				
			 }
		 }		 		 
	 }
	 if($monto <> 0){

    $cobro->fechaCobro = strftime("%Y-%m-%d", time());
	$cobro->importe = $monto;
	$cobro->cobrador_id = $this->input->post('Cobrador');;
 $cobro->save();
	 }      
		 foreach($deudas as $deuda)
	 {
		 foreach (($this->input->post('deudasId')) as $listaIdDeudas)
		 {
			 if ($deuda->id == $listaIdDeudas )
			 {
				$deuda->cobro_id = $cobro->id;
						 $deuda->save();			
			 }			 
		 }		 			 
	 }
		
        echo 'success';
        }
 

public function ObtenerDeudasSocio()
    {
	$meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Dicembre");
	 $idCobrador = $this->input->post('cobradorSeleccionado');
	 $mes = $this->input->post('mesSeleccionado');
	 $anio = $this->input->post('anioSeleccionado');
	 //vector que se devuelve
	 $resultado = array();
        $resultado['data'] = array();
		// Se pregunta si se ha seleccionado un cobrador
		if($idCobrador <> 0){
			
		$Socios = new Socio();
		
		$Socios->get();
		
        $DeudasSocio = new Deuda();
		$DeudasSocio->where('cobro_id',null);
        $DeudasSocio->get();
	
        $resultado = array();
        $resultado['data'] = array();

        foreach($Socios as $Socio)
        {
			if($Socio->estado == 0){
		$Persona = new Persona();
		$Persona->get_by_id($Socio->persona_id);
		
		 foreach($DeudasSocio as $DeudaSocio){
		
		//Verifica que sean las deudas correctas y coincidan los meses y años.  O estos tengan la opcion "todos"
			if( ($DeudaSocio->socio_id == $Socio->id) && ($Socio->persona_id == $Persona->id) && (($mes==0) ||($mes == $DeudaSocio->mesAdeudado)) && (($anio == $DeudaSocio->anioAdeudado) || $anio==0) && ($Socio->cobrador_id == $idCobrador)) 
			
			{
				if($DeudaSocio->monto <> 0) {
            $DeudasSocioResultado = array();

	$DeudasSocioResultado['numSocio'] = $DeudaSocio->socio_id;
	$DeudasSocioResultado['nombreCompleto'] = $Persona->apellidos . ' ' . $Persona->nombre;
	$DeudasSocioResultado['Mes'] = $meses[$DeudaSocio->mesAdeudado];
	$DeudasSocioResultado['Anio'] = $DeudaSocio->anioAdeudado;
	$DeudasSocioResultado['Monto'] = $DeudaSocio->monto;
	$DeudasSocioResultado['NumDeuda'] = $DeudaSocio->id;
	$DeudasSocioResultado['NumCobrador'] = $Socio->cobrador_id;
	$Cobrador = new Cobrador();
	$Cobrador->get_by_id($Socio->cobrador_id);
	$DeudasSocioResultado['comision'] = $Cobrador->comision;
	$DeudasSocioResultado['stringFecha'] = $DeudaSocio->anioAdeudado . '' . sprintf('%02d', $DeudaSocio->mesAdeudado);
	array_push($resultado['data'], $DeudasSocioResultado);
}
			}
			}
			

        }}}
        echo json_encode($resultado);		
    }
	

//La misma que la anterior pero para vistaCobrador. Se podria dejar todo dentro de una misma funcion (anda no se toca)
public function ObtenerDeudasSocio1()
    {
	$meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Dicembre");
	
		$numeroCobrador = ($this->input->post('Cobrador'));
		
		$Socios = new Socio();
		$Socios->where('cobrador_id',$numeroCobrador);
		$Socios->get();
		
        $DeudasSocio = new Deuda();
		$DeudasSocio->where('cobro_id',null);
        $DeudasSocio->get();
	
        $resultado = array();
        $resultado['data'] = array();

        foreach($Socios as $Socio)
        { 
		$Persona = new Persona();
		$Persona->get_by_id($Socio->persona_id);
		
		 foreach($DeudasSocio as $DeudaSocio){
		
		 
			if( ($DeudaSocio->socio_id == $Socio->id) && ($Socio->persona_id == $Persona->id))
			
			{
            $DeudasSocioResultado = array();

$DeudasSocioResultado['Nº Socio'] = $DeudaSocio->socio_id;
$DeudasSocioResultado['Apellidos'] = $Persona->apellidos;
$DeudasSocioResultado['Nombres'] = $Persona->nombre;
            $DeudasSocioResultado['Mes'] = $meses[$DeudaSocio->mesAdeudado];;
            $DeudasSocioResultado['Anio'] = $DeudaSocio->anioAdeudado;
            $DeudasSocioResultado['Monto'] = $DeudaSocio->monto;
           $DeudasSocioResultado['NumDeuda'] = $DeudaSocio->id;
		   $DeudasSocioResultado['NumCobrador'] = $Socio->cobrador_id;
		   $Cobrador = new Cobrador();
		$Cobrador->get_by_id($Socio->cobrador_id);
		   $DeudasSocioResultado['comision'] = $Cobrador->comision;
            array_push($resultado['data'], $DeudasSocioResultado);
			}
			}
			

        }
        echo json_encode($resultado);
    }


public function obtenerCobros()

	{// se puede mejorar esta funcion. Muy a lo perro
		$resultadoCobro = array();
   		$resultadoCobro['data'] = array();
		
		$Cobros = new Cobro();
		$Cobros -> get();
		foreach ($Cobros as $Cobro){
			$Cobrador = new Cobrador();
			$Cobrador -> get_by_id($Cobro->cobrador_id);
			$Persona = new Persona();
			$Persona -> get_by_id ($Cobrador->persona_id);
			
			$cobroCompleto['id'] = $Cobro->id;
                $cobroCompleto['nombre'] = $Persona->nombre;
   				$cobroCompleto['apellido'] = $Persona->apellidos;
   				$cobroCompleto['fechaCobro'] = $Cobro->fechaCobro;	
   				$cobroCompleto['importe'] = $Cobro->importe;
				
			
				array_push($resultadoCobro['data'], $cobroCompleto);
			
			}
		echo json_encode($resultadoCobro);
	}
	
	

public function CobradoresSeleccion()
	{
		$cobradores = new Cobrador();
		$cobradores->where('estadoCobrador <>', EstadoCobrador::DadoDeBaja);
		$cobradores->get();

		$resultado = '<select name="cobrador" id="campoCobrador">';
		$resultado = $resultado . '<option value="0" selected> Seleccione Cobrador </option>';
		foreach ($cobradores as $cobrador)
        {
			$persona_cobrador = new Persona();
			$persona_cobrador ->get_by_id($cobrador->persona_id);
			$resultado = $resultado . '<option value='.$cobrador->id.'>'.$persona_cobrador->nombre.' '.$persona_cobrador->apellidos.' </option>';
			
		}
		
		echo $resultado . '</select>';
	}


public function MesSeleccion()
	{
	$resultado1 = '<select name="Mes" id="campoMes">';
	        $resultado1 = $resultado1 . '<option value="0" selected> Todos </option>';
			$resultado1 = $resultado1 . '<option value=1> Enero </option>';
			$resultado1 = $resultado1. '<option value=2> Febrero </option>';
			$resultado1 = $resultado1 . '<option value=3> Marzo </option>';
			$resultado1 = $resultado1 . '<option value=4> Abril </option>';
			$resultado1 = $resultado1 . '<option value=5> Mayo </option>';
			$resultado1 = $resultado1 . '<option value=6> Junio </option>';
			$resultado1 = $resultado1 . '<option value=7> Julio </option>';
			$resultado1 = $resultado1 . '<option value=8> Agosto </option>';
			$resultado1 = $resultado1 . '<option value=9> Septiembre </option>';
			$resultado1 = $resultado1 . '<option value=10> Octubre </option>';
			$resultado1 = $resultado1 . '<option value=11> Noviembre </option>';
			$resultado1 = $resultado1 . '<option value=12> Diciembre </option>';
			
	echo $resultado1 . '</select>';
	}
	
public function AnioSeleccion()
	{
		
		
		
		$Deudas = new Deuda();
		$Deudas ->order_by("anioAdeudado","desc");
		$Deudas ->get();
	
		$valorAño=0;
		
		
	$resultado2 = '<select name="Año" id="campoAño">';
	$añoActual = date("Y");
	$resultado2 = $resultado2 . '<option value="0" selected> Todos </option>';
	foreach($Deudas as $Deuda){
		if($valorAño <> $Deuda->anioAdeudado){
    $valorAño= $Deuda->anioAdeudado; 

			$resultado2 = $resultado2 . '<option value='.$valorAño.'> '.$valorAño.' </option>';
		}
		}	
	echo $resultado2 . '</select>';
	}	


public function cancelarCobro()
{
	
	
 $idCobro = $this->input->post('idCobro');
 $deudas = new Deuda();
 $deudas->where('cobro_id', $idCobro);
  $deudas->update(array('cobro_id = NULL'=> null));
// $deudas->get();
 
 /*foreach($deudas as $deuda)
 {
	 $deuda->cobro_id="NULL";
 }*/

 $cobro = new Cobro();
 $delete = $cobro->eliminar_cobro($idCobro);
 
 
//$socio_id = $this->uri->segment(3);	
//$Socio = new Socio();
//$Socio->where('id', $socio_id)->update('estado', EstadoSocio::DadoDeBaja);
}
}