<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class CEmbarcacion extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */

	public function index()

	{
		ini_set('date.timezone', 'America/New_York');
		$data['seccion'] = 'EMBARCACIONES';
		$this->load->view('header',$data);
		$this->load->view('menu');
		$this->load->view('embarcaciones');


	}



	public function agregar()
	{
        $Embarcacion = new Embarcacion();

        if($this->input->post('nombre')=='')
        {
            $_POST = json_decode(file_get_contents('php://input'), true);
        }

        $Embarcacion->matricula = $this->input->post('matricula');
        $Embarcacion->nombre = $this->input->post('nombre');
		$Embarcacion->tipoembarcacion_id = $this->input->post('tipoEmbarcaciones');
		$Embarcacion->motorMarca = $this->input->post('motorMarca');
		$Embarcacion->motorNumero = $this->input->post('motorNumero');
		$Embarcacion->motorTipo = $this->input->post('motorTipo');
		if ($this->input->post('amarre') <> null){
		$Embarcacion->amarre = $this->input->post('amarre');}
	    if ($this->input->post('fechaAdquisicion')<> null){
		$Embarcacion->fechaAdquisicion = $this->input->post('fechaAdquisicion');}
		if($this->input->post('anioConstruccion') <> null){
		$Embarcacion->anioConstruccion = $this->input->post('anioConstruccion');}
		if ($this->input->post('arboladura')<>null){
		$Embarcacion->arboladura = $this->input->post('arboladura');}
		if($this->input->post('eslora')<>null){
		$Embarcacion->eslora = $this->input->post('eslora');}
		if($this->input->post('manga')){
		$Embarcacion->manga = $this->input->post('manga');}
		if($this->input->post('puntal')){
		$Embarcacion->puntal = $this->input->post('puntal');}
		if($this->input->post('tonelaje')<>null){
		$Embarcacion->tonelaje = $this->input->post('tonelaje');}
		if($this->input->post('HP')<>null){
		$Embarcacion->HP = $this->input->post('HP');}
		$Embarcacion->observaciones = $this->input->post('observaciones');




		$Embarcacion->socio_id = $this->input->post('socio_id'); 
     
        ($Embarcacion->save());

        $this->load->helper('date');
		echo "La embarcacion se ha agregado con éxito";
		
	}


	public function obtenerEmbarcacion()
	{
		$embarcacion_id = $this->uri->segment(3);
		$embarcacion = new Embarcacion();
		$embarcacion->get_by_id($embarcacion_id);
		$socio_embarcacion = new Socio();
		$socio_embarcacion->get_by_id($embarcacion->socio_id);
		$persona_socio = new Persona();
		$persona_socio->get_by_id($socio_embarcacion->persona_id);
		$tipoEmbarcaciones = new TipoEmbarcaciones();
		$tipoEmbarcaciones->get_by_id($embarcacion->tipoembarcacion_id);

		$data = array(
		'id'=> $this->uri->segment(3),
		'matricula'=> $embarcacion->matricula,
		'nombre'=> $embarcacion->nombre,
		'tipoEmbarcacion'=> $tipoEmbarcaciones->id,
		'motorMarca'=> $embarcacion->motorMarca,
		'motorNumero'=> $embarcacion->motorNumero,
		'motorTipo'=> $embarcacion->motorTipo,
		'amarre'=> $embarcacion->amarre,
		'fechaAdquisicion'=> $embarcacion->fechaAdquisicion,
		'anioConstruccion'=> $embarcacion->anioConstruccion,
		'arboladura'=> $embarcacion->arboladura,
		'eslora'=> $embarcacion->eslora,
		'manga'=> $embarcacion->manga,
		'puntal'=> $embarcacion->puntal,
		'tonelaje'=> $embarcacion->tonelaje,
		'HP'=> $embarcacion->HP,
		'observaciones'=> $embarcacion->observaciones,
		'nombrePersona'=> $persona_socio->nombre,
		'apellidosPersona'=> $persona_socio->apellidos,
		'dni'=> $persona_socio->dni,





		);
		echo json_encode($data);
	}




public function obtenerEmbarcaciones()
	{
		$embarcaciones = new Embarcacion();
		$socio_embarcacion = new Socio();
		$persona_socio = new Persona();
		$tipoEmbarcaciones = new TipoEmbarcaciones();


        $_POST = json_decode(file_get_contents('php://input'), true);
        $Socio_id= $this->input->post('socio');

        if($Socio_id!=null)
        {
            $embarcaciones->where(array('socio_id'=>$Socio_id))->get();
        }
        else
        {
            $embarcaciones->get();
        }


		$resultado = array();
   		$resultado['embarcaciones'] = array();
		$tabla = "";

		foreach ($embarcaciones as $Embarcacion)
		{
			$socio_embarcacion->get_by_id($Embarcacion->socio_id);
			$persona_socio -> get_by_id($socio_embarcacion->persona_id); 
			$tipoEmbarcaciones->get_by_id($Embarcacion->tipoembarcacion_id);
			$embarcacionCompleto = array();			

			$tabla.='{"nroEmbarcacion":"'.$Embarcacion->id.'","nroSocio":"'.sprintf('%04d',$socio_embarcacion->id).'","ApellidoYNombre":"'.$persona_socio->apellidos.' '.$persona_socio->nombre.'","Nombre":"'.$Embarcacion->nombre.'","Matricula":"'.$Embarcacion->matricula.'","Tipo":"'.$tipoEmbarcaciones->descripcion.'","Tel/Cel":"'.$persona_socio->telefonoFijo.''." / ".''.$persona_socio->telefonoCelular.'","Monto":"'.$tipoEmbarcaciones->monto.'"},';

		}
		$tabla =substr($tabla,0, strlen($tabla)-1);
		echo '{"data":['.$tabla.']}';
		//echo json_encode($resultado);
	}
	

public function Editar()
{
	$unAmarre =NULL;
	$unaFechaConstruccion =NULL;
		$embarcacion_id = $this->uri->segment(3);

        $Embarcacion = new Embarcacion();
     //   $Socio = new Socio();
	//	$Persona = new Persona();
		
        $Embarcacion->get_by_id($embarcacion_id);
     //   $socio_id = $Embarcacion->socio_id;
	//	$Socio->get_by_id($socio_id);
	//	 $persona_id = $Socio->persona_id;
	//	$Persona->get_by_id($spersona_id);
	if($this->input->post('amarre')<>null){
		$unAmarre=$this->input->post('amarre');
	}
	if($this->input->post('fechaAdquisicion')<>null){
		$unaFechaConstruccion= $this->input->post('fechaAdquisicion');
	}


        $Embarcacion->where('id', $embarcacion_id)->update(array('matricula'=>$this->input->post('matricula'),
        	'nombre'=>$this->input->post('nombre'),'tipoembarcacion_id'=>$this->input->post('tipoEmbarcaciones'),
			'motorMarca'=>$this->input->post('motorMarca'),
			'motorNumero'=>$this->input->post('motorNumero'),
			'motorTipo'=>$this->input->post('motorTipo'),
			'amarre'=> $unAmarre,
			'fechaAdquisicion'=> $unaFechaConstruccion,
			'anioConstruccion'=>$this->input->post('anioConstruccion'),
			'arboladura'=>$this->input->post('arboladura'),
			'eslora'=>$this->input->post('eslora'),
			'manga'=>$this->input->post('manga'),
			'puntal'=>$this->input->post('puntal'),
			'tonelaje'=>$this->input->post('tonelaje'),
			'HP'=>$this->input->post('HP'),
			'observaciones'=>$this->input->post('observaciones')));







}



public function DarDeBaja()
{
	
	
 $id = $this->uri->segment(3);

 $delete = $this->Embarcacion->eliminar_embarcacion($id);
 
 
//$socio_id = $this->uri->segment(3);	
//$Socio = new Socio();
//$Socio->where('id', $socio_id)->update('estado', EstadoSocio::DadoDeBaja);
}




}
