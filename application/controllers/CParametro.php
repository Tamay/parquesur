<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class CParametro extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		{
		if (!$this->session->userdata('username')) //si esto existe devuelve verdadero
		{
			redirect('CLogin');
		}
		else
		{
				$Usuario = new usuario_model();
				$Usuario->where('username',$this->session->userdata('username'));
				$Usuario->get();
			if ($Usuario->tipoUsuario == 0)
			{
				$data['seccion'] = 'INICIO';
				$this->load->view('header',$data);
				$this->load->view('menu');
				$this->load->view('parametros');
			}
			else {redirect('CPermiso');}
		}
		//$data['seccion'] = 'Parametro';
		//$this->load->view('header',$data);
		//$this->load->view('menu');
		//$this->load->view('parametros');
	}
}


		public function obtenerParametros()
	{
		//Instancio un parametro y las obtengo a todas
		$Parametro = new Parametro();
		$Parametro->get();
		
		//Resultado devuelto como vector de parametros
		$resultado = array();
   		$resultado['parametros'] = array();

		$i=1; //valor del seleccion por defecto 1
		foreach ($Parametro as $Param)
		{
			$Parametros_datos = array();			
	
				//Los datos de la persona que solicita
				$Parametros_datos['idParametro'] = $Param->id;
   				$Parametros_datos['nombre'] = $Param->nombre;
   				$Parametros_datos['valor'] = $Param->valor;
   				$Parametros_datos['fechaModificacion'] = $Param->fechaModificacion;
				

   				array_push($resultado['parametros'], $Parametros_datos);
     	 
		}
		
		echo json_encode($resultado);
	}
	
	public function obtenerParametro()
	{
		$id = $this->uri->segment(3); 
		$parame = new Parametro();
		
		//Obtengo el parametro
		$parame->get_by_id($id);
				
		$data = array(
		'idParametro' => $parame->id,
		'nombre'=> $parame->nombre,
		'valor'=> $parame->valor,
		'fechaModificacion'=> $parame->fechaModificacion,
		);
		echo json_encode($data);
	}



public function editarParametro()
{
		$id = $this->uri->segment(3);
        $Parametro = new Parametro();
        $Parametro->get_by_id($id);
		//Obtengo la fecha de hoy
		$fecha=strftime( "%Y-%m-%d", time() );

        $Parametro->where('id', $id)->update(array('nombre'=>$this->input->post('nombre'),
        	'valor'=>$this->input->post('valor'),'fechaModificacion'=>$fecha));
        

}



}

