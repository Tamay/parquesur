<?php

class CLogin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		//Esto es para que, si ya esta la sesion creada que redireccione para el menu si queres entrar al login
		//$this->load->view('header');


		if ($this->session->userdata('username')) //si esto existe devuelve verdadero
		{
			
			redirect('inicio');
	
		}

		if(null!=$this->input->post('password')){
		$this->load->model('usuario_model');
		
		if ($this->usuario_model->login($this->input->post('username'),$this->input->post('password'))) { 
			
			$this->session->set_userdata('username',$this->input->post('username'));
			
				$Usuario = new usuario_model();
				$Usuario->where('username',$this->session->userdata('username'));
				$Usuario->get();


				switch ($Usuario->tipoUsuario) {
					case 0: //Tipo administrador
						redirect('inicio');
						break;
					
					case 1: //Tipo cobrador. 
						redirect('CCobradorVista');
						break;
					case 2:
					    redirect('CPermiso'); //ESTA PARTE ES DE LOS USUARIOS TODAVIA NO ESTA INCORPORADO.
					break;}

				
			
			}
			
			else
			{
			 	redirect('CLogin');
			} 
		}
		$this->load->view('login');
	}

	//public function key ()
	//{
	//	echo md5("ParqueSur");
	//}

	public function logout()
	{
		
		$this->session->sess_destroy(); //No esta cerrando sesion porque si cambias lo de abajo te redirecciona a cualquier modulo
		redirect('CLogin');
	}
     
}
?>