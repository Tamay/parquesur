<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CUsuario extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if (!$this->session->userdata('username')) //si esto existe devuelve verdadero
		{
			redirect('CLogin');
		}
		else
		{
				$Usuario = new usuario_model();
				$Usuario->where('username',$this->session->userdata('username'));
				$Usuario->get();
			if ($Usuario->tipoUsuario == 0)
			{
				$data['seccion'] = 'INICIO';
				$this->load->view('header',$data);
				$this->load->view('menu');
				$this->load->view('usuarios');
			}
			else {redirect('CPermiso');}
		}
	}

public function agregar()

	{
		$Usuario= new usuario_model();
        $Usuario->username = $this->input->post('username');
        $Usuario->password = $this->input->post('password');
		$Usuario->tipoUsuario = $this->input->post('tipoUsuario');
		$Usuario->persona_id = $this->input->post('persona_id'); 
       
        ($Usuario->save());

        echo "success";

	}
public function editar()
{
		$usuario_id = $this->uri->segment(3);

        $Usuario = new usuario_model();
        
        $Usuario->get_by_id($usuario_id);
        
        $Usuario->where('Id', $usuario_id)->update(array('username'=>$this->input->post('username'),
        	'password'=>$this->input->post('password'),'tipoUsuario'=>$this->input->post('tipoUsuario')));
        
}


	public function obtenerUsuario()
	{
		$usuario_id = $this->uri->segment(3);
		$usuario = new Usuario_model();
		$usuario->get_by_id($usuario_id);

		$persona_usuario = new Persona();
		$persona_usuario->get_by_id($usuario->persona_id);
		
		

		$data = array(
		'username'=> $usuario->username,
		'password'=> $usuario->password,
		'tipoUsuario'=> $usuario->tipoUsuario,
		//'nombre' => $persona_usuario->nombre,
		);
		echo json_encode($data);
	}

	public function obtenerUsuarios()
	{
		$usuarios = new usuario_model();
		$usuarios->get();

		$buscado = $this->input->post('valorBusqueda');
	
		$persona_usuario = new Persona();

		$resultado = array();
   		$resultado['usuarios'] = array();

		foreach ($usuarios as $usuario)
		{
			$persona_usuario->get_by_id($usuario->persona_id);
			$usuarioCompleto = array();			


			if($buscado=="" || stristr(''.$persona_usuario->nombre.' '.$persona_usuario->apellidos,$buscado)!==false
			|| stristr(''.$persona_usuario->apellidos.' '.$persona_usuario->nombre,$buscado)!==false)
			{

				$usuarioCompleto['nroUsuario']= $usuario->Id;
   				$usuarioCompleto['tipoUsuario'] = $usuario->tipoUsuario;
   				$usuarioCompleto['usuario'] = $usuario->username;
   				$usuarioCompleto['contrasenia'] = $usuario->password;	
   				
   				$usuarioCompleto['nombre'] = $persona_usuario->nombre;
   				$usuarioCompleto['apellidos'] = $persona_usuario->apellidos;
   				

               
   				array_push($resultado['usuarios'], $usuarioCompleto);
			}
		}
		
		echo json_encode($resultado);
	}

	public function obtenerPersonas()
	{
		$personas = new Persona();
		$personas->get();

		$buscado = $this->input->post('valorBusqueda');
	
		$resultado = array();
   		$resultado['personas'] = array();

		foreach ($personas as $persona)
		{
			
			$personaData = array();			


			if($buscado=="" || stristr(''.$persona->nombre.' '.$persona->apellidos,$buscado)!==false
			|| stristr(''.$persona->apellidos.' '.$persona->nombre,$buscado)!==false)
			{

				$personaData['id'] = $persona->id;
				$personaData['dni']= $persona->dni;
   				$personaData['nombre'] = $persona->nombre;
   				$personaData['apellidos'] = $persona->apellidos;
   					            
   				array_push($resultado['personas'], $personaData);
			}
		}
		
		echo json_encode($resultado);
	}

	
}
