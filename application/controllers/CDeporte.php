<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class CDeporte extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */

	public function index()

	{
		if (!$this->session->userdata('username')) 
		{
			redirect('CLogin');
		}
		else
		{
				$Usuario = new usuario_model();
				$Usuario->where('username',$this->session->userdata('username'));
				$Usuario->get();
			if ($Usuario->tipoUsuario == 0)
			{
				$data['seccion'] = 'DEPORTES';
				$this->load->view('header',$data);
				$this->load->view('menu');
				$this->load->view('deportes');

			}
			else {redirect('CPermiso');}
		}
		
		
	}



	public function agregarDeporte()

	{

		$Deporte = new Deporte();
		$nombreDeporteBusqueda = $Deporte->get_by_nombre($this->input->post('nombre'))->nombre;
		if($nombreDeporteBusqueda=='')
		{
        $Deporte->nombre = $this->input->post('nombre');
        $Deporte->monto = $this->input->post('monto');
        $Deporte->estado = EstadoDeporte::Habilitado;
        ($Deporte->save());

        $this->load->helper('date');
		echo "success";}

	}

	public function DeportesEnSelect()
	{
		$deporte= new Deporte();
		$deporte->get();

		$resultado = '<select name="tipoDeporte" id="campoTipoDeporte">';
		foreach ($deporte as $depo)
		{
			$resultado = $resultado . '<option value="'.$depo->id.'">'.$depo->nombre.' ($'.$depo->monto.')'.'</option>';
		}
			$resultado = $resultado. '<option value =0 selected>NINGUNA</option>';
		echo $resultado . '</select>';
	}

	public function obtenerDeportes()
	{
		$Deportes = new Deporte();
		$Deportes->where('estado <>', EstadoDeporte::DadoDeBaja);
		$Deportes->get();
		
		$resultado = array();
        $resultado['data'] = array();

        foreach($Deportes as $Deporte)
        {
            $deporteResultado = array();

            $deporteResultado['id'] = $Deporte->id;
            $deporteResultado['nombre'] = $Deporte->nombre;
            $deporteResultado['monto'] = $Deporte->monto;
			switch ($Deporte->estado)
			{
			case 1:$deporteResultado['estado'] = "Inactivo";
			break;
			case 0:$deporteResultado['estado'] = "Activo";
			break;
			}          
         
            array_push($resultado['data'], $deporteResultado);

        }
        echo json_encode($resultado);
	}

	public function obtenerDeporte()
	{
		$deporte_id = $this->uri->segment(3);
		$deporte = new Deporte();
		$deporte->get_by_id($deporte_id);
		

		$data = array(
		'nombre'=> $deporte->nombre,
		'monto'=> $deporte->monto,
		'estado'=> $deporte->estado,
		);
		echo json_encode($data);
	}

public function editar()
{
		$deporte_id = $this->uri->segment(3);

        $Deporte = new Deporte();
        $Deporte->get_by_id($deporte_id);
        $Deporte->where('id', $deporte_id)->update(array('nombre'=>$this->input->post('nombre'), 'monto'=>$this->input->post('monto'), 'estado'=>$this->input->post('estado')));
}


public function DarDeBajaDeporte()
{
$deporte_id = $this->uri->segment(3);	
$Deporte = new Deporte();
$Deporte->where('id', $deporte_id)->update('estado', EstadoDeporte::DadoDeBaja);

$inscripciones = new Inscripcion();
$inscripciones->where('actividad_id',$deporte_id)->update('fechaBaja',strftime("%Y-%m-%d", time()));
}

}
