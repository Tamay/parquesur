<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class CPreinscripcion extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		if (!$this->session->userdata('username')) 
		{
			redirect('CLogin');
		}

		$data['seccion'] = 'Preinscripcion';
		$this->load->view('header',$data);
		$this->load->view('menu');
		$this->load->view('preinscripciones');
		$this->load->helper('date');
	}

    //Esta funcion está para cuando una persona se acerca al club para asociarse pero no paga el monto entonces queda como 		preinscripcion hasta que lo pague
	public function agregar()
	{
		$Persona = new Persona();
		//Obtengo el dni de la persona que se creo
	  	$dniBusqueda = $Persona->get_by_dni($this->input->post('dni'))->dni;
		//Una variable que contiene un mensaje de fallo o exito
		$mensaje = 'fallo';
		//Si la persona no existe en la base de datos se la deja solicitar una preinscripcion
	if($dniBusqueda=='')
       {
			$Persona->nombre = $this->input->post('nombre');
			$Persona->apellidos = $this->input->post('apellidos');
			$Persona->dni = $this->input->post('dni');
			$Persona->genero = $this->input->post('genero');
			$Persona->correoElectronico = $this->input->post('correoElectronico');
			$Persona->fechaNacimiento = $this->input->post('fechaNacimiento');
			$Persona->profesion = $this->input->post('profesion');
			$Persona->domicilio = $this->input->post('domicilio');
			$Persona->telefonoFijo = $this->input->post('telefonoFijo');
			$Persona->telefonoCelular = $this->input->post('telefonoCelular');
			$Persona->nacionalidad = $this->input->post('nacionalidad');
			$Persona->estadoCivil = $this->input->post('estadoCivil');
			$Persona->Save();
			
			//Creo una preinscripcion
			$Preinscripto = new Preinscripcion();
			//Obtengo la fecha actual, hoy.
			$fecha=strftime( "%Y-%m-%d", time() );
			//Asigno los valores a la preinscripcion
			$Preinscripto->fechaSolicitud =  $fecha;
			//La fecha de vencimiento es la fecha mas el intervalo (1 mes)
			$nuevafecha = strtotime ( '+30 day' , strtotime ( $fecha ) ) ;
			$nuevafecha = date ( 'Y-m-j' , $nuevafecha );
			$Preinscripto->fechaVencimiento = $nuevafecha;
			
			//Busco el valor del monto actual
			$param = new Parametro();
			//El id del parametro que indica el motno es el numero 2
			$param->get_by_id(2);
			$montoPreinscripcion = $param->valor;
			$Preinscripto->monto = $montoPreinscripcion;
			
			//$Preinscripto->persona_id = $Persona->dni; esto no seria necesario gracias al save array
			$Preinscripto->save();
			//Guardo el registro
			($Preinscripto->save(array($Persona)));
			$mensaje = 'exito';
			echo $mensaje;
		}
		
		//Entonces la persona ya existe en la BDD		
		else
		{
			echo $mensaje;
		}
}



	public function obtenerPreinscriptos()
	{
		//Instancio una preinscripcion y las obtengo a todas
		$Preinscripcion = new Preinscripcion();
		$Preinscripcion->get();
		//Si se busca alguino en especial
		$buscado = $this->input->post('valorBusqueda');
		
		//Resultado devuelto como vector de preinscripciones
		$resultado = array();
   		$resultado['preinscripciones'] = array();

		//Persona que solicita
		$Persona_Solicita = new Persona();

		$i=1; //valor del seleccion por defecto 1
		foreach ($Preinscripcion as $Preinscripto)
		{
			$Persona_Solicita->get_by_id($Preinscripto->persona_id);
			$PreinscripcionCompleta = array();			

			
			
			if($buscado=="" || stristr(''.$Persona_Solicita->nombre.' '.$Persona_Solicita->apellidos,$buscado)!==false
			|| stristr(''.$Persona_Solicita->apellidos.' '.$Persona_Solicita->nombre,$buscado)!==false)
		{
				//Los datos de la persona que solicita
   				$PreinscripcionCompleta['dni'] = $Persona_Solicita->dni;
   				$PreinscripcionCompleta['nombre'] = $Persona_Solicita->nombre;
   				$PreinscripcionCompleta['apellidos'] = $Persona_Solicita->apellidos;	
				//Los datos de la preinscripcion que solicito la persona
   				$PreinscripcionCompleta['fechaSolicitud'] = $Preinscripto->fechaSolicitud;
   				$PreinscripcionCompleta['fechaVencimiento'] = $Preinscripto->fechaVencimiento;
   				$PreinscripcionCompleta['monto'] = $Preinscripto->monto;
				$PreinscripcionCompleta['idPreinscripcion'] = $Preinscripto -> id;

   				array_push($resultado['preinscripciones'], $PreinscripcionCompleta);
		}
		}
		
		echo json_encode($resultado);
	}
	
	public function obtenerPreinscripto()
	{
		$preinscripto_id = $this->uri->segment(3); 
		$preinscripto = new Preinscripcion();
		
		//Obtengo la preinscripcion
		$preinscripto->get_by_id($preinscripto_id);
				
		//obtengo la persona
		$personaPreinscripta = new Persona();
		$personaPreinscripta ->get_by_id($preinscripto->persona_id);

		$data = array(
		'nombre'=> $personaPreinscripta->nombre,
		'apellidos'=> $personaPreinscripta->apellidos,
		'dni'=> $personaPreinscripta->dni,
		'domicilio'=> $personaPreinscripta->domicilio,
		'correoElectronico'=> $personaPreinscripta->correoElectronico,
		'fechaNacimiento'=> $personaPreinscripta->fechaNacimiento,
		'profesion'=> $personaPreinscripta->profesion,
		'genero'=> $personaPreinscripta->genero,
		'nacionalidad' =>$personaPreinscripta->nacionalidad,
		'telefonoFijo' =>$personaPreinscripta->telefonoFijo,
		'telefonoCelular' =>$personaPreinscripta->telefonoCelular,
		'estadoCivil' =>$personaPreinscripta->estadoCivil,
		
		//'domicilioCobro'=> $socio->domicilioCobro,
		//'intervaloPago'=> $socio->intervaloPago,
		//'tiposocio'=> $socio->tiposocio_id,

		);
		echo json_encode($data);
	}

public function borrar_preinscripcion()
 {
 //Obtengo el id y lo elimino (gracias codeigniter)
 $id = $this->uri->segment(3);
 $delete = $this->Preinscripcion->eliminar_preinscripcion($id);
 }
 
 //Funcion para eliminar las preinscripciones que ya vencieron
 public function eliminarVencidas()
 {
	 $Preinscripcion = new Preinscripcion();
	//Las obtengo a todas
	 $Preinscripcion->get();
	 
	 foreach($Preinscripcion as $preinscripto)
	 {
		 //Si la fecha de vencimiento es menor a la fecha de hoy, o sea que ya vencio
		 if ($preinscripto->fechaVencimiento <= $fecha=strftime( "%Y-%m-%d", time()))
		 {
			//Elimino la preinscripcion
			$deletePrein = $this->Preinscripcion->eliminar_preinscripcion($preinscripto->id);
			//Busco la persona que solicito esa preinscripcion
			$Persona = new Persona();
			$Persona->get_by_id($preinscripto->persona_id);
			$idPersona = $Persona->id;
			//Elimino la persona que solicito la preinscripcion
			$deletePersona=$this->Persona->eliminarPersona($Persona->id);
		 }
	 }
	
 }
 
 
	
}

