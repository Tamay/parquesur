<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CSocioAdherente extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function Agregar()
    {
        $Socio = new Socio();
        $Socio ->get_by_id($this->uri->segment(3));




        $socioAdherente = new SocioAdherente();
        $this->load->helper('date');

        $dniBusqueda = $socioAdherente->get_by_dni($this->input->post('dni'))->dni;

        if($dniBusqueda=='')
        {
        $socioAdherente->nombreCompleto =  $this->input->post('nombreCompleto');
        $socioAdherente->dni =  $this->input->post('dni');
        $socioAdherente->fechaAlta = strftime("%Y-%m-%d", time());
        $socioAdherente->fechaNacimiento = $this->input->post('fechaNacimiento');
        $socioAdherente->parentezco = $this->input->post('parentezco');
        $socioAdherente->save();

        $socioAdherente->save(array($Socio));

        echo 'success';
        }
        else{
            echo 'fail';
        }
    }


    public function Eliminar()
    {
        $socioAdherente = new SocioAdherente();
        $socioAdherente ->get_by_id($this->uri->segment(3));

        $socioAdherente->delete();

        $this->load->helper('date');
        echo 'success';
    }



    public function ObtenerSociosAdherentes()
    {
        $socio_id = $this->uri->segment(3);
        $SociosAdherentes = new SocioAdherente();
        $SociosAdherentes->where('socio_id',$socio_id);
        $SociosAdherentes->get();
	
        $resultado = array();
        $resultado['sociosAdherentes'] = array();

        foreach($SociosAdherentes as $SocioAdherente)
        {
            $socioAdherenteResultado = array();

            $socioAdherenteResultado['id'] = $SocioAdherente->id;
            $socioAdherenteResultado['nombreCompleto'] = $SocioAdherente->nombreCompleto;
            $socioAdherenteResultado['dni'] = $SocioAdherente->dni;
            $socioAdherenteResultado['fechaAlta'] = $SocioAdherente->fechaAlta;
            $socioAdherenteResultado['fechaNacimiento'] = $SocioAdherente->fechaNacimiento;
            $socioAdherenteResultado['parentezco'] = $SocioAdherente->parentezco;

            array_push($resultado['sociosAdherentes'], $socioAdherenteResultado);

        }
        echo json_encode($resultado);
    }
}
