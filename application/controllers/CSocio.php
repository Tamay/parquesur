<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class CSocio extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore ill

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */

	public function index()

	{
		if (!$this->session->userdata('username')) 
		{
			redirect('CLogin');
		}
		else
		{
				$Usuario = new usuario_model();
				$Usuario->where('username',$this->session->userdata('username'));
				$Usuario->get();
			if ($Usuario->tipoUsuario == 0)
			{
				$data['seccion'] = 'SOCIOS';
				$this->load->view('header',$data);
				$this->load->view('menu');
				$this->load->view('socios');


			}
			else {redirect('CPermiso');}
		}
		
		

	}



	public function agregar()

	{
		$Persona = new Persona();
		$dniBusqueda = $Persona->get_by_dni($this->input->post('dni'))->dni;

		if($dniBusqueda=='')
        {
        	$Persona->nombre = $this->input->post('nombre');
                $Persona->apellidos = $this->input->post('apellidos');
                $Persona->dni = $this->input->post('dni');
                $Persona->genero = $this->input->post('genero');
                $Persona->correoElectronico = $this->input->post('correoElectronico');
                $Persona->fechaNacimiento = $this->input->post('fechaNacimiento');
                $Persona->profesion = $this->input->post('profesion');
                $Persona->domicilio = $this->input->post('domicilio');
            $Persona->telefonoFijo = $this->input->post('telefonoFijo');
            $Persona->telefonoCelular = $this->input->post('telefonoCelular');
            $Persona->nacionalidad = $this->input->post('nacionalidad');
            $Persona->estadoCivil = $this->input->post('estadoCivil');
            ($Persona->save());
        

                $TipoSocio = new TipoSocio();
                $TipoSocio->get_by_id($this->input->post('tipoSocio'));
				
				$Cobrador = new Cobrador();
				$Cobrador->get_by_id($this->input->post('idCobrador'));

        
                $this->load->helper('date');
        
                $Socio = new Socio();
                //$Socio->fechaAlta = strftime("%Y-%m-%d", time());
                $Socio->fechaAlta = $this->input->post('fechaAlta');
                $Socio->domicilioCobro = $this->input->post('domicilioDeCobro');
                $Socio->intervaloPago = $this->input->post('intervaloDePago');
                $Socio->observaciones = $this->input->post('observaciones');
                $Socio->estado = EstadoSocio::Activo;
                $Socio->save();
                ($Socio->save(array($Persona,$TipoSocio,$Cobrador)));

			if ($this->input->post('idDeporte') > 0)
			{
				$Inscripcion = new Inscripcion();
				$Inscripcion->actividad_id = $this->input->post('idDeporte');
				$Inscripcion->socio_id = $Socio->id;
				$Inscripcion->fechaAlta = strftime("%Y-%m-%d", time());
				$Inscripcion->save();
			}

			if ($this->input->post('nombreEmbarcacion')<>'' && $this->input->post('matriculaEmbarcacion')<>'' && $this->input->post('tipoEmbarcacion')>0)
			{
				$embarcacion = new Embarcacion();
				$embarcacion->nombre = $this->input->post('nombreEmbarcacion');
				$embarcacion->matricula = $this->input->post('matriculaEmbarcacion');
				$embarcacion->socio_id = $Socio->id;
				$embarcacion->tipoembarcacion_id = $this->input->post('tipoEmbarcacion');

				$embarcacion->motorMarca = $this->input->post('marcaMotor');
				$embarcacion->motorNumero = $this->input->post('numeroMotor');
				$embarcacion->motorTipo = $this->input->post('tipoMotor');

				if ($this->input->post('amarre')=='')
				{
					$embarcacion->amarre = NULL;
				}
				else {$embarcacion->amarre = $this->input->post('amarre');}

				if ($this->input->post('fechaAdquisicion')=='')
				{
					$embarcacion->fechaAdquisicion = NULL;
				}
				else{$embarcacion->fechaAdquisicion = $this->input->post('fechaAdquisicion');}


				$embarcacion->anioConstruccion = $this->input->post('anioConstruccion');
				$embarcacion->arboladura = $this->input->post('arboladura');
				$embarcacion->eslora = $this->input->post('eslora');
				$embarcacion->manga = $this->input->post('manga');
				$embarcacion->puntal = $this->input->post('puntal');
				$embarcacion->tonelaje = $this->input->post('tonelaje');
				$embarcacion->HP = $this->input->post('hp');
				$embarcacion->observaciones = $this->input->post('observacionesBarco');



				$embarcacion->save();
			}

			if ($this->input->post('nombreAdherente')<>'')
			{
				$socioAd = new SocioAdherente();
				$socioAd->socio_id = $Socio->id;
				$socioAd->nombreCompleto = $this->input->post('nombreAdherente');
				$socioAd->dni = $this->input->post('dniAdherente');
				$socioAd->fechaAlta = strftime("%Y-%m-%d", time());
				$socioAd->parentezco = $this->input->post('paretntezcoAdherente');
				$socioAd->fechaNacimiento =  $this->input->post('nacimientoAdherente');
				$socioAd-> save();
			}

                echo 'El socio se ha agregado con éxito con el número de socio: '.$Socio->id;
        }
        else
        {

			//Si encontro la persona ses porque ya esta preinscripta o ya es cobrador o ya es socio, entonces se guardan los datos de socio nomas
        		$Socio = new Socio();
        		$cont=$Socio->where(array('persona_id'=>$Persona->id,'estado'=>0))->count();

				if($cont==0)
				{

					$Persona->nombre = $this->input->post('nombre');
                $Persona->apellidos = $this->input->post('apellidos');
                $Persona->genero = $this->input->post('genero');
                $Persona->correoElectronico = $this->input->post('correoElectronico');
                $Persona->fechaNacimiento = $this->input->post('fechaNacimiento');
                $Persona->profesion = $this->input->post('profesion');
                $Persona->domicilio = $this->input->post('domicilio');
            $Persona->telefonoFijo = $this->input->post('telefonoFijo');
            $Persona->telefonoCelular = $this->input->post('telefonoCelular');
            $Persona->nacionalidad = $this->input->post('nacionalidad');
            $Persona->estadoCivil = $this->input->post('estadoCivil');
			$Persona->observaciones = $this->input->post('observaciones');
            ($Persona->save());


        		$TipoSocio = new TipoSocio();
                $TipoSocio->get_by_id($this->input->post('tipoSocio'));
				
				$Cobrador = new Cobrador();
				$Cobrador->get_by_id($this->input->post('idCobrador'));

        
                $this->load->helper('date');
        
                
                $Socio->fechaAlta = strftime("%Y-%m-%d", time());
                $Socio->domicilioCobro = $this->input->post('domicilioDeCobro');
                $Socio->intervaloPago = $this->input->post('intervaloDePago');
                $Socio->estado = EstadoSocio::Activo;
                $Socio->save();
                ($Socio->save(array($Persona,$TipoSocio,$Cobrador)));
                echo 'El socio se ha agregado con éxito con el número de socio: '.$Socio->id;
                }
                else
                {
                	$Socio->where(array('persona_id'=>$Persona->id,'estado'=>0))->get();
                	echo 'La persona que desea agregar ya es socia con el siguiente nro de socio: '.$Socio->id;
                }
            }

	}


	public function obtenerSocio()
	{
		$socio_id = $this->uri->segment(3);
		$socio = new Socio();
		$socio->get_by_id($socio_id);
		$persona_socio = new Persona();
		$persona_socio->get_by_id($socio->persona_id);
		
		

		$data = array(
		'nombre'=> $persona_socio->nombre,
		'apellidos'=> $persona_socio->apellidos,
		'dni'=> $persona_socio->dni,
		'domicilio'=> $persona_socio->domicilio,
		'correoElectronico'=> $persona_socio->correoElectronico,
        'telefonoFijo'=> $persona_socio->telefonoFijo,
        'telefonoCelular'=> $persona_socio->telefonoCelular,
        'nacionalidad'=> $persona_socio->nacionalidad,
        'estadoCivil'=> $persona_socio->estadoCivil,
		'fechaNacimiento'=> $persona_socio->fechaNacimiento,
		'profesion'=> $persona_socio->profesion,
		'genero'=> $persona_socio->genero,
		'domicilioCobro'=> $socio->domicilioCobro,
		'intervaloPago'=> $socio->intervaloPago,
        'observaciones'=>$socio->observaciones,
		'tiposocio'=> $socio->tiposocio_id,
		'cobrador'=>$socio->cobrador_id,
        'fechaAlta'=>$socio->fechaAlta
		);
		echo json_encode($data);
	}

public function obtenerMaxIdSocio()
{
	$resultado = $this->db->query("select max(id) as 'id' from socios;");
	$maximo = $resultado->row_array();
	//$socio = new Socio();
	//$socio->select_max('id');
	//log_message('debug',"##### $maximo[id]");
	echo $maximo['id']+1;
}


public function obtenerSocios()

	{
		$socios = new Socio();
		$persona_socio = new Persona();
		$socios->get();
		$resultado = array();
   		$resultado['data'] = array();

		foreach ($socios as $socio)
		{
			$persona_socio->get_by_id($socio->persona_id);
			$socioCompleto = array();			

				$socioCompleto['id'] = sprintf('%04d',$socio->id);
   				$socioCompleto['nombreCompleto'] = $persona_socio->apellidos.' '.$persona_socio->nombre;
   				$socioCompleto['dni'] = $persona_socio->dni;
   				$socioCompleto['domicilio'] = $persona_socio->domicilio;
   				$socioCompleto['correoElectronico'] = $persona_socio->correoElectronico;
				$socioCompleto['observaciones'] = $socio->observaciones;
   				$estadoString="";
   				switch ($socio->estado) {
   					case EstadoSocio::Activo:
   						$estadoString="Activo";
   						break;
   					case EstadoSocio::DadoDeBaja:
   						$estadoString="Inactivo";
   						break;
   					default:
   						# code...
   						break;
   					}
   				
   				$socioCompleto['estado']= $estadoString;
                $tipoDeSocio = new TipoSocio();
                $tipoDeSocio->get_by_id($socio->tiposocio_id);
                $socioCompleto['tipoSocio']= $tipoDeSocio->descripcion;


   				array_push($resultado['data'], $socioCompleto);
		}
		
		echo json_encode($resultado);
	}


public function editar()
{
		$socio_id = $this->uri->segment(3);

        $Socio = new Socio();
        
        $Socio->get_by_id($socio_id);
        $personaid = $Socio->persona_id;
        $Socio->where('id', $socio_id)->update(array('domicilioCobro'=>$this->input->post('domicilioDeCobro')
        ,'observaciones'=>$this->input->post('observaciones'),
        	'intervaloPago'=>$this->input->post('intervaloDePago'),'tiposocio_id'=>$this->input->post('tipoSocio'), 'cobrador_id'=>$this->input->post('idCobrador')));
        


		$Persona = new Persona();
		$personasConEseDNI = $Persona->where('dni',$this->input->post('dni'))->count();


		if($personasConEseDNI>1)
		{
			echo "Fallo: El dni ya se encuentra registrado.";
		}
		else
		{
			$Persona->where('id',$personaid);
		$Persona->update(array('nombre'=>$this->input->post('nombre'), 'apellidos'=>$this->input->post('apellidos'),
			'dni'=>$this->input->post('dni'),'genero'=>$this->input->post('genero'),
			'correoElectronico'=>$this->input->post('correoElectronico'),'fechaNacimiento'=>$this->input->post('fechaNacimiento'),
			'profesion'=>$this->input->post('profesion'),'domicilio'=>$this->input->post('domicilio')
        ,'nacionalidad'=>$this->input->post('nacionalidad'),'estadoCivil'=>$this->input->post('estadoCivil')
        ,'telefonoFijo'=>$this->input->post('telefonoFijo'),'telefonoCelular'=>$this->input->post('telefonoCelular')));

        echo "Los datos del socio han sido editado con éxito";
		}
		

}
	public function DarDeAlta()
	{
		$socio_id = $this->uri->segment(3);
		$Socio = new Socio();
		$Socio->where('id', $socio_id)->update('estado', EstadoSocio::Activo);

		$Socio->where('id', $socio_id)->update('fechaBaja',NULL);
	}

public function DarDeBaja()
{
$socio_id = $this->uri->segment(3);	
$Socio = new Socio();
$Socio->where('id', $socio_id)->update('estado', EstadoSocio::DadoDeBaja);

$Socio->where('id', $socio_id)->update('fechaBaja',strftime("%Y-%m-%d", time()));
}




public function InformeDetallado()
{
	if (!$this->session->userdata('username')) 
		{
			redirect('CLogin');
		}
		else
		{
				$Usuario = new usuario_model();
				$Usuario->where('username',$this->session->userdata('username'));
				$Usuario->get();
			if ($Usuario->tipoUsuario == 0)
			{
	

		$socio_id = $this->uri->segment(3);
		$socio = new Socio();
		$socio->get_by_id($socio_id);

		$persona_socio = new Persona();
		$persona_socio->get_by_id($socio->persona_id);

		$tipoSocio = new TipoSocio();
		$tipoSocio->get_by_id($socio->tiposocio_id);
		
		$cobrador = new Cobrador();
		$cobrador->get_by_id($socio->cobrador_id);
		$cobrador_Persona = new Persona();
		$cobrador_Persona -> get_by_id($cobrador->persona_id);

		$montoTotal = 0;
		

		$datosSocio['nroSocio'] = $socio->id;
		$datosSocio['nombreCompleto'] = $persona_socio->nombre." ".$persona_socio->apellidos;
		$datosSocio['domicilioReal'] = $persona_socio->domicilio;
		$datosSocio['domicilioCobro'] = $socio->domicilioCobro;
		$datosSocio['dni'] = $persona_socio->dni;
		$datosSocio['fechaNacimiento'] = $persona_socio->fechaNacimiento;
		$datosSocio['telefonoFijo'] = $persona_socio->telefonoFijo;
        $datosSocio['telefonoCelular'] = $persona_socio->telefonoCelular;
		$datosSocio['observaciones'] = $persona_socio->observaciones;
		$datosSocio['tiposocio'] = $tipoSocio->descripcion;
		$datosSocio['monto_tiposocio'] = $tipoSocio->monto;
		$montoTotal = $montoTotal + $tipoSocio->monto;
		$datosSocio['correoElectronico'] = $persona_socio->correoElectronico;
		$datosSocio['cobrador'] = $cobrador_Persona->nombre." ".$cobrador_Persona->apellidos;
		$datosSocio['intervaloPago'] = $socio->intervaloPago;
		$datosSocio['fechaAlta'] = $socio->fechaAlta;
		$datosSocio['fechaBaja'] = $socio->fechaBaja;

		//----PARTE DE DEUDAS y CUOTAS -----//
		$deudas = new Deuda();
		$cuotasDebe = array();
		$deudas->get_where(array('socio_id =' => $socio_id, 'cobro_id is null'=>null));
				$debe =0;
				$mes=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
				foreach($deudas as $deuda)
				{
                    $deudaArray = array("mesDeuda" => $mes[$deuda->mesAdeudado].'-'.$deuda->anioAdeudado,"monto" => $deuda->monto);
                    $debe += $deuda->monto;
					array_push($cuotasDebe,$deudaArray);
				}

		$cuotasPaga = array();
		$deudasPagadas = new Deuda();
		$deudasPagadas->get_where(array('socio_id =' => $socio_id, 'cobro_id >'=>0),10,0);
                $montoPagado=0;
				foreach($deudasPagadas as $deuda)
				{
					$cobro = new Cobro();
					$cobro->get_by_id($deuda->cobro_id);

					$fechaCobro = new DateTime($cobro->fechaCobro);

                    $deudaArray = array("mesDeuda" => $mes[$deuda->mesAdeudado].'-'.$deuda->anioAdeudado,"monto" => $deuda->monto,"fechaCobro" => $fechaCobro->format('d-m-Y'));
                    $montoPagado+=$deuda->monto;
                    array_push($cuotasPaga, $deudaArray);
				}
				$cuotasPaga= array_reverse($cuotasPaga);

		$datosSocio['deuda'] = $debe;
        $datosSocio['montoPagado'] = $montoPagado;
		$datosSocio['cuotasDeuda'] = $cuotasDebe;
		$datosSocio['cuotasPagas'] = $cuotasPaga;


				//--------PARTE DE EMBARCACIONES----------//
		 $embarcaciones = array();
         $embarcacion = new Embarcacion();
         $embarcacion->where('socio_id', $socio->id);
	     $embarcacion->get();
	     foreach ($embarcacion as $barco) 
           	{
           	//Busco el tipo de embarcacion
	        $tipoEmbarcacion = new TipoEmbarcaciones();
	        $tipoEmbarcacion->where('id',$barco->tipoembarcacion_id);
	        $tipoEmbarcacion->get();
			
			array_push($embarcaciones,$tipoEmbarcacion->descripcion." ($".$tipoEmbarcacion->monto.")");
			$montoTotal = $montoTotal + $tipoEmbarcacion->monto;
	        }


	     $datosSocio['embarcaciones'] = $embarcaciones; 


	     //--------PARTE DE ACTIVIDADES SOCIO ---------//
         $inscripcion = new Inscripcion();
         $inscripcion->where(array('socio_id' => $socio->id,'fechaBaja is NULL'=>null));
         $inscripcion->get();
         
         $deportes = array();
         foreach ($inscripcion as $inscrip) 
                {
	                //obtengo el deporte de la inscripcion
	                $deporte = new Deporte();
	                $deporte->get_by_id($inscrip->actividad_id);

	                array_push($deportes,$deporte->nombre." (".$deporte->monto.")");
					$montoTotal = $montoTotal + $deporte->monto;
				}

		$datosSocio['deportes'] = $deportes; 				

		$datosSocio['montoTotal'] = $montoTotal;


		$data = $datosSocio;
		$data['seccion'] = 'SOCIOS';
		$this->load->view('header',$data);
		$this->load->view('informedetallado',$data);


			}
			else {redirect('CPermiso');}
		}
}




}
