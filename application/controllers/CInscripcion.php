<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class CInscripcion extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */

	public function index()

	{
		if (!$this->session->userdata('username')) 
		{
			redirect('CLogin');
		}
		else
		{
				$Usuario = new usuario_model();
				$Usuario->where('username',$this->session->userdata('username'));
				$Usuario->get();
			if ($Usuario->tipoUsuario == 0)
			{
				$data['seccion'] = 'INSCRIPCIONES';
				$this->load->view('header',$data);
				$this->load->view('menu');
				$this->load->view('inscripciones');

			}
			else {redirect('CPermiso');}
		}
		
		
		
	}


	public function agregarInscripcion()
	{
		//Obtengo el deporte
		$Deporte = new Deporte();
		$Deporte_id = $this->input->post('deporte');

		
		//Obtengo el socio
		$Socio = new Socio();
		$Socio_id = $this->input->post('socio');


        if($Deporte_id==null && $Socio_id==null)
        {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $json = $this->input->post();
            $Deporte_id=$json['deporte'];
            $Socio_id=$json['socio'];
        }

        $Socio->get_by_id($Socio_id);
        $Deporte->get_by_id($Deporte_id);
	
		//Busco todos las inscripciones que estan asociadas con ese deporte.
		$Inscripcion = new Inscripcion();
		$Inscripcion->where('actividad_id =',$Deporte_id);
		//En las inscripciones busco las que estan asociadas con ese socio.
		$Inscripcion->where('socio_id =',$Socio->id);
		$Inscripcion ->get();
		$estaDadoDeBaja=true;
		foreach ($Inscripcion as $Insc)
		{
			if ($Insc->fechaBaja==null)
			{
				$estaDadoDeBaja=false;
				}
		}
				
		if ($Inscripcion->count()==0 || $estaDadoDeBaja== true)
		{
		$Inscripcion = new Inscripcion();
		$Inscripcion->actividad_id = $Deporte->id;
        $Inscripcion->socio_id = $Socio->id;
        
		$Inscripcion->fechaAlta = strftime("%Y-%m-%d", time());
		$Inscripcion->save();
		$this->load->helper('date');
		echo "El servicio se ha agregado con éxito";
			}
        else{
            echo 'El socio ya se encuentra registrado a este servicio';
        }
	}

public function DarDeBajaUnaInscripcion()
{
$Inscripcion_id = $this->uri->segment(3);	
$Inscripcion = new Inscripcion();
$Inscripcion->where('id', $Inscripcion_id)->update('fechaBaja', strftime("%Y-%m-%d", time()));
}

public function ObtenerInscripciones()
	{

        $_POST = json_decode(file_get_contents('php://input'), true);
        $json = $this->input->post();
        $Socio_id=$json['socio'];


		$Inscripciones = new Inscripcion();
		if($Socio_id!=null)
        {
            $Inscripciones->where(array('socio_id'=>$Socio_id,'fechaBaja IS NULL'=>null))->get();
        }
        else
        {
            $Inscripciones->where(array('fechaBaja IS NULL' => null))->get();
        }

		
		$Deporte = new Deporte();
		$Socio = new Socio();
		$Persona = new Persona();
		
		$resultado = array();
        $resultado['data'] = array();



        foreach($Inscripciones as $Inscripcion)
        {
            $inscripcionResultado = array();
			
			$obSocio = $Socio->get_by_id($Inscripcion->socio_id);
			$obPersona = $Persona->get_by_id($obSocio->persona_id);
			$obActividad = $Deporte->get_by_id($Inscripcion->actividad_id);

            $inscripcionResultado['id'] = $Inscripcion->id;
			$inscripcionResultado['socio_id'] = $Inscripcion->socio_id;
			$inscripcionResultado['nombreSocio'] = $obPersona->nombre;
			$inscripcionResultado['apellidoSocio'] = $obPersona->apellidos;
			$inscripcionResultado['dniSocio'] = $obPersona->dni;
			$inscripcionResultado['nombreActividad'] = $obActividad->nombre;
			$inscripcionResultado['fechaAlta'] = $Inscripcion->fechaAlta;
			$inscripcionResultado['fechaBaja'] = $Inscripcion->fechaBaja;
            $inscripcionResultado['monto'] = $obActividad->monto;
             
            array_push($resultado['data'], $inscripcionResultado);

        }
        echo json_encode($resultado);
	}

}
