<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CCobradorVista extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if (!$this->session->userdata('username')) 
		{
			redirect('CLogin');
		}
		else
		{
			//Reconozco qué cobrador es:
			//1. Busco el usuario loggeado
			$user = new usuario_model();
			$user-> where('username', $this->session->userdata('username'));
			$user->get();
			//2. Busco la persona a la que pertenece el usuario, esto lo hago por si quiero mostrar los datos del Cobrador como nombre y apellido a la hora de que le muestre la pantalla principal; sino se puede saltear al paso 3.
			$persona = new Persona();
			$persona->where('id',$user->persona_id);
			$persona->get();
			//3. Busco el cobrador que es esa persona
			$cobradorPersona = new Cobrador();
			$cobradorPersona->where('persona_id',$persona->id);
			$cobradorPersona->get();

		$data['seccion'] = 'Cobrador';
		$data['nombre']= $persona->nombre;
		$data['apellido'] = $persona->apellidos;
		$data['idCobrador'] = $cobradorPersona->id;
		$this->load->view('header',$data);
		//$this->load->view('menu');
		$this->load->view('cobradorVista',$data);
		}

	}

public function Pagar()
    {
		$this->load->helper('date');
	    $deudas = new Deuda();
	    $deudas->get();
		$monto=0;
	  
	    $cobro = new Cobro();
	  
	  
     foreach($deudas as $deuda)
	 {
		 foreach (($this->input->post('deudasId')) as $listaIdDeudas)
		 {
			 if ($deuda->id == $listaIdDeudas)
			 {
				   
				$monto = $monto + $deuda->monto;
				
								
			 }
			 
		 }		 
			 
	 }
	 if($monto <> 0){


		    $cobro->fechaCobro = strftime("%Y-%m-%d", time());
			$cobro->importe = $monto;
			$cobro->cobrador_id = $this->input->post('Cobrador');;
	 		$cobro->save();
	 }
        
		 foreach($deudas as $deuda)
	 {
		 foreach (($this->input->post('deudasId')) as $listaIdDeudas)
		 {
			 if ($deuda->id == $listaIdDeudas )
			 {
				 $deuda->cobro_id = $cobro->id;
				 $deuda->save();	
			 }
			 
		 }		 
			 
	 }
		
        echo 'success';
	
        }
 


public function ObtenerDeudasSocios()
    {
		{
		$buscado = $this->input->post('valorBusqueda');
		$idCobradorLogin=$this->input->post('idCobrador');
		$Mes = $this->input->post('Mes');
		$Anio = $this->input->post('Anio');
		//($this->input->post('Cobrador'))
		$Personas = new Persona();
		$Personas->get();
		$Socios = new Socio();
		//$Cobrador = new Cobrador();
			//	$Cobrador->get_by_id($this->input->post('Cobrador'));
			
		
		//$Socios->where('cobrador_id',$cobradorID);
		$Socios->where('cobrador_id',$idCobradorLogin);
		$Socios->get();
		
        $DeudasSocio = new Deuda();
		$DeudasSocio->where('cobro_id is null');
        $DeudasSocio->get();
	
        $resultado = array();
        $resultado['deudas'] = array();

        foreach($Socios as $Socio)
        { 
		 foreach($DeudasSocio as $DeudaSocio){
			 foreach ($Personas as $Persona){
		 
			if(($DeudaSocio->cobro_id == "") && ($DeudaSocio->socio_id == $Socio->id) && ($Socio->persona_id == $Persona->id) && ($buscado=="" || stristr(''.$Persona->nombre.' '.$Persona->apellidos,$buscado)!==false
			|| stristr(''.$Persona->apellidos.' '.$Persona->nombre,$buscado)!==false || $buscado == $Socio->id) && ($Mes==0 || $Mes == $DeudaSocio->mesAdeudado) && ($Anio==0 || $Anio == $DeudaSocio->anioAdeudado))
			{
            $DeudasSocioResultado = array();

			$DeudasSocioResultado['id'] = $DeudaSocio->socio_id;
			$DeudasSocioResultado['apellidos'] = $Persona->apellidos;
			$DeudasSocioResultado['nombre'] = $Persona->nombre;
            $DeudasSocioResultado['mesAdeudado'] = $DeudaSocio->mesAdeudado;
            $DeudasSocioResultado['anioAdeudado'] = $DeudaSocio->anioAdeudado;
            $DeudasSocioResultado['monto'] = $DeudaSocio->monto;
           $DeudasSocioResultado['idDeuda'] = $DeudaSocio->id;
            array_push($resultado['deudas'], $DeudasSocioResultado);
			}
			}
			}

        }
        echo json_encode($resultado);
    }
  }



public function obtenerCobros()

	{
		
		$valorCobroId=0;
		$cobro = new Cobro();
		$socio = new socio();
		$persona = new persona();
		
		$Deudas = new Deuda();
		
		
		$Deudas ->order_by("cobro_id","desc");
		$Deudas ->where('cobro_id <>',"");
		$Deudas ->get();
		
		$resultadoCobro = array();
   		$resultadoCobro['cobros'] = array();
		
		foreach($Deudas as $Deuda){
		if($valorCobroId <> $Deuda->cobro_id){
    $valorCobroId= $Deuda->cobro_id; 
	
	$socio ->get_by_id($Deuda->socio_id);
	$persona ->get_by_id($socio->persona_id);
	$cobro ->get_by_id($Deuda->cobro_id);
	$cobroCompleto = array();	
	
                $cobroCompleto['nombre'] = $persona->nombre;
   				$cobroCompleto['apellido'] = $persona->apellidos;
   				$cobroCompleto['fechaCobro'] = $cobro->fechaCobro;	
   				$cobroCompleto['monto'] = $cobro->importe;
				array_push($resultadoCobro['cobros'], $cobroCompleto);
				
			;
		}
		}
		echo json_encode($resultadoCobro);
}



public function CobradoresSeleccion()
	{
		$cobradores = new Cobrador();
		$cobradores->where('estadoCobrador <>', EstadoCobrador::DadoDeBaja);
		$cobradores->get();

		$resultado = '<select name="cobrador" id="campoCobrador">';
		foreach ($cobradores as $cobrador)
        {
			$persona_cobrador = new Persona();
			$persona_cobrador ->get_by_id($cobrador->persona_id);
			$resultado = $resultado . '<option value='.$cobrador->id.'>'.$persona_cobrador->nombre.' '.$persona_cobrador->apellidos.' </option>';
			
		}
		
		echo $resultado . '</select>';
	}


public function MesSeleccion()
	{
	$resultado1 = '<select name="Mes" id="campoMes">';
	        $resultado1 = $resultado1 . '<option value="" selected> Todos </option>';
			$resultado1 = $resultado1 . '<option value=1> Enero </option>';
			$resultado1 = $resultado1. '<option value=2> Febrero </option>';
			$resultado1 = $resultado1 . '<option value=3> Marzo </option>';
			$resultado1 = $resultado1 . '<option value=4> Abril </option>';
			$resultado1 = $resultado1 . '<option value=5> Mayo </option>';
			$resultado1 = $resultado1 . '<option value=6> Junio </option>';
			$resultado1 = $resultado1 . '<option value=7> Julio </option>';
			$resultado1 = $resultado1 . '<option value=8> Agosto </option>';
			$resultado1 = $resultado1 . '<option value=9> Septiembre </option>';
			$resultado1 = $resultado1 . '<option value=10> Octubre </option>';
			$resultado1 = $resultado1 . '<option value=11> Noviembre </option>';
			$resultado1 = $resultado1 . '<option value=12> Diciembre </option>';
			
	echo $resultado1 . '</select>';
	}
	
public function AnioSeleccion()
	{
		
		
		
		$Deudas = new Deuda();
		$Deudas ->order_by("anioAdeudado","desc");
		$Deudas ->get();
	
		$valorAño=0;
		
		
	$resultado2 = '<select name="Año" id="campoAño">';
	$añoActual = date("Y");
	$resultado2 = $resultado2 . '<option value="" selected> Todos </option>';
	foreach($Deudas as $Deuda){
		if($valorAño <> $Deuda->anioAdeudado){
    $valorAño= $Deuda->anioAdeudado; 

			$resultado2 = $resultado2 . '<option value='.$valorAño.'> '.$valorAño.' </option>';
		}
		}	
	echo $resultado2 . '</select>';
	}	






	
	
}
