<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CTipoSocio extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

		if (!$this->session->userdata('username')) 
		{
			redirect('CLogin');
		}
		else
		{
				$Usuario = new usuario_model();
				$Usuario->where('username',$this->session->userdata('username'));
				$Usuario->get();
			if ($Usuario->tipoUsuario == 0)
			{
			$data['seccion'] = 'TIPOS DE SOCIOS';
			$this->load->view('header',$data);
	        $this->load->view('menu');
	        $this->load->view('tiposocios');
			}
			else {redirect('CPermiso');}
		}
		
	}



    public function agregar()
    {
        $tipoSocio = new TipoSocio();
        $tipoSocio->descripcion = $this->input->post('descripcion');
        $tipoSocio->monto = $this->input->post('monto');
        $tipoSocio->save();

        echo 'success';
    }


    public function obtenerTipoSocios()
    {
        $tipoSocios = new TipoSocio();
        $tipoSocios->get();

        $resultado = array();
        $resultado['tipoSocio'] = array();

        foreach($tipoSocios as $tipoSocio)
        {
            $tipoSocio = array(
            'id'=> $tipoSocio->id,
            'descripcion'=> $tipoSocio->descripcion,
            'monto'=> $tipoSocio->monto,
        );
        array_push($resultado['tipoSocio'], $tipoSocio);
        }
        echo json_encode($resultado);
    }




	public function TiposSociosEnSelect()
	{
		$tipoSocios = new TipoSocio();
		$tipoSocios->get();

		$resultado = '<select name="tipoSocio" id="campoTipoSocio">';
		foreach ($tipoSocios as $tipoSocio)
        {
			$resultado = $resultado . '<option value="'.$tipoSocio->id.'">'.$tipoSocio->descripcion.'</option>';
		}
		
		echo $resultado . '</select>';
	}



	public function obtenerTipoSocio()

	{
		$id = $this->uri->segment(3); 
		$tipoSocio = new TipoSocio();
		
		//Obtengo el tipo socio
		$tipoSocio->get_by_id($id);
		
		$data = array(
		'id' => $tipoSocio->id,
		'descripcion'=> $tipoSocio->descripcion,
		'monto'=> $tipoSocio->monto,
		);
		echo json_encode($data);
	}


public function Editar()
{
		$id = $this->uri->segment(3); 
		$tipoSocio = new TipoSocio();
		//Obtengo el tipoSocio
		$tipoSocio->get_by_id($id);

		
        $tipoSocio->where('id', $id)->update(array('descripcion'=>$this->input->post('descripcion'),
        	'monto'=>$this->input->post('monto')));
}



}