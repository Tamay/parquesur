<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CSocioLogin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['seccion'] = 'INICIO';
		$this->load->view('header',$data);
		$this->load->view('sociosusuarios');
	}

	
	public function validar($dni)
	{
		$persona = new Persona();
		$persona->where('dni',$dni);
		$persona->get();

		$socio = new Socio();
		$socio->where('persona_id', $persona->id);
		$socio->get();
		$mensaje ='';


		if($socio->id <> '')
		{
		
			//$data['seccion'] = 'Estado de cuotas';
			$data['nombre']= $persona->nombre;
			$data['apellidos']= $persona->apellidos;
			$data['idPersona']= $persona->id;
			$data['idSocio']= $socio->id;
		    $this->load->view('header',$data);
		    $this->load->view('estadocuotas',$data);


		}

		else
			{
				$data['seccion'] = 'Ups!';
				$this->load->view('header',$data);
				$this->load->view('dniIncorrecto');
			}
		
      
	}

	public function actualizarEstados()
	{
		$idSocio = $this->input->post('idSocio');

		$deudas = new Deuda();
		$deudas->where('socio_id',$idSocio);
		$deudas->get();

		$resultado = array();
   		$resultado['estados'] = array();
   		$meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");



   		foreach ($deudas as $deuda) 
   		{
   			$deudaCompleta = array();
   			$deudaCompleta['mes'] = $meses[$deuda->mesAdeudado];
   			$deudaCompleta['anio'] = $deuda->anioAdeudado;
   			$deudaCompleta['monto'] = $deuda->monto;
   			
   			if($deuda->cobro_id ==0)
   			{
	   			$deudaCompleta['estado']='No pagada/cobrada';
	   		}
	   		else
	   		{
	   			if($deuda->cobro_id<>0)
	   			{
	   				$deudaCompleta['estado']='Pagada/Cobrada';
	   			}
	   		}

			array_push($resultado['estados'], $deudaCompleta);


   		}

		echo json_encode($resultado);


	}


}