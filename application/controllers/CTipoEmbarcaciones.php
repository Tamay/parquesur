<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CTipoEmbarcaciones extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if (!$this->session->userdata('username')) 
		{
			redirect('CLogin');
		}
		else
		{
				$Usuario = new usuario_model();
				$Usuario->where('username',$this->session->userdata('username'));
				$Usuario->get();
			if ($Usuario->tipoUsuario == 0)
			{
				$data['seccion'] = 'TIPOS DE EMBARCACIONES';
				$this->load->view('header',$data);
		        $this->load->view('menu');
		        $this->load->view('tipoembarcaciones');
			}
			else {redirect('CPermiso');}
		}

	}

	public function TiposEmbarcacionesEnSelect()
	{
		$tipoEmbarcaciones = new TipoEmbarcaciones();
		$tipoEmbarcaciones->get();

		$resultado = '<select name="tipoEmbarcaciones" id="campoTipoEmbarcaciones">';
		foreach ($tipoEmbarcaciones as $tipoEmbarcaciones)
        {
			$resultado = $resultado . '<option value="'.$tipoEmbarcaciones->id.'">'.$tipoEmbarcaciones->descripcion.'</option>';
		}
		
		echo $resultado . '</select>';
	}

	public function TiposEmbarcacionesEnSelect2()
	{
		$tipoEmbarcaciones = new TipoEmbarcaciones();
		$tipoEmbarcaciones->get();

		$resultado = '<select name="tipoEmbarcaciones" id="campoTipoEmbarcaciones">';
		foreach ($tipoEmbarcaciones as $tipoEmbarcaciones)
		{
			$resultado = $resultado . '<option value="'.$tipoEmbarcaciones->id.'">'.$tipoEmbarcaciones->descripcion.'</option>';
		}
		$resultado = $resultado. '<option value =0 selected>NINGUNA</option>';

		echo $resultado . '</select>';
	}

	
	
	 public function agregar()
    {
        $tipoEmbarcaciones = new TipoEmbarcaciones();
        $tipoEmbarcaciones->descripcion = $this->input->post('descripcion');
        $tipoEmbarcaciones->monto = $this->input->post('monto');
        $tipoEmbarcaciones->save();
        echo 'success';
    }
	
	
	public function obtenerTipoEmbarcaciones()
    {
        $tipoEmbarcaciones = new TipoEmbarcaciones();
        $tipoEmbarcaciones->get();

        $resultado = array();
        $resultado['tipoEmbarcaciones'] = array();

        foreach($tipoEmbarcaciones as $tipoEmbarcacion)
        {
            $data = array(
            'id'=> $tipoEmbarcacion->id,
            'descripcion'=> $tipoEmbarcacion->descripcion,
            'monto'=> $tipoEmbarcacion->monto,
        );
        array_push($resultado['tipoEmbarcaciones'], $data);
        }
        echo json_encode($resultado);
    }
	
	
	public function obtenerTipoEmbarcacion()

	{
		$id = $this->uri->segment(3); 
		$tipoEmbarcacion = new TipoEmbarcaciones();
		
		//Obtengo el tipo embarcacion
		$tipoEmbarcacion->get_by_id($id);
		
		$data = array(
		'id' => $tipoEmbarcacion->id,
		'descripcion'=> $tipoEmbarcacion->descripcion,
		'monto'=> $tipoEmbarcacion->monto,
		);
		echo json_encode($data);
	}
	
	
	public function Editar()
{
		$id = $this->uri->segment(3); 
		$tipoEmbarcacion = new TipoEmbarcaciones();
		//Obtengo el tipo embarcaciones
		$tipoEmbarcacion->get_by_id($id);		
        $tipoEmbarcacion->where('id', $id)->update(array('descripcion'=>$this->input->post('descripcion'),
        	'monto'=>$this->input->post('monto')));
}
	
}
