<body>
<!-- HEADER -->
 <header>
  <div class="jumbotron">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <h1 class="text-center" style="color: #000066">SGS</h1>
          <p class="text-center" style="color: #000066">El Sistema de Gestión de Socios surgió como proyecto de la materia Habilitación Profesional de la UTN-FRCU, durante el 2016. Tiene como objetivo solucionar la vida administrativa de un club deportivo y social que día a día expande sus horizontes.</p>
          <p>&nbsp;</p>
        </div>
      </div>
    </div>
  </div>
 </header>
  <!-- / HEADER --> 

  <!--  SECTION-1 -->
  <section>
  <div class="row">
    <div class="col-lg-12 page-header text-center">
      <h2>Equipo de trabajo</h2>
    </div>
  </div>
  <div class="container ">
    <div class="row">
      <div class="col-lg-6 col-sm-12 text-center"> <img class="img-circle" alt="140x140" style="width: 140px; height: 140px;" src="/parquesur/images/fernando.jpg" data-holder-rendered="true">
        <h3>Fernando Escalante</h3>
        <p>Contacto: <a>ferchuescalante@gmail.com</a></p>
      </div>
      <div class="col-lg-6 col-sm-12 text-center"><img class="img-circle" alt="140x140" style="width: 140px; height: 140px;" src="/parquesur/images/facundo.jpg" data-holder-rendered="true">
        <h3>Facundo Coto</h3>
        <p>Contacto: <a>facundocoto@hotmail.com</a></p>

      </div>
      <div class="col-lg-6 col-sm-12 text-center"><img class="img-circle" alt="140x140" style="width: 140px; height: 140px;" src="/parquesur/images/estefani.jpg" data-holder-rendered="true">
        <h3>Estefani Bouvier</h3>
        <p>Contacto: <a>bouvierestefani@gmail.com</a></p>
      </div>
      <div class="col-lg-6 col-sm-12 text-center"><img class="img-circle" alt="140x140" style="width: 140px; height: 140px;" src="/parquesur/images/gabriel.jpg" data-holder-rendered="true">
        <h3>Gabriel Tamay</h3>
        <p>Contacto: <a>jgabriel.tamay@gmail.com</a></p>
      </div>
</div>
    <div class="row">
      <div class="col-lg-12 page-header text-center"> </div>
    </div>
</div>
<!-- /container -->
  
  
  <!-- / CONTAINER--> 
</section>
<div class="well"> 

<!-- FOOTER -->
<div class="container">
  <div class="row">
  </div>
</div>
<footer class="text-center">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <p style="color:#000000 ">Copyright © 2016. All rights reserved.</p>
      </div>
    </div>
  </div>

</footer>
</div>
<!-- / FOOTER --> 
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) 
<script src="../../js/jquery-1.11.3.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="../../js/bootstrap.js"></script>
</body>