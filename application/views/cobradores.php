<!------------------ JAVASCRIPT -------------------->

<script>

var editar;

function seVaAEditar(vof)
{
  editar=vof;
}

//Funcion para agregar un cobradr
function agregarCobrador(){
        var parametros = {
			"nombre" : ($('#campoNombre').val()).toUpperCase(),
                "apellidos" : ($('#campoApellidos').val()).toUpperCase(),
                "dni" : $('#campoDNI').val(),
                "domicilio" : ($('#campoDomicilio').val()).toUpperCase(),
            "genero" : $('#campoGenero').val(),
        "correoElectronico" : $('#campoCorreoElectronico').val(),
        "telefonoFijo" : $('#campoTelefonoFijo').val(),
        "telefonoCelular" : $('#campoTelefonoCelular').val(),
        "nacionalidad" : ($('#campoNacionalidad').val()).toUpperCase(),
        "estadoCivil" : ($('#campoEstadoCivil').val()).toUpperCase(),
        "fechaNacimiento" : $('#campoFechaNacimiento').val(),
        "profesion" : ($('#campoProfesion').val()).toUpperCase(),
		    "comision": $('#campoComision').val()
        };
           
        $.ajax({
                data:  parametros,
                url:   '<?= site_url("/CCobrador/AgregarCobrador") ?>',
                type:  'post',
                success:  function (response) {
					    //La persona no existia
                       	switch (response)
						{
							case 'exito':
											window.alert("Cobrador creado con éxito");
											actualizarLista();
											vaciarCampos();  
											break;
							case 'yaescobrador':
     										window.alert("La persona ya está registrada como cobrador en el sistema");
											vaciarCampos();
											break;
							case 'noeracobradorsisocio':
											window.alert("La persona ya estaba registrada como persona y se le asignó el cargo de cobrador");
											actualizarLista();
											vaciarCampos(); 
											break;

						}
					   
						  	
                        
                }
        });
}


function editarCobrador()
{
  var i=1;
  while($("#radio_"+i).is(":checked")==false)
  {
    i++;
  }
  var valorRadio = $("#radio_"+i).val();


  var parametros = {
        "nombre" : ($('#campoNombre').val()).toUpperCase(),
        "apellidos" : ($('#campoApellidos').val()).toUpperCase(),
        "dni" : $('#campoDNI').val(),
        "domicilio" : ($('#campoDomicilio').val()).toUpperCase(),
        "correoElectronico" : $('#campoCorreoElectronico').val(),
        "fechaNacimiento" : $('#campoFechaNacimiento').val(),
        "profesion" : ($('#campoProfesion').val()).toUpperCase(),
        "genero" : $('#campoGenero').val(),
		"telefonoFijo" : $('#campoTelefonoFijo').val(),
        "telefonoCelular" : $('#campoTelefonoCelular').val(),
        "nacionalidad" : ($('#campoNacionalidad').val()).toUpperCase(),
        "estadoCivil" : ($('#campoEstadoCivil').val()).toUpperCase(),
		"comision": $('#campoComision').val()
        };
        $.ajax({
                data:  parametros,
                url:   '<?= site_url("/CCobrador/EditarCobrador") ?>/'+$('#radio_'+i).val(),
                type:  'post',
                beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                },
                success:  function (response) {
                        $("#resultado").html(response);
            actualizarLista();
            vaciarCampos();
                }
        });
}

function comprobarCampos()
{
  if($('#campoNombre').val()=='' || $('#campoApellidos').val()=='' || $('#campoComision').val() < 0 ||
    $('#campoDNI').val()==0 || $('#campoDomicilio').val()=='' || $('#campoNacionalidad').val()=='' || $('#campoTelefonoFijo').val()=='' || $('#campoTelefonoCelular').val()=='' || $('#campoEstadoCivil').val()=='' ||
    $('#campoCorreoElectronico').val()=='' || $('#campoFechaNacimiento').val()=='' ||
    $('#campoDomicilio').val()=='')
  {
    $('#botonAceptar').prop('disabled', true);
    $("#advertenciaDatos").text("Faltan datos obligatorios");
  }
  else
  {
   $('#botonAceptar').prop('disabled', false); 
   $("#advertenciaDatos").text("");
  }   
}

function vaciarCampos()
{
        $('#campoNombre').val("");
        $('#campoApellidos').val("");
        $('#campoDNI').val("");
        $('#campoDomicilio').val("");
        $('#campoCorreoElectronico').val("");
        $('#campoFechaNacimiento').val("");
        $('#campoProfesion').val("");
        $('#campoGenero').val("");
		$('#campoEstadoCivil').val("");
		$('#campoTelefonoFijo').val("");
		$('#campoTelefonoCelular').val("");
		$('#campoNacionalidad').val("");
		$('#campoComision').val("");
        comprobarCampos();
}



function obtenerCobrador()
{
  var i=0;
  var table = document.getElementById('bodytabladecobradores');
var rowCount = table.rows.length;
  while($("#radio_"+i).is(":checked")==false && i<=rowCount)
  {
    i++;
  }

/// Si no hubo ninguno seleccionado
  if(i>rowCount)
    {
    window.alert("Debe seleccionar un cobrador");
    }
    
    else{
      $("#ventanaAgregar").modal("show");
      var valorRadio = $("#radio_"+i).val();

  $.ajax({
    url:'<?= site_url("/CCobrador/ObtenerCobrador") ?>/'+ valorRadio,
    type:  'get',
    dataType: 'json',
    success: function(res) {
        $('#campoNombre').val(res.nombre);
        $('#campoApellidos').val(res.apellidos);
        $('#campoDNI').val(res.dni);
        $('#campoDomicilio').val(res.domicilio);
        $('#campoCorreoElectronico').val(res.correoElectronico);
        $('#campoFechaNacimiento').val(res.fechaNacimiento);
        $('#campoProfesion').val(res.profesion);
        $('#campoGenero').val(res.genero);
		$('#campoEstadoCivil').val(res.estadoCivil);
		$('#campoNacionalidad').val(res.nacionalidad);
		$('#campoTelefonoFijo').val(res.telefonoFijo);
		$('#campoTelefonoCelular').val(res.telefonoCelular);
  $('#campoComision').val(res.comision);
        comprobarCampos();
  }});
}
}

function darDeBaja()
{
  var i=0;
  var table = document.getElementById('bodytabladecobradores');
var rowCount = table.rows.length;
  while($("#radio_"+i).is(":checked")==false && i<=rowCount)
  {
    i++;
  }

/// Si no hubo ninguno seleccionado
  if(i>rowCount)
    {
    window.alert("Debe seleccionar un cobrador");
    }
    
    else{
      var valorRadio = $("#radio_"+i).val();



var respuesta = confirm("¿Está seguro que desea dar de baja este cobrador?");
if(respuesta)
{
   $.ajax({
    url:'<?= site_url("/CCobrador/DarDeBajaCobrador") ?>/'+ valorRadio,
    type:  'POST',
    success: function() 
    {
      window.alert("El cobrador ha sido dado de baja con éxito.")
      actualizarLista();
    }
});
}
}
}


function insertarElementoEnTabla(nroCobrador,apellidos,nombre,dni,domicilio,correoElectronico,estado)
{
var table = document.getElementById('bodytabladecobradores');
var rowCount = table.rows.length;
var row = table.insertRow(rowCount);

var celdaNro = row.insertCell(0);
var celdaApellidos = row.insertCell(1);
var celdaNombre = row.insertCell(2);
var celdaDNI = row.insertCell(3);
var celdaDomicilio = row.insertCell(4);
var celdaCorreoElecronico = row.insertCell(5);
var celdaSeleccion = row.insertCell(6);

//Dependiendo el estado, 0=Activo, 1=DadoDeBaja
var estadostring;
switch(parseInt(estado)) {
    case 0:
        estadostring="Activo";
        break;
    case 1:
        estadostring="Dado de baja";
        row.bgColor = '#F6CECE';
        break;
    default:
        estadostring="Indefinido";
}




celdaNro.innerHTML = nroCobrador;
celdaApellidos.innerHTML = apellidos;
celdaNombre.innerHTML = nombre;
celdaDNI.innerHTML = dni;
celdaDomicilio.innerHTML = domicilio;
celdaCorreoElecronico.innerHTML = correoElectronico;
celdaSeleccion.innerHTML = '<input type="radio" id="radio_'+rowCount+'" name="idElegido" onchange="" value="'+nroCobrador+'">';
}

function actualizarLista()
{
    var parametros = 
    {
                "valorBusqueda" : $('#campoBusqueda').val()
    };
   $.ajax({
                url:   '<?= site_url("/CCobrador/obtenerCobradores") ?>',
                type:  'post',
                dataType: "json",
                data:   parametros,
                success:  function (response)
                {
                  // La siguiente línea es para borrar todas las filas excepto la primera (sería la cabecera)
                 
				  $("#bodytabladecobradores").children().remove();

                  $.each(response.cobradores, function(i, item) {
                    
                    // Asigno a la variable socio, cada socio que se recorre del archivo JSON
                    var cobrador = response.cobradores[i];

                    // Inserto en la tabla cada socio
                    insertarElementoEnTabla(cobrador.id,cobrador.nombre,cobrador.apellidos,cobrador.dni,cobrador.domicilio,cobrador.correoElectronico);
                });
                }});
}

function obtenerIdCobradorSeleccionado()
{
    var i=0;
    var valorRadio=-1;
    var table = document.getElementById('bodytabladecobradores');
    var rowCount = table.rows.length;
    while($("#radio_"+i).is(":checked")==false && i<=rowCount)
    {
        i++;
    }

/// Si no hubo ninguno seleccionado
    if(i>rowCount)
    {
        valorRadio=-1;
    }

    else{
        $("#ventanaSociosVinculados").modal("show");
         (valorRadio = $("#radio_"+i).val());
    }
    return valorRadio;

}

function abrirventanaSociosVinculados()
{
    nroCobrador = obtenerIdCobradorSeleccionado();

    if(nroCobrador==-1)
    {
        window.alert("Debe seleccionar un cobrador");
    }
    else
    {
        actualizarListaSociosVinculados(nroCobrador);
    }
}


function actualizarListaSociosVinculados(idCobrador)
{
    $.ajax({
        url:   '<?= site_url("/CCobrador/ObtenerSociosVinculados") ?>/'+ idCobrador,
        type:  'get',
        dataType: "json",
        success:  function (response)
        {
            // La siguiente línea es para borrar todas las filas excepto la primera (sería la cabecera)
        
			$("#bodytablaSociosVinculados").children().remove();

            $.each(response.sociosVinculados, function(i, item) {

                // Asigno a la variable socio, cada socio que se recorre del archivo JSON
                var socioVinculado = response.sociosVinculados[i];

                // Inserto en la tabla cada socio
                insertarElementoEnTablaSociosVinculados(socioVinculado.id,socioVinculado.nombre,socioVinculado.apellidos,socioVinculado.domicilio,socioVinculado.dni);
            });

        }});
}

  function  insertarElementoEnTablaSociosVinculados(id,nombre,apellidos,domicilio,dni)
    {
        var table = document.getElementById('bodytablaSociosVinculados');
        var rowCount = table.rows.length;
        var row = table.insertRow(rowCount);
        row.style.textAlign = "center";

        var celdaID = row.insertCell(0);
        var celdaNombre = row.insertCell(1);
        var celdaApellidos = row.insertCell(2);
        var celdaDomicilio = row.insertCell(3);
		var celdaDni = row.insertCell(4);

        celdaID.innerHTML = id;
        celdaNombre.innerHTML = nombre;
        celdaApellidos.innerHTML = apellidos;
        celdaDomicilio.innerHTML = domicilio;
		celdaDni.innerHTML = dni;
    }





var k = new Kibo();

function pasajeTabs() {
  //console.log('last key: ' + k.lastKey());
  if(k.lastKey()=='right')
  {
  $('.nav-tabs a:last').tab('show');  
  }
  if(k.lastKey()=='left')
    {
      $('.nav-tabs a:first').tab('show');   
    }
  
}


k.down(['shift right','shift left'], pasajeTabs);


$(document).ready(function(){
$("#bodytabladecobradores").delegate("tr", "click", function(e) {
    $("#radio_"+$(e.currentTarget).index()).prop("checked", true);

    $("#bodytabladecobradores").children().css("font-weight", "normal");
    $("#bodytabladecobradores tr:eq("+$(e.currentTarget).index()+")").css("font-weight", "bold");

});
});

</script>


<body onLoad="actualizarLista(), comprobarCampos()">
<div class="container">    

  <div class="row text-justify cajas">

        	<p>
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ventanaAgregar" onClick="vaciarCampos(),seVaAEditar(false)">Agregar
              </button>&nbsp;

	          <button type="button" class="btn btn-warning"  onClick="obtenerCobrador(),seVaAEditar(true)">Editar</button>&nbsp;

	          <button type="button" class="btn btn-danger" onClick="darDeBaja()">Dar de baja</button>&nbsp;
            <a  type="button" class="btn btn-info" style="float: right;" href="#myModal" data-toggle="modal">Ayuda</a>

          </p>
          <p style="text-align:right;">
              <button type="button" class="btn btn-primary" onClick="abrirventanaSociosVinculados()">Socios vinculados
              </button>&nbsp;
          </p>

  <div class="row">

  <div class="col-lg-6"><h1>Lista de cobradores</h1></div>

  <div class="col-lg-6 text-right" style="padding-top:30px;">

  <form><input id="campoBusqueda" onChange="actualizarLista($(this).val());" onKeyUp="this.onchange();" type="text" size="30" placeholder="Buscar..."></form></div>

  </div>

  <!------------------ TABLA DE DATOS --------------------> 

<table id="tabladecobradores" class="table table-hover">
      <thead>
        <tr>
        <th>Nº Cobrador</th>
        <th>Nombre</th>
        <th>Apellidos</th>
        <th>DNI</th>
        <th>Domicilio</th>
        <th>Email</th>
        <th>Seleccion</th>
        </tr>
      </thead>
    <tbody id="bodytabladecobradores" class="cursorManito">

    </tbody></table>


  <!-- <div id='listaDeSocios'>

  </div> -->

    <!------------------ /TABLA DE DATOS --------------------> 

	</div>

    

    <span id="resultado"></span>

    

</div>







  <!------------------ VENTANA AGREGAR --------------------> 

<!-- Modal -->

<div id="ventanaAgregar"  style="color:black;" class="modal fade" role="dialog">

  <div class="modal-dialog">



    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

<button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Agregar Cobrador</h4>

      </div>

<div class="modal-body">



<ul class="nav nav-tabs">

  <li class="active"><a data-toggle="tab" href="#datosPersonales">Datos del cobrador</a></li>

</ul>



<form id="datos">

<div class="tab-content">

  <!-- Datos Personales-->

  <div id="datosPersonales" class="tab-pane fade in active">

      <h3>Datos Personales</h3>

  <table style="border-spacing:5px;border-collapse: separate;">

  <tr>

  <td style="text-align:right"><label>Nombre</label></td>

  <td><input id="campoNombre" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="nombre" type="text" /></td>

  </tr>

 

  <tr>

  <td style="text-align:right"><label>Apellidos</label></td>

  <td><input id="campoApellidos" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="apellidos" type="text" /></td>

  </tr>

  

  <tr>

  <td style="text-align:right"><label>DNI</label></td>

  <td><input id="campoDNI" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="dni" type="number" /></td>

  </tr>	          

	   

  <tr>

  <td style="text-align:right"><label>Domicilio</label></td>

  <td><input id="campoDomicilio" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="domicilio" type="text" /></td>

  </tr>

       

  <tr>

  <td style="text-align:right"><label>Genero</label></td>

  <td><select name="genero" id="campoGenero">

  			<option value="1">Masculino</option>

			<option value="0">Femenino</option>

		</select></td>

  </tr> 

  

  <tr>

  <td style="text-align:right"><label>Correo electrónico</label></td>

  <td> <input id="campoCorreoElectronico" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="correoElectronico" type="email" /></td>

  </tr>


 <tr>
    <td style="text-align:right"><label>Telefono fijo</label></td>
    <td> <input id="campoTelefonoFijo" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="number" /></td>
 </tr>


      <tr>
          <td style="text-align:right"><label>Telefono Celular</label></td>
          <td> <input id="campoTelefonoCelular" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="number" /></td>
      </tr>



      <tr>
          <td style="text-align:right"><label>Nacionalidad</label></td>
          <td> <input id="campoNacionalidad" onChange="comprobarCampos();" value="Argentino" onKeyUp="this.onchange();" type="text" /></td>
      </tr>


      <tr>
          <td style="text-align:right"><label>Estado Civil</label></td>
          <td> <input id="campoEstadoCivil" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="text" /></td>
      </tr>
  

  

  <tr>

  <td style="text-align:right"><label>Profesión</label></td>

  <td> <input id="campoProfesion"  name="profesion" type="text" /></td>

  </tr>

  

  <tr>

  <td style="text-align:right" ><label>Fecha de Nacimiento</label></td>

  <td> <input id="campoFechaNacimiento" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="fechaDeNacimiento" type="date" /></td>

  </tr>     

    <tr>
          <td style="text-align:right"><label>comision</label></td> 
          <td> <input id="campoComision" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="text" />      valores de 0-100</td> 
      </tr>
</table>

  </div>

 </div>

		

      </div>

      <div class="modal-footer">
      <span id="advertenciaDatos" style="color:red;"></span>
        <button type="button" id="botonAceptar" class="btn btn-default" data-dismiss="modal" href="javascript:;" onClick="if(!editar){agregarCobrador();}else{editarCobrador()}return false;" >Aceptar</button>

        <button type="button" class="btn btn-default" data-dismiss="modal" href="javascript:;" onClick="">Cancelar</button>

      </div>

      </form>

    </div>

  </div>

</div>
<!---     ****     TERMINA VENTANA AGREGAR  ***             -->





<!-- ************VENTANA SOCIOS Vinculados****************  --> 

<!-- Modal -->

<div id="ventanaSociosVinculados"  style="color:black;" class="modal fade" role="dialog">

  <div class="modal-dialog">



    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

<button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Socios del cobrador</h4>

      </div>



<div class="modal-body">
<br>

<form id="datoSocioAdherente">

  

<table id="tablaSociosVinculados" class="table table-hover">
    <thead>
    <tr>
        <th>Nro Socio</th>
        <th>Nombre</th>
        <th>Apellidos</th>
        <th>Domicilio</th>
        <th>DNI</th>
    </tr>
    </thead>
    <tbody id="bodytablaSociosVinculados" class="cursorManito">

    </tbody>
</table>

</div>

      <div class="modal-footer">
      <span id="advertenciaDatos" style="color:red;"></span>
        <button type="button" class="btn btn-default" data-dismiss="modal" href="javascript:;" onClick="">Aceptar</button>
      </div>

</form>


</div>
</div>
</div>
</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: black">Módulo de cobradores</h4>
      </div>
      <div class="modal-body" style="color: black">
        <p align="justify">Este es el módulo de cobradores. En pantalla se puede observar el listado de los cobradores que posee el club. Las funciones disponibles en esta sección son: </p>
        <p><u>Alta, Baja y Modificacion:</u></p>
        <p align="justify">Este módulo permite agregar un nuevo cobrador, darlo de baja o modificar sus datos. La informacion de estos cobradores (nombre y apellido) aparecerán en el módulo de socios para asignar un cobrador.</p>
        <p><u>Socios vinculados:</u></p>
        <p align="justify">Esta opción muestra un listado de los socios que están vinculados a un cobrador seleccionado</p>

       </div>
      <div class="modal-footer">
        <center><button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button></center>
      </div>
    </div>

  </div>
</div>
</body>

</html>