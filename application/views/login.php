<script type="text/javascript" src="/parquesur/js/jquery-3.1.0.js"></script>



<script type="text/javascript" src="<?php echo base_url("/js/jQuery-1.10.2.js"); ?>"></script>


<script type="text/javascript" src="<?php echo base_url("/js/angular.min.js"); ?>"></script>
<!--- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script> -->

<script type="text/javascript" src="<?php echo base_url("/js/kibo.js"); ?>"></script>

<script type="text/javascript" src="<?php echo base_url("/js/bootstrap.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("/js/app.js"); ?>"></script>

<html lang="en">
<head>
	<meta charset="utf-8">
    <title> Ingresar al sistema </title>
    <link rel="stylesheet" href="<?= base_url() ?>static/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>static/css/flat-ui.min.css">
    <style type="text/css">
    body {
	/* background-image: url(../../images/fondolog.png); */
	background-color: #0e2b49;
}
    </style>
</head>

<body> 
	</style>
	<div class="container" >
    	<div class="row">
        	<div class="col-md-4 col-md-offset-4">
           	  <center><h2 style="color:#ECECEC"> Bienvenido a SGS</h2> </center>
              <center><h5 style="color:#ECECEC"> Ingrese sus datos </h5></center>
                <form action="" method="POST">
                <div class="form-group">
                	<label for="username"></label>
                    <input type="text" name="username" id="username" class="form-control" placeholder="Usuario">
                </div>
                
                <div class="form-group">
                	<label for="password"></label>
                    <input type="password" name="password" id="password" class="form-control" placeholder="Contraseña">
                </div>
                
                <center> <input type="submit" value="Ingresar" class="btn btn-primary"> </center>
                <center><a href="#myModal" data-toggle="modal">Necesita ayuda?</a></center>
              </form>
          </div>
      </div>
</div>

 

<style> .versionad
{
height: 15px;

color: #fff;
text-align: left;
clear: both;
position: absolute;
		top:93%;
		left:1%;
}
</style>

<style> .autores
{
height: 15px;

color: #fff;
text-align: left;
clear: both;
position:absolute;
		top:93%;
		left:93%;
}
</style>
 <div id="pie" class="versionad">
 	<label>V 1.9</label>
 </div>
 
 <div class="autores">
  <a href="CNosotros">Autores</a>
 </div>
 



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ayuda para entrar al sitio</h4>
      </div>
      <div class="modal-body">
        <p align="justify">Para acceder al sistema, debe tener un Usuario y una Contraseña que deberían ser asignadas por la administración del club. Usted puede acceder al sitio en el rol de Administrador o Cobrador para realizar sus tareas pertinentes. Si usted cumple con uno de estos cargos en el sitio, y desea acceder al sistema, favor de solicitar un usuario y contraseña en la administración del club.</p>

        <p> Pasos para ingresar: </p>
        <p>1) Ingrese su Usuario. </p>
        <p>2) Ingrese su Contraseña</p>
        <p>3) Seleccione la opción "Ingresar"</p>

        <p align="justify">Recuerde que la información cargada quedará registrada, por favor no divulgue su información de acceso al sitio a otras personas, ya que quedarán registradas las actividades dentro del sistema a su nombre. </p>
      </div>
      <div class="modal-footer">
        <center><button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button></center>
      </div>
    </div>

  </div>
</div>

</body>
</html>