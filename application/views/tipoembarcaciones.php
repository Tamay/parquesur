<!------------------ JAVASCRIPT -------------------->
<script>

var editar;

function seVaAEditar(vof)
{
  editar=vof;
}



function obtenerIdTipoEmbarcacion()
{
    var i=0;
    var table = document.getElementById('bodyTablaTipoEmbarcaciones');
    var rowCount = table.rows.length;
    while($("#radio_"+i).is(":checked")==false && i<=rowCount)
    {
        i++;
    }

/// Si no hubo ninguno seleccionado
    if(i>rowCount)
    {
        valorRadio=-1;
    }

    else{
         (valorRadio = $("#radio_"+i).val());
    }
    return valorRadio;

}




   //Esta funcion es para instertar los elementos de la base de datos en la tabla de html (la vista)
function insertarElementoEnTabla(idTipoEmbarcacion,descripcion,monto)
{
var table = document.getElementById('bodyTablaTipoEmbarcaciones');
var rowCount = table.rows.length;
var row = table.insertRow(rowCount);

var celdaDescripcion = row.insertCell(0);
var celdaMonto = row.insertCell(1);
var celdaSeleccion = row.insertCell(2);

celdaDescripcion.innerHTML = descripcion;
celdaMonto.innerHTML = monto;
celdaSeleccion.innerHTML = '<input type="radio" id="radio_'+rowCount+'" value="'+idTipoEmbarcacion+'" name="idElegido">';
}


//Funcion para actualizar lista ya sea en una busqueda o cuando se agrega/elimina un Tipo Embarcacion
function actualizarLista()
{
    
   $.ajax({
                url:   '<?= site_url("/CTipoEmbarcaciones/obtenerTipoEmbarcaciones") ?>',
                type:  'post',
                dataType: "json",
                success:  function (response)
                {
                    // La siguiente línea es para borrar todas las filas excepto la primera (sería la cabecera)
                  $("#bodyTablaTipoEmbarcaciones").children().remove();
              $.each(response.tipoEmbarcaciones, function(i, item) {
                    
                    // Asigno a la variable parametro, cada parametro que se recorre del archivo JSON
                    var tipoEmbarcacion = response.tipoEmbarcaciones[i];

                    // Inserto en la tabla cada parametro
                    insertarElementoEnTabla(tipoEmbarcacion.id,tipoEmbarcacion.descripcion,tipoEmbarcacion.monto);
                });

                }});
}


function agregarTipoEmbarcacion(){
    var parametros = {
            "descripcion" : $('#campoDescripcion').val().toUpperCase(),
            "monto" : $('#campoMonto').val()
        };
        $.ajax({
            data:  parametros,
            url:   '<?= site_url("/CTipoEmbarcaciones/Agregar") ?>',
            type:  'post',
            success:  function (response) {
                if(response=="success")
                {
                    actualizarLista();
                    vaciarCampos();
                }
                else{
                    window.alert(response);
                }

            }
        });
    }






    //Este es para los campos de la ventana
    function comprobarCampos()
    {
        if($('#campoDescripcion').val()=='' || $('#campoMonto').val()=='')
        {
            $('#botonAceptar').prop('disabled', true);
            $("#advertenciaDatos").text("Faltan datos obligatorios");
        }
        else
        {
            $('#botonAceptar').prop('disabled', false);
            $("#advertenciaDatos").text("");
        }
    }



    //Vacia los campos de las 2 ventanas
    function vaciarCampos()
    {
        $('#campoDescripcion').val("");
        $('#campoMonto').val("");
        comprobarCampos();
    }
    
    function editarTipoEmbarcacion()
    {
        var valorRadio = obtenerIdTipoEmbarcacion();
        /// Si no hubo ninguno seleccionado
        if(valorRadio!=-1)
        {
        var parametros = {
            "descripcion" : $('#campoDescripcion').val().toUpperCase(),
            "monto" : $('#campoMonto').val()
        };
        $.ajax({
            data:  parametros,
            url:   '<?= site_url("/CTipoEmbarcaciones/Editar") ?>/'+valorRadio,
            type:  'post',
            beforeSend: function () {
                $("#resultado").html("Procesando, espere por favor...");
            },
            success:  function (response) {
                $("#resultado").html(response);
                actualizarLista();
                vaciarCampos();
            }
        });
    }
}

    //Funcion para obtener un parametro para editarlo
    function obtenerTipoEmbarcacion()
    {
        var valorRadio = obtenerIdTipoEmbarcacion();

        /// Si no hubo ninguno seleccionado
        if(valorRadio==-1)
        {
        window.alert("Debe seleccionar un tipo de Embarcacion");
        }
        else
        {
            $("#ventanaAgregar").modal("show");
            $.ajax({
                url:'<?= site_url("/CTipoEmbarcaciones/obtenerTipoEmbarcacion") ?>/'+valorRadio,
                type:  'get',
                dataType: 'json',
                success: function(res) {
                    $('#campoDescripcion').val(res.descripcion);
                    $('#campoMonto').val(res.monto);
                    comprobarCampos();
                }});
        }
    }


    //Esto de aca abajo es para cuando haces click en una fila de la tabla seleccione esa preinscripcion
 $(document).ready(function(){
$("#bodyTablaTipoEmbarcaciones").delegate("tr", "click", function(e) {
    $("#radio_"+$(e.currentTarget).index()).prop("checked", true);

    $("#bodyTablaTipoEmbarcaciones").children().css("font-weight", "normal");
    $("#bodyTablaTipoEmbarcaciones tr:eq("+$(e.currentTarget).index()+")").css("font-weight", "bold");

});
});
</script>

<!-- Elementos de la pagina -->
<body onload="actualizarLista()">
<div class="container">
    <div class="row text-justify cajas">
        <form>
            <p>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ventanaAgregar" href="javascript:;" on-click="seVaAEditar(false)">Agregar</button>&nbsp;
                <button type="button" class="btn btn-warning" onClick="obtenerTipoEmbarcacion(),seVaAEditar(true)">Editar</button>&nbsp;
            </p>

            <div class="row">

                <div class="col-lg-6">
                    <h1>Tipos de embarcaciones</h1>
                </div>

                <div class="col-lg-6 text-right" style="padding-top:30px;">

                </div>
            </div>


            <!------------------ TABLA DE DATOS -------------------->

            <div>

            <table id='TablaTipoEmbarcaciones' class="table table-hover table-responsive">
                <thead>
                <tr>
                    <th>Descripcion</th>
                    <th> Valor </th>
                    <th>Selección</th>
                </tr>
                </thead>
                <tbody id="bodyTablaTipoEmbarcaciones">

                </tbody>
            </table>

                </div>
        </form>
    </div>
    <span id="resultado"></span>
</div>


<!------------------ VENTANAS AGREGAR -------------------->

<!-- Modificar -->
<div id="ventanaAgregar"  style="color:black;"class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tipo de Embarcacion</h4>
            </div>
            <div class="modal-body">
                <form id="datos">
                    <div class="tab-content">
                        <div id="datosTipoEmbarcaciones" class="tab-pane fade in active">
                            <h3>Tipo de Embarcacion</h3>
                            <table style="border-spacing:5px;border-collapse: separate;">
                                <tr>
                                    <td style="text-align:right"><label>Descripcion</label></td>
                                    <td><input id="campoDescripcion" onchange="comprobarCampos();" onkeyup="this.onchange();" type="text" /></td>
                                </tr>
                                <tr>
                                    <td style="text-align:right"><label>Monto</label></td>
                                    <td><input id="campoMonto" onchange="comprobarCampos();" onkeyup="this.onchange();" type="text" /></td>
                                </tr>
                            </table>
                        </div>
                        </table>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" href="javascript:;" onclick="if(!editar){agregarTipoEmbarcacion();}else{editarTipoEmbarcacion()}return false;" data-dismiss="modal">Aceptar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
            </form>
        </div>
    </div>
</div>

</body>

</html>