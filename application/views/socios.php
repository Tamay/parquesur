<html ng-app="sociosApp">


<script type="text/javascript" src="<?php echo base_url("/js/funSocios.js");?>"></script>
<script type="text/javascript" src="<?=base_url("/js/tablas/js/dataTables.buttons.min.js");?>"></script>
<script type="text/javascript" src="<?=base_url("/js/tablas/js/buttons.print.min.js");?>"></script>
<script type="text/javascript" src="<?=base_url("/js/tablas/js/dataTables.select.min.js");?>"></script>
<script type="text/javascript" src="<?=base_url("/js/tablas/js/dataTables.scroller.min.js");?>"></script>
<script type="text/javascript" src="<?=base_url("/js/tablas/js/pdfmake.min.js");?>"></script>
<script type="text/javascript" src="<?=base_url("/js/tablas/js/vfs_fonts.js");?>"></script>
<script type="text/javascript" src="<?=base_url("/js/tablas/js/buttons.html5.min.js");?>"></script>
<script type="text/javascript" src="<?=base_url("/js/jquery-ui.js");?>"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url("/css/tablas/css/scroller.dataTables.min.css");?>">
<link rel="stylesheet" type="text/css" href="<?=base_url("/css/jquery-ui.css");?>">

<style>

    .modal .modal-body { /* Vertical scrollbar if necessary */
        max-height: 500px;
        overflow-y: auto;
    }

    .modal-dialog { /* Width */
        max-width: 100%;
        width: auto !important;
        display: inline-block;
    }
    .modal.in{
        text-align: center;
    }

</style>



<body onLoad="inicializarLista(),comprobarCampos(),actualizarTipoSocios(),actualizarCobradores(),actualizarDeportes(), actualizarTipoEmbarcaciones(),actualizarBotones()">
<div id="controladorAngular" ng-controller="mainController">
<div class="container cajas">

  <div class="row">

        	<div class="col-md-9">
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ventanaAgregar" onClick="actualizarTipoSocios(),numSocioAAgregar(), vaciarCampos(),seVaAEditar(false)">Agregar
              </button>
	          <button type="button" class="btn btn-warning"  onClick="vaciarCampos(),obtenerSocio(),seVaAEditar(true)">Editar</button>
	          <button type="button" id="botonDarDeBaja" class="btn btn-danger" onClick="darDeBaja()">Dar de baja</button>
                <button type="button" id="botonDarDeAlta" class="btn btn-deault" style="background-color: #5c6bc0; color: white"  onClick="darDeAlta()">Devolver alta</button>

            </div>

            <div class="col-md-3">
              <button type="button" class="btn btn-info" onClick="informeDetallado()">Informe detallado</button>
                <button type="button" class="btn btn-success" onClick="abrirVentanaServicios()">Servicios</button>
              </div>
</div>
<div class="row">
<h1>Lista de socios</h1>
     <!-- <a style="color: black;" href="" onclick="seleccionarUltimo()">Seleccionar último</a> -->

  <!------------------ TABLA DE DATOS -------------------->

      <table id="tabladesocios" class="table display table-responsive">
      <thead>
        <tr>
        <th>Nº Socio</th>
        <th>Apellido y nombre</th>
        <th>DNI</th>
        <th>Domicilio</th>
        <th>Tipo de Socio</th>
        <th>Estado</th>
        </tr>
      </thead>


    <tbody id="bodyDetabladesocios" class="cursorManito">
    </tbody>
   <tfoot>
            <tr>
        <th>Nº Socio</th>
        <th>Apellido y nombre</th>
        <th>DNI</th>
        <th>Domicilio</th>
        <th>Tipo de Socio</th>
        <th>Estado</th>
        </tr>
        </tfoot>
    </table>



    <!------------------ /TABLA DE DATOS --------------------> 

	</div>

</div>






  <!------------------ VENTANA AGREGAR --------------------> 

<!-- Modal -->

<div id="ventanaAgregar"  style="color:black;" class="modal fade" role="dialog">

  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="tituloVentana" style="text-align: left" class="modal-title"></h4>
      </div>
<div class="modal-body">
<ul class="nav nav-tabs" id="tabAgregar">
  <li class="active"><a data-toggle="tab" href="#datosPersonales">Datos personales</a></li>
  <li><a data-toggle="tab" href="#datosDelSocio">Socio</a></li>
</ul>


<form id="datos">
<div class="tab-content">
  <!-- Datos Personales-->
  <div id="datosPersonales" style="text-align: left" class="tab-pane fade in active">
      <h3>Datos Personales</h3>
  <table style="border-spacing:1px;border-collapse: separate;"  class="table table-striped">
  <tr>


      <td style="text-align:right"><label>Apellidos</label></td>
      <td><input id="campoApellidos" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="apellidos" type="text" /></td>
      <td style="text-align:right"><label>Nombre</label></td>
      <td><input id="campoNombre" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="nombre" type="text" /></td>
  </tr>
      <!--- Campo DNI -->
  <tr>
  <td style="text-align:right"><label>DNI</label></td>
  <td><input id="campoDNI" onChange="comprobarCampos(),controlarNoNegatividad();" onKeyUp="this.onchange();" name="dni" type="number" /></td>

      <td style="text-align:right"><label>Genero</label></td>
      <td><select name="genero" id="campoGenero">
              <option value="1" selected>Masculino</option>
              <option value="0">Femenino</option>
          </select></td>
  </tr>

      <tr>
          <td style="text-align:right" ><label>Fecha de Nacimiento</label></td>
          <td style="text-align: left" colspan="3"> <input id="campoFechaNacimiento" onChange="comprobarEdad();" onKeyUp="" name="fechaDeNacimiento" type="date" />    <span id="textoFechaNacimiento" style="color:red;"></span>   </td>
      </tr>

      <!--- Campo Domicilio -->
  <tr>
  <td style="text-align:right"><label>Domicilio</label></td>
  <td><input id="campoDomicilio" onChange="comprobarCampos(),copiarDomicilio();" onKeyUp="this.onchange();" name="domicilio" type="text" /></td>

      <td style="text-align:right"><label>E-Mail</label></td>
      <td> <input id="campoCorreoElectronico" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="correoElectronico" type="email" /></td>
  </tr>

 <tr>
    <td style="text-align:right"><label>Telefono fijo</label></td>
    <td> <input id="campoTelefonoFijo" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="number" /></td>

     <td style="text-align:right"><label>Celular</label></td>
     <td> <input id="campoTelefonoCelular" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="number" /></td>
 </tr>
      <tr>
          <td style="text-align:right"><label>Nacionalidad</label></td>
          <td> <input id="campoNacionalidad" onChange="comprobarCampos();" value="Argentino" onKeyUp="this.onchange();" type="text" /></td>
          <td style="text-align:right"><label>Estado Civil</label></td>
          <td> <input id="campoEstadoCivil" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="text" /></td>
      </tr>
  <tr>
  <td style="text-align:right"><label>Profesión</label></td>
  <td> <input id="campoProfesion"  name="profesion" type="text" /></td>
      <td></td>
      <td></td>
  </tr>

<tr>
    <td style="text-align:right; valign:bottom;" ><label>Observaciones</label></td>
    <td colspan="3" style="text-align: left"> <textarea  id="campoObservaciones" name="observaciones" cols="50" rows="5"></textarea>
</tr>
  </table>
  </div>
  <!-- Datos del socio -->
  <div id="datosDelSocio" class="tab-pane fade" style="text-align: left">
    <h3>Datos del Socio</h3>
    <table style="border-spacing:5px;border-collapse: separate;">
        <tr>
            <td style="text-align:right"><label>Nº de Socio</label></td>
            <td style="valign:bottom"> <input id="campoNroSocio" type="text" /></span></td>
        </tr>
      <tr>
        <tr>
            <td style="text-align:right"><label>Fecha de alta</label></td>
            <td> <input id="campoFechaAlta" onChange="" onKeyUp="" type="date" value="<?php echo date('Y-m-d'); ?>" /> </td>
        </tr>
  <td style="text-align:right"><label>Domicilio de Cobro</label></td>
  <td> <input id="campoDomicilioDeCobro" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="domicilioDeCobro" type="text" /></td>
  </tr>
  <tr>
  <td style="text-align:right"><label>Intervalo de pago (expresado en meses)</label></td>
  <td> <input id="campoIntervaloDePago" onChange="comprobarCampos(),controlarNoNegatividad();" onKeyUp="this.onchange();" name="DePago" type="number" /></td>
  </tr>
    <tr>
  <td style="text-align:right"><label>Tipo de socios</label></td>
  <td><div id="tipoSociosEnSelect"></div></td>
    </tr>     
        <tr>
  	<td style="text-align:right"><label>Cobrador</label></td>
	<td><div id="CobradoresSeleccion"></div></td>
  </tr>    
</table>
  </div>
</div>
      </div>
      <div class="modal-footer">
      <span id="advertenciaDatos" style="color:red;"></span>
          <!-- data-dismiss="modal" -->
        <button type="button" id="botonAceptar" class="btn btn-default"  href="javascript:;" onClick="if(!editar){agregarSocio();}else{editarSocio()}return false;" >Aceptar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" href="javascript:;" onClick="">Cancelar</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!---     ****     TERMINA VENTANA AGREGAR  *** ss            -->


<!-- ************VENTANA SERVICIOS****************  -->
<div id="ventanaServicios"  style="color:black;" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Servicios</h4>
            </div>
            <div class="modal-body">
                <h4>Servicios activos del socio <input id="numeroSocio" style="text-align: center"  disabled size="2"></h4>
                <!--- TABS de la cabecera -->
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#deportes">Deportes</a></li>
                    <li><a data-toggle="tab" href="#embarcaciones">Embarcaciones</a></li>
                    <li><a data-toggle="tab" href="#sociosAdherentes">Socios Adherentes</a></li>
                </ul>
                <!--- TABS de la cabecera -->


                <div class="tab-content">

                    <!--- Tab deporte -->
                    <div id="deportes" class="tab-pane fade in active">
                        <center>
                        <br>
                        <form>
                            <table style="border-spacing:5px;border-collapse: separate;">
                                <tr>
                                    <td style="text-align:right"><label>Actividad</label></td>
                                    <td>
                                        <select ng-model="deporteSeleccionado">
                                            <option ng-repeat="x in deportes" value="{{x.id}}">{{x.nombre}}</option>
                                        </select></td>
                                    <td><input type="button" class="button btn-success" ng-click="agregarInscripcion()" value="Agregar" class="cursorManito" onClick=""></td>
                                </tr>
                            </table>

                            <table id="tablaServiciosDelSocio" class="table">
                                <thead>
                                <tr>
                                    <th>Fecha Inscripcion</th>
                                    <th>Servicio</th>
                                    <th>Monto</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="x in inscripcionesSocio">
                                    <td>{{x.fechaAlta}}</td>
                                    <td>{{x.nombreActividad}}</td>
                                    <td>{{x.monto}}</td>
                                    <td><input type="button" ng-click="eliminarInscripcion(x.id)" class="button btn-danger" value="-"> </td>
                                </tr>
                                </tbody>
                            </table>
                        </center>
                    </div>

                    <!--- Tab embarcaciones -->
                    <div id="embarcaciones" class="tab-pane fade">
                        <center>
                            <br>
                        <table style="border-collapse: separate;"  class="table table-striped">
                            <tr>
                                <td style="text-align:right"><label>Nombre</label></td>
                                <td><input ng-model="nombreEmbarcacion" ng-change="nombreEmbarcacion = (nombreEmbarcacion | uppercase)" onChange="comprobarCampos();" onKeyUp="this.onchange(); type="text" /></td>

                                <td style="text-align:right"><label>Matricula</label></td>
                                <td><input ng-model="matriculaEmbarcacion" ng-change="matriculaEmbarcacion = (matriculaEmbarcacion | uppercase)" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="text" /></td>
                            </tr>
                            <tr>
                                <td style="text-align:right"><label>Tipo de embarcacion</label></td>
                                <td> <select ng-model="tipoEmbarcacion">
                                        <option ng-repeat="x in tipoEmbarcaciones" value="{{x.id}}">{{x.descripcion}}</option>
                                    </select></td>

                                <td style="text-align:right"><label>Marca de motor</label></td>
                                <td><input ng-model="marcaMotorEmbarcacion" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="text" /></td>
                            </tr>
                            <tr>
                                <td style="text-align:right"><label>Numero de motor</label></td>
                                <td><input ng-model="numeroMotorEmbarcacion" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="int" /></td>

                                <td style="text-align:right"><label>Tipo de motor</label></td>
                                <td><input ng-model="tipoMotorEmbarcacion" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="text" /></td>
                            </tr>

                            <tr>
                                <td style="text-align:right"><label>Amarre</label></td>
                                <td><input ng-model="amarreEmbarcacion" onChange="comprobarCampos();" onKeyUp="this.onchange();"  type="date" /></td>

                                <td style="text-align:right"><label>Fecha adquisicion</label></td>
                                <td><input ng-model="fechaAdquisicionEmbarcacion" onChange="comprobarCampos();" onKeyUp="this.onchange();"  type="date" /></td>
                            </tr>

                            <tr>
                                <td style="text-align:right"><label>Año de construccion</label></td>
                                <td><input ng-model="anioConstruccionEmbarcacion" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="int" /></td>

                                <td style="text-align:right"><label>Arboladura</label></td>
                                <td><input ng-model="arboladuraEmbarcacion" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="int" /></td>
                            </tr>

                            <tr>
                                <td style="text-align:right"><label>Eslora</label></td>
                                <td><input ng-model="esloraEmbarcacion" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="int" /></td>

                                <td style="text-align:right"><label>Manga</label></td>
                                <td><input ng-model="mangaEmbarcacion" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="int" /></td>
                            </tr>

                            <tr>
                                <td style="text-align:right"><label>Puntal</label></td>
                                <td><input ng-model="puntalEmbarcacion" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="int" /></td>

                                <td style="text-align:right"><label>Tonelaje</label></td>
                                <td><input ng-model="tonelajeEmbarcacion" onChange="comprobarCampos();" onKeyUp="this.onchange();"  type="int" /></td>
                            </tr>

                            <tr>
                                <td style="text-align:right"><label>HP</label></td>
                                <td><input ng-model="hpEmbarcacion" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="int" /></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td style="text-align:right; valign:bottom;" ><label>Observaciones</label></td>
                                <td colspan="3" style="text-align: left"> <textarea ng-model="observacionesEmbarcacion" cols="50" rows="5"></textarea>
                            </tr>

                            <tr>
                                <td colspan="3"></td>
                                <td style="text-align: right"><input type="button" value="Agregar" ng-click="agregarEmbarcacion()" class="btn btn-success"></td>
                            </tr>
                        </table>


                        <table class="table">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Matricula</th>
                                <th>Tipo</th>
                                <th>Monto</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="x in embarcacionesSocio">
                                <td>{{x.Nombre}}</td>
                                <td>{{x.Matricula}}</td>
                                <td>{{x.Tipo}}</td>
                                <td>{{x.Monto}}</td>
                                <td><input type="button" ng-click="eliminarEmbarcacion(x.nroEmbarcacion)" class="btn btn-danger" value="Eliminar"> </td>
                            </tr>
                            </tbody>
                        </table>
                        </center>
                    </div>

                    <!-- Tab socios adherentes -->
            <div id="sociosAdherentes" class="tab-pane fade">
            <form id="datoSocioAdherente">
                <table width="100%" style="border:0px; border-spacing:5px;border-collapse: separate;">

                    <tr>
                        <td style="text-align:right"><label>Apellidos</label></td>
                        <td><input id="campoApellidoSA" onChange="" onKeyUp="this.onchange();" type="text" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right"><label>Nombres</label></td>
                        <td><input id="campoNombreSA" onChange="" onKeyUp="this.onchange();"  type="text" /></td>
                    </tr>

                    <tr>
                        <td style="text-align:right"><label>DNI</label></td>
                        <td><input id="campoDNISA" onChange="" onKeyUp="this.onchange();" name="dniSA" type="number" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right" ><label>Fecha de Nacimiento</label></td>
                        <td> <input id="campoFechaNacimientoSA" onChange="" onKeyUp="this.onchange();" name="fechaDeNacimientoSA" type="date" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right"><label>Parentezco</label></td>
                        <td> <input id="campoParentezcoSA"  name="parentezcoSA" type="text" /></td>
                        <td align="right"><button type="button" class="btn btn-success" href="javascript:;" onClick="agregarSocioAdherente()">Agregar</button></td>
                        <td align="right"><button type="button" class="btn btn-danger" href="javascript:;" onClick="eliminarSocioAdherente()">Eliminar</button></td>
                    </tr>
                </table>

                <table id="tablaSociosAdherentes" class="table">
                    <thead>
                    <tr>
                        <th>Nombre Completo</th>
                        <th>DNI</th>
                        <th>Parentezco</th>
                        <th>Fecha de Nacimiento</th>
                        <th>Seleccion</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
                </form>
                </div>

                </div>
            </div>






            <div class="modal-footer">
                <span id="advertenciaDatos" style="color:red;"></span>
                <button type="button" class="btn btn-default" data-dismiss="modal" href="javascript:;" onClick="aceptarVentanaServicios()">Aceptar</button>
            </div>
            </form>
        </div>
    </div>
</div>













<!-- Modal Cargando -->
<div id="modal_Cargando" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title" style="color: black">Cargando....</h4>
      </div>
      <div class="modal-body" style="color: black">
      <h3>Aguarde un momento por favor...</h3>
      <p>Esta operación puede tardar algunos minutos.</p>
      <p>Por favor, no cierre esta ventana</p>
        <center><img src="<?php echo base_url('images/loading.gif')?>"></center>
       </div>
    </div>
  </div>
</div>
</div>
</body>

</html>