<!------------------ MENU --------------------> 

<nav class="navbar navbar-default navbar-fixed-top">

  <div class="container-fluid">

    <!-- Brand and toggle get grouped for better mobile display -->

    <div class="navbar-header">

      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#topFixedNavbar1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>

      <a class="navbar-brand" href= <?= base_url()?> >Club Parque Sur - Gestión</a></div>

    <!-- Collect the nav links, forms, and other content for toggling -->

    <div class="collapse navbar-collapse" id="topFixedNavbar1">

      <ul class="nav navbar-nav navbar-right">

        <li><a href= <?= base_url() ?>index.php/CSocio>Socios</a></li>
        <li><a href= <?= base_url() ?>index.php/CDeporte>Deportes</a></li>

        <li><a href=<?= base_url() ?>index.php/CCobrador>Cobradores</a></li>

        <li><a href=<?= base_url() ?>index.php/CReporte>Reportes</a></li>

        <!--<li><a href="deportes.html">Deportes</a></li>-->
        <li><a href=<?= base_url() ?>index.php/CDeuda>Cuotas</a></li>

        <li><a href=<?= base_url() ?>index.php/CCobro>Cobros</a></li>

        <li><a href=<?= base_url() ?>index.php/CEmbarcacion>Embarcaciones</a></li>

        <li><a style="text-decoration:none;color:#F08080" href="<?=site_url('Clogin/logout') ?>"> Cerrar Sesion </a></li>

      </ul>

    </div>

  </div>

</nav>

<!------------------ /MENU --------------------> 