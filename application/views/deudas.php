<script>

    totalRecaudado=0;
    totalRecaudadoXServicios=0;
    totalRecaudadoXAbonos=0;
    cantidadDeSocios=0;
    cobradorSeleccionado='';
    mesSeleccionado='';
    añoSeleccionado='';


    function actualizarCamposSeleccionados()
    {
        cobradorSeleccionado=$('#campoCobrador option:selected').text();
        mesSeleccionado=$('#campoMes').val();
        añoSeleccionado=$('#campoAnio').val();
    }

    function sleep(milliseconds) {
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds){
                break;
            }
        }
    }



    function ImprimirReporte()
    {
        //var mywindow = window.open('', 'PRINT', 'height=400,width=600');
        var mywindow = window.open('', 'PRINT');

        //Titulo de la página
        mywindow.document.write('<html><head><title> Planilla '+cobradorSeleccionado+' - '+mesSeleccionado+'-'+añoSeleccionado+ '</title>');

        //Los estilos de la tabla
        mywindow.document.write('<style>' +
            'table, th, td {border: 1px solid black;border-collapse: collapse;}' +

            '@media print {.reporte {background-color: white;height: 100%;width: 100%;position: fixed;top: 0;left: 0;margin: 0;padding: 15px;'
            +'font-size: 14px;line-height: 18px;}}'+

            'table{width:100%;}'+

            '#izquierda{float: left;}'+

            '#contenedorCabecera{display: inline-block; position: relative;}'+

            '#contenedorGlobal{background-image:url(<?php echo base_url('/images/escudo-parque-sur.png')?>);'+
            'background-repeat:no-repeat; background-position:right 0px top 0px;}'+

            '#centro{display: block;}'+
            '</style>');

        mywindow.document.write('</head><body >');
        mywindow.document.write('<div id="contenedorGlobal">');
            mywindow.document.write('<div id="contenedorCabecera">');
                mywindow.document.write('<div id="izquierda">');
                mywindow.document.write('<h1>CLUB PARQUE SUR</h1>');
                mywindow.document.write('</div>');
            mywindow.document.write('</div>');


            mywindow.document.write('<div>');
            mywindow.document.write('<span style="font-weight:bold; font-size:20px">Deudas del '+mesSeleccionado+'/'+añoSeleccionado+'<br>');
            mywindow.document.write('- Cobrador: '+cobradorSeleccionado+'<br>');
            mywindow.document.write('- Total a recaudar: '+totalRecaudado+' | Total servicios: '+totalRecaudadoXServicios+' | Total Abonos: '+totalRecaudadoXAbonos+'<br>');
            mywindow.document.write('- Socios a cobrar: '+totalSocios+'<span>');
            mywindow.document.write('</div>');
        mywindow.document.write('<br>');
        //mywindow.document.write('');
        mywindow.document.write($('#reporte').html());
        mywindow.document.write('</body></html>');
        mywindow.document.close(); // necessary for IE >= 10
        mywindow.onload=function(){
        mywindow.focus(); // necessary for IE >= 10*/
        mywindow.print();
        mywindow.close();
        };
        return true;
    }





function comprobarCampos()
{

  if($('#campoMes').val()=='' || $('#campoAnio').val()=='') 
  {
    $('#botonGenerar').prop('disabled', true);
    $("#botonHistorial").prop('disabled', true);
  }
  else
  {
   $('#botonGenerar').prop('disabled', false); 
   $("#botonHistorial").prop('disabled', false); 
  }   
}

var clic = 1;
function ocultar(){ 
   if(clic==1){
   document.getElementById("caja").style.height = "100px";
   clic = clic + 1;
   } else{
       document.getElementById("caja").style.height = "0px";      
    clic = 1;
   }   
}
</script>

<script>
function insertarElementoEnTabla(nrosocio,nombreCompleto,domicilioCobro,mes,montoSocietario,montoServicios,montoTotal)
{
var table = document.getElementById('bodyDetablaDeuda');
var rowCount = table.rows.length;
var row = table.insertRow(rowCount);


var celdaNro = row.insertCell(0);
var celdanombreCompleto = row.insertCell(1);
var celdaDomilicioCobro = row.insertCell(2);
var celdaMes = row.insertCell(3);
var celdaMontoSocietario = row.insertCell(4);
var celdaMontoServicios = row.insertCell(5);
var celdaMontoTotal = row.insertCell(6);

    celdaNro.innerHTML = nrosocio;
    celdanombreCompleto.innerHTML = nombreCompleto;
    celdaDomilicioCobro.innerHTML = domicilioCobro;
    celdaMes.innerHTML = mes;
    celdaMontoSocietario.innerHTML = montoSocietario;
    celdaMontoServicios.innerHTML = montoServicios;
    celdaMontoTotal.innerHTML = montoTotal;
}

function obtenerDeudas()
{
		var parametros = {
                "anio" : $('#campoAnio').val(),
                "mes" : $('#campoMes').val(),
				"cobrador" : $('#campoCobrador').val()
                
        };
        $.ajax({
                data:  parametros,
                url:   '<?= site_url("/CDeuda/ObtenerDeudas") ?>',
                type:  'post',
                dataType: "JSON",
                beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                        $("#modal_Cargando").modal({backdrop: 'static', keyboard: false});
                        $("#modal_Cargando").modal('show');
                },
                success:  function (response) {
                    $("#modal_Cargando").modal('hide');
                    $("#resultado").html("");
                    // La siguiente línea es para borrar todas las filas excepto la primera (sería la cabecera)
                  $("#bodyDetablaDeuda").children().remove();
	          $.each(response.deudas, function(i, item) {
                    
                    // Asigno a la variable deuda, cada socio que se recorre del archivo JSON
                    var deuda = response.deudas[i];

                    // Inserto en la tabla cada deuda
                    insertarElementoEnTabla(deuda.nrosocio, deuda.nombreCompleto,deuda.domicilioCobro,deuda.mes,deuda.montoSocietario,deuda.montoServicios,deuda.montoTotal);
                });
            totalARecaudar();
                    actualizarCamposSeleccionados();

                    var table = document.getElementById('bodyDetablaDeuda');
                    var rowCount = table.rows.length;
                    var row = table.insertRow(rowCount);

                    var celdaTotal = row.insertCell(0);

                    row.insertCell(1);
                    row.insertCell(2);
                    var celdaTextoTotal = row.insertCell(3);
                    var celdaMontoSocietario = row.insertCell(4);
                    var celdaMontoServicios = row.insertCell(5);
                    var celdaMontoTotal = row.insertCell(6);
                    celdaTextoTotal.innerHTML = "TOTAL: ";
                    celdaMontoSocietario.innerHTML = totalRecaudadoXAbonos;
                    celdaMontoServicios.innerHTML = totalRecaudadoXServicios;
                    celdaMontoTotal.innerHTML = totalRecaudado;


                    if(totalSocios>0)
                    {
                        $("#opcionesDeListado").show("slow");
                    }
                    else
                    {
                        $("#resultado").html("No se han encontrado resultados");
                        $("#opcionesDeListado").hide("slow");
                    }
                }
        });
    }

function generarDeudas()
{
	var parametros = {
                "anio" : $('#campoAnio').val(),
                "mes" : $('#campoMes').val(),
				"cobrador" : $('#campoCobrador').val()
                
        };
        $.ajax({
                data:  parametros,
                url:   '<?= site_url("/CDeuda/Generar") ?>',
                type:  'post',
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    //Download progress
                    xhr.addEventListener("progress", function (evt) {
                        console.log(evt.lengthComputable); // false
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            console.log(Math.round(percentComplete * 100) + "%");
                        }
                    }, false)
                    return xhr;
                },
                beforeSend: function () {
                        $("#modal_Cargando").modal({backdrop: 'static', keyboard: false});
                        $("#modal_Cargando").modal('show');
                },
                success:  function (response) {
                        $("#modal_Cargando").modal('hide');

                }
        });
}


function actualizarCobradores()
{
    $.ajax({
                url:   '<?= site_url("/CCobrador/CobradoresSeleccion") ?>',
                type:  'post',
                success:  function (response) {
          $('#CobradoresSeleccion').html(response);
                }});
}

function actualizarCobradores()
{
    $.ajax({
                url:   '<?= site_url("/CCobrador/CobradoresSeleccion") ?>',
                type:  'post',
                success:  function (response) {
          $('#CobradoresSeleccion').html(response);
                }});
}


function verCupones()
    {

        año =  $('#campoAnio').val();
        mes=$('#campoMes').val(),
        cobrador= $('#campoCobrador').val();
        window.location = '<?= site_url("/CDeuda/Cupones")?>?anio='+año+'&mes='+mes+'&cobrador='+cobrador;
    }



$(document).ready(function() {
    $(".columnaPago").hide();
})

function exportarA(tipo)
{
  $(".columnaPago").show();
  if(tipo=='pdf')
  {
  $('#reporte').tableExport({type:tipo,escape:'false'});  
  }
  $('#tablaDeudas').tableExport({type:tipo,escape:'false'});
  $(".columnaPago").hide();
}


function totalARecaudar()
{
    totalRecaudadoXServicios=0;
    totalRecaudadoXAbonos=0;
    totalRecaudado=0;
    totalSocios=0;

  $("#tablaDeudas tbody tr").each(function (index) 
        {
            totalSocios+=1;
            $(this).children("td").each(function (index2) 
            {
                switch (index2) 
                {
                    case 4: totalRecaudadoXAbonos = totalRecaudadoXAbonos+ parseInt($(this).text());
                        break;
                    case 5: totalRecaudadoXServicios = totalRecaudadoXServicios+ parseInt($(this).text());
                        break;
                    case 6: totalRecaudado = totalRecaudado+ parseInt($(this).text());

                            break;
                }
            })
        })
}


    function fechaActualEnCampos()
    {
        var fechaActual = new Date();
        var mesActual = fechaActual.getMonth()+1;
        var anioActual = fechaActual.getFullYear();
        $('#campoMes').val(mesActual);
        $('#campoAnio').val(anioActual);
    }

</script>

 

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<body onload="comprobarCampos(),actualizarCobradores(),$('#opcionesDeListado').hide(),fechaActualEnCampos()">
<div class="container">    
 	<style>
		.centrar
	{
		position: absolute;
		/*nos posicionamos en el centro del navegador*/
		top:50%;
		left:50%;
		/*determinamos una anchura*/
			width:400px;
		/*indicamos que el margen izquierdo, es la mitad de la anchura*/
		margin:-200px;
		/*determinamos una altura*/
		height:300px;
		/*indicamos que el margen superior, es la mitad de la altura*/
		margin-top:-200px;
		border:10px solid #808080;
		padding:50px;
		background-color:#ECE4E4;
	  color:#000000
	}

	</style>
  <div class="centrar">
  <div class="col-lg-12"><h3 style="text-align:center">Cuotas de socios</h3></div>
  	
    <table id="tabla" style="border-spacing:10px;border-collapse: separate;">
        <form id="formularioCuota" method="post">
   	  <tr>
     	   	<td style="text-align:left"><label>Año</label></td>
        	<td><input id="campoAnio" onchange="comprobarCampos();" onkeyup="this.onchange();" name="anio" type="number" value="2016"/></td>
        </tr>

      	<tr>
     	   	<td style="text-align:left"><label>Mes</label></td>
            <td><select name="mes" id="campoMes">
                    <option value="1" selected>Enero</option>
                    <option value="2">Febrero</option>
                    <option value="3">Marzo</option>
                    <option value="4">Abril</option>
                    <option value="5">Mayo</option>
                    <option value="6">Junio</option>
                    <option value="7">Julio</option>
                    <option value="8">Agosto</option>
                    <option value="9">Septiembre</option>
                    <option value="10">Octubre</option>
                    <option value="11">Noviembre</option>
                    <option value="12">Diciembre</option>
				</select> 
			</td>
        	
        </tr>
         <tr>
  		 <td style="text-align:left"><label>Cobrador</label></td>
		 <td><div id="CobradoresSeleccion"></div></td>
  		</tr>    
      </form>
</table>
<p><center>
  <button type="button" class="btn btn-success" onClick="generarDeudas()" id="botonGenerar">Generar</button>&nbsp;
  <button type="button" class="btn btn-info" onClick="obtenerDeudas()" id="botonHistorial">Listar</button>&nbsp;
  <button type="button" class="btn btn-primary" onClick="verCupones()">Cupón</button>&nbsp;
  </center>
  </p>
		
  </div>

</div>

<!-- Aca va la parte de cuando quiere consultar algo -->

<div class="container" style="position: relative; top:400px" id="caja">    

  <div class="row text-justify cajas">

          


  <div class="row">
  <div class="col-lg-6"><h1>Listado de deudas</h1></div>
  <div class="col-lg-6 text-right">

  <!--<form><input id="campoBusqueda" onchange="actualizarLista($(this).val());" onkeyup="this.onchange();" type="text" size="30" placeholder="Buscar..."></form></div>-->
  
  <div id="opcionesDeListado"><h3>Opciones de listado:</h3>
  <h4><a href="#" onClick ="exportarA('excel');">Exportar a Excel</a></h4>
  <h4><a href="#" onClick ="ImprimirReporte();">Imprimir Reporte</a></h4>
  </div>
  </div>

  <div id="reporte">
  	 <table id='tablaDeudas' class="table">
      <thead>
        <tr>
        <th>Socio</th>
        <th>Apellido y nombre</th>
        <th>Domicilio</th>
        <th>Mes</th>
        <th>Abono</th>
        <th>Serv.</th>
        <th>Total</th>
        <th class="columnaPago">Pagó</th>
        </tr>
      </thead>

    <tbody id="bodyDetablaDeuda" class="cursorManito">
    </tbody>
    </table>
    
    </div>
    
	
    <span id="resultado"></span>


    <h3 style="text-align: right;" id="textoTotalARecaudar"></h3>

    </div>

</div>





<!-- Modal Cargando -->
<div id="modal_Cargando" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title" style="color: black">Cargando....</h4>
      </div>
      <div class="modal-body" style="color: black">
      <h3>Aguarde un momento por favor...</h3>
      <p>Esta operación puede tardar algunos minutos.</p>
      <p>Por favor, no cierre esta ventana</p>
        <center><img src="<?php echo base_url('images/loading.gif')?>"></center>
       </div>
    </div>
  </div>
</div>

</body>

</html>