<!------------------ JAVASCRIPT --------------------> 
<script>


//Este es para los campos de la ventana SOLICITAR PREINSCRIPCION
function comprobarCampos()
{
  if($('#campoNombre').val()=='' || $('#campoValor').val()=='' ||
    $('#campoFechaModificacion').val()=='') 
  {
    $('#botonAceptar').prop('disabled', true);
    $("#advertenciaDatos").text("Faltan datos obligatorios");
  }
  else
	  {
   		$('#botonAceptar').prop('disabled', false); 
   		$("#advertenciaDatos").text("");
  	  }   
  }



//Vacia los campos de las 2 ventanas
function vaciarCampos()
{
        $('#campoNombre').val("");
        $('#campoValor').val("");
        $('#campoFechaModificacion').val("");
         
        comprobarCampos();
}
function editarParametro()
{
  var i=1;
  while($("#radioSelecc"+i).is(":checked")==false)
  {
    i++;
  }
  var valorRadio = $("#radioSelecc"+i).val();


  var parametros = {
        "nombre" : $('#campoNombre').val(),
        "valor" : $('#campoValor').val(),
        "fechaModificacion" : $('#campoFechaModificacion').val(),

        };
        $.ajax({
                data:  parametros,
                url:   '<?= site_url("/CParametro/EditarParametro") ?>/'+$('#radioSelecc'+i).val(),
                type:  'post',
                beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                },
                success:  function (response) {
                        $("#resultado").html(response);
            actualizarLista();
            vaciarCampos();
                }
        });
}
//Esta funcion es para instertar los elementos de la base de datos en la tabla de html (la vista)
function insertarElementoEnTabla(nombre,valor,fechaModificacion,idParametro)
{
var table = document.getElementById('bodytablaParametros');
var rowCount = table.rows.length;
var row = table.insertRow(rowCount);

var celdaID = row.insertCell(0);
var celdaNombre = row.insertCell(1);
var celdaValor = row.insertCell(2);
var celdaFechaModificacion = row.insertCell(3);
var celdaSeleccion = row.insertCell(4);

celdaID.innerHTML = idParametro;
celdaNombre.innerHTML = nombre;
celdaValor.innerHTML = valor;
celdaFechaModificacion.innerHTML = fechaModificacion;
celdaSeleccion.innerHTML = '<input type="radio" id="radioSelecc'+rowCount+'" name="idElegido" value="'+idParametro+'">';
}

//Funcion para actualizar lista ya sea en una busqueda o cuando se agrega/elimina una preinscripcion
function actualizarLista()
{
	
   $.ajax({
                url:   '<?= site_url("/CParametro/ObtenerParametros") ?>',
                type:  'post',
				dataType: "json",
                success:  function (response)
				{
					// La siguiente línea es para borrar todas las filas excepto la primera (sería la cabecera)
                 
				  $("#bodytablaParametros").children().remove();
	          $.each(response.parametros, function(i, item) {
                    
                    // Asigno a la variable parametro, cada parametro que se recorre del archivo JSON
                    var parametro = response.parametros[i];

                    // Inserto en la tabla cada parametro
                    insertarElementoEnTabla(parametro.nombre,parametro.valor,parametro.fechaModificacion, parametro.idParametro);
                });

                }});
}



//Funcion para obtener un parametro para editarlo
function obtenerParametro()
{
  var i=0;
  var table = document.getElementById('bodytablaParametros');
  var rowCount = table.rows.length;

  while($("#radioSelecc"+i).is(":checked")==false && i<=rowCount)
  {
    i++;
  }
	//Si no hay seleccionado
		if(i>rowCount)
			{
			window.alert("Debe seleccionar un parámetro");
			}
    
			else{
			  $("#ventanaEditar").modal("show");
			  
			  
  var numeroRadio =  $("#radioSelecc"+i).val();
 
  $.ajax({
    url:'<?= site_url("/CParametro/ObtenerParametro") ?>/'+numeroRadio,
    type:  'get',
    dataType: 'json',
    success: function(res) {
        $('#campoNombre').val(res.nombre);
        $('#campoValor').val(res.valor);
        $('#campoFechaModificacion').val(res.fechaModificacion);

       comprobarCampos();
  	}});
  }
}


//Esto de aca abajo es para cuando ahces click en una fila de la tabla seleccione esa preinscripcion


$(document).ready(function(){
$("#bodytablaParametros").delegate("tr", "click", function(e) {
    $("#radioSelecc"+$(e.currentTarget).index()+"").prop("checked", true); 

    $("#bodytablaParametros").children().css("font-weight", "normal");
    $("#bodytablaParametros tr:eq("+$(e.currentTarget).index()+")").css("font-weight", "bold");

});
});
</script>

<!-- Elementos de la pagina -->
<body onLoad="actualizarLista()">
 <div class="container">    
  <div class="row text-justify cajas">
       <form>
        	<p>
	          <button type="button" class="btn btn-warning" onClick="obtenerParametro()">Editar Datos</button>&nbsp;
            <a  type="button" class="btn btn-info" style="float: right;" href="#myModal" data-toggle="modal">Ayuda</a>

            </p>
  
  <div class="row">
   
   <div class="col-lg-6"> 
    <h1>Datos del Sistema</h1> 
   </div>
   
   <div class="col-lg-6 text-right" style="padding-top:30px;">
  		
   </div>
  </div>


  <!------------------ TABLA DE DATOS --------------------> 

  <table id='tablaParametros' class="table table-hover">
  <thead>
        <tr>
        <th> ID </th>
        <th> Nombre/Descripcion</th> 
     	<th> Valor </th>
		<th> Fecha de última modificación </th>
        <th> Seleccion</th>
        </tr>
  </thead>
    <tbody id="bodytablaParametros" class="cursorManito">
    </tbody>
  </table>
</form>
  </div>
  <span id="resultado"></span>
  </div>


  <!------------------ VENTANAS MODIFICAR --------------------> 

<!-- Modificar -->
<div id="ventanaEditar"  style="color:black;"class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
 
 <div class="modal-content">
      <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Editar valores del sistema</h4>
      </div>
      
<div class="modal-body">
		
<form id="datos">
<div class="tab-content">


  <!-- Datos Personales del solicitante-->
  <div id="datosPersonales" class="tab-pane fade in active">
      <h3>Valores Actuales</h3>
  <table style="border-spacing:5px;border-collapse: separate;">
  <tr>
  	<td style="text-align:right"><label>Nombre</label></td>
  	<td><input id="campoNombre" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="nombre" type="text" /></td>
  </tr>
  <tr>
  	<td style="text-align:right"><label>Valor</label></td>
  	<td><input id="campoValor" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="valor" type="text" /></td>
  </tr>
  <tr>
  	<td style="text-align:right"><label>Ultima Modificacion</label></td>
  	<td><input id="campoFechaModificacion" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="fechaModificacion" type="date"  disabled  /></td>
  </tr>	          
   
</table>
  </div>
  
</table>

  </div>

  

 
</div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" href="javascript:;" onClick="editarParametro(),comprobarCampos();return false;" >Cambiar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
      </form>
    </div>
  </div>
</div>  



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: black">Módulo de Datos del sistema</h4>
      </div>
      <div class="modal-body" style="color: black">
        <p align="justify">En este apartado del sistema se guarda informacion relevante para el mismo. Los datos que se guardan, y que se pueden modificar en este modulo son: </p>
        <p><u>Valor de preinscripción</u></p>
        <p align="justify">Este es el valor (en pesos) que se tomará para avisar a las personas que se quieren asociar al club como parámetro de monto.</p>

        <p><u>Intervalo de BackUP</u></p>
        <p align="justify">Este valor se corresponde en semanas para saber cada cuanto el usuario desea realizar un BackUP de los datos del sistema.</p>
        <p align="justify">Este modulo tambien guarda la ultima modificacion de los datos para llevar un mejor control y facilitar el uso del mismo.</p>

        

       </div>
      <div class="modal-footer">
        <center><button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button></center>
      </div>
    </div>

  </div>
</div>



</body>

</html>