<!------------------ JAVASCRIPT --------------------> 
<script>



//Funcion para agregar una preinscripcion con los valores ingresados
function agregarPreinscripcion(){
        var parametros = {
               	"nombre" : ($('#campoNombre').val()).toUpperCase(),
                "apellidos" : ($('#campoApellidos').val()).toUpperCase(),
                "dni" : $('#campoDNI').val(),
                "domicilio" : ($('#campoDomicilio').val()).toUpperCase(),
            "genero" : $('#campoGenero').val(),
        "correoElectronico" : $('#campoCorreoElectronico').val(),
        "telefonoFijo" : $('#campoTelefonoFijo').val(),
        "telefonoCelular" : $('#campoTelefonoCelular').val(),
        "nacionalidad" : ($('#campoNacionalidad').val()).toUpperCase(),
        "estadoCivil" : ($('#campoEstadoCivil').val()).toUpperCase(),
        "fechaNacimiento" : $('#campoFechaNacimiento').val(),
        "profesion" : ($('#campoProfesion').val()).toUpperCase(),    
		
        };
		
        $.ajax({
                data:  parametros,
                url:   '<?= site_url("/CPreinscripcion/agregar") ?>',
                type:  'post',
                success:  function (response)
				 {
					if(response=='exito')
						{
						  $("#resultado").html(response);
						  actualizarLista();
						  vaciarCampos();
                		}
					else
						{
						  window.alert("El dni solicitante ya pertenece al sistema");
						  		
						}
				 }
				});
}

//Funcion para agregar el socio una vez confirmada la preinscripcion con los valores cargados y faltantes del socio
function agregarSocio(){
        var parametros = {
                "nombre" : ($('#campoNombre2').val()).toUpperCase(),
                "apellidos" : ($('#campoApellidos2').val()).toUpperCase(),
                "dni" : $('#campoDNI2').val(),
                "domicilio" : ($('#campoDomicilio2').val()).toUpperCase(),
        "correoElectronico" : $('#campoCorreoElectronico2').val(),
        "fechaNacimiento" : $('#campoFechaNacimiento2').val(),
        "profesion" : ($('#campoProfesion2').val()).toUpperCase(),
        "genero" : $('#campoGenero2').val(),
	    "nacionalidad" : ($('#campoNacionalidad2').val()).toUpperCase(),
        "estadoCivil" : ($('#campoEstadoCivil2').val()).toUpperCase(),
		"telefonoFijo" : $('#campoTelefonoFijo2').val(),
		"telefonoCelular" : $('#campoTelefonoCelular2').val(),
	
		//Datos del socio
        "domicilioDeCobro": $('#campoDomicilioDeCobro').val(),
        "intervaloDePago": $('#campoIntervaloDePago').val(),
        "tipoSocio": $('#campoTipoSocio').val(),
		"idCobrador": $('#campoCobrador').val()
        };
        $.ajax({
                data:  parametros,
                url:   '<?= site_url("/CSocio/Agregar") ?>',
                type:  'post',
                beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                },
                success:  function (response) {
                        $("#resultado").html(response);
            actualizarLista();
            vaciarCampos();
                }
        });
}


//Este es para los campos de la ventana SOLICITAR PREINSCRIPCION
function comprobarCampos()
{
  if($('#campoNombre').val()=='' || $('#campoApellidos').val()=='' ||
    $('#campoDNI').val()==0 || $('#campoDomicilio').val()=='' ||
    $('#campoCorreoElectronico').val()=='' || $('#campoFechaNacimiento').val()=='' ||
    $('#campoDomicilio').val()=='')
  {
    $('#botonAceptar').prop('disabled', true);
    $("#advertenciaDatos").text("Faltan datos obligatorios");
  }
  else
	  {
   		$('#botonAceptar').prop('disabled', false); 
   		$("#advertenciaDatos").text("");
  	  }   
  }

//Este es para los campos de la ventana CONFIRMAR SOCIO
function comprobarCampos2()
{
  if($('#campoNombre2').val()=='' || $('#campoApellidos2').val()=='' ||
    $('#campoDNI2').val()==0 || $('#campoDomicilio2').val()=='' ||
    $('#campoCorreoElectronico2').val()=='' || $('#campoFechaNacimiento2').val()=='' ||
    $('#campoDomicilioDeCobro2').val()=='' || $('#campoIntervaloDePago2').val()==0 || 
    $('#campoDomicilio2').val()=='' || $('#campoDomicilio2').val()=='')
  {
    $('#botonAceptar2').prop('disabled', true);
    $("#advertenciaDatos2").text("Faltan datos obligatorios");
  }
  else
	  {
   		$('#botonAceptar2').prop('disabled', false); 
   		$("#advertenciaDatos2").text("");
  	  }   
  }

//Vacia los campos de las 2 ventanas
function vaciarCampos()
{
        $('#campoNombre').val("");
        $('#campoApellidos').val("");
        $('#campoDNI').val("");
        $('#campoDomicilio').val("");
        $('#campoCorreoElectronico').val("");
        $('#campoFechaNacimiento').val("");
        $('#campoProfesion').val("");
        $('#campoGenero').val("");
		$('#campoNacionalidad').val("");
		$('#campoTelefonoFijo').val("");
		$('#campoTelefonoCelular').val("");
		$('#campoEstadoCivil').val("");


		
		$('#campoNombre2').val("");
        $('#campoApellidos2').val("");
        $('#campoDNI2').val("");
        $('#campoDomicilio2').val("");
        $('#campoCorreoElectronico2').val("");
        $('#campoFechaNacimiento2').val("");
        $('#campoProfesion2').val("");
        $('#campoGenero2').val("");
		$('#campoNacionalidad2').val("");
		$('#campoTelefonoFijo2').val("");
		$('#campoTelefonoCelular2').val("");
		$('#campoEstadoCivil2').val("");

      
        comprobarCampos();
}
</script>



<script>
//Esta funcion es para instertar los elementos de la base de datos en la tabla de html (la vista)
function insertarElementoEnTabla(dni,nombre,apellidos,fechaSolicitud,fechaVencimiento,monto,preinscripcionID)
{
var table = document.getElementById('bodylistaDePreinscriptos');
var rowCount = table.rows.length;
var row = table.insertRow(rowCount);

var celdaDni = row.insertCell(0);
var celdaNombre = row.insertCell(1);
var celdaApellidos = row.insertCell(2);
var celdaFechaSolcitud = row.insertCell(3);
var celdaFechaVencimiento = row.insertCell(4);
var celdaMonto = row.insertCell(5);
var celdaSeleccion = row.insertCell(6);



celdaDni.innerHTML = dni;
celdaNombre.innerHTML = nombre;
celdaApellidos.innerHTML = apellidos;
celdaFechaSolcitud.innerHTML = fechaSolicitud;
celdaFechaVencimiento.innerHTML = fechaVencimiento;
celdaMonto.innerHTML = "$ "+monto;
celdaSeleccion.innerHTML = '<input type="radio" id="radio_'+rowCount+'" name="idElegido" value="'+preinscripcionID+'">';
}

//Funcion para actualizar lista ya sea en una busqueda o cuando se agrega/elimina una preinscripcion
function actualizarLista()
{
	var parametros = 
    {
                "valorBusqueda" : $('#campoBusqueda').val()
    };
   $.ajax({
                url:   '<?= site_url("/CPreinscripcion/obtenerPreinscriptos") ?>',
                type:  'post',
				dataType: "json",
				data: parametros,
                success:  function (response)
				{
					// La siguiente línea es para borrar todas las filas excepto la primera (sería la cabecera)
             console.log("Hola");
				  $("#bodylistaDePreinscriptos").children().remove();
	          $.each(response.preinscripciones, function(i, item) {
                    
                    // Asigno a la variable preinscripto, cada socio que se recorre del archivo JSON
                    var preinscripcion = response.preinscripciones[i];

                    // Inserto en la tabla cada preinscripcion
                    insertarElementoEnTabla(preinscripcion.dni,preinscripcion.nombre,preinscripcion.apellidos,preinscripcion.fechaSolicitud,preinscripcion.fechaVencimiento, preinscripcion.monto, preinscripcion.idPreinscripcion);
                });

                }});
}

//Funcion para actualizar los tipos de socios, es necesario para confirmar un socio (se le asigna un tipo)
function actualizarTipoSocios()
{
    $.ajax({
                url:   '<?= site_url("/CTipoSocio/TiposSociosEnSelect") ?>',
                type:  'post',
                success:  function (response) {
          $('#tipoSociosEnSelect').html(response);
                }});
}

function actualizarCobradores()
{
    $.ajax({
                url:   '<?= site_url("/CCobrador/CobradoresSeleccion") ?>',
                type:  'post',
                success:  function (response) {
          $('#CobradoresSeleccion').html(response);
                }});
}

function comprobarVencidas()
{
 $.ajax({
                url:   '<?= site_url("/CPreinscripcion/eliminarVencidas") ?>',
                type:  'post',
				dataType: "json",
                success:  function (response)
				{
					window.alert("Se eliminaron preinscripciones que vencieron");
				}});
}

//Funcion para eliminar una preinscripcion de forma manual porque el usuario la quiere eliminar
function eliminarPreinscripto()
{
	var i=0;
  	var table = document.getElementById('bodylistaDePreinscriptos');
  	var rowCount = table.rows.length;

  	while($("#radio_"+i).is(":checked")==false && i<=rowCount)
  	{
    	i++;
  	}
	if(i>rowCount)
			{
			window.alert("Debe seleccionar una preinscripcion");
			}
    
	else{
									  
		 var valorRadio = $("#radio_"+i).val();
		 if (confirm("¿Realmente desea eliminarlo?")==true)
		 {
			$.ajax({
			url:'<?= site_url("/CPreinscripcion/borrar_preinscripcion") ?>/'+ valorRadio,
			type:  'post',
			beforeSend: function () 
							{
                        		$("#resultado").html("Procesando, espere por favor...");
                			},
			
			success: function (response) 
						{
							$("#resultado").html(response);
							actualizarLista();
						}
				   });
		 }
	   }
}

//Funcion para eliminar de forma automatica la preinscripcion una vez que el usuario confirma un socio.
function eliminarPreinscriptoAuto()
{
	var i=0;
  	var table = document.getElementById('bodylistaDePreinscriptos');
  	var rowCount = table.rows.length;

  	while($("#radio_"+i).is(":checked")==false && i<=rowCount)
  	{
    	i++;
  	}
	if(i>rowCount)
			{
			window.alert("Debe seleccionar una preinscripcion");
			}
    
	else{
									  
 		var valorRadio = $("#radio_"+i).val();
  		$.ajax({
    	url:'<?= site_url("/CPreinscripcion/borrar_preinscripcion") ?>/'+ valorRadio,
    	type:  'post',
    	success: function (response) {
                        				$("#resultado").html(response);
            							actualizarLista();}
		      });
		}
	
}

//Funcion para obtener una preinscripcion, ya sea para confirmar el socio o para eliminarlo
function obtenerPreinscripto()
{
  var i=0;
  var table = document.getElementById('bodylistaDePreinscriptos');
  var rowCount = table.rows.length;

  while($("#radio_"+i).is(":checked")==false && i<=rowCount)
  {
    i++;
  }
	//Si no hay seleccionado
		if(i>rowCount)
			{
			window.alert("Debe seleccionar una preinscripcion");
			}
    
			else{
			  $("#ventanaAgregar").modal("show");
			  
			  
      var valorRadio = $("#radio_"+i).val();
 
  $.ajax({
    url:'<?= site_url("/CPreinscripcion/ObtenerPreinscripto") ?>/'+ valorRadio,
    type:  'get',
    dataType: 'json',
    success: function(res) {
        $('#campoNombre2').val(res.nombre);
        $('#campoApellidos2').val(res.apellidos);
        $('#campoDNI2').val(res.dni);
        $('#campoDomicilio2').val(res.domicilio);
        $('#campoCorreoElectronico2').val(res.correoElectronico);
        $('#campoFechaNacimiento2').val(res.fechaNacimiento);
        $('#campoProfesion2').val(res.profesion);
        $('#campoGenero2').val(res.genero);
		$('#campoNacionalidad2').val(res.nacionalidad);
    	$('#campoEstadoCivil2').val(res.estadoCivil);
	    $('#campoTelefonoFijo2').val(res.telefonoFijo);
		$('#campoTelefonoCelular2').val(res.telefonoCelular);
        //$('#campoDomicilioDeCobro').val(res.domicilioCobro);
        //$('#campoIntervaloDePago').val(res.intervaloPago);
        //$('#campoTipoSocio').val("");
        comprobarCampos();
  	}});
  }
}

//Lo que esta aca abajo es una funcion para poder desplazarse por los formularios con la combinacion 'Shift + -> o <-'
var k = new Kibo();

function handler() {
  //console.log('last key: ' + k.lastKey());
  if(k.lastKey()=='right')
  {
  $('.nav-tabs a:last').tab('show');  
  }
  if(k.lastKey()=='left')
    {
      $('.nav-tabs a:first').tab('show');   
    }
  
}

//Esto de aca abajo es para cuando ahces click en una fila de la tabla seleccione esa preinscripcion
k.down(['shift right','shift left'], handler);
$(document).ready(function(){
$("#bodylistaDePreinscriptos").delegate("tr", "click", function(e) {
    $("#radio_"+$(e.currentTarget).index()+"").prop("checked", true); 

    $("#bodylistaDePreinscriptos").children().css("font-weight", "normal");
    $("#bodylistaDePreinscriptos tr:eq("+$(e.currentTarget).index()+")").css("font-weight", "bold");

});
});

</script>

<!-- Elementos de la pagina -->
<body onLoad="actualizarLista(),actualizarTipoSocios(), actualizarCobradores(),comprobarVencidas()">
 <div class="container">    
  <div class="row text-justify cajas">
       <form>
        	<p>
	          <button type="button" class="btn btn-success" onClick="obtenerPreinscripto()">Confirmar Socio</button>&nbsp;
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ventanaSolicitar" > Solicitar 						alta</button>&nbsp;
              <button type="button" class="btn btn-danger" onClick="eliminarPreinscripto()">Eliminar Preinscripción</button>&nbsp;
              <a  type="button" class="btn btn-info" style="float: right;" href="#myModal" data-toggle="modal">Ayuda</a>
            </p>
  
  <div class="row">
   
   <div class="col-lg-6"> 
    <h1>Lista de preinscripciones</h1> 
   </div>
   
   <div class="col-lg-6 text-right" style="padding-top:30px;">
  		<form><input id="campoBusqueda" onChange="actualizarLista($(this).val());" onKeyUp="this.onchange();" type="text" size="30" placeholder="Buscar..."></form>
   </div>
  </div>

  <!------------------ TABLA DE DATOS --------------------> 

  <table id='listaDePreinscriptos' class="table table-hover">
  <thead>
        <tr>
        <th> DNI Solicitante</th> 
     	<th> Nombre </th>
		<th> Apellido </th>
        <th>Fecha Solicitud</th>
		<th> Fecha Vencimiento </th>
		<th> A pagar </th>
		<th> Seleccion</th>
        </tr>
  </thead>
    <tbody id="bodylistaDePreinscriptos" class="cursorManito">
    </tbody>
  </table>
</form>
  </div>
  <span id="resultado"></span>
  </div>


  <!------------------ VENTANAS AGREGAR SOCIO / SOLICITAR PREINSCRIPCION --------------------> 

<!-- Solicitar alta -->
<div id="ventanaSolicitar"  style="color:black;" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
 
 <div class="modal-content">
      <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Solicitud de inscripcion a Parque Sur</h4>
      </div>
      
<div class="modal-body">
		<ul class="nav nav-tabs">
  			<li class="active"><a data-toggle="tab" href="#datosPersonales">Datos personales</a></li>
		</ul>

<form id="datos">
<div class="tab-content">


  <!-- Datos Personales del solicitante-->
  <div id="datosPersonales" class="tab-pane fade in active">
      <h3>Datos Personales</h3>
  <table style="border-spacing:5px;border-collapse: separate;">
  <tr>
  	<td style="text-align:right"><label>Nombre</label></td>
  	<td><input id="campoNombre" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="nombre" type="text" /></td>
  </tr>
  <tr>
  	<td style="text-align:right"><label>Apellidos</label></td>
  	<td><input id="campoApellidos" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="apellidos" type="text" /></td>
  </tr>
  <tr>
  	<td style="text-align:right"><label>DNI</label></td>
  	<td><input id="campoDNI" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="dni" type="number" /></td>
  </tr>	          
 <tr>
  	<td style="text-align:right"><label>Domicilio</label></td>
  	<td><input id="campoDomicilio" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="domicilio" type="text" /></td>
 </tr>  
 <tr>
  	<td style="text-align:right"><label>Genero</label></td>
  	<td><select name="genero" id="campoGenero">
  			<option value="1">Masculino</option>
			<option value="0">Femenino</option>
		</select></td>
  </tr> 
 <tr>
  	<td style="text-align:right"><label>Correo electrónico</label></td>
  	<td> <input id="campoCorreoElectronico" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="correoElectronico" type="email" /></td>
  </tr>
  
  
 <tr>
    <td style="text-align:right"><label>Telefono fijo</label></td>
    <td> <input id="campoTelefonoFijo" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="number" /></td>
 </tr>


      <tr>
          <td style="text-align:right"><label>Telefono Celular</label></td>
          <td> <input id="campoTelefonoCelular" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="number" /></td>
      </tr>



      <tr>
          <td style="text-align:right"><label>Nacionalidad</label></td>
          <td> <input id="campoNacionalidad" onChange="comprobarCampos();" value="Argentino" onKeyUp="this.onchange();" type="text" /></td>
      </tr>


      <tr>
          <td style="text-align:right"><label>Estado Civil</label></td>
          <td> <input id="campoEstadoCivil" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="text" /></td>
      </tr>
  

  
 <tr>
  	<td style="text-align:right"><label>Profesión</label></td>
  	<td> <input id="campoProfesion" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="profesion" type="text" /></td>
  </tr>
  <tr>
  	<td style="text-align:right" ><label>Fecha de Nacimiento</label></td>
  	<td> <input id="campoFechaNacimiento" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="fechaDeNacimiento" type="date" /></td>
  </tr>     
</table>
  </div>
  
</table>

  </div>

  

 
</div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" href="javascript:;" onClick="agregarPreinscripcion(),comprobarCampos();return false;" >Aceptar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
      </form>
    </div>
  </div>
</div>  
 
 
 
 
<!--Confirmar socio-->
  
<div id="ventanaAgregar"  style="color:black;"class="modal fade" role="dialog">
  <div class="modal-dialog">
   
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agregar Socio</h4>
      </div>

      <div class="modal-body">

<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#datosPersonalesSocio">Datos personales</a></li>
  <li><a data-toggle="tab" href="#datosDelSocio">Socio</a></li>
</ul>



<form id="datos">
<div class="tab-content">

  <!-- Datos Personales-->
  <div id="datosPersonalesSocio" class="tab-pane fade in active">
      <h3>Datos Personales</h3>
  <table style="border-spacing:5px;border-collapse: separate;">
  <tr>
  <td style="text-align:right"><label>Nombre</label></td>
  <td><input id="campoNombre2" onChange="comprobarCampos2();" onKeyUp="this.onchange();" name="nombre2" type="text" /></td>
  </tr>

  <tr>
  <td style="text-align:right"><label>Apellidos</label></td>
  <td><input id="campoApellidos2" onChange="comprobarCampos2();" onKeyUp="this.onchange();" name="apellidos2" type="text" /></td>
  </tr>

  <tr>
  <td style="text-align:right"><label>DNI</label></td>
  <td><input id="campoDNI2" onChange="comprobarCampos2();" onKeyUp="this.onchange();" name="dni2" type="number" /></td>
  </tr>	          

  <tr>
  <td style="text-align:right"><label>Domicilio</label></td>
  <td><input id="campoDomicilio2" onChange="comprobarCampos2();" onKeyUp="this.onchange();" name="domicilio2" type="text" /></td>
  </tr>

  <tr>
  <td style="text-align:right"><label>Genero</label></td>
  <td><select name="genero2" id="campoGenero2">
  			<option value="1">Masculino</option>
			<option value="0">Femenino</option>
	   </select>
  </td>
  </tr> 

  <tr>
  <td style="text-align:right"><label>Correo electrónico</label></td>
  <td> <input id="campoCorreoElectronico2" onChange="comprobarCampos2();" onKeyUp="this.onchange();" name="correoElectronico2" type="email" /></td>
  </tr>
  
  
  
 <tr>
    <td style="text-align:right"><label>Telefono fijo</label></td>
    <td> <input id="campoTelefonoFijo2" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="number" /></td>
 </tr>


      <tr>
          <td style="text-align:right"><label>Telefono Celular</label></td>
          <td> <input id="campoTelefonoCelular2" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="number" /></td>
      </tr>



      <tr>
          <td style="text-align:right"><label>Nacionalidad</label></td>
          <td> <input id="campoNacionalidad2" onChange="comprobarCampos();" value="Argentino" onKeyUp="this.onchange();" type="text" /></td>
      </tr>


      <tr>
          <td style="text-align:right"><label>Estado Civil</label></td>
          <td> <input id="campoEstadoCivil2" onChange="comprobarCampos();" onKeyUp="this.onchange();" type="text" /></td>
      </tr>
  

  

  <tr>
  <td style="text-align:right"><label>Profesión</label></td>
  <td> <input id="campoProfesion2"  name="profesion2" type="text" /></td>
  </tr>

  <tr>
  <td style="text-align:right" ><label>Fecha de Nacimiento</label></td>
  <td> <input id="campoFechaNacimiento2" onChange="comprobarCampos2();" onKeyUp="this.onchange();" name="fechaDeNacimiento2" type="date" /></td>
  </tr>     

</table>
  </div>

  

  <!-- Datos del socio -->

  <div id="datosDelSocio" class="tab-pane fade">
    <h3>Datos del Socio</h3>
    <table style="border-spacing:5px;border-collapse: separate;">
      <tr>
	  <td style="text-align:right"><label>Domicilio de Cobro</label></td>
	  <td> <input id="campoDomicilioDeCobro2" onChange="comprobarCampos2();" onKeyUp="this.onchange();" name="domicilioDeCobro2" type="text" /></td>
	  </tr>

  <tr>
  <td style="text-align:right"><label>Intervalo de pago (expresado en meses)</label></td>
  <td> <input id="campoIntervaloDePago2" onChange="comprobarCampos2();" onKeyUp="this.onchange();" name="intervaloDePago2" type="number" /></td>
  </tr> 
  
  <tr>
  	<td style="text-align:right"><label>Cobrador</label></td>
	<td><div id="CobradoresSeleccion"></div></td>
  </tr>    

      <tr>
	  <td style="text-align:right"><label>Tipo de socios</label></td>
	  <td><div id="tipoSociosEnSelect"></div></td>
	  </tr>     

</table>
  </div>
</div>

      </div>
      <div class="modal-footer">
      <span id="advertenciaDatos2" style="color:red;"></span>
        <button type="button" id="botonAceptar2" class="btn btn-default" data-dismiss="modal" href="javascript:;" onClick="agregarSocio(), eliminarPreinscriptoAuto();return false;" >Aceptar</button>

        <button type="button" class="btn btn-default" data-dismiss="modal" href="javascript:;" onClick="vaciarCampos()">Cancelar</button>

      </div>
      </form>
    </div>
  </div>
</div>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: black">Módulo de preinscripciones</h4>
      </div>
      <div class="modal-body" style="color: black">
        <p align="justify">Este es el módulo de Preinscripciones. En pantalla se puede observar el listado de las personas que solicitan asociarse al club. Las funciones disponibles en esta sección son: </p>
        <p><u>Confirmar Socio:</u></p>
        <p align="justify">Seleccione una preinscripción del listado y haga click en el botón "Confirmar Socio". El sistema solicitará los datos de socio faltantes para culminar la preinscripcíon y luego se guardará el socio nuevo, desarpareciendo la presincripcion solicitante.</p>
        <p><u>Solicitar alta:</u></p>
        <p align="justify">Esta opción está pensada para cuando una persona se acerca a la secretaría para asociarse al club pero queda pendiente de pago el abono para culminar la transacción. En esta funcion se solicitarán los datos personales del interesado/a, y quedará pendiente su carga de datos social para cuando se efectúe el pago y se utilice la primer opción de este módulo.</p>

        <p align="justify"><u>Eliminar Preinscripción:</u></p>
        <p>Esta opción elimina definitivamente una preinscripción. Para usarla, seleccione una preinscripción y haga click en "Eliminar Preinscripcion", donde previa confirmación se eliminaran por completo los datos de dicha preiniscripcion</p>

        <p>Además, el sistema cuenta con una eliminiacion automática de preinscripciones que superan los 30 días de solicitud</p>
      </div>
      <div class="modal-footer">
        <center><button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button></center>
      </div>
    </div>

  </div>
</div>


</body>

</html>