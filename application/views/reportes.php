<?php $this->load->helper('url'); ?>

<!doctype html>
<html lang="es">
<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="/parquesur/img/apple-icon.png" />
  <link rel="icon" type="image/png" href="" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="/parquesur//css/bootstrap.min.css" rel="stylesheet" />

    <!--  Material Dashboard CSS    -->
    <link href="/parquesur/css/material-dashboard.css" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="/parquesur/css/demo.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>

  <!--   Core JS Files   -->
  <script src="/parquesur/js/jquery-3.1.0.min.js" type="text/javascript"></script>
  <script src="/parquesur/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="/parquesur/js/material.min.js" type="text/javascript"></script>

  <!--  Charts Plugin -->
  <script src="/parquesur/js/chartist.min.js"></script>

  <!-- Material Dashboard javascript methods -->
  <script src="/parquesur/js/material-dashboard.js"></script>

  <!-- Material Dashboard - Métodos de los gráficos -->
  <script src="/parquesur/js/funcionesgraficos.js"></script>

    <title>Club Parque Sur -=REPORTES=-</title>

<script type="text/javascript">
function obtenerTotalSocios()
{
$.ajax({
                url:   "<?=site_url("CReporte/totalSocios")?>",
                type:  'post',
                success:  function (response) 
                {                     
                  $('#cantidadSocios').html(response);
                }
        });
}


function obtenerSociosUltimoAño()
{
$.ajax({
                url:   "<?=site_url("CReporte/sociosNuevosAnio")?>",
                type:  'post',
                success:  function (response) 
                {                     
                  $('#sociosUltimoAño').html(response);
                }
        });
}


function totalRecaudadoUltimoMes()
{
$.ajax({
                url:   "<?=site_url("CReporte/totalRecaudadoUltimoMes")?>",
                type:  'post',
                success:  function (response) 
                {                     
                  $('#totalRecaudadoUltimoMes').html("$"+response);
                }
        });
}


function sociosActivos()
{
$.ajax({
                url:   "<?=site_url("CReporte/sociosActivos")?>",
                type:  'post',
                success:  function (response) 
                {                     
                  //$('#totalRecaudadoUltimoMes').html("$"+response);
                }
        });
}

</script>


</head>

<body onload="obtenerTotalSocios(),obtenerSociosUltimoAño(),totalRecaudadoUltimoMes(),demo.initDashboardPageCharts();">

  <div class="wrapper">

      <div class="sidebar" data-color="purple" data-image="/img/sidebar-1.jpg">
      <!--
            Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

            Tip 2: you can also add an image using data-image tag
        -->

      <div class="logo">
        <a href="" class="simple-text">
          Reportes
        </a>
      </div>



       <!-- MENU IZQUIERDO -->
        <div class="sidebar-wrapper">
              <ul class="nav">
                  <li class="active">
                      <a href="<?= site_url('CReporte/index') ?>">
                          <i class="material-icons">dashboard</i>
                          <p>Tabla general</p>
                      </a>
                  </li>
                   <ul class="nav">
                  <h4><i class="material-icons">person</i>Societarios</h4>
                  <li>
                      <a href="<?= site_url('CReporte/reportesSocietarios?opcion=sociosactivos') ?>">
                          
                          <p>Socios Activos</p>
                      </a>
                  </li>
                     <li>
                      <a href="<?= site_url('CReporte/reportesSocietarios?opcion=sociosMorosos') ?>">
                          
                          <p>Socios Morosos</p>
                      </a>
                  </li>
                   <li>
                      <a href="<?= site_url('CReporte/reportesSocietarios?opcion=altasybajas') ?>">
                          
                          <p>Altas y bajas</p>
                      </a>
                  </li>
                  <li>
                      <a href="<?= site_url('CReporte/reportesSocietarios?opcion=sociosconembarcaciones') ?>">
                          
                          <p>Socios con embarcaciones</p>
                      </a>
                  </li>
                  <li>
                      <a href="<?= site_url('CReporte/reportesSocietarios?opcion=sociosadherentes') ?>">
                          
                          <p>Socios Adherentes</p>
                      </a>
                  </li>

                  </ul>
                  <ul class="nav">
                  <h4><i class="material-icons">content_paste</i>Financieros</h4>
                  <li>
                      <a href="<?= site_url('CReporte/reportesFinancieros?opcion=recaudacion') ?>">
                          <p>Recaudación bruta</p>
                      </a>
                  </li>
                  <li>
                      <a href="<?= site_url('CReporte/reportesFinancieros?opcion=recaudadoxcobrador') ?>">
                          <p>Recaudado por cobrador (bruto)</p>
                      </a>
                  </li>
                  <li>
                      <a href="<?= site_url('CReporte/reportesFinancieros?opcion=recaudacionxdeporte') ?>">
                          <p>Recaudación por deporte (bruta mensual)</p>
                      </a>
                  </li>

                  </ul>
                  <ul class="nav">
                      <h4><i class="material-icons">directions_boat</i>Embarcaciones</h4>
                      <li>
                          <a href="<?= site_url('CReporte/reportesEmbarcaciones?opcion=reporteParaPrefectura') ?>">
                              <p>Reporte para prefectura</p>
                          </a>
                      </li>

                  </ul>
              </ul>
        </div>
      </div>
      <!-- FINALIZA EL MENÚ IZQUIERDO  -->

      <div class="main-panel">
      <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= base_url();?>">Volver atrás</a>
          </div>
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">


            <!---  De la siguiente manera se pueden agregar elementos en el extremo superior derecho
              <li>
                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="material-icons">dashboard</i>
                  <p class="hidden-lg hidden-md">Dashboard</p>
                </a>
              </li>
              ---->

            </ul>

          </div>
        </div>
      </nav>

      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header" data-background-color="orange">
                  <i class="material-icons">supervisor_account</i>
                </div>
                <div class="card-content">
                  <p class="category">Socios</p>
                  <h3 class="title" id="cantidadSocios"></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">date_range</i>En actividad
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header" data-background-color="green">
                  <i class="material-icons">equalizer</i>
                </div>
                <div class="card-content">
                  <p class="category">Recaudación</p>
                  <h3 class="title" id="totalRecaudadoUltimoMes"></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">date_range</i> Último mes (bruto)
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header" data-background-color="red">
                  <i class="material-icons">person_add</i>
                </div>
                <div class="card-content">
                  <p class="category">Nuevos socios</p>
                  <h3 class="title" id="sociosUltimoAño"></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">date_range</i> Último año
                  </div>
                </div>
              </div>
            </div>

           
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="card">
                <div class="card-header card-chart" data-background-color="green">
                  <div class="ct-chart" id="graficoRecaudacion"></div>
                </div>
                <div class="card-content">
                  <h4 class="title">Recaudado por mes</h4>
                  <p class="category">Vista anual.</p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> Al día de hoy
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="card">
                <div class="card-header card-chart" data-background-color="orange">
                  <div class="ct-chart" id="graficoSociosActivos"></div>
                </div>
                <div class="card-content">
                  <h4 class="title">Socios activos</h4>
                  <p class="category">Vista anual</p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> Al día de hoy
                  </div>
                </div>

              </div>
            </div>
            </div>  
    </div>
  </div>
  </div>
</body>
</html>
