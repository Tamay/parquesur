<body>
<script type="text/javascript">

var nroSocio = '<?php echo $nroSocio?>';
var nombreCompleto = '<?php echo $nombreCompleto?>';


    function imprimirEstadoCuotas() {

        //var mywindow = window.open('', 'PRINT', 'height=400,width=600');
        var mywindow = window.open('', 'PRINT');

        //Titulo de la página
        mywindow.document.write('<html><head><title> Informe detallado </title>');
        mywindow.document.write('</head><body>');
        mywindow.document.write('<h1>CLUB PARQUE SUR - Informe de cuotas</h1>');
        mywindow.document.write('<b>Nro socio:</b> '+nroSocio+'<br>');
        mywindow.document.write('<b>Nombre del socio:</b> '+nombreCompleto+'<br>');
        mywindow.document.write($('#imprimir').html());
        mywindow.document.write('</body></html>');
        mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10
            mywindow.print();
            mywindow.close();
        return true;
    }








function cerrarVentana()
{
	window.alert("Cerrar ventana");
	window.close();
}
<?php
$fechaNacimientoConFormato = new DateTime($fechaNacimiento);
$fechaAltaConFormato = new DateTime($fechaAlta);
if($fechaBaja!='')
{
$fechaBajaConFormato = new DateTime($fechaBaja);
$fechaBaja=$fechaBajaConFormato->format('d-m-Y');
}

$fechaAlta=$fechaAltaConFormato->format('d-m-Y');

$fechaNacimiento = $fechaNacimientoConFormato->format('d-m-Y');
?>
</script>
<div class="container cajas" style="margin-top: -50px;">
<div class="modal-header">
        <h3 class="modal-title"><img src="/parquesur/images/reporte.png" width="50px"/> Informe detallado</h3>
      </div>

      <div class="modal-body">
          <h4>Número de socio: <span id="span_nroSocio"><?php echo $nroSocio?></span></h4>
          <hr>
          <div id="izquierda" class="col-md-6">
      <b><u>NOMBRE COMPLETO</u>:</b> <span id="span_nombreCompleto"><?php echo $nombreCompleto?></span>
      <br>
      <b><u>DOMICILIO REAL / DE COBRO</u>:</b> <span id="span_domicilioReal"><?php echo $domicilioReal?></span> /  <span id="span_domicilioCobro"><?php echo $domicilioCobro?></span>
      <br>
      <b><u>DNI</u>:</b> <span id="span_dni"><?php echo $dni?></span>
      <br>
      <b><u>FECHA DE NACIMIENTO</u>:</b> <span id="span_fechaNacimiento"><?php echo $fechaNacimiento?></span>
      <br>
      <b><u>TELEFONO FIJO / CELULAR</u>:</b> <span id="span_telefonoFijo"><?php echo $telefonoFijo?></span> / <span id="span_telefonoCelular"><?php echo $telefonoCelular?></span>
      <br>
      <b><u>TIPO DE ABONO</u>:</b> <span id="span_tipoDeSocio"><?php echo $tiposocio?> ($<?php echo $monto_tiposocio?>)</span>
      <br>
      <b><u>E-MAIL</u>:</b> <span id="span_correoElectronico"><?php echo $correoElectronico?></span>
      <br>
      <b><u>COBRADOR</u>:</b> <span id="span_cobrador"><?php echo $cobrador?></span>
      <br>
      <b><u>PAGA CADA</u>:</b> <span id="span_intervaloPago"><?php echo $intervaloPago?></span> MES/ES </br>

      <b><u>FECHA DE INGRESO</u>:</b> <span id="span_FechaAlta"><?php echo $fechaAlta?></span> </br>

      <b><u>FECHA DE BAJA</u>:</b> <span id="span_FechaBaja"><?php echo $fechaBaja?></span>
          <br>
          <b><u>OBSERVACIONES</u>:</b> <span id="span_observaciones"><?php echo $observaciones?></span>
          <br>
      <br>

      <h4>Está inscripto a los siguientes servicios:</h4>
          <span id="span_Actividades">
          <?php foreach($deportes as $deporte)
          {
          	echo $deporte;
          	echo '<br>';
          	//FUTBOL X1 CHICO ($120)
          }
          ?>
          
          </span>
          <br>
          <span id="span_Embarcaciones">
          <?php foreach($embarcaciones as $embarcacion)
          {
          	echo $embarcacion;
          	echo '<br>';
          	//LANCHA ($600)
          }
          ?>
          </span>

      <br>
      <br>
          </div>
          <div id="imprimir" class="col-md-6">
      <h4>Paga una cuota societaria total de:  <span id="span_cuotaSocietaria">$ <?php echo $montoTotal?></span></h4>

          <h4>El estado de sus cuotas es:</h4>
          <b><u>Deuda corespondiente a los meses</u>:</b>
          <table border="1px black solid" style="text-align: center;border-collapse: collapse;width: 40%; margin: 10px 0 10px 25px">
              <thead style="font-weight: bold">
              <td>Mes-Año</td>
              <td>Monto</td>
              </thead>
              <?php
              foreach($cuotasDeuda as $cuota)
              {
                 ?>
                  <tr>
                      <td><?php echo $cuota["mesDeuda"]?></td>
                      <td><?php echo '$ '.$cuota["monto"]?></td>
                  </tr>
                 <?php
              }
              ?>
              <tfoot style="text-align: center;font-weight: bold">
              <td>Total</td>
              <td>$ <?php echo $deuda ?></td>
              </tfoot>
          </table>
          <b><u>Sus últimas cuotas pagas son:</u></b>
          <table border="1px black solid" style="text-align: center;border-collapse: collapse; width: 40%; margin: 10px 0 10px 25px">
              <thead style="font-weight: bold">
              <td>Mes-Año</td>
              <td>Cobrado el</td>
              <td>Monto</td>
              </thead>
              <?php
              foreach($cuotasPagas as $cuota)
              {
                  ?>
                  <tr>
                      <td><?php echo $cuota["mesDeuda"]?></td>
                      <td><?php echo $cuota["fechaCobro"]?></td>
                      <td><?php echo '$ '.$cuota["monto"]?></td>
                  </tr>
              <?php
              }
              ?>
              <tfoot style="text-align: center;font-weight: bold">
              <td colspan="2">Total</td>
              <td>$ <?php echo $montoPagado ?></td>
              </tfoot>
          </table></br>
          </div>

          <button onclick="imprimirEstadoCuotas()">Imprimir Estado Cuotas</button>
      </div>

      <form> 
		<input type="button" style="float: right" class="btn btn-default" value="Cerrar" onClick="top.close()" name="Cerrar página"></center>
	</form>
</div>
</body>
</html>