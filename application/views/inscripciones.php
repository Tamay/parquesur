<!------------------ JAVASCRIPT -------------------->
<html>

<link rel="stylesheet" type="text/css" href="/ParqueSur/css/tablas/css/jquery.dataTables.css">

<script type="text/javascript" src="<?php echo base_url("/js/tablas/js/jquery.dataTables.js");?>"></script>
<script>
var idInscripcionSeleccionada =-1;

function obtenerInscripcion()
{
  var valorRadio = obtenerIdSocioSeleccionado();

/// Si no hubo ninguno seleccionado
  if(valorRadio==-1)
    {
    window.alert("Debe seleccionar una inscripción");
    }
    
    else{
      $("#ventanaInscripcion").modal("show");

  $.ajax({
    url:'<?= site_url("/CInscripcion/ObtenerInscripcion") ?>/'+ valorRadio,
    type:  'get',
    dataType: 'json',
    success: function(res) {
        $('#campoSocio').val(res.socio);
        $('#campoDeporte').val(res.deporte);
        
  }});
}
}

function darDeBajaInscripcion()
{
  var valorRadio = obtenerIdSocioSeleccionado();

/// Si no hubo ninguno seleccionado
  if(valorRadio==-1)
    {
    window.alert("Debe seleccionar una inscripción");
    }
    
    else{

var respuesta = confirm("¿Está seguro que desea dar de baja esta inscripción?");
if(respuesta)
{
   $.ajax({
    url:'<?= site_url("/CInscripcion/DarDeBajaUnaInscripcion") ?>/'+ valorRadio,
    type:  'POST',
    success: function() 
    {
      window.alert("La inscripción ha sido dado de baja con éxito.")
      actualizarListaInscripciones();
    }
});
}
}
}

function inicializarListaInscripciones()
{
   $('#tabladeinscripciones').DataTable( {
        "ajax": 'CInscripcion/ObtenerInscripciones',
        "columns": [
            { "data": "id" },
            { "data": "socio_id" },
			{ "data": "dniSocio" },
            { "data": "nombreSocio" },
            { "data": "apellidoSocio" },
			{ "data": "nombreActividad" },
			{ "data": "fechaAlta" }
        ]
    });

   cargarPropiedadesTablaInscripciones(); 
}


function actualizarListaInscripciones()
{
var t = $('#tabladeinscripciones').DataTable();
//t.clear().draw()ñ
 t.ajax.reload();
}

function cargarPropiedadesTablaInscripciones()
{
    ///AQUÍ SE CARGAN LAS PROPIEDADES DE LA TABLA  
    var table = $('#tabladeinscripciones').DataTable();
                    $('#bodyDetabladeinscripciones').on( 'click', 'tr', function () {
						
                   if ( $(this).hasClass('selected') ) 
                    {
						
                        $(this).removeClass('selected');
                        idInscripcionSeleccionada=-1;
						console.log(idInscripcionSeleccionada);
                    }
                    else {
                        table.$('tr.selected').removeClass('selected');
                        $(this).addClass('selected');
                        idInscripcionSeleccionada = (this.cells[0].innerHTML);
						console.log(idInscripcionSeleccionada);
                        }
                    
                    
                    });

                    $('#button').click( function () {
                   table.row('.selected').remove().draw( false );
                    } );
                    /// Aquí termina lo relacionado con las propiedades de la tabla
}
function obtenerIdSocioSeleccionado()
{
    return idInscripcionSeleccionada;
}

</script>

<body onLoad="inicializarListaInscripciones()">
<div class="container">    

  <div class="row text-justify cajas">

       <form>

        	<p>

              <button type="button" class="btn btn-danger" onClick="darDeBajaInscripcion()">Dar de baja</button>&nbsp;
            </p>

  <div class="row">

  <div class="col-lg-6"><h1>Lista de inscripciones</h1></div>

  </div>

  <!------------------ TABLA DE DATOS --------------------> 


<table id="tabladeinscripciones" class="table table-hover cursorManito">

      <thead>

        <tr>
        <th>Nº Inscripción</th>
        <th>Nº Socio</th>
        <th>DNI</th>
        <th>Nombres</th>
        <th>Apellidos</th>
        <th>Deporte</th>
        <th>Fecha alta</th>

       </tr>

      </thead>

    <tbody id="bodyDetabladeinscripciones">

    </tbody>
    <tfoot>
    <tr>
        <th>Nº Inscripción</th>
        <th>Nº Socio</th>
        <th>DNI</th>
        <th>Nombres</th>
        <th>Apellidos</th>
        <th>Deporte</th>
        <th>Fecha alta</th>

       </tr>
    </tfoot>
    </table>

    <!------------------ /TABLA DE DATOS --------------------> 

</form>

	</div>

    

    <span id="resultado"></span>

    

</div>