<script type="text/javascript" src="<?=base_url("/js/tablas/js/dataTables.buttons.min.js");?>"></script>
<script type="text/javascript" src="<?=base_url("/js/tablas/js/buttons.print.min.js");?>"></script>
<script type="text/javascript" src="<?=base_url("/js/tablas/js/dataTables.select.min.js");?>"></script>
<script type="text/javascript" src="<?=base_url("/js/tablas/js/dataTables.scroller.min.js");?>"></script>
<script type="text/javascript" src="<?=base_url("/js/tablas/js/pdfmake.min.js");?>"></script>
<script type="text/javascript" src="<?=base_url("/js/tablas/js/vfs_fonts.js");?>"></script>
<script type="text/javascript" src="<?=base_url("/js/tablas/js/buttons.html5.min.js");?>"></script>
<script type="text/javascript" src="<?=base_url("/js/jquery-ui.js");?>"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url("/css/tablas/css/scroller.dataTables.min.css");?>">
<link rel="stylesheet" type="text/css" href="<?=base_url("/css/jquery-ui.css");?>">

<script>

var dialog;
idCobroSeleccionado=-1;
var vectorDeudas = new Array();
var montoTotal=0;
var totalCuotas=0;
var montoTotalCobrador=0;
var comision=1;
var mostrarFiltros=false;
var primerIngreso=true;
var valor;
var totalSocios=0;

parametrosDeudas = {
		"cobradorSeleccionado": 0,
		"mesSeleccionado":0 ,
		"anioSeleccionado": 0
}


function ActualizarTotalCobro()
{
    var table = $('#tabladedeudas').DataTable();
    montoTotal=0;
    totalCuotas=0;
    comision=1;
    vectorDeudas= new Array();
    totalSocios=0;
    socioActual=0;

    table.rows( { selected: true } ).data().each( function ( value, index ) {
      if(parseInt(value.numSocio)!=socioActual)
      {
        totalSocios+=1;
        socioActual=parseInt(value.numSocio);
      }



        montoTotal+=parseInt(value.Monto);
        comision=parseInt(value.comision);
        vectorDeudas.push(value.NumDeuda);
        totalCuotas+=1;
        });

    console.log(vectorDeudas);
    montoTotalCobrador=montoTotal/comision;
    $('#totalCobro').html(montoTotal);
    $('#totalCobrador').html(montoTotalCobrador);
}

function vaciarCampos()
{
      
		$('#campoCobrador').val(0);
		$('#campoMes').val(0);
		$('#campoAño').val(0);
}


function actualizarCobradores()
{
    $.ajax({
                url:   '<?= site_url("/CCobro/CobradoresSeleccion") ?>',
                type:  'post',
                success:  function (response) {
          $('#CobradoresSeleccion').html(response);
		  vaciarCampos();
                }});
}

function actualizarMes()
{
    $.ajax({
                url:   '<?= site_url("/CCobro/MesSeleccion") ?>',
                type:  'post',
                success:  function (response) {
          $('#MesSeleccion').html(response);
		  vaciarCampos();
                }});
				
}

function actualizarAño()
{
    $.ajax({
                url:   '<?= site_url("/CCobro/AnioSeleccion") ?>',
                type:  'post',
                success:  function (response) {
          $('#AñoSeleccion').html(response);
		  vaciarCampos();
                }});
}


function cancelarCobro()
{
	if(idCobroSeleccionado !== -1){
	var parametros =
    {
		"idCobro": idCobroSeleccionado
    }
$.ajax({
	            data:parametros,
                url:   '<?= site_url("/CCobro/cancelarCobro") ?>',
                type:  'post',
                success:  function (response) {
          //$('#AñoSeleccion').html(response);
		  //vaciarCampos();
		   actualizarListaCobros();
				  actualizarListaDeudasImpagas();
                }});

	}
	else{
		window.alert("debe seleccionar un cobro");}
}




function agregarPago(idDeudas){

    var deudasId = idDeudas;
	//var valorRadio = obtenerIdSocioSeleccionado();

    var parametros = {
		"Cobrador": $('#campoCobrador').val(),
		//"Cobrador": parseFloat( data[7] ),
        "deudasId" : deudasId
}
    $.ajax({
        data:  parametros,
        url:   '<?= site_url("/CCobro/Pagar") ?>/',
        type:  'post',
		    beforeSend: function () 
                {
                $("#modal_Cargando").modal({backdrop: 'static', keyboard: false});
                $("#modal_Cargando").modal('show');

                },
        success:  function (response) 
        {
			     $("#modal_Cargando").modal('hide');
            if(response=="success")
            {
				actualizarListaDeudasImpagas();
				console.log("seeeee");
            }
            else{
                window.alert("No ha seleccionado ninguna deuda");
            }
            ActualizarTotalCobro();
        }
    });

}


var vectorPosicionIdSocio = [];

function agregarIDDeuda()
{
        var posicion;
        var table = $('#tabladedeudas').dataTable();
        var tabla = $('#tabladedeudas').DataTable();

        tabla.rows().eq( 0 ).each( function (idx) {
            var row = tabla.row( idx );

            if ( row.data().numSocio === valor ) {
                posicion=idx;
                vectorDeudas.push(tabla.row(posicion).data().NumDeuda);
                tabla.row(posicion, {order:'current'}).select();
                tabla.row(posicion).scrollTo();
                valor=-1;
            }
        } );
        ActualizarTotalCobro();
        console.log(vectorDeudas);
}



function fechaDeHoy()
{
    var d = new Date();
    var month = d.getMonth()+1;
    var day = d.getDate();
    console.log(day+'/'+month+'/'+ d.getFullYear());
    return (day+'/'+month+'/'+ d.getFullYear());
}


function inicializarLista()	{

    $('#tabladedeudas').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'print',
                //download: 'open',
                title: function(){return 'Cobro del día '+fechaDeHoy();},
                message: function()
                {
                  return 'El total a recaudar es: $'+montoTotal+' | Se ha cobrado un total de '+totalCuotas+' cuotas a '+ totalSocios+' socios <br><br>';
                },
                text: '<button type="button" class="btn btn-default">Vista previa</button>',
                exportOptions: {
                    modifier: {
                        selected: true
                    },
                    columns: [ 0, 1, 2, 3,4 ]
                },
                customize: function ( win ) { 
                    console.log(win);

                    $(win.document.body).css('margin','0 0 0 0')
                      .css('padding','0 0 0 0');

                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', '11pt' );

                        $(win.document.body).find( 'h1' )
                        .css( 'font-size', '20pt' );


                    $(win.document.body).find( 'table td' )
                                            .css( 'border-bottom', '1px solid black' )
                                            .css( 'border-collapse', 'collapse' )
                                            .css( 'height', '50px' )
                                            .css( 'vertical-align', 'center' );

                    $(win.document.body).find( 'table th' )
                                            .css( 'border-bottom', '1px solid black' )
                                            .css( 'border-collapse', 'collapse' );
                    }
            }
        ],
        select: 'multi',
	"ajax": {
                "data": function(d)
                {
                  return parametrosDeudas;
                },
                "url":   "<?=site_url("CCobro/ObtenerDeudasSocio")?>",
                "type":  'post'
          },
        rowId: 'numSocio',
	        "columns": [
            { "data": "numSocio" },
            { "data": "nombreCompleto" },
            { "data": "Mes" },
            { "data": "Anio" },
			{ "data": "Monto" },
			{ "data": "NumDeuda" },
			{ "data": "NumCobrador" },
			{ "data": "comision" },
            { "data": "stringFecha"}
             ],
        aLengthMenu: [
            [-1],
            ["Todos"]
        ],
        iDisplayLength: -1,
        //paging: false,
        "order": [[1,"asc"],[8, "asc"]],
        scrollY:        300,
        scrollCollapse: true,
        deferRender:    true,
        scroller:       true
    } );
	
   cargarPropiedadesTablaDeudas();
}


$(document).ready(function() {
    $( "#ventanaSeleccionarSocio").hide();
    dialog = $( "#ventanaSeleccionarSocio" ).dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            "Aceptar": function() {
                valor=$("#inputsocioSeleccionado").val();
                console.log(valor);
                agregarIDDeuda();
                dialog.dialog( "close" );
            },
            "Cancelar": function() {
                console.log(valor);
                dialog.dialog( "close" );
            }
        },
        close: function() {
            $("#inputsocioSeleccionado").val("");
        }
    });


    $('#bodyTablaDeDeudas').on( 'click', 'tr', function () {
        setTimeout(
            function()
            {

                ActualizarTotalCobro();
            }, 100);

    } );

});


$(document).keyup(function(e) {
    if (e.keyCode == 13)
    {
        if ($("#inputsocioSeleccionado").is(":focus"))
        {
            valor=$("#inputsocioSeleccionado").val();
            console.log(valor);
            agregarIDDeuda();
            dialog.dialog( "close" );
        }
    }
});


function abrirDialogNumeroSocio()
{
    if (!$("input").is(":focus"))
    {
        dialog.dialog( "open" );
    }

}
var k = new Kibo();
k.down(['any'],abrirDialogNumeroSocio);








function cargarPropiedadesTablaDeudas()
{
	//Aca es donde la magia y la realidad se mezclan
    var table = $('#tabladedeudas').DataTable();
    table.column(5).visible(false);
    table.column(6).visible(false);
    table.column(7).visible(false);
     table.column(8).visible(false);


 

//Evento para boton seleccionarTodo
$('#seleccionarTodo').on( 'click', function () {
	totalesCobro=0;
	totalesCobrador=0;
	if((table.data().length !== 0)) {
vectorDeudas= [];

nodos = table.rows().data();

cantFilasTotal = table.column( 0 ).data().length;
//Se seleccionan todas las filas
for (var i = 0; i < cantFilasTotal; i++) {
    table.row(i, {order:'current'}).select();

vectorDeudas.push((nodos[i]).NumDeuda);
}}
 ActualizarTotalCobro();
console.log(vectorDeudas);
	});


//Evento para boton quitar seleccion
$('#quitarSeleccion').on( 'click', function () {
	 if(table.data().length !== 0){
        vectorDeudas= [];
        totalesCobro=0;
        totalesCobrador=0;
        cantFilas = table.column( 0 ).data().length;

        for (var i = 0; i < cantFilas; i++) {
            table.row(i, {order:'current'}).deselect();

        }

        console.log(vectorDeudas);
	 }
	 ActualizarTotalCobro();
});
	
	
	
	//Evento para el boton listar
	$('#Listar').on( 'click', function () {
            vectorDeudas= new Array();
          totalesCobro=0;
          totalesCobrador=0;
           ActualizarTotalCobro();
            parametrosDeudas =
            {
            "cobradorSeleccionado": $('#campoCobrador').val(),
            "mesSeleccionado":$('#campoMes').val() ,
            "anioSeleccionado": $('#campoAño').val()
            }
actualizarListaDeudasImpagas();
	
});
 }



function actualizarListaDeudasImpagas()
{
    var t = $('#tabladedeudas').DataTable();
    t.clear().draw();
    t.ajax.reload();
    vectorDeudas=[];
    montoTotal=0;
    totalCuotas=0;
    montoTotalCobrador=0;
    comision=1;
}



function inicializarListaCobros()
{
   $('#tablaCobros').DataTable( {
        "ajax": 'CCobro/obtenerCobros',
        "columns": [
            { "data": "id" },
            { "data": "nombre" },
            { "data": "apellido" },
            { "data": "fechaCobro" },
            { "data": "importe" }
		
        ]
    });

   cargarPropiedadesTablaCobros(); 
}


function actualizarListaCobros()
{
var t = $('#tablaCobros').DataTable();
 t.ajax.reload();
 idCobroSeleccionado=-1;
}

function cargarPropiedadesTablaCobros()
{
	
    ///AQUÍ SE CARGAN LAS PROPIEDADES DE LA TABLA  
    var table = $('#tablaCobros').DataTable();
	
	
                    $('#bodyTablaCobros').on( 'click', 'tr', function () {
						
                   if ( $(this).hasClass('selected') ) 
                    {
						
                        $(this).removeClass('selected');
                        idCobroSeleccionado=-1;
						console.log(idCobroSeleccionado);
                    }
                    else {
                        table.$('tr.selected').removeClass('selected');
                        $(this).addClass('selected');
                        idCobroSeleccionado = (this.cells[0].innerHTML);
						console.log(idCobroSeleccionado);
                        }
                    
                    
                    });

                    $('#eliminarCobro').click( function () {
				  cancelarCobro(idCobroSeleccionado);	
				
                    } );
                    
}
function obtenerIdCobroSeleccionado()
{
    return idCobroSeleccionado;
}

function visualizacionFiltros()
{
    if(mostrarFiltros)
    {
        $('#divFiltros').show("slow");
        $('#textoMostrarFiltro').html("OCULTAR FILTROS");
        mostrarFiltros=false;
    }
    else
    {
        if(primerIngreso)
        {
            $('#divFiltros').hide();
            primerIngreso=false;
        }
        else
        {
            $('#divFiltros').hide("slow");
        }

        $('#textoMostrarFiltro').html("VER FILTROS");
        mostrarFiltros=true;
    }
}

</script>


<body onLoad="inicializarLista(),inicializarListaCobros(),actualizarCobradores(),actualizarMes(),actualizarAño(),vaciarCampos(),visualizacionFiltros();">
<div class="container">

  <div class="row text-justify cajas">
      <div class="container">
          <div class="row">
              <div class="col-md-9">
                  <h1>Deudas Impagas</h1>
              </div>
              <div class="col-md-3">
                  <h1><button type="button" class="btn btn-success" data-toggle="modal" data-target="#ventanaCobros" onClick="actualizarListaCobros()">Ver pagos realizados</button>&nbsp;</h1>
              </div>

              <div class="col-md-12" >
                  <a class="cursorManito" onclick="visualizacionFiltros()"><span id="textoMostrarFiltro"></span></a>
                  <div id="divFiltros">

                  <h4>Seleccione los valores del listado</h4>
                  <table>
                      <tr>
                          <td style="padding-top: 5px; text-align: right"><label>Cobrador:</label>&nbsp;</td>
                          <td><div id="CobradoresSeleccion"></div></td>
                      </tr>
                      <tr>
                          <td style="padding-top: 5px; text-align: right"> <label>Mes:</label>&nbsp;</td>
                          <td><div id="MesSeleccion"></div></td>
                      </tr>
                      <tr>
                          <td style="padding-top: 5px; text-align: right"> <label>Año:</label>&nbsp;</td>
                          <td><div id="AñoSeleccion"></div></td>
                      </tr>
                      <tr>
                          <td style="padding-top: 5px; text-align: right" colspan="2"> <button id="Listar" type="button" class="btn btn-danger" onClick="">Listar
                              </button>&nbsp;</td>
                      </tr>
                  </table>
                  </div>
                  <br>
                  <br>
                  </div>





              <div style="" class="col-md-6">
                  <div>
                      <table>
                          <tr>
                              <td style="text-align: right;padding-top: 4px"><label>Total a pagar:</label>&nbsp;</td>
                              <td><span id="totalCobro">0</span></td>
                              <td style="text-align: right;padding: 4px 0px 0px 10px"><label>Total a cobrador:</label>&nbsp;</td>
                              <td><span id="totalCobrador">0</span></td>
                          </tr>
                      </table>
                  </div>
              </div>
              <div class="col-md-5" style="text-align: right">
                  <button id="pagar" type="button" class="btn btn-default" data-dismiss="modal" href="javascript:;" onClick="agregarPago(vectorDeudas)">Pagar</button>
                  <!-- <button id="seleccionarTodo" type="button" class="btn btn-default" data-dismiss="modal" href="javascript:;" onClick="">Seleccionar Todo</button> -->
                  <button id="quitarSeleccion" type="button" class="btn btn-default" data-dismiss="modal" href="javascript:;" onClick="">Quitar Seleccion</button>
              </div>
  </div>
      </div>
      <br>

  <!------------------ TABLA DE DATOS -------------------->
<div id="tabla">
      <table id="tabladedeudas" class="display" cellspacing="0" width="100%">
      <thead>
        <tr>
        <th>Nº Socio</th>
        <th>Apellido y nombre</th>
        <th>Mes</th>
        <th>Año</th>
        <th>Monto</th>
        <th>Num Deuda</th>
        <th>Num Cobrador</th>
         <th>Comision</th>
         <th>Numero mes</th>
        </tr>
      </thead>


    <tbody id="bodyTablaDeDeudas" class="display cursorManito">

    </tbody></table>
</div>
     
      


    <!------------------ /TABLA DE DATOS --------------------> 








<!-- ************VENTANA COBROS ****************  -->

<!-- Modal -->

<div id="ventanaCobros"  style="color:black;" class="modal fade" role="dialog">

  <div class="modal-dialog">



    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

<button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Cobros</h4>

      </div>



<div class="modal-body">
<h4>Registro de cobros</h4>
<br>



</table>

<table id="tablaCobros" class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
    <th>Id Cobro</th>
     <th>Nombre</th>
        <th>Apellido</th>
    <th>Fecha Cobro</th>
        <th>Importe</th>
    </tr>
    </thead>
    <tbody id="bodyTablaCobros" class="display cursorManito">

    </tbody>
</table>

</div>

      <div class="modal-footer">
      <span id="advertenciaDatos" style="color:red;"></span>
      <button id="eliminarCobro" type="button" class="btn btn-danger" data-dismiss="modal" href="javascript:;" onClick="">Eliminar Cobro</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" href="javascript:;" onClick="">Salir</button>
      </div>

</form>


</div>
</div>
</div>

<!-- Modal Cargando -->
<div id="modal_Cargando" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title" style="color: black">Cargando....</h4>
      </div>
      <div class="modal-body" style="color: black">
      <h3>Aguarde un momento por favor...</h3>
      <p>Esta operación puede tardar algunos minutos.</p>
      <p>Por favor, no cierre esta ventana</p>
        <center><img src="<?php echo base_url('images/loading.gif')?>"></center>
       </div>
    </div>
  </div>
</div>

      <div id="ventanaSeleccionarSocio" title="Seleccione un socio">
          <input type="number" id="inputsocioSeleccionado">
      </div>



</body>

</html>