<head>
    <title>CUPONES</title>

<script language="JavaScript">
function imprimir(){
document.all.item("noImprimir").style.display='none';
window.print();
document.all.item("noImprimir").style.display='inline';
}
</script>
    <style type="text/css">

        .tablaCupon
        {
            background-color: white;
            color: #000000;
            border: 1px solid #000000;
            width: 8cm;
            height: 7cm;
            margin: 0.2cm 0cm 0cm 0cm;
            font-size: 11pt;

        }

        td
        {
            padding: 0px 5px 0px 5px;
        }
        body
        {
            background-color: #ffffff;
            font-family: Calibri;
        }


        @media all {
            .divsaltopagina{
                display:none;
            }
        }

        @media print{
            .divsaltopagina{
                display:block;
                page-break-before:always;
            }
        }

        .filtros
        {
            -webkit-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
            -moz-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
            box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
            background-color: #f2be57;
            padding: 5px;
        }
    </style>


</head>

<body>


    <div id="noImprimir">
        <div style="margin-left: 15%;text-align: left; width: 400px;">
        <div  class="filtros">
        <h3>Filtros</h3>
<form method="post">
    <label>Numero de socio a imprimir: </label><input type="number" name="numcuponAImprimir" placeholder="Mostrar todo">
<br>
    <input type="checkbox" name="mostrarCuponesIgualACero" value="1"> Mostrar cupones de $0
    <br>
    <input type="submit" value="Aplicar filtros">
</form>
        </div>
        </div>

        <br><br>
        <div style="text-align: center">
            <input type="button" name="botonImprimir" value="Volver atrás" onclick="document.location.href='<?php echo site_url('CDeuda')?>'">
            <input type="button" name="botonImprimir" value="Imprimir" onclick="imprimir();">


        </div>
</div>
    <center>
<?php

//Definicion de variables
$cuponxfila = 3;
$cuponxfilaActual = 1;
$primero=true;
$i=0;
$cantidadDeRecibosMostrados=0;
$meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

if(isset($_POST['numcuponAImprimir']))
{
    $variableCupon = $_POST['numcuponAImprimir'];
}
else
{
    $variableCupon = '';
}
if(isset($_POST['mostrarCuponesIgualACero']))
{
    $variableCuponesIgualACero = $_POST['mostrarCuponesIgualACero'];
}
else
{
    $variableCuponesIgualACero = 0;
}

?>


<!--- Comienza la tabla con todos los cupones -->
<?php foreach($cupones as $cupon)
{
    $i+=1;


/// Si vamos a listar todos los cupones
    if($variableCupon=='' && ($cupon['monto']>0 || $variableCuponesIgualACero==1))
    {
        $cantidadDeRecibosMostrados+=1;

 if($cuponxfilaActual==1)
 {
     ?><table class="tablaCupones"><tr><?php
 }
 ?>
  <td><table background="<?php echo base_url('/images/escudo-parque-sur.png')?>" class="tablaCupon" style="border-spacing:0px;background-repeat:no-repeat;background-position:right 10px top 35px;">
                <tr>
                    <td style="text-align: center" colspan="2">
                        <span style="font-weight: bold; font-size: 14pt;">CLUB PARQUE SUR</span>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;"><span style="text-decoration: underline">Nº Socio:</span> <?php echo $cupon['nroSocio'] ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold"><?php echo $cupon['apellidos'] ?>,&nbsp <?php echo $cupon['nombre'] ?></td>
                </tr>
                <tr>
                    <td>Domicilio: <?php echo $cupon['domicilio'] ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <?php echo $meses[$cupon['mesAdeudado']]?> <?php echo $cupon['añoAdeudado']?></td>
                </tr>

                <!-- DESCRIPCIÓN DEL CUPÓN -->
                <?php
                $cantidadLineas=5;
                $j=0;
                foreach($cupon['lineasDeudas'] as $lineaDeuda)
                {
                    $j+=1;
                    ?>
                    <tr>
                        <td><?php echo $lineaDeuda->descripcion?></td>
                        <td style="text-align: right;">$<?php echo $lineaDeuda->monto?></td>
                    </tr>
                <?php }

                while($j<$cantidadLineas)
                {?>
                    <tr>
                        <td></td>
                        <td style="text-align: right;">&nbsp;</td>
                    </tr>
                    <?php
                    $j+=1;
                }

                ?>
                <tr>
                    <td colspan="2"><hr></td>
                </tr>
                <tr>
                    <td>Total a pagar</td>
                    <td style="text-align: right;font-weight: bold">$<?php echo $cupon['monto']?></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center;">Número de recibo: <?php echo sprintf('%06d',$i);?></td>
                </tr>
            </table></td>

<?php
$cuponxfilaActual+=1;

        if($cuponxfilaActual>$cuponxfila)
            {
            ?></tr></table>
             <?php
            $cuponxfilaActual=1;
                if($cantidadDeRecibosMostrados%15==0)
                {
                    ?><div class="divsaltopagina"></div><?php
                }
            }
}
    else
    {



        /// SI SE ELIGE UN CUPON EN PARTICULAR QUE MOSTRAR
        if($variableCupon==$cupon['nroSocio'])
        {?>
            <table background="<?php echo base_url('/images/escudo-parque-sur.png')?>" class="tablaCupon" style="border-spacing:0px;background-repeat:no-repeat;background-position:right 10px top 35px;">
                <tr>
                    <td style="text-align: center" colspan="2">
                        <span style="font-weight: bold; font-size: 14pt;">CLUB PARQUE SUR</span>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;"><span style="text-decoration: underline">Nº Socio:</span> <?php echo $cupon['nroSocio'] ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold"><?php echo $cupon['apellidos'] ?>,&nbsp <?php echo $cupon['nombre'] ?></td>
                </tr>
                <tr>
                    <td>Domicilio: <?php echo $cupon['domicilio'] ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <?php echo $meses[$cupon['mesAdeudado']]?> <?php echo $cupon['añoAdeudado']?></td>
                </tr>

                <!-- DESCRIPCIÓN DEL CUPÓN -->
                <?php
                $cantidadLineas=5;
                $j=0;
                foreach($cupon['lineasDeudas'] as $lineaDeuda)
                {
                    $j+=1;
                    ?>
                    <tr>
                        <td><?php echo $lineaDeuda->descripcion?></td>
                        <td style="text-align: right;">$<?php echo $lineaDeuda->monto?></td>
                    </tr>
                <?php }

                while($j<$cantidadLineas)
                {?>
                    <tr>
                        <td></td>
                        <td style="text-align: right;">&nbsp;</td>
                    </tr>
                    <?php
                    $j+=1;
                }

                ?>
                <tr>
                    <td colspan="2"><hr></td>
                </tr>
                <tr>
                    <td>Total a pagar</td>
                    <td style="text-align: right;font-weight: bold">$<?php echo $cupon['monto']?></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center;">Número de recibo: <?php echo sprintf('%06d',$i);?></td>
                </tr>
            </table><?php
        }







    }

}
    if(($cuponxfilaActual<=$cuponxfila) && ($cuponxfilaActual>1))
    {
        while($cuponxfilaActual<=$cuponxfila)
        {
            ?>
            <td>
                <table class="tablaCupon" style="border:0px;">
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                </table>
            </td>
            <?php
            $cuponxfilaActual+=1;
        }
    }
?>

</table>
</center>
</body>