<!doctype html>
<html lang="es">
<head>
    

<!--   Core JS Files   -->
  <script src="/parquesur/js/jquery-3.1.0.min.js" type="text/javascript"></script>
  <script src="/parquesur/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="/parquesur/js/material.min.js" type="text/javascript"></script>

  <!-- Material Dashboard javascript methods -->
  <script src="/parquesur/js/material-dashboard.js"></script>

  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="/parquesur/js/demo.js"></script>


    <!--     Tablas     -->
    <link rel="stylesheet" type="text/css" href="<?=base_url("/css/tablas/css/jquery.dataTables.css");?>">
    <script type="text/javascript" src="<?=base_url("/js/tablas/js/jquery.dataTables.js");?>"></script>




  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="/parquesur/img/apple-icon.png" />
  <link rel="icon" type="image/png" href="" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="/parquesur//css/bootstrap.min.css" rel="stylesheet" />

    <!--  Material Dashboard CSS    -->
    <link href="/parquesur/css/material-dashboard.css" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="/parquesur/css/demo.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>

    


<script type="text/javascript">




function controlarOpcion()
{
var opcion=$("#opcion").val();


///------- RECAUDACION ---------///
if(opcion=="recaudacion")
{
    obtenerRecaudacion();
  // QUE MOSTRAMOS
  $("#recaudacion").show();

  //QUE ESCONDEMOS
  $("#textoPrevio").hide();
  $("#recaudadoxcobrador").hide();
  $("#recaudacionxdeporte").hide();
}
///------- RECAUDACION ---------///

else
{


  ///------- RECAUDADO POR COBRADOR ---------///
  if(opcion=="recaudadoxcobrador")
  {
  // QUE MOSTRAMOS
  $("#recaudadoxcobrador").show();
  obtenerCobradores();

  //QUE ESCONDEMOS
  $("#recaudacion").hide();  
  $("#recaudacionxdeporte").hide();
  }
  ///------- RECAUDADO POR COBRADOR ---------///





  else
    {
      ///------- RECAUDADO POR DEPORTE ---------///
      if(opcion=="recaudacionxdeporte")
      {
        obtenerRecaudacionxDeporte();

        // QUE MOSTRAMOS
        $("#recaudacionxdeporte").show();
        obtenerCobradores();

        //QUE ESCONDEMOS
        $("#recaudacion").hide(); 
        $("#recaudadoxcobrador").hide();
      }
      ///------- RECAUDADO POR DEPORTE ---------///

    }
}
}  


function obtenerRecaudacion()
{
var parametros =
{
  "fechaDesde": $("#campoFechaDesdeRecaudacion").val(),
  "fechaHasta": $("#campoFechaHastaRecaudacion").val()
}

  $.ajax({
                data: parametros,
                url:   "<?=site_url("CReporte/recaudacionMensual")?>",
                type:  'post',
                success:  function (response) 
                {                     
                  $('#resultadoRecaudacion').html(response);
                  $("#textoPrevioRecaudacion").show();
                }
        });      
}

function obtenerCobradores()
{
  $.ajax({
                url:   "<?=site_url("CCobrador/CobradoresSeleccion")?>",
                type:  'post',
                success:  function (response) {
          $('#cobradores').html(response);
                }}); 
}



function obtenerRecaudadoxCobrador()
{
    var todos
    if($("#checkBox_Todos").is(":checked"))
    {
        todos =$("#checkBox_Todos").val();
    }
    else
    {
        todos = 0;
    }
var parametros =
{
  "fechaDesde": $("#campoFechaDesdeRecaudadoxCobrador").val(),
  "fechaHasta": $("#campoFechaHastaRecaudadoxCobrador").val(),
  "cobrador": $("#campoCobrador").val(),
  "todos": todos
}

  $.ajax({
                data: parametros,
                url:   "<?=site_url("CReporte/recaudadoCompleto")?>",
                type:  'post',
                success:  function (response) 
                {                     
                  $('#resultadoRecaudadoxCobrador').html(response);

                    $("#textoPrevioRecaudadoxCobrador").html("A continuación se detallará lo cobrado por el cobrador "+$('#campoCobrador :selected').text());
                    if(todos>0)
                    {
                        $("#textoPrevioRecaudadoxCobrador").html("A continuación se detallará lo recaudado por todos los cobradores");
                    }

                  $("#textoPrevioRecaudadoxCobrador").show();
                }
        });      
}



function obtenerRecaudacionxDeporte()
{
$('#tablaRecaudacionxDeporte').DataTable( {
        "ajax": '<?=site_url("CReporte/informacionActividades")?>',
        "columns": [
            { "data": "deporte" },
            { "data": "cantidad" },
            { "data": "precioIndividual" },
            { "data": "monto" }
        ],
    aLengthMenu: [
        [-1],
        ["Todos"]
    ],
    }); 
}



function controlarBotonesCobradores()
{


    if($("#checkBox_Todos").is(":checked"))
    {
        $('#campoCobrador').prop('disabled', 'disabled');
    }
    else
    {
        $('#campoCobrador').prop('disabled', false);
    }
}


</script>



</head>

<body onload="controlarOpcion()">
<input id="opcion" type="hidden" value="<?=$_GET['opcion']?>">

  <div class="wrapper">

      <div class="sidebar" data-color="purple" data-image="/img/sidebar-1.jpg">
      <!--
            Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

            Tip 2: you can also add an image using data-image tag
        -->

      <div class="logo">
        <a href="" class="simple-text">
          Reportes
        </a>
      </div>

           <!-- MENU IZQUIERDO -->
        <div class="sidebar-wrapper">
              <ul class="nav">
                  <li class="active">
                      <a href="<?= site_url('CReporte/index') ?>">
                          <i class="material-icons">dashboard</i>
                          <p>Tabla general</p>
                      </a>
                  </li>
                   <ul class="nav">
                  <h4><i class="material-icons">person</i>Societarios</h4>
                  <li>
                      <a href="<?= site_url('CReporte/reportesSocietarios?opcion=sociosactivos') ?>">
                          
                          <p>Socios Activos</p>
                      </a>
                  </li>
                     <li>
                      <a href="<?= site_url('CReporte/reportesSocietarios?opcion=sociosMorosos') ?>">
                          
                          <p>Socios Morosos</p>
                      </a>
                  </li>
                   <li>
                      <a href="<?= site_url('CReporte/reportesSocietarios?opcion=altasybajas') ?>">
                          
                          <p>Altas y bajas</p>
                      </a>
                  </li>
                  <li>
                      <a href="<?= site_url('CReporte/reportesSocietarios?opcion=sociosconembarcaciones') ?>">
                          
                          <p>Socios con embarcaciones</p>
                      </a>
                  </li>
                  <li>
                      <a href="<?= site_url('CReporte/reportesSocietarios?opcion=sociosadherentes') ?>">
                          
                          <p>Socios Adherentes</p>
                      </a>
                  </li>

                  </ul>
                  <ul class="nav">
                  <h4><i class="material-icons">content_paste</i>Financieros</h4>
                  <li>
                      <a href="<?= site_url('CReporte/reportesFinancieros?opcion=recaudacion') ?>">
                          <p>Recaudación bruta</p>
                      </a>
                  </li>
                  <li>
                      <a href="<?= site_url('CReporte/reportesFinancieros?opcion=recaudadoxcobrador') ?>">
                          <p>Recaudado por cobrador (bruto)</p>
                      </a>
                  </li>
                  <li>
                      <a href="<?= site_url('CReporte/reportesFinancieros?opcion=recaudacionxdeporte') ?>">
                          <p>Recaudación por deporte (bruta mensual)</p>
                      </a>
                  </li>

                  </ul>

              </ul>
            <ul class="nav">
                <h4><i class="material-icons">directions_boat</i>Embarcaciones</h4>
                <li>
                    <a href="<?= site_url('CReporte/reportesEmbarcaciones?opcion=reporteParaPrefectura') ?>">
                        <p>Reporte para prefectura</p>
                    </a>
                </li>

            </ul>

              </ul>
        </div>
      </div>
      <!-- FINALIZA EL MENÚ IZQUIERDO -->


      <div class="main-panel">
      <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= base_url();?>">Volver atrás</a>
          </div>
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">


            <!---  De la siguiente manera se pueden agregar elementos en el extremo superior derecho
              <li>
                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="material-icons">dashboard</i>
                  <p class="hidden-lg hidden-md">Dashboard</p>
                </a>
              </li>
              ---->

            </ul>

          </div>
        </div>
      </nav>

      <div class="content">
        <div class="container-fluid">
          <div class="row">
          


          </div>

          <!--  RECAUDACIÓN MENSUAL -->
          <div class="row" id="recaudacion">
              <h2>Recaudación bruta</h2>

          <div class="col-md-12">
          <p id="textoPrevioRecaudacion">A continuación se detallará lo que se debe recaudar por mes en bruto, sin contar comisiones. <br>
          <span id="resultadoRecaudacion">Cargando...</span></p>
          </div>
          </div>

          


          <!-- RECAUDADO POR COBRADOR ENTRE DOS FECHAS -->
          <div class="row" id="recaudadoxcobrador">
          <div class="col-md-6">
          <h3> Seleccione el rango de fechas</h3>
          <p>Desde: <input type="month" id="campoFechaDesdeRecaudadoxCobrador">  | Hasta &nbsp<input type="month" id="campoFechaHastaRecaudadoxCobrador"> &nbsp
          </p>
          </div>

          <div class="col-md-6">
          <h3> Seleccione el cobrador</h3>
          <p id="cobradores"></p><input id="checkBox_Todos" onclick="controlarBotonesCobradores()" type="checkbox" value="1"> Todos
          </div>

          <div style="margin-top: 20px" class="col-md-12">
          <p><input type="button" onClick="obtenerRecaudadoxCobrador()": value="Buscar"></p>
          </div>


          <div class="col-md-12" style="margin-top: 50px">
          <p id="textoPrevioRecaudadoxCobrador"></p>
          <span id="resultadoRecaudadoxCobrador"></span>
          </div>
          </div>




          <!-- RECAUDADO POR DEPORTES -->
          <div class="row" id="recaudacionxdeporte">
        
           <!------------------ TABLA DE DATOS -------------------->
                  <table id="tablaRecaudacionxDeporte" class="table display table-responsive">
                  <thead>
                  <tr>
                  <th>Deporte</th>
                  <th>Cantidad de inscripciones</th>
                  <th>Precio individual</th>
                  <th>Monto a recaudar</th>
                  </tr>
                </thead>


              <tbody id="bodytablaRecaudacionxDeporte" class="cursorManito">
              </tbody>
             <tfoot>
                  <tr>
                  <th>Deporte</th>
                  <th>Cantidad de inscripciones</th>
                  <th>Precio individual</th>
                  <th>Monto a recaudar</th>
                  </tr>
                  </tfoot>
                </table>
    <!------------------ /TABLA DE DATOS -------------------->   



        <!-- RECAUDADO POR DEPORTES -->



          </div>




          </div>
          

          

                
    </div>
  </div>

</body>
</html>
