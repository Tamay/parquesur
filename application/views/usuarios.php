<!------------------ JAVASCRIPT -------------------->

<script>

var editar;

function seVaAEditar(vof)
{
  editar=vof;
}


function agregarUsuario(){
         var i=0;
    var valorRadio=-1;
    var table = document.getElementById('bodyBusquedaPersonas');
    var rowCount = table.rows.length;
    while($("#radioPersona_"+i).is(":checked")==false && i<=rowCount)
    {
        i++;
    }

/// Si no hubo ninguno seleccionado
    if(i>rowCount)
    {
        valorRadio=-1;
    }

    else{
         (valorRadio = $("#radioPersona_"+i).val());
    
  
        var parametros = {
                "username" : $('#campoUsuario').val(),
                "password" : $('#campoContraseña').val(),
                "tipoUsuario" : $('#campoTipoUsuario').val(),
                "persona_id" : valorRadio,
             
        };
        $.ajax({
                data:  parametros,
                url:   '<?= site_url("/CUsuario/Agregar") ?>',
                type:  'post',
                success:  function (response) {
                        if(response=="success")
                        {
                        actualizarLista();
                        vaciarCampos();  
                        }
                        else{
                          window.alert("Ya existe el usuario de esa persona");
                        }
                        
                }
    });}
  }


function editarUsuario()
{
  var valorRadio = obtenerIdUsuarioSeleccionado();

    if(valorRadio==-1)
    {
        alert("Debe seleccionar un usuario");
    }
    else{




  var parametros = {
      "username" : $('#campoUsuario').val(),
      "password" : $('#campoContraseña').val(),
      "tipoUsuario" : $('#campoTipoUsuario').val(),
      
        };
        $.ajax({
                data:  parametros,
                url:   '<?= site_url("/CUsuario/Editar") ?>/'+valorRadio,
                type:  'post',
                beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                },
                success:  function (response) {
                        $("#resultado").html(response);
            actualizarLista();
            vaciarCampos();
                }
        });
    }
}

function comprobarCampos()
{
  if($('#campoUsuario').val()=='' || $('#campoContraseña').val()=='')//|| $('#radioPersona_').is(":checked")==false)  
  {
    $('#botonAceptar').prop('disabled', true);
    $("#advertenciaDatos").text("Faltan datos obligatorios");
  }
  else
  {
   $('#botonAceptar').prop('disabled', false); 
   $("#advertenciaDatos").text("");
  }   
}

function vaciarCampos()
{
        $('#campoUsuario').val("");
        $('#campoContraseña').val("");
        $('#valorBusqueda').val("");
        comprobarCampos();
}

function actualizarTipoSocios()
{
    $.ajax({
                url:   '<?= site_url("/CTipoSocio/TiposSociosEnSelect") ?>',
                type:  'post',
                success:  function (response) {
          $('#tipoSociosEnSelect').html(response);
                }});
}




function obtenerUsuario()
{
  var valorRadio = obtenerIdUsuarioSeleccionado();

/// Si no hubo ninguno seleccionado
  if(valorRadio==-1)
    {
    window.alert("Debe seleccionar un usuario");
    }
    
    else{
      $("#ventanaAgregar").modal("show");

  $.ajax({
    url:'<?= site_url("/CUsuario/obtenerUsuario") ?>/'+ valorRadio,
    type:  'get',
    dataType: 'json',
    success: function(res) {
        $('#campoUsuario').val(res.username);
        $('#campoContraseña').val(res.password);
        $('#campoTipoUsuario').val(res.tipoUsuario);
        
        comprobarCampos();
  }});
}
}

function darDeBaja()
{
  var valorRadio = obtenerIdSocioSeleccionado();

/// Si no hubo ninguno seleccionado
  if(valorRadio==-1)
    {
    window.alert("Debe seleccionar un socio");
    }
    
    else{

var respuesta = confirm("¿Está seguro que desea dar de baja este socio?");
if(respuesta)
{
   $.ajax({
    url:'<?= site_url("/CSocio/DarDeBaja") ?>/'+ valorRadio,
    type:  'POST',
    success: function() 
    {
      window.alert("El socio ha sido dado de baja con éxito.")
      actualizarLista();
    }
});
}
}
}


function insertarElementoEnTabla(usuario, contrasenia, nombre, apellidos,tipo,nroUsuario)
{
var table = document.getElementById('bodyDetabladeusuarios');
var rowCount = table.rows.length;
var row = table.insertRow(rowCount);

var celdaTipo = row.insertCell(0);
var celdaUsuario = row.insertCell(1);
var celdaContrasenia = row.insertCell(2);
var celdaNombre = row.insertCell(3);
var celdaApellidos = row.insertCell(4);
var celdaSeleccion = row.insertCell(5);

//Dependiendo el tipo, 0=Admin, 1=Cobrador, 2=Socio
var tipoString;
switch(parseInt(tipo)) {
    case 0:
        tipoString="Administrador";
        //row.bgColor = '#FFFF99';
        break;
    case 1:
        tipoString="Cobrador";
        //row.bgColor = '#33CC33';
        break;
    case 2:
        estadostring="Socio";
        break;
    
}


celdaTipo.innerHTML = tipoString;
celdaUsuario.innerHTML = usuario;
celdaContrasenia.innerHTML = contrasenia;
celdaNombre.innerHTML = nombre;
celdaApellidos.innerHTML = apellidos;
celdaSeleccion.innerHTML = '<input type="radio" id="radio_'+rowCount+'" name="idElegido" onchange="" value="'+nroUsuario+'">';
}

function actualizarLista()
{
    var parametros = 
    {
                "valorBusqueda" : $('#campoBusqueda').val()
    };
   $.ajax({
                url:   '<?= site_url("/CUsuario/obtenerUsuarios") ?>',
                type:  'post',
                dataType: "json",
                data:   parametros,
                success:  function (response)
                {
                  // La siguiente línea es para borrar todas las filas excepto la primera (sería la cabecera)
                  $("#bodyDetabladeusuarios").children().remove();

                  $.each(response.usuarios, function(i, item) {
                    
                    // Asigno a la variable socio, cada socio que se recorre del archivo JSON
                    var usuario = response.usuarios[i];

                    // Inserto en la tabla cada socio
                    insertarElementoEnTabla(usuario.usuario,usuario.contrasenia,usuario.nombre,usuario.apellidos,usuario.tipoUsuario,usuario.nroUsuario);
                });

                }});
}


function obtenerIdUsuarioSeleccionado()
{
    var i=0;
    var valorRadio=-1;
    var table = document.getElementById('bodyDetabladeusuarios');
    var rowCount = table.rows.length;
    while($("#radio_"+i).is(":checked")==false && i<=rowCount)
    {
        i++;
    }

/// Si no hubo ninguno seleccionado
    if(i>rowCount)
    {
        valorRadio=-1;
    }

    else{
         (valorRadio = $("#radio_"+i).val());
    }
    return valorRadio;

}




function actualizarListaBusquedaPersonas()
{
    var parametros = 
    {
                "valorBusqueda" : $('#campoBusqueda1').val()
    };
   $.ajax({
                url:   '<?= site_url("/CUsuario/obtenerPersonas") ?>',
                type:  'post',
                dataType: "json",
                data:   parametros,
                success:  function (response)
                {
                  // La siguiente línea es para borrar todas las filas excepto la primera (sería la cabecera)
                  $("#bodyBusquedaPersonas").children().remove();

               $.each(response.personas, function(i, item) {
                    
                    // Asigno a la variable socio, cada socio que se recorre del archivo JSON
                    var persona = response.personas[i];

                // Inserto en la tabla cada socio
                insertarElementoEnTablaBusquedaPersonas(persona.dni,persona.nombre,persona.apellidos,persona.id);
            });

        }});
}

function  insertarElementoEnTablaBusquedaPersonas(dni,nombre,apellidos,id)
    {
        var table = document.getElementById('bodyBusquedaPersonas');
        var rowCount = table.rows.length;
        var row = table.insertRow(rowCount);
        row.style.textAlign = "center";

        var celdaDNI = row.insertCell(0);
        var celdaNombre = row.insertCell(1);
        var celdaApellidos = row.insertCell(2);
        celdaSeleccion = row.insertCell(3);

        celdaDNI.innerHTML = dni;
        celdaNombre.innerHTML = nombre;
        celdaApellidos.innerHTML = apellidos;
        celdaSeleccion.innerHTML = '<input type="radio" id="radioPersona_'+rowCount+'" name="idElegido" value="'+id+'">';
    }




var k = new Kibo();

function pasajeTabs() {
  //console.log('last key: ' + k.lastKey());
  if(k.lastKey()=='right')
  {
  $('.nav-tabs a:last').tab('show');  
  }
  if(k.lastKey()=='left')
    {
      $('.nav-tabs a:first').tab('show');   
    }
  
}

function crearNuevoSocio()
{
    $("#ventanaAgregar").modal("show");
}

k.down(['shift right','shift left'], pasajeTabs);
k.down(['shift n'], crearNuevoSocio);
//k.down(['shift e'], eliminarSocioAdherente);



$(document).ready(function(){
$("#bodyDetabladeusuarios").delegate("tr", "click", function(e) {
    $("#radio_"+$(e.currentTarget).index()).prop("checked", true);

    $("#bodyDetabladeusuarios").children().css("font-weight", "normal");
    $("#bodyDetabladeusuarios tr:eq("+$(e.currentTarget).index()+")").css("font-weight", "bold");

});
});

</script>


<body onload="actualizarLista(),comprobarCampos()">
<div class="container">    

  <div class="row text-justify cajas">

        	<p>
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ventanaAgregar" onClick="actualizarListaBusquedaPersonas(),vaciarCampos(),seVaAEditar(false)">Nuevo Usuario
              </button>&nbsp;

	          <button type="button" class="btn btn-warning"  onClick="obtenerUsuario(),seVaAEditar(true), actualizarListaBusquedaPersonas()">Editar</button>&nbsp;
          <a  type="button" class="btn btn-info" style="float: right;" href="#myModal" data-toggle="modal">Ayuda</a>


          </p>
        

  <div class="row">

  <div class="col-lg-6"><h1>Lista de usuarios</h1></div>

  <div class="col-lg-6 text-right" style="padding-top:30px;">

  <form><input id="campoBusqueda" onchange="actualizarLista($(this).val());" onkeyup="this.onchange();" type="text" size="30" placeholder="Buscar por nombre o apellidos..."></form></div>

  </div>

  <!------------------ TABLA DE DATOS -------------------->

      <table id="tabladesocios" class="table table-striped table-hover">
      <thead>
        <tr>
        <th>Tipo de usuario</th>
        <th>Usuario</th>
        <th>Contraseña</th>
        <th>Nombre</th>
        <th>Apellido</th>
        
        </tr>
      </thead>


    <tbody id="bodyDetabladeusuarios" class="cursorManito">

    </tbody></table>



    <!------------------ /TABLA DE DATOS --------------------> 

	</div>

    <span id="resultado"></span>

    

</div>







  <!------------------ VENTANA AGREGAR --------------------> 

<!-- Modal -->

<div id="ventanaAgregar"  style="color:black;" class="modal fade" role="dialog">

  <div class="modal-dialog">



    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

<button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Agregar Usuario </h4>

      </div>

<div class="modal-body">



<ul class="nav nav-tabs">

  <li class="active"><a data-toggle="tab" href="#datosCuenta">Datos de la cuenta</a></li>

  <li><a data-toggle="tab" href="#datosDePersona">Persona</a></li>

</ul>



<form id="datos">

<div class="tab-content">

  <!-- Datos Personales-->

  <div id="datosCuenta" class="tab-pane fade in active">

      <h3>Información del Usuario</h3>

  <table style="border-spacing:5px;border-collapse: separate;">

   <tr>

  <td style="text-align:right"><label>Tipo de Usuario</label></td>

  <td><select name="genero" id="campoTipoUsuario">

            <option value="0" selected>Administrador</option>

            <option value="1">Cobrador</option>

         

        </select></td>

  </tr> 



      <!--- Campo Apellido -->
  <tr>

  <td style="text-align:right"><label>Usuario</label></td>

  <td><input id="campoUsuario" onchange="comprobarCampos();" onkeyup="this.onchange();" name="apellidos" type="text" /></td>

  </tr>


      <!--- Campo DNI -->
  <tr>

  <td style="text-align:right"><label>Contraseña</label></td>

  <td><input id="campoContraseña" onchange="comprobarCampos();" onkeyup="this.onchange();" name="dni" type="password" /></td>

  </tr>





</table>

  </div>

  

  <!-- Datos del socio -->

  <div id="datosDePersona" class="tab-pane fade">

    <h4>Datos de la persona</h4>

<form>
   <input type="text" class="tamayestilo" id="campoBusqueda1" placeholder="Buscar..." onChange="actualizarListaBusquedaPersonas($(this).val());" onKeyUp="this.onchange();" size="30">
 </form>

      <table id="tablaBusquedaPersonas" class="table" >
    <thead>
    <tr>
        <th>DNI</th>
        <th>Nombre</th>
        <th>Apellidos</th>
         <th>Seleccion</th>
    </tr>
    </thead>
    <tbody id="bodyBusquedaPersonas" class="cursorManito">

    </tbody>
    


</table>

  </div>

</div>

		

      </div>

      <div class="modal-footer">
      <span id="advertenciaDatos" style="color:red;"></span>
        <button type="button" id="botonAceptar" class="btn btn-success" data-dismiss="modal" href="javascript:;" onclick="if(!editar){agregarUsuario();}else{editarUsuario()}return false;" >Aceptar</button>

        <button type="button" class="btn btn-default" data-dismiss="modal" href="javascript:;" onclick="">Cancelar</button>

      </div>

      </form>

    </div>

  </div>

</div>
<!---     ****     TERMINA VENTANA AGREGAR  ***             -->





<!-- ************VENTANA SOCIOS ADHERENTES****************  --> 

<!-- Modal -->

<div id="ventanaSociosAdherentes"  style="color:black;" class="modal fade" role="dialog">

  <div class="modal-dialog">



    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

<button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Socios Adherentes</h4>

      </div>



<div class="modal-body">
<h4>Agregar nuevo socio adherente</h4>
<br>

<form id="datoSocioAdherente">

  <table width="100%" style="border:0px; border-spacing:5px;border-collapse: separate;">

  <tr>

  <td style="text-align:right"><label>Nombre Completo</label></td>

  <td><input id="campoNombreCompletoSA" onchange="" onkeyup="this.onchange();" name="nombreCompletoSA" type="text" /></td>

  </tr>  

  <tr>

  <td style="text-align:right"><label>DNI</label></td>

  <td><input id="campoDNISA" onchange="" onkeyup="this.onchange();" name="dniSA" type="number" /></td>

  </tr>           


  <tr>

  <td style="text-align:right" ><label>Fecha de Nacimiento</label></td>

  <td> <input id="campoFechaNacimientoSA" onchange="" onkeyup="this.onchange();" name="fechaDeNacimientoSA" type="date" /></td>

  </tr>     

  <tr>

  <td style="text-align:right"><label>Parentezco</label></td>

  <td> <input id="campoParentezcoSA"  name="parentezcoSA" type="text" /></td>

  <td align="right"><button type="button" class="btn btn-success" href="javascript:;" onClick="agregarSocioAdherente()">Agregar</button></td>
  <td align="right"><button type="button" class="btn btn-danger" href="javascript:;" onClick="eliminarSocioAdherente()">Eliminar</button></td>

  </tr>

</table>

<table id="tablaSociosAdherentes" class="table">
    <thead>
    <tr>
        <th>Nombre Completo</th>
        <th>DNI</th>
        <th>Parentezco</th>
        <th>Fecha de Nacimiento</th>
        <th>Seleccion</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>

</div>

      <div class="modal-footer">
      <span id="advertenciaDatos" style="color:red;"></span>
        <button type="button" class="btn btn-default" data-dismiss="modal" href="javascript:;" onclick="">Aceptar</button>
      </div>

</form>


</div>
</div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: black">Módulo de Usuarios</h4>
      </div>
      <div class="modal-body" style="color: black">
        <p align="justify">Este es el módulo de usuarios. Se muestra en pantalla el listado de los usuarios que tienen acceso al sistema con la persona que la posee. Las funciones disponibles en esta sección son: </p>
        <p><u>Alta y Modificacion:</u></p>
        <p align="justify">Este módulo permite agregar un nuevo usuario para una persona que se desee que tenga acceso al sistema, asignandole un usuario y contraseña y un rol. El rol de usuario permitidos es Cobrador o Administrador, los cuales son independientes uno del otro.</p>
        

       </div>
      <div class="modal-footer">
        <center><button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button></center>
      </div>
    </div>

  </div>
</div>


</body>

</html>