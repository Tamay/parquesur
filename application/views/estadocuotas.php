<!------------------ JAVASCRIPT -------------------->


<!------------------ JAVASCRIPT -------------------->
<link rel="stylesheet" type="text/css" href="/ParqueSur/css/tablas/css/jquery.dataTables.css">
<script type="text/javascript" src="<?php echo base_url("/js/tablas/js/jquery.dataTables.js");?>"></script>
<center><h2><?php echo"Hola ". $nombre ." ". $apellidos.", ". "estos son los estados de sus cuotas"?></center></h2>
<script>
function actualizarLista()
{
  var parametros = 
    {
                "idSocio" : "<?php echo ($idSocio); ?>"
    };
   $.ajax({
                url:   '<?= site_url("/CSocioLogin/actualizarEstados") ?>',
                type:  'post',
        dataType: "json",
        data: parametros,
                success:  function (response)
        {
          // La siguiente línea es para borrar todas las filas excepto la primera (sería la cabecera)
             
          $("#bodylistaDeEstados").children().remove();
            $.each(response.estados, function(i, item) {
                    
                    // Asigno a la variable preinscripto, cada socio que se recorre del archivo JSON
                    var estado = response.estados[i];

                    // Inserto en la tabla cada preinscripcion
                    insertarElementoEnTabla(estado.mes,estado.anio,estado.monto,estado.estado);
                });

                }});
}


function insertarElementoEnTabla(mes,anio,monto,estado)
{
var table = document.getElementById('bodylistaDeEstados');
var rowCount = table.rows.length;
var row = table.insertRow(rowCount);

var celdaMes = row.insertCell(0);
var celdaAnio = row.insertCell(1);
var celdaMonto = row.insertCell(2);
var celdaEstado = row.insertCell(3);

celdaMes.innerHTML = mes;
celdaAnio.innerHTML = anio;
celdaMonto.innerHTML = monto;
celdaEstado.innerHTML = estado;

}
</script>


<body onload="actualizarLista()">
<div class="container cajas">    

 
<div class="row">
<h1>Lista de cuotas</h1>

  <!------------------ TABLA DE DATOS -------------------->

      <table id="tabladeestados" class="table display table-responsive">
      <thead>
        <tr>
         
        <th>Mes</th>
        <th>Año</th>
        <th>Monto</th>
        <th>Estado</th>
        </tr>
      </thead>
      <tbody id="bodylistaDeEstados" class="cursorManito">
    </tbody>

    </table>



    <!------------------ /TABLA DE DATOS --------------------> 

	</div>

</div>


</div>
</body>
</html>