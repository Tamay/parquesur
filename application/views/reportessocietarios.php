<!doctype html>
<html lang="es">
<head>
<!--   Core JS Files   -->
  <script src="/parquesur/js/jquery-3.1.0.min.js" type="text/javascript"></script>
  <script src="/parquesur/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="/parquesur/js/material.min.js" type="text/javascript"></script>


<!--   Para exportar los datos   -->
<script type="text/javascript" src="<?php echo base_url("/js/tableExport.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("/js/jquery.base64.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("/js/jspdf/libs/sprintf.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("/js/jspdf/jspdf.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("/js/jspdf/libs/base64.js");?>"></script>

 
  <!-- Material Dashboard javascript methods -->
  <script src="/parquesur/js/material-dashboard.js"></script>


    <!--     Tablas     -->
    <link rel="stylesheet" type="text/css" href="<?=base_url("/css/tablas/css/jquery.dataTables.css");?>">
    <script type="text/javascript" src="<?=base_url("/js/tablas/js/jquery.dataTables.js");?>"></script>
    <script type="text/javascript" src="<?=base_url("/js/tablas/js/dataTables.buttons.min.js");?>"></script>
    <script type="text/javascript" src="<?=base_url("/js/tablas/js/buttons.print.min.js");?>"></script>
    <script type="text/javascript" src="<?=base_url("/js/tablas/js/dataTables.select.min.js");?>"></script>
    <script type="text/javascript" src="<?=base_url("/js/tablas/js/dataTables.scroller.min.js");?>"></script>
    <script type="text/javascript" src="<?=base_url("/js/tablas/js/pdfmake.min.js");?>"></script>
    <script type="text/javascript" src="<?=base_url("/js/tablas/js/jszip.min.js");?>"></script>
    <script type="text/javascript" src="<?=base_url("/js/tablas/js/vfs_fonts.js");?>"></script>
    <script type="text/javascript" src="<?=base_url("/js/tablas/js/buttons.html5.min.js");?>"></script>
    <script type="text/javascript" src="<?=base_url("/js/jquery-ui.js");?>"></script>
    <link rel="stylesheet" type="text/css" href="<?=base_url("/css/tablas/css/scroller.dataTables.min.css");?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url("/css/jquery-ui.css");?>">




  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="/parquesur/img/apple-icon.png" />
  <link rel="icon" type="image/png" href="" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="/parquesur//css/bootstrap.min.css" rel="stylesheet" />

    <!--  Material Dashboard CSS    -->
    <link href="/parquesur/css/material-dashboard.css" rel="stylesheet"/>

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
<style>
    @media print {
  @bottom-left {
        content: counter(page);
     }
    }
    @page {
    @bottom-left {
        content: counter(page);
     }
 }
</style>


<script type="text/javascript">
var actualizarAltasYBajas;
var actualizarSociosActivos;
var actualizarSociosMorosos;
var cantidadSocios=0;
var cantidadSociosMorosos=0;
var cantidadSociosEmbarcaciones=0;
var cantidadEmbarcaciones=0;
var montoTotalEmbarcaciones=0;
var mostrarInactivos;
var FechaDesde;
var FechaHasta;


var montoTotalServicios=0;
var montoTotalAbonos=0;
var montoTotal=0;



function fechaDeHoy()
{
    var d = new Date();
    var month = d.getMonth()+1;
    var day = d.getDate();
    console.log(day+'/'+month+'/'+ d.getFullYear());
    return (day+'/'+month+'/'+ d.getFullYear());
}


function controlarOpcion()
{
var opcion=$("#opcion").val();

///------------------------------------------ SOCIOS ACTIVOS ----------------------------------------///
if(opcion=="sociosactivos")
{
	//QUE HACEMOS
    actualizarSociosActivos=false;

  // QUE MOSTRAMOS
  $("#sociosactivos").show();


  //QUE ESCONDEMOS
  $("#altasybajas").hide();
  $("#sociosconembarcaciones").hide();
  $("#sociosadherentes").hide();
  $("#sociosMorosos").hide();
}
///------- SOCIOS ACTIVOS ---------///

else
{


  ///------- ALTAS Y BAJAS ---------///
  if(opcion=="altasybajas")
  {
  // QUE MOSTRAMOS
  $("#altasybajas").show();
  actualizarAltasYBajas=false;

  //QUE ESCONDEMOS
  $("#sociosactivos").hide();
  $("#sociosconembarcaciones").hide();
  $("#sociosadherentes").hide();
  $("#sociosMorosos").hide();
  }
  ///------- ALTAS Y BAJAS ---------///





  else
    {
      ///------- SOCIOS CON EMBARCACIONES ---------///
      if(opcion=="sociosconembarcaciones")
      {
        obtenerSociosConEmbarcaciones();

        // QUE MOSTRAMOS
        $("#sociosconembarcaciones").show();

        //QUE ESCONDEMOS
        $("#sociosactivos").hide(); 
        $("#altasybajas").hide();
        $("#sociosadherentes").hide();
        $("#sociosMorosos").hide();
      }
      ///------- SOCIOS CON EMBARCACIONES ---------///


      else
      {
      	///------- SOCIOS ADHERENTES PASADOS DE EDAD ---------///
      	if(opcion=="sociosadherentes")
      	{
      		obtenerSociosAdherentes();
      		// QUE MOSTRAMOS
        $("#sociosadherentes").show();

        //QUE ESCONDEMOS
        $("#sociosactivos").hide(); 
        $("#altasybajas").hide();
        $("#sociosconembarcaciones").hide();
        $("#sociosMorosos").hide();
      	}
      	///------- SOCIOS ADHERENTES PASADOS DE EDAD ---------///



        else
        {
                  ///------------ SOCIOS MOROSOS ---------///
        if(opcion=="sociosMorosos")
        {
            var actualizarSociosActivos=false;
          // QUE MOSTRAMOS
        $("#sociosMorosos").show();

        //QUE ESCONDEMOS
        $("#sociosactivos").hide(); 
        $("#altasybajas").hide();
        $("#sociosconembarcaciones").hide();
        $("#sociosadherentes").hide();
        }
        ///------------- SOCIOS MOROSOS ---------///
        }
      }

    }
}
}  

  var valorInactivos;
  var valorPadronElectoral;
  var parametrosSociosActivos;


///------------------------------------------ SOCIOS ACTIVOS ----------------------------------------///
function obtenerSociosActivos()
{
    montoTotalAbonos=0;
    montoTotalServicios=0;
    montoTotal=0;


    var fixNewLine = {
        exportOptions: {
            format: {
                body: function ( data, column, row ) {
                    // Strip $ from salary column to make it numeric
                    return column === 5 ?
// THIS WORKS:          data.replace(/test/ig, "blablabla"):
                        data.toString().replace( /<br\s*\/?>/ig, "\n" ) :
                        data;
                }
            }
        }
    };

actualizarparametrosSociosActivos();
    if(!actualizarSociosActivos)
    {
        cantidadSocios = 0;
        actualizarSociosActivos=true;
        $('#tablaSociosActivos').DataTable( {
        "ajax": {
                //"data": parametros,
                "data": function(d)
                {
                  return parametrosSociosActivos;
                },
                "url":   "<?=site_url("CReporte/sociosactivos")?>",
                "type":  'post',
                "beforeSend": function () {
                        $("#modal_Cargando").modal({backdrop: 'static', keyboard: false});
                        $("#modal_Cargando").modal('show');
                },
                  "complete": function (response) {
                        $("#modal_Cargando").modal('hide');
                      var table = $('#tablaSociosActivos').DataTable();
                      cantidadSocios = table.data().count();
                      $('#cantidadDeSocios').html('La cantidad resultante de socios es: <h5>'+cantidadSocios+'<h5>');

                      
                      if(parametrosSociosActivos['inactivos']==1)
                        {mostrarInactivos='SI'}
                    else{mostrarInactivos='NO'}



               $('#tablaSociosActivos tbody tr').each(function(indiceFila) {
                 $(this).children('td').each(function(indiceColumna) {
                    if(indiceColumna == 9)
                        {
                            montoTotalServicios+=parseInt($(this).text());
                        }
                    if(indiceColumna == 10)
                        {
                            montoTotalAbonos+=parseInt($(this).text());
                        }
                        if(indiceColumna == 11)
                        {
                            montoTotal+=parseInt($(this).text());
                        }
                    });
                });



                
                }
    


                },
            dom: 'Bfrtip',
            buttons: [
                    {
                        extend: 'excelHtml5',
                        text: '<button type="button" class="btn btn-default">EXPORTAR A EXCEL</button>',
                        title: 'Lista de socios'
                    },
                      $.extend( true, {}, fixNewLine, {
                        extend: 'print',
                        title: "Padrón de Socios - Club Parque Sur",
                        text: '<button type="button" class="btn btn-default">IMPRIMIR</button>',
                        orientation: 'landscape',
                        message: function(){return 'Cantidad de socios: '+cantidadSocios+' | Reporte emitido el '+fechaDeHoy()
                        +' | FILTRO: No más de '+parametrosSociosActivos['cuotasImpagas']+' cuotas impagas y ' +
                            ' más de '+parametrosSociosActivos['mesesAntiguedad']+' meses de antigüedad.'+
                            ' Mostrar socios inactivos: '+mostrarInactivos},
                customize: function ( win ) { 
                    console.log(win);
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', '10pt' )
                        .css( 'font-family', 'sans-serif' );

                        $(win.document.body).find( 'h1' )
                        .css( 'font-size', '20pt' );


                    $(win.document.body).find( 'table td' )
                                            .css( 'border-bottom', '1px solid black' )
                                            .css( 'border-collapse', 'collapse' )
                                            .css( 'height', '45px' )
                                            .css( 'vertical-align', 'center' );

                    $(win.document.body).find( 'table th' )
                                            .css( 'border-bottom', '1px solid black' )
                                            .css( 'border-collapse', 'collapse' );
                    

                    win.document.body.innerHTML =  win.document.body.innerHTML+('<h4 style="text-align:right">Facturación total -  Servicios: $'+montoTotalServicios+' | Abonos: $'+montoTotalAbonos+' | Total: $'+montoTotal+'</h4>');
                    }
                })
            ],


        "columns": [
            { "data": "id" },
            { "data": "nombreCompleto" },
            { "data": "dni" },
            { "data": "domicilio" },
            { "data": "fechaNacimiento" },
            { "data": "fechaAlta" },
            { "data": "fechaBaja" },
            { "data": "telefonos" },
            { "data": "tipoSocio" },
            { "data": "montoServicios" },
            { "data": "montoAbonos" },
            { "data": "montoTotal" }
        ],
            aLengthMenu: [
                [-1],
                ["Todos"]
            ],
            "order": [[1,"asc"]]});

}
    else{
        actualizarTablaSociosActivos();
    }
}

function actualizarTablaSociosActivos()
{
    var t = $('#tablaSociosActivos').DataTable();
//t.ajax.clear().draw();
    t.ajax.reload();
}

function actualizarparametrosSociosActivos()
{
    var valorCheckbox;
    var valorMesesAntiguedad=$("#textbox_mesesAntiguedad").val();
    var valorCuotasImpagas=$("#textbox_cuotasImpagas").val();

    if(valorMesesAntiguedad=='')
    {
        valorMesesAntiguedad=0;
        $("#textbox_mesesAntiguedad").val(0);
    }
    if(valorCuotasImpagas=='')
    {
        valorCuotasImpagas=0;
        $("#textbox_cuotasImpagas").val(0);
    }

    if($("#checkbox_Inactivos").is(':checked')) {
        valorCheckbox=1;
    } else {
        valorCheckbox=0;
    }

    parametrosSociosActivos =
    {
        "inactivos": valorCheckbox,
        "mesesAntiguedad": valorMesesAntiguedad,
        "cuotasImpagas":valorCuotasImpagas
    }
}




///------------------------------------------ SOCIOS MOROSOS ----------------------------------------///

var parametrosSociosMorosos;

function controlarIntervaloCuotasImpagas()
{
    var valorMin=parseInt($("#textbox_MinCuotasImpagas").val());
    var valorMax=parseInt($("#textbox_MaxCuotasImpagas").val());
    if(valorMin>valorMax)
    {
        console.log("Valor Min: "+valorMin);
        console.log("Valor Max: "+valorMax);
        $("#textbox_MaxCuotasImpagas").val($("#textbox_MinCuotasImpagas").val());
    }
}


function actualizarParametrosSociosMorosos()
{
    parametrosSociosMorosos =
    {
        "minCuotas": $("#textbox_MinCuotasImpagas").val(),
        "maxCuotas": $("#textbox_MaxCuotasImpagas").val()
    };
}

function obtenerSociosMorosos()
{

var fixNewLine = {
        exportOptions: {
            format: {
                body: function ( data, column, row ) {
                    // Strip $ from salary column to make it numeric
                    return column === 5 ?
// THIS WORKS:          data.replace(/test/ig, "blablabla"):
                        data.toString().replace( /<br\s*\/?>/ig, "\n" ) :
                        data;
                }
            }
        }
    };


actualizarParametrosSociosMorosos();

    if(!actualizarSociosMorosos)
    {
        actualizarSociosMorosos=true;
            $('#tablaSociosMorosos').DataTable( {
                "ajax": {
                    "url":   "<?=site_url("CReporte/sociosMorosos")?>",
                    "type":  'post',
                    "data": function(d)
                    {
                        return parametrosSociosMorosos;
                    },
                    "beforeSend": function () {
                          $("#modal_Cargando").modal({backdrop: 'static', keyboard: false});
                          $("#modal_Cargando").modal('show');
                    },
                    "complete":  function (response) {
                            $("#modal_Cargando").modal('hide');
                            var table = $('#tablaSociosMorosos').DataTable();
                            cantidadSociosMorosos = table.data().count();
                        $('#cantidadDeSociosMorosos').html('Cantidad total de socios morosos: <h5>'+cantidadSociosMorosos+'<h5>');

                    }
                },
                "columns": [
                    { "data": "id" },
                    { "data": "nombreCompleto" },
                    { "data": "dni" },
                    { "data": "deudasImpagas" },
                    { "data": "cobrador" },
                    { "data": "tipoSocio" }
                ],
                "order": [[1,"asc"]],
                dom: 'Bfrtip',
                buttons: [
                    $.extend( true, {}, fixNewLine, {
                        extend: 'print',
                        title: "Padrón de socios morosos",
                        text: '<button type="button" class="btn btn-default">IMPRIMIR</button>',
                        orientation: 'landscape',
                        message: function(){return 'Cantidad de socios: '+cantidadSociosMorosos+' | Reporte emitido el '+fechaDeHoy()
                            +' | FILTRO: Entre '+parametrosSociosMorosos['minCuotas']+' y ' +
                            parametrosSociosMorosos['maxCuotas']+' cuotas impagas'},
                        orientation: 'landscape',
                        customize: function ( win ) { 
                            console.log(win);
                            $(win.document.body).find( 'table' )
                                .addClass( 'compact' )
                                .css( 'font-size', '10pt' );

                            $(win.document.body).find( 'h1' )
                                .css( 'font-size', '20pt' );

                            $(win.document.body).find( 'table td' )
                                                    .css( 'border-bottom', '1px solid black' )
                                                    .css( 'border-collapse', 'collapse' )
                                                    .css( 'height', '40px' )
                                                    .css( 'vertical-align', 'center' );

                            $(win.document.body).find( 'table th' )
                                                    .css( 'border-bottom', '1px solid black' )
                                                    .css( 'border-collapse', 'collapse' );
                            
                        }
                        }),



                    {
                        extend: 'excelHtml5',
                        text: '<button type="button" class="btn btn-default">EXPORTAR A EXCEL</button>',
                        title: "PADRÓN DE SOCIOS MOROSOS"
                    }
                ]
             });
}
    else
    {
        var t = $('#tablaSociosMorosos').DataTable();
        //t.ajax.clear().draw();
        t.ajax.reload();
    }
}



///------------------------------------------ SOCIOS ALTAS Y BAJAS ----------------------------------------///

var parametrosAltasYBajas;

function actualizarparametrosAltasyBajas()
{
    parametrosAltasYBajas =
    {
        "fechaDesde": $("#campoFechaDesdeAltasyBajas").val()+'-01',
        "fechaHasta": $("#campoFechaHastaAltasyBajas").val()+'-01'
    };
}


function obtenerAltasYBajas()
{
 var fixNewLine = {
        exportOptions: {
            format: {
                body: function ( data, column, row ) {
                    // Strip $ from salary column to make it numeric
                    return column === 5 ?
// THIS WORKS:          data.replace(/test/ig, "blablabla"):
                        data.toString().replace( /<br\s*\/?>/ig, "\n" ) :
                        data;
                }
            }
        }
    };


    actualizarparametrosAltasyBajas();

    if(!actualizarAltasYBajas)
    {
        actualizarAltasYBajas=true;
        $('#tablaAltasYBajas').DataTable( {
            "ajax": {
                "data": function(d)
                {
                    return parametrosAltasYBajas;
                },
                "url":   "<?=site_url("CReporte/altasBajas")?>",
                "type":  'post'
            },
            "columns": [
                { "data": "id" },
                { "data": "nombreCompleto" },
                { "data": "situacion" },
                { "data": "fecha" },
                { "data": "tipoSocio" }
            ],
            "order": [[1,"asc"]],
          dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                text: '<button type="button" class="btn btn-default">EXCEL</button>',
                title: "Altas y bajas de los socios"
            },
             
                $.extend( true, {}, fixNewLine, {
                extend: 'print',
                title: "Listado de altas y bajas",
                text: '<button type="button" class="btn btn-default">IMPRIMIR</button>',
                message: function(){
                    var fechaDesdeFormato
                    var fechaHastaFormato

                    fechaDesdeFormato = new Date(parametrosAltasYBajas['fechaDesde'].toString());
                    var d = fechaDesdeFormato.getDate();
                    var m =  fechaDesdeFormato.getMonth();
                    m += 1;  // JavaScript months are 0-11
                    var y = fechaDesdeFormato.getFullYear();
                    fechaDesdeFormato = d+'/'+m+'/'+y;

                    fechaHastaFormato = new Date(parametrosAltasYBajas['fechaHasta'].toString());
                    var d = fechaHastaFormato.getDate();
                    var m =  fechaHastaFormato.getMonth();
                    m += 1;  // JavaScript months are 0-11
                    var y = fechaHastaFormato.getFullYear();
                    fechaHastaFormato = d+'/'+m+'/'+y;


                    return 'Reporte emitido el '+fechaDeHoy()+
                ' | '+
                'FILTROS: Desde '+fechaDesdeFormato+' Hasta '+fechaHastaFormato+
                '<br><br>'},
                orientation: 'landscape',
                customize: function ( win ) { 
                    console.log(win);
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', '11pt' );

                    $(win.document.body).find( 'h1' )
                        .css( 'font-size', '20pt' );

                    $(win.document.body).find( 'table td' )
                                            .css( 'border-bottom', '1px solid black' )
                                            .css( 'border-collapse', 'collapse' )
                                            .css( 'height', '50px' )
                                            .css( 'vertical-align', 'center' );

                    $(win.document.body).find( 'table th' )
                                            .css( 'border-bottom', '1px solid black' )
                                            .css( 'border-collapse', 'collapse' );
                    
                }
                })
        ]  
        });
    }
    else{
        var t = $('#tablaAltasYBajas').DataTable();
        //t.ajax.clear().draw();
        t.ajax.reload();
    }

}


function obtenerCantidadTotalDeEmbarcaciones()
{
    $.ajax({
        url:   "<?=site_url("CReporte/CantidadDeEmbarcaciones")?>",
        success:  function (response)
        {
            $('#cantidadEmbarcaciones').html('Cantidad de embarcaciones: '+response);
            cantidadEmbarcaciones=response;
        }
    });
}



function obtenerSociosConEmbarcaciones()
{

var fixNewLine = {
        exportOptions: {
            format: {
                body: function ( data, column, row ) {
                    // Strip $ from salary column to make it numeric
                    return column === 5 ?
// THIS WORKS:          data.replace(/test/ig, "blablabla"):
                        data.toString().replace( /<br\s*\/?>/ig, "\n" ) :
                        data;
                }
            }
        }
    };




    cantidadSociosEmbarcaciones=0;
$('#tablaSociosConEmbarcaciones').DataTable( {
        "ajax": {
            "url":'<?=site_url("CReporte/sociosConEmbarcaciones")?>',
            "complete":  function () {
                var table = $('#tablaSociosConEmbarcaciones').DataTable();
                cantidadSociosEmbarcaciones = table.data().count();
                $('#cantidadDeSociosConEmbarcaciones').html('Cantidad resultante de socios es: '+cantidadSociosEmbarcaciones+'<br>');
                obtenerCantidadTotalDeEmbarcaciones();

               $('#tablaSociosConEmbarcaciones tbody tr').each(function(indiceFila) {
                 $(this).children('td').each(function(indiceColumna) {
                    if(indiceColumna == 8)
                        {//Indice de la columna Office
                            montoTotalEmbarcaciones+=parseInt($(this).text());
                        }
                    });
                });
            }
        },
        "columns": [
            { "data": "id" },
            { "data": "nombreCompleto" },
            { "data": "nombresEmbarcaciones" },
            { "data": "matriculasEmbarcaciones" },
            { "data": "tiposEmbarcaciones" },
            { "data": "domicilio" },
            { "data": "telefonos" },
            { "data": "observaciones" },
            { "data": "monto" }
            
        ],
        "columnDefs": [
            { "targets": 0, "className": "text-left" }
        ],
        aLengthMenu: [
            [-1],
            ["Todos"]
        ],
        "order": [[1,"asc"]],
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                text: '<button type="button" class="btn btn-default">EXCEL</button>',
                title: "Padrón de socios con embarcaciones"
            },
             
                $.extend( true, {}, fixNewLine, {
                extend: 'print',
                title: "Padrón de socios con embarcaciones",
                text: '<button type="button" class="btn btn-default">IMPRIMIR</button>',
                message: function(){return 'Cantidad de socios: '+cantidadSociosEmbarcaciones+' | ' +
                    'Cantidad de embarcaciones: '+cantidadEmbarcaciones +
                    ' | '+
                    'Reporte emitido el '+fechaDeHoy()+'<br><br>'},
                orientation: 'landscape',
                pageSize: 'A4',
                customize: function ( win ) { 
                    console.log(win);
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', '11pt' );

                    $(win.document.body).find( 'h1' )
                        .css( 'font-size', '20pt' );

                    $(win.document.body).find( 'table td' )
                                            .css( 'border-bottom', '1px solid black' )
                                            .css( 'border-collapse', 'collapse' )
                                            .css( 'height', '50px' )
                                            .css( 'vertical-align', 'center' );

                    $(win.document.body).find( 'table th' )
                                            .css( 'border-bottom', '1px solid black' )
                                            .css( 'border-collapse', 'collapse' );
                    
                win.document.body.innerHTML =  win.document.body.innerHTML+('<br><p style="text-align:right">Promedio de dinero por embarcacion: '+parseFloat(montoTotalEmbarcaciones/cantidadEmbarcaciones).toFixed(2)+'</p><h4 style="text-align:right">Facturación total: $'+montoTotalEmbarcaciones+'</h4>');
                }
                })
        ]
    });
}


function obtenerSociosAdherentes()
{
$('#tablasociosadherentes').DataTable( {
        "ajax": '<?=site_url("CReporte/sociosAdherentes")?>',
        "columns": [
            { "data": "nombre" },
            { "data": "nroSocio" },
            { "data": "apellidos" },
            { "data": "edad" }
        ]
    }); 
}

</script>




</head>

<body onload="controlarOpcion()">
<input id="opcion" type="hidden" value="<?=$_GET['opcion']?>">

  <div class="wrapper">

      <div class="sidebar" data-color="purple" data-image="/img/sidebar-1.jpg">
      <!--
            Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

            Tip 2: you can also add an image using data-image tag
        -->

      <div class="logo">
        <a href="" class="simple-text">
          Reportes
        </a>
      </div>

             <!-- MENU IZQUIERDO -->
        <div class="sidebar-wrapper">
              <ul class="nav">
                  <li class="active">
                      <a href="<?= site_url('CReporte/index') ?>">
                          <i class="material-icons">dashboard</i>
                          <p>Tabla general</p>
                      </a>
                  </li>
                   <ul class="nav">
                  <h4><i class="material-icons">person</i>Societarios</h4>
                  <li>
                      <a href="<?= site_url('CReporte/reportesSocietarios?opcion=sociosactivos') ?>">
                          
                          <p>Socios Activos</p>
                      </a>
                  </li>
                     <li>
                      <a href="<?= site_url('CReporte/reportesSocietarios?opcion=sociosMorosos') ?>">
                          
                          <p>Socios Morosos</p>
                      </a>
                  </li>
                   <li>
                      <a href="<?= site_url('CReporte/reportesSocietarios?opcion=altasybajas') ?>">
                          
                          <p>Altas y bajas</p>
                      </a>
                  </li>
                  <li>
                      <a href="<?= site_url('CReporte/reportesSocietarios?opcion=sociosconembarcaciones') ?>">
                          
                          <p>Socios con embarcaciones</p>
                      </a>
                  </li>
                  <li>
                      <a href="<?= site_url('CReporte/reportesSocietarios?opcion=sociosadherentes') ?>">
                          
                          <p>Socios Adherentes</p>
                      </a>
                  </li>

                  </ul>
                  <ul class="nav">
                  <h4><i class="material-icons">content_paste</i>Financieros</h4>
                  <li>
                      <a href="<?= site_url('CReporte/reportesFinancieros?opcion=recaudacion') ?>">
                          <p>Recaudación bruta</p>
                      </a>
                  </li>
                  <li>
                      <a href="<?= site_url('CReporte/reportesFinancieros?opcion=recaudadoxcobrador') ?>">
                          <p>Recaudado por cobrador (bruto)</p>
                      </a>
                  </li>
                  <li>
                      <a href="<?= site_url('CReporte/reportesFinancieros?opcion=recaudacionxdeporte') ?>">
                          <p>Recaudación por deporte (bruta mensual)</p>
                      </a>
                  </li>

                  </ul>
              </ul>
            <ul class="nav">
                <h4><i class="material-icons">directions_boat</i>Embarcaciones</h4>
                <li>
                    <a href="<?= site_url('CReporte/reportesEmbarcaciones?opcion=reporteParaPrefectura') ?>">
                        <p>Reporte para prefectura</p>
                    </a>
                </li>

            </ul>

              </ul>
        </div>
      </div>
      <!--   FINALIZA EL MENÚ IZQUIERDO -->

      

      <div class="main-panel">
      <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= base_url();?>">Volver atrás</a>
          </div>
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">

            </ul>

          </div>
        </div>
      </nav>

      <div class="content">
        <div class="container-fluid">
          


        <!-- SOCIOS ACTIVOS -->
          <div class="row" id="sociosactivos">
          <h2>Padrón de socios</h2>
          <h3>Opciones de filtrado</h3>
              <table>
                  <tr>
                      <td colspan="3"><input type="checkbox" id="checkbox_Inactivos" value="1" onclick=""> Inactivos</td>
                  </tr>
                  <tr>
                      <td style="text-align:right;">Menos de &nbsp;</td>
                      <td><input type="number"  id="textbox_cuotasImpagas" value="0" onclick=""></td>
                      <td>&nbsp;Cuotas impagas</td>
                  </tr>
                  <tr>
                      <td style="text-align:right;">Mas de &nbsp;</td>
                      <td><input type="number"  id="textbox_mesesAntiguedad" value="0" onclick=""></td>
                      <td>&nbsp;Meses de antigüedad</td>
                  </tr>
              </table>
              <p>Ambos valores son inclusives</p>
              <input type="button" onClick="obtenerSociosActivos()" value="Listar">


                 <br><br>
                <span id="cantidadDeSocios"></span>
              <div class="col-md-12"> <br>  <h3>Listado</h3></div>
          <div class="col-md-12">
                  <table id="tablaSociosActivos" class="table display table-responsive">
                  <thead>
                  <tr>
                  <th>Nro Socio</th>
                  <th>Apellido y nombre</th>
                  <th>DNI</th>
                  <th>Domicilio</th>
                  <th>Fecha Nac.</th>
                  <th>Fecha Alta</th>
                  <th>Fecha Baja</th>
                  <th>TEL/CEL</th>
                  <th>Tipo Soc.</th>
                  <th>Serv.</th>
                  <th>Abono</th>
                  <th>Total</th>
                  </tr>
                </thead>

              <tbody class="cursorManito">
              </tbody>
             <tfoot>
                  <tr>
                      <th>Nro Socio</th>
                  <th>Apellido y nombre</th>
                  <th>DNI</th>
                  <th>Domicilio</th>
                  <th>Fec. Nac</th>
                  <th>Fec. Alta</th>
                  <th>Fec. Baja</th>
                  <th>TEL/CEL</th>
                  <th>Tipo Soc.</th>
                  <th>Serv.</th>
                  <th>Abono</th>
                  <th>Total</th>
                  </tr>
                  </tfoot>
                </table>
                </div>
          </div>
        <!-- SOCIOS ACTIVOS -->

        
		<!-- ALTAS Y BAJAS -->
        <div class="row" id="altasybajas">
            <h3>Registro de Altas y Bajas</h3>
        <div class="col-md-12">
        <h3> Seleccione el rango de fechas</h3>
        <p>Desde: <input type="month" id="campoFechaDesdeAltasyBajas"> &nbsp | &nbsp Hasta &nbsp<input type="month" id="campoFechaHastaAltasyBajas"> &nbsp <input type="button" onClick="obtenerAltasYBajas()": value="Buscar">
        </p>
            <p>(Se toma el primero de cada mes y ambas fechas son inclusivas)</p>
        </div>


        <div class="col-md-12">
        <table id="tablaAltasYBajas" class="table display table-responsive">
                  <thead>
                  <tr>
                  <th>Nro Socio</th>
                  <th>Apellido y nombre</th>
                  <th>Situación</th>
                  <th>Fecha</th>
                  <th>Tipo de Socio</th>
                  </tr>
                </thead>


              <tbody class="cursorManito">
              </tbody>
             <tfoot>
                  <tr>
                  <th>Nro Socio</th>
                  <th>Apellido y nombre</th>
                  <th>Situación</th>
                  <th>Fecha</th>
                  <th>Tipo de Socio</th>
                  </tr>
                  </tfoot>
                </table>	
        </div>
        </div>
        <!-- ALTAS Y BAJAS -->


        <!-- SOCIOS CON EMBARCACIONES -->
          <div class="row" id="sociosconembarcaciones">
              <h3>Padrón de socios con embarcaciones</h3>
              <span id="cantidadDeSociosConEmbarcaciones"></span><span id="cantidadEmbarcaciones"></span>
          <div class="col-md-12">
                  <table id="tablaSociosConEmbarcaciones" class="table display table-responsive">
                  <thead>
                  <tr>
				  <th>Nº</th>
                  <th>Nombre Completo</th>
                  <th>Embarcaciones</th>
                  <th>Matriculas</th>
                  <th>Tipos de embarcaciones</th>
                  <th>Domicilio</th>
                  <th>TEL/CEL</th>
                  <th>Obs.</th>
                  <th>Monto</th>
                  </tr>
                </thead>


              <tbody class="cursorManito">
              </tbody>
             <tfoot>
                  <tr>
                  <th>Nº</th>
                  <th>Nombre Completo</th>
                  <th>Embarcaciones</th>
                  <th>Matriculas</th>
                  <th>Tipos de embarcaciones</th>
                  <th>Domicilio</th>
                  <th>TEL/CEL</th>
                  <th>Obs.</th>
                  <th>Monto</th>
                  </tr>
                  </tfoot>
                </table>
                </div>
                </div>
        <!-- SOCIOS CON EMBARCACIONES -->



        <!-- SOCIOS ADHERENTES  -->
          <div class="row" id="sociosadherentes">
              <h2>Socios adherentes</h2>
              <br>
          <div class="col-md-12">
                  <table id="tablasociosadherentes" class="table display table-responsive">
                  <thead>
                  <tr>
                  <th>Nombre Completo del Socio Adherente</th>
                  <th>Nº de Socio</th>
                  <th>Nombre Completo del Socio</th>

                  <th>Edad</th>
                  </tr>
                </thead>


              <tbody class="cursorManito">
              </tbody>
             <tfoot>
                  <tr>
                      <th>Nombre Completo del Socio Adherente</th>
                      <th>Nº de Socio</th>
                      <th>Nombre Completo del Socio</th>
                  <th>Edad</th>
                  </tr>
                  </tfoot>
                </table>
                </div>
                </div>
        <!-- SOCIOS ADHERENTES PASADOS DE EDAD -->




        <!-- SOCIOS MOROSOS-->
          <div class="row" id="sociosMorosos">
              <h2>Socios morosos</h2>
              <table>
              <tr>
                  <td style="text-align:right;">Tiene mas de &nbsp;</td>
                  <td><input type="number"  id="textbox_MinCuotasImpagas" onChange="controlarIntervaloCuotasImpagas()" value="0" onclick=""></td>
                  <td>&nbsp;Cuotas impagas</td>
              </tr>
                  <tr>
                      <td style="text-align:right;">Tiene menos de &nbsp;</td>
                      <td><input type="number"  id="textbox_MaxCuotasImpagas" value="0" onChange="controlarIntervaloCuotasImpagas()" onclick=""></td>
                      <td>&nbsp;Cuotas impagas</td>
                  </tr>
              </table>
              <p><br>(Se mostrarán aquellos socios que tengan igual o más de X cuotas<br> impagas
                  e igual o menos de X cuotas impagas)</p>
              <input type="button" onClick="obtenerSociosMorosos()": value="Listar">
              <br>
              <br>
              <span id="cantidadDeSociosMorosos"></span>
          <div class="col-md-12">
                  <table id="tablaSociosMorosos" class="table display table-responsive">
                  <thead>
                  <tr>
                  <th>Nro Socio</th>
                  <th>Nombre Completo</th>
                  <th>DNI</th>
                  <th>Deudas impagas</th>
                  <th>Cobrador</th>
                  <th>Tipo de abono</th>
                  </tr>
                </thead>


              <tbody class="cursorManito">
              </tbody>
             <tfoot>
                  <tr>
                  <th>Nro Socio</th>
                  <th>Nombre Completo</th>
                  <th>DNI</th>
                  <th>Deudas impagas</th>
                  <th>Cobrador</th>
                  <th>Tipo de abono</th>
                  </tr>
                  </tfoot>
                </table>
                </div>
                </div>
        <!-- SOCIOS MOROSOS -->



  </div>
  </div>
  
  
  
  
  
  <!-- Modal Cargando -->
<div id="modal_Cargando" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: black">Cargando....</h4>
      </div>
      <div class="modal-body" style="color: black">
      <h3>Aguarde un momento por favor...</h3>
      <p>Esta operación puede tardar algunos minutos.</p>
      <p>Por favor, no cierre esta ventana</p>
        <center><img src="<?php echo base_url('images/loading.gif')?>"></center>
       </div>
    </div>
  </div>
</div>

</body>
</html>
