<script>
  //Funcion para agregar una preinscripcion con los valores ingresados
function agregarPreinscripcion(){
        var parametros = {
                "nombre" : ($('#campoNombre').val()).toUpperCase(),
                "apellidos" : ($('#campoApellidos').val()).toUpperCase(),
                "dni" : $('#campoDNI').val(),
                "domicilio" : ($('#campoDomicilio').val()).toUpperCase(),
            "genero" : $('#campoGenero').val(),
        "correoElectronico" : $('#campoCorreoElectronico').val(),
        "telefonoFijo" : $('#campoTelefonoFijo').val(),
        "telefonoCelular" : $('#campoTelefonoCelular').val(),
        "nacionalidad" : ($('#campoNacionalidad').val()).toUpperCase(),
        "estadoCivil" : ($('#campoEstadoCivil').val()).toUpperCase(),
        "fechaNacimiento" : $('#campoFechaNacimiento').val(),
        "profesion" : ($('#campoProfesion').val()).toUpperCase(),    
    
        };
    
        $.ajax({
                data:  parametros,
                url:   '<?= site_url("/CPreinscripcion/agregar") ?>',
                type:  'post',
                success:  function (response)
         {
          if(response=='exito')
            {
              $("#resultado").html(response);
              vaciarCampos();
              $("#ventanaSuccess").modal("show");



              //window.alert("Gracias por preinscribirte, para completar la afiliación debes ir a la secretaria del club a pagar el monto de inscripción y listo!");

            }
          else
            {
              window.alert("El dni solicitante ya tiene una preinscripcion pendiente");
                  
            }
         }
        });
}



function comprobarCampos()
{
  if($('#campoNombre').val()=='' || $('#campoApellidos').val()=='' ||
    $('#campoDNI').val()==0 || $('#campoDomicilio').val()=='' ||
    $('#campoCorreoElectronico').val()=='' || $('#campoFechaNacimiento').val()=='' ||
    $('#campoDomicilio').val()=='')
  {
    $('#botonAceptar').prop('disabled', true);
    
  }
  else
    {
      $('#botonAceptar').prop('disabled', false); 
      }   
  }


function vaciarCampos()
{
        $('#campoNombre').val("");
        $('#campoApellidos').val("");
        $('#campoDNI').val("");
        $('#campoDomicilio').val("");
        $('#campoCorreoElectronico').val("");
        $('#campoFechaNacimiento').val("");
        $('#campoProfesion').val("");
        $('#campoGenero').val("");
    $('#campoNacionalidad').val("");
    $('#campoTelefonoFijo').val("");
    $('#campoTelefonoCelular').val("");
    $('#campoEstadoCivil').val("");
    
    comprobarCampos();
}

</script>
<body onLoad="comprobarCampos()">

<div class="col-md-3"> </div>
<div class="col-md-6 cajas">
<fieldset>

<!-- Form Name -->
<center><legend>Asociate a Parque Sur, el club más grande de la región</legend></center>

<!-- Text input-->

<div class="form-group">
  <label class="col-md-0 control-label" style="color: black">Nombres</label>  
  <div class="col-md-0 inputGroupContainer">
  <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
  <input  name="campoNombre" placeholder="Persona" class="form-control"  type="text" id="campoNombre" onChange="comprobarCampos()">
    </div>
  </div>
</div>

<!-- Text input-->

<div class="form-group">
  <label class="col-md-0 control-label" style="color:black">Apellidos</label> 
    <div class="col-md-0 inputGroupContainer">
    <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
  <input name="campoApellidos" placeholder="Ejemplo" class="form-control"  type="text" id="campoApellidos" onChange="comprobarCampos()">
    </div>
  </div>
</div>

<!-- Text input-->
       <div class="form-group">
  <label class="col-md-0 control-label" style="color:black">DNI</label>  
    <div class="col-md- inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
  <input name="campoDNI" placeholder="Nº documento" class="form-control"  type="number" id="campoDNI" onChange="comprobarCampos()">
    </div>
  </div>
</div>

<!-- Text input-->
       <div class="form-group">
  <label class="col-md-0 control-label" style="color:black">E-Mail</label>  
    <div class="col-md-0 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
  <input name="campoCorreoElectronico" placeholder="persona@ejemplo.com" class="form-control"  type="text" id="campoCorreoElectronico" onChange="comprobarCampos()">
    </div>
  </div>
</div>


<!-- Text input-->
       
<div class="form-group">
  <label class="col-md-0 control-label" style="color:black">Telefono #</label>  
    <div class="col-md-0 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
  <input name="campoTelefonoFijo" placeholder="42-##-##" class="form-control" type="text" id="campoTelefonoFijo" onChange="comprobarCampos()">
    </div>
  </div>
</div>

<!-- Text input-->
       
<div class="form-group">
  <label class="col-md-0 control-label" style="color:black">Celular #</label>  
    <div class="col-md-0
     inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
  <input name="campoTelefonoCelular" placeholder="3442-######" class="form-control" type="text" id="campoTelefonoCelular" onChange="comprobarCampos()">
    </div>
  </div>
</div>
<!-- Text input-->
      
<div class="form-group">
  <label class="col-md-0 control-label" style="color:black">Domicilio</label>  
    <div class="col-md-0 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
  <input name="campoDomicilio" placeholder="Domicilio Ejemplo 123" class="form-control" type="text" id="campoDomicilio" onChange="comprobarCampos()">
    </div>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-0 control-label" style="color:black">Fecha de Nacimiento</label>  
   <div class="col-md-0 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
  <input name="campoFechaNacimiento" placeholder="dd/mm/aaaa" class="form-control" type="date" id="campoFechaNacimiento" onChange="comprobarCampos()">
    </div>
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-0 control-label" style="color:black">Nacionalidad</label>  
   <div class="col-md-0 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
  <input name="campoNacionalidad" placeholder="Argentino" class="form-control" type="text" id="campoNacionalidad" onChange="comprobarCampos()">
    </div>
  </div>
</div>

<!-- radio checks -->
 <div class="form-group">
                        <label class="col-md-0 control-label" style="color:black">Genero</label>
                        <div class="col-md-0">
                            <div class="radio">
                              
                              <select name="campoGenero" id="campoGenero" class="form-control">
                              <option value="1" style="color: black">Masculino</option>
                              <option value="0" style="color: black">Femenino</option>
                              </select>
                            </div>
                        </div>
  </div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-0 control-label" style="color:black">Profesion</label>  
   <div class="col-md-0 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-thumbs-up"></i></span>
  <input name="campoProfesion" placeholder="Estudiante / Empleado /..." class="form-control" type="text" id="campoProfesion" onChange="comprobarCampos()">
    </div>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-0 control-label" style="color:black">Estado Civil</label>  
   <div class="col-md-0 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
  <input name="campoEstadoCivil" placeholder="Soltero / Casado" class="form-control" type="text" id="campoEstadoCivil" onChange="comprobarCampos()">
    </div>
  </div>
</div>


<!-- Success message -->
<!--<div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div>--  >

<!-- Button -->
<div class="form-group">
  <label class="col-md-0 control-label"></label>
  <div class="col-md-0">
    <center><button class="btn btn-warning btn-lg" onClick="comprobarCampos(), agregarPreinscripcion()" id="botonAceptar">Solicitar <span class="glyphicon glyphicon-send"></span></button></center>
    <p style="color: grey">*Recordá que para terminar tu afiliación al club Parque Sur tenés que acercarte a la secretaria del club para confirmar la asociacion pagando por unica vez un monto de inscripción.  </p>

  </div>
</div>
</div>

</fieldset>
<div class="col-md-3"> </div>



</div>
</div><!-- /.container -->






<!-- Solicitar alta -->
<div id="ventanaSuccess"  style="color:black;"class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
 
 <div class="modal-content">
      <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Solicitud de inscripcion a Parque Sur</h4>
      </div>
      
<div class="modal-body">
    

<form id="datos">
<div class="tab-content">
<div class="alert alert-success" role="alert" id="success_message">Tu preinscripción ha sido guardada. <i class="glyphicon glyphicon-thumbs-up"></i> Tiene una validez de 30 días antes de ser eliminada automaticamente. Acérquese a la secretaría del club para terminar su asociación. Muchas gracias!</div>
   
</div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" href="javascript:;" onClick="cerrarVentana()" >Aceptar</button>
      </div>
      </form>
    </div>
  </div>
 







<style> .versionad
{
height: 15px;

color: #fff;
text-align: left;
clear: both;

    top:93%;
    left:1%;
}
</style>

 <div id="pie" class="versionad">
  <center><font size=5><a href="CNosotros">Acerca de</a></center></font>
 </div>
</body>