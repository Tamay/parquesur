<!------------------ JAVASCRIPT -------------------->
<link rel="stylesheet" type="text/css" href="/ParqueSur/css/tablas/css/jquery.dataTables.css">
 
<script type="text/javascript" src="<?php echo base_url("/js/tablas/js/jquery.dataTables.js");?>"></script>
<script type="text/javascript" src="<?=base_url("/js/tablas/js/buttons.print.min.js");?>"></script>
<script type="text/javascript" src="<?=base_url("/js/tablas/js/dataTables.buttons.min.js");?>"></script>


<script>

var editar;
var idSocioSeleccionado=0;
var idSeleccionado=-1;

var Radio = 0;




function seVaAEditar(vof)
{
  editar=vof;
}


function agregarEmbarcacion(){
	

/// aca le paso todos los inputs

    
        var parametros = {
                "matricula" : $('#campoMatricula').val().toUpperCase(),
                "nombre" : $('#campoNombre').val().toUpperCase(),
            "tipoEmbarcaciones": $('#campoTipoEmbarcaciones').val(),

            "motorMarca" : $('#campoMotorMarca').val(),
            "motorNumero" : $('#campoMotorNumero').val(),
            "motorTipo" : $('#campoMotorTipo').val(),
            "amarre" : $('#campoAmarre').val(),

            "fechaAdquisicion" : $('#campoFechaAdquisicion').val(),
            "anioConstruccion" : $('#campoAñoConstruccion').val(),
            "arboladura" : $('#campoArboladura').val(),
            "eslora" : $('#campoEslora').val(),
            "manga" : $('#campoManga').val(),
            "puntal" : $('#campoPuntal').val(),
            "tonelaje" : $('#campoTonelaje').val(),
            "HP" : $('#campoHP').val(),
            "observaciones" : $('#campoObservaciones').val(),
            "socio_id" : idSocioSeleccionado


        };
        $.ajax({
                data:  parametros,
                url:   '<?= site_url("/CEmbarcacion/Agregar") ?>',
                type:  'post',
                success:  function (response) {
                        if(response=="success")
                        {
                    
					actualizarLista();
                        vaciarCampos();  
                        }
                        else{
                          window.alert("Ya existe la matricula en la base de datos");
                        }
                        
				}
		});
	

	}


function editarEmbarcacion()
{
	
	var valorRadio = obtenerIdEmbarcacionSeleccionada();


  var parametros = {
      "matricula" : $('#campoMatricula').val().toUpperCase(),
      "nombre" : $('#campoNombre').val().toUpperCase(),
        "tipoEmbarcaciones": $('#campoTipoEmbarcaciones').val(),
      "motorMarca" : $('#campoMotorMarca').val(),
      "motorNumero" : $('#campoMotorNumero').val(),
      "motorTipo" : $('#campoMotorTipo').val(),
      "amarre" : $('#campoAmarre').val(),
      "fechaAdquisicion" : $('#campoFechaAdquisicion').val(),
      "anioConstruccion" : $('#campoAñoConstruccion').val(),
      "arboladura" : $('#campoArboladura').val(),
      "eslora" : $('#campoEslora').val(),
      "manga" : $('#campoManga').val(),
      "puntal" : $('#campoPuntal').val(),
      "tonelaje" : $('#campoTonelaje').val(),
      "HP" : $('#campoHP').val(),
      "observaciones" : $('#campoObservaciones').val(),
		//"Nombre Persona": Nombre Persona1,
//"Apellidos Persona": Apellidos Persona1,
//"DNI" : DNI1
        };
        $.ajax({
                data:  parametros,
                url:   '<?= site_url("/CEmbarcacion/Editar") ?>/'+ valorRadio,
                type:  'post',
                beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                },
                success:  function (response) {
                        $("#resultado").html(response);      
						 actualizarLista();   
            vaciarCampos();
                }
        });
}

function comprobarCampos()
{
  
  if($('#campoNombre').val()=='' || $('#campoTipoEmbarcaciones').text()=="" ||      (idSocioSeleccionado==-1) )
  {
    $('#botonAceptar').prop('disabled', true);
    $("#advertenciaDatos").text("Faltan datos obligatorios");
  }
  else
  {
   $('#botonAceptar').prop('disabled', false); 
   $("#advertenciaDatos").text("");
  }   
}

function vaciarCampos()
{
        $('#campoMatricula').val("");
        $('#campoNombre').val("");
		$('#campoTipoEmbarcaciones').val("");
    $('#campoMotorMarca').val("");
    $('#campoMotorNumero').val("");
    $('#campoMotorTipo').val("");
    $('#campoAmarre').val("");
    $('#campoFechaAdquisicion').val("");
    $('#campoAñoConstruccion').val("");
    $('#campoArboladura').val("");
    $('#campoEslora').val("");
    $('#campoManga').val("");
    $('#campoPuntal').val("");
    $('#campoTonelaje').val("");
    $('#campoHP').val("");
    $('#campoObservaciones').val("");
     
        comprobarCampos();
}

function actualizarTipoEmbarcaciones()
{
    $.ajax({
                url:   '<?= site_url("/CTipoEmbarcaciones/TiposEmbarcacionesEnSelect") ?>',
                type:  'post',
                success:  function (response) {
          $('#tipoEmbarcacionesEnSelect').html(response);
                }});
}



function obtenerEmbarcacion()
{
    //  $("#ventanaAgregar").modal("show");
valorRadio = obtenerIdEmbarcacionSeleccionada();
  $.ajax({
    url:'<?= site_url("/CEmbarcacion/ObtenerEmbarcacion") ?>/'+ valorRadio,
    type:  'get',
    dataType: 'json',
    success: function(res) {
        $('#campoMatricula').val(res.matricula);
        $('#campoNombre').val(res.nombre);
		    $('#campoTipoEmbarcaciones').val(res.tipoEmbarcacion);
        $('#campoMotorMarca').val(res.motorMarca),
          $('#campoMotorNumero').val(res.motorNumero),
          $('#campoMotorTipo').val(res.motorTipo),
            $('#campoAmarre').val(res.amarre),
            $('#campoFechaAdquisicion').val(res.fechaAdquisicion),
            $('#campoAñoConstruccion').val(res.anioConstruccion),
            $('#campoArboladura').val(res.arboladura),
            $('#campoEslora').val(res.eslora),
            $('#campoManga').val(res.manga),
            $('#campoPuntal').val(res.puntal),
            $('#campoTonelaje').val(res.tonelaje),
            $('#campoHP').val(res.HP),
            $('#campoObservaciones').val(res.observaciones),
        comprobarCampos();
			
  }});
}





function darDeBaja()
{    
	var valorRadio = obtenerIdEmbarcacionSeleccionada();

/// Si no hubo ninguno  seleccionado
  if(valorRadio==-1)
    {
    window.alert("Debe seleccionar un socio");
    }
	
    else{

var respuesta = confirm("¿Está seguro que desea dar de baja esta embarcacion?");
if(respuesta)
{
   $.ajax({
    url:'<?= site_url("/CEmbarcacion/DarDeBaja") ?>/'+ valorRadio,
    type:  'POST',
    success: function() 
    {
      window.alert("La embarcacion ha sido dada de baja con éxito.")
	   actualizarLista();
    
    }
});
}
}
}



function inicializarLista()	
	{
    $('#tabladeembarcaciones').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'print',
                customize: function ( win ) {
                    $(win.document.body).css( 'padding', '0px 0px 0px 0px' );
                },
                title: "Lista de embarcaciones",
                text: '<button type="button" class="btn btn-default">Imprimir</button>',
                exportOptions: {
                    columns: [ 1, 2, 3,4,5,6,7 ]
                }
            }
        ],
	"ajax": "CEmbarcacion/obtenerEmbarcaciones",
        "columns": [
            { "data": "nroEmbarcacion" },
            { "data": "nroSocio" },
            { "data": "ApellidoYNombre" },
            { "data": "Nombre" },
            { "data": "Matricula" },
            { "data": "Tipo" },
            { "data": "Tel/Cel" },
            { "data": "Monto" }
             ],
        "order": [[2,"asc"]]
    } );

   cargarPropiedadesTabla();
   $('#botonEditar').attr("disabled", true);
$('#botonBaja').attr("disabled", true);
}

function actualizarLista()
{
	$('#botonEditar').attr("disabled", true);
$('#botonBaja').attr("disabled", true);
var t = $('#tabladeembarcaciones').DataTable();
//t.clear().draw()ñ
 t.ajax.reload();
    idSocioSeleccionado=0;
}


function cargarPropiedadesTabla()
{
    ///AQUÍ SE CARGAN LAS PROPIEDADES DE LA TABLA  
    var table = $('#tabladeembarcaciones').DataTable();
                    $('#bodytabladeembarcaciones').on( 'click', 'tr', function () {
                   if ( $(this).hasClass('selected') ) 
                    {
						$('#botonEditar').attr("disabled", true);
$('#botonBaja').attr("disabled", true);
                        $(this).removeClass('selected');
                        idSeleccionado=-1;
                    }
                    else {
						
$('#botonEditar').attr("disabled", false);
$('#botonBaja').attr("disabled", false);
                        table.$('tr.selected').removeClass('selected');
                        $(this).addClass('selected');
                        idSeleccionado = (table.row(this).data().nroEmbarcacion);
                        }
                    
                    
                    });

                    $('#button').click( function () {
                   table.row('.selected').remove().draw( false );
                    } );
                    /// Aquí termina lo relacionado con las propiedades de la tabla
}


function obtenerIdEmbarcacionSeleccionada()
{
    return idSeleccionado;
}






function actualizarListaEmbarcacion()
{
    var parametros = 
    {
                "valorBusqueda" : $('#campoBusqueda').val()
    };
   $.ajax({
                url:   '<?= site_url("/CEmbarcacion/obtenerEmbarcaciones") ?>',
                type:  'post',
                dataType: "json",
                data:   parametros,
                success:  function (response)
                {
              

                }});
}


function limpiarSocios()
{
var table =	$('#tablaBusquedaSocios').DataTable();
table.clear().draw()
;
}

var k = new Kibo();

function pasajeTabsEmbarcacion() {
  if(k.lastKey()=='right')
  {
  $('.nav-tabs a:last').tab('show');  
  }
  if(k.lastKey()=='left')
    {
      $('.nav-tabs a:first').tab('show');   
    }
  
}

function crearNuevaEmbarcacion()
{
    $("#ventanaAgregar").modal("show");
}

k.down(['shift right','shift left'], pasajeTabsEmbarcacion);
k.down(['shift n'], crearNuevaEmbarcacion);





function inicializarListaSocios()
{
   $('#tablaBusquedaSocios').DataTable( {
        "ajax": 'CSocio/obtenerSocios',
        "columns": [
            { "data": "id" },
            { "data": "nombreCompleto" },
            { "data": "domicilio" },
            { "data": "dni" }
        ]
    });

   cargarPropiedadesTablaSocios(); 
}


function actualizarListaSocios()
{
	idSocioSeleccionado=-1;
var t = $('#tablaBusquedaSocios').DataTable();
//t.clear().draw()ñ
 t.ajax.reload();
}

function cargarPropiedadesTablaSocios()
{
    ///AQUÍ SE CARGAN LAS PROPIEDADES DE LA TABLA  
    var table = $('#tablaBusquedaSocios').DataTable();

    var tablaEmbarcacion = $('#tabladeembarcaciones').DataTable();
     tablaEmbarcacion.column(0).visible(false);
                   
                    $('#bodyBusquedaSocios').on( 'click', 'tr', function () {
						
                   if ( $(this).hasClass('selected') ) 
                    {
						
                        $(this).removeClass('selected');
                        idSocioSeleccionado=-1;
						console.log(idSocioSeleccionado);
						comprobarCampos();
                    }
                    else {
                        table.$('tr.selected').removeClass('selected');
                        $(this).addClass('selected');
                        idSocioSeleccionado = (this.cells[0].innerHTML);
						console.log(idSocioSeleccionado);
                        }
                  
                      comprobarCampos();
                    });

   
                    /// Aquí termina lo relacionado con las propiedades de la tabla
}
function obtenerIdSocioSeleccionado()
{
    return idSocioSeleccionado;
}



</script>


<style type="text/css">
.tamayestilo {
	float: right;
	width: 150px;
	top: 0px;
	right: 0px;
	bottom: 0px;
	position: static;
	left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	margin-left: 125px;
}
</style>
<body onLoad="inicializarLista(),inicializarListaSocios(),comprobarCampos(), actualizarTipoEmbarcaciones()">
<div class="container">    

  <div class="row text-justify cajas">

        	<p>
               <button type="button" id="botonAgregar" class="btn btn-primary" data-toggle="modal" data-target="#ventanaAgregar" onClick="vaciarCampos(),actualizarListaSocios(),seVaAEditar(false),$('#tabBusquedaSocio').show();">Agregar
              </button>&nbsp;

	          <button type="button" id="botonEditar" class="btn btn-warning" data-toggle="modal" data-target= "#ventanaAgregar" onClick="obtenerEmbarcacion(),limpiarSocios(),seVaAEditar(true),$('#tabBusquedaSocio').hide();">Editar</button>&nbsp;

	          <button type="button" id="botonBaja" class="btn btn-danger" onClick="darDeBaja()">Dar de baja</button>&nbsp;
            <a  type="button" class="btn btn-info" style="float: right;" href="#myModal" data-toggle="modal">Ayuda</a>

          </p>
    

  <div class="row">

  <div class="col-lg-6"><h1>Lista de Embarcaciones</h1></div>

  <div class="col-lg-6 text-right" style="padding-top:30px;">

  </div>

  </div>

  <!------------------ TABLA DE DATOS --------------------> 

<table id="tabladeembarcaciones" class="display" cellspacing="0" width="100%">
      <thead>
        <tr>
        <th>nroEmbarcacion</th>
        <th>Nro Socio</th>
        <th>Apellido y Nombre</th>
        <th>Nombre</th>
        <th>Matricula</th>
        <th>Tipo</th>
        <th>Tel/Cel</th>
      <th>Monto</th>
        </tr>
      </thead>
    <tbody id="bodytabladeembarcaciones" class="display cursorManito">

    </tbody>
    </table>

  <!-- <div id='listaDeSocios'>

  </div> -->

    <!------------------ /TABLA DE DATOS --------------------> 

	

	</div>

    

    <span id="resultado"></span>

    

</div>







  <!------------------ VENTANA AGREGAR --------------------> 

<!-- Modal -->

<div id="ventanaAgregar"  style="color:black;" class="modal fade" role="dialog">

  <div class="modal-dialog">



    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

<button type="button" id="agregarBarco" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Agregar Embarcacion</h4>

      </div>

<div class="modal-body">



<ul class="nav nav-tabs">

  <li class="active"><a data-toggle="tab" href="#DatosEmbarcacion">Datos Embarcacion</a></li>
  <li id="tabBusquedaSocio"><a data-toggle="tab" href="#BusquedaSocio">Busqueda socio</a></li>
<!--<li><a data-toggle="tab" href="#datosDelSocio">Socio</a></li> -->

 <!--  <div
<form>
   <input type="text" class="tamayestilo" id="campoBusqueda1" placeholder="Buscar..." onChange="actualizarListaBusquedaSocios($(this).val());" onKeyUp="this.onchange();" size="30">
 </form></div>-->

</ul>



<form id="datos">

<div class="tab-content">

  <!-- Datos Personales-->

  <div id="DatosEmbarcacion" class="tab-pane fade in active">

      <h3>Datos Embarcacion</h3>


  <table style="border-spacing:5px;border-collapse: separate;">

  <tr>

  <td style="text-align:right"><label>Matricula</label></td>

  <td><input id="campoMatricula" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="matricula" type="text" /></td>

  </tr>

 

  <tr>

  <td style="text-align:right"><label>Nombre</label></td>

  <td><input id="campoNombre" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="nombre" type="text" /></td>

  </tr>


 <tr>

  <td style="text-align:right"><label>Tipo de embarcacion</label></td>

  <td><div id="tipoEmbarcacionesEnSelect"></div></td>
    </tr>





      <tr>

          <td style="text-align:right"><label>Motor Marca</label></td>

          <td><input id="campoMotorMarca" onChange="" onKeyUp="this.onchange();" name="motorMarca" type="text" /></td>

      </tr>
      <tr>

          <td style="text-align:right"><label>Motor Numero</label></td>

          <td><input id="campoMotorNumero" onChange="" onKeyUp="this.onchange();" name="motorNumero" type="text" /></td>

      </tr>
      <tr>

          <td style="text-align:right"><label>Motor Tipo</label></td>

          <td><input id="campoMotorTipo" onChange="" onKeyUp="this.onchange();" name="motorTipo" type="text" /></td>

      </tr>
      <tr>

          <td style="text-align:right"><label>Amarrre</label></td>

          <td><input id="campoAmarre" onChange="" onKeyUp="this.onchange();" name="Amarre" type="text" /></td>

      </tr>
      <tr>

          <td style="text-align:right"><label>Fecha Adquisicion</label></td>

          <td><input id="campoFechaAdquisicion" onChange="" onKeyUp="this.onchange();" name="fechaAdquisicion" type="text" /></td>

      </tr>
      <tr>

          <td style="text-align:right"><label>Año construccion</label></td>

          <td><input id="campoAñoConstruccion" onChange="" onKeyUp="this.onchange();" name="añoConstruccion" type="text" /></td>

      </tr>
      <tr>

          <td style="text-align:right"><label>Arboladura</label></td>

          <td><input id="campoArboladura" onChange="" onKeyUp="this.onchange();" name="arboladura" type="text" /></td>

      </tr>
      <tr>

          <td style="text-align:right"><label>Eslora</label></td>

          <td><input id="campoEslora" onChange="" onKeyUp="this.onchange();" name="eslora" type="text" /></td>

      </tr>
      <tr>

          <td style="text-align:right"><label>Manga</label></td>

          <td><input id="campoManga" onChange="" onKeyUp="this.onchange();" name="manga" type="text" /></td>

      </tr>
      <tr>

          <td style="text-align:right"><label>Puntal</label></td>

          <td><input id="campoPuntal" onChange="" onKeyUp="this.onchange();" name="fechaAdquisicion" type="text" /></td>

      </tr>
      <tr>

          <td style="text-align:right"><label>Tonelaje</label></td>

          <td><input id="campoTonelaje" onChange="" onKeyUp="this.onchange();" name="fechaAdquisicion" type="text" /></td>

      </tr>
      <tr>

          <td style="text-align:right"><label>HP</label></td>

          <td><input id="campoHP" onChange="" onKeyUp="this.onchange();" name="HP" type="text" /></td>

      </tr>
      <tr>

          <td style="text-align:right"><label>Observaciones</label></td>
          <td><textarea id="campoObservaciones" onChange="" onKeyUp="this.onchange();" name="observaciones" rows="5" cols="30"></textarea></td>

      </tr>



</table>

  </div>

  

  

 <!-- Busqueda de socio -->


 <div id="BusquedaSocio" class="tab-pane fade">

  



    <!-- Modal content-->

    



<h3>Busqueda Socio</h3>
<div>
</div>


<br>

<form id="Busqueda Socio">

  <div class="tab-content">
  

<table id="tablaBusquedaSocios" class="display" cellspacing="0" width="100%" >
    <thead>
    <tr>
        <th>Nro Socio</th>
        <th>Nombre Completo</th>
        <th>Domicilio</th>
        <th>DNI</th>
       
    </tr>
    </thead>
    <tbody id="bodyBusquedaSocios" class="display cursorManito">

    </tbody>
    
</table>

</div>
 

</form>

</div>
</div> 
</div>
   




		

      

      <div class="modal-footer">
      <span id="advertenciaDatos" style="color:red;"></span>
        <button type="button" id="botonAceptar" class="btn btn-default" data-dismiss="modal" href="javascript:;" onClick="if(!editar){agregarEmbarcacion();}else{editarEmbarcacion()}return false;" >Aceptar</button>


        <button type="button" class="btn btn-default" data-dismiss="modal" href="javascript:;" onClick="idSocioSeleccionado=0;">Cancelar</button>

      </div>

      </form>

    </div>

  </div>

</div>
<!---     ****     TERMINA VENTANA AGREGAR  ***             -->




<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: black">Módulo de Embarcaciones</h4>
      </div>
      <div class="modal-body" style="color: black">
        <p align="justify">Este es el modulo de Embarcaciones. Aqui se permite dar de alta, modificar y dar de baja las embarcaciones de los socios. Para dar de alta una embaracion haga click en Agregar. Luego cargue los datos y seleccione el socio dueño de la embarcacion al cual se le cobrarán los importes estimados. </p>
        <p>Para editar una embarcacion seleccione una embarcacion de la lista y haga click en "Editar".</p>
        <p>Para dar de baja una embarcacion seleccione una embarcacion de la lista y haga click en "Dar de baja"</p>
        


        

       </div>
      <div class="modal-footer">
        <center><button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button></center>
      </div>
    </div>

  </div>
</div>



</body>

</html>