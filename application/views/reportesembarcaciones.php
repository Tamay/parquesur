<!doctype html>
<html lang="es">
<head>


    <!--   Core JS Files    -->
    <script src="/parquesur/js/jquery-3.1.0.min.js" type="text/javascript"></script>
    <script src="/parquesur/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/parquesur/js/material.min.js" type="text/javascript"></script>


    <!--    Para exportar los datos   -->
    <script type="text/javascript" src="<?php echo base_url("/js/tableExport.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("/js/jquery.base64.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("/js/jspdf/libs/sprintf.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("/js/jspdf/jspdf.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("/js/jspdf/libs/base64.js");?>"></script>


    <!-- Material Dashboard javascript methods -->
    <script src="/parquesur/js/material-dashboard.js"></script>


 <!--     Tablas     -->
    <link rel="stylesheet" type="text/css" href="<?=base_url("/css/tablas/css/jquery.dataTables.css");?>">
    <script type="text/javascript" src="<?=base_url("/js/tablas/js/jquery.dataTables.js");?>"></script>
    <script type="text/javascript" src="<?=base_url("/js/tablas/js/dataTables.buttons.min.js");?>"></script>
    <script type="text/javascript" src="<?=base_url("/js/tablas/js/buttons.print.min.js");?>"></script>
    <script type="text/javascript" src="<?=base_url("/js/tablas/js/dataTables.select.min.js");?>"></script>
    <script type="text/javascript" src="<?=base_url("/js/tablas/js/dataTables.scroller.min.js");?>"></script>
    <script type="text/javascript" src="<?=base_url("/js/tablas/js/pdfmake.min.js");?>"></script>
    <script type="text/javascript" src="<?=base_url("/js/tablas/js/jszip.min.js");?>"></script>
    <script type="text/javascript" src="<?=base_url("/js/tablas/js/vfs_fonts.js");?>"></script>
    <script type="text/javascript" src="<?=base_url("/js/tablas/js/buttons.html5.min.js");?>"></script>
    <script type="text/javascript" src="<?=base_url("/js/jquery-ui.js");?>"></script>
    <link rel="stylesheet" type="text/css" href="<?=base_url("/css/tablas/css/scroller.dataTables.min.css");?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url("/css/jquery-ui.css");?>">




    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="/parquesur/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="/parquesur//css/bootstrap.min.css" rel="stylesheet" />

    <!--  Material Dashboard CSS    -->
    <link href="/parquesur/css/material-dashboard.css" rel="stylesheet"/>

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>






    <script type="text/javascript">




                function controlarOpcion()
        {
            var opcion=$("#opcion").val();


///------- REPORTE PARA PREFECTURA ---------///
            if(opcion=="reporteParaPrefectura") {
                // QUE HACEMOS
                obtenerEmbarcaciones();
                // QUE MOSTRAMOS
                $("#reporteParaPrefectura").show();

                //QUE ESCONDEMOS
            }

        }


            function obtenerEmbarcaciones()
            {

                            var fixNewLine = {
                    exportOptions: {
                        format: {
                            body: function ( data, column, row ) {
                                // Strip $ from salary column to make it numeric
                                return column === 5 ?
            // THIS WORKS:          data.replace(/test/ig, "blablabla"):
                                    data.toString().replace( /<br\s*\/?>/ig, "\n" ) :
                                    data;
                            }
                        }
                    }
                };



                $('#tablaReporteParaPrefectura').DataTable( {
                    "ajax": {
                        "url":   "<?=site_url("CReporte/sociosConEmbarcaciones")?>",
                        "type":  'post',
                        "beforeSend": function () {
                            $("#modal_Cargando").modal({backdrop: 'static', keyboard: false});
                            $("#modal_Cargando").modal('show');
                        },
                        "complete": function (response) {
                            $("#modal_Cargando").modal('hide');
                        }
                        },

                    "columns": [
                        { "data": "nombresEmbarcaciones" },
                        { "data": "matriculasEmbarcaciones" },
                        { "data": "Marca" },
                        { "data": "Numero" },
                        { "data": "Tipo" },
                        { "data": "nombreCompleto" },
                        { "data": "domicilio" },
                        { "data": "DNI" },
                        { "data": "telefonos" }
                    ],
                    "order": [[5,"asc"]],
                    dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                text: '<button type="button" class="btn btn-default">EXCEL</button>',
                title: "Padrón de socios con embarcaciones"
            },
             
                $.extend( true, {}, fixNewLine, {
                extend: 'print',
                title: "Padrón de socios con embarcaciones",
                text: '<button type="button" class="btn btn-default">IMPRIMIR</button>',
                message: function(){return   'Reporte emitido el '+fechaDeHoy()+'<br><br>'},
                orientation: 'landscape',
                pageSize: 'A4',
                customize: function ( win ) { 
                    console.log(win);
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', '11pt' );

                    $(win.document.body).find( 'h1' )
                        .css( 'font-size', '20pt' );

                    $(win.document.body).find( 'table td' )
                                            .css( 'border-bottom', '1px solid black' )
                                            .css( 'border-collapse', 'collapse' )
                                            .css( 'height', '50px' )
                                            .css( 'vertical-align', 'center' );

                    $(win.document.body).find( 'table th' )
                                            .css( 'border-bottom', '1px solid black' )
                                            .css( 'border-collapse', 'collapse' );
                    
                }
                })
        ]
    });
}


function fechaDeHoy()
{
    var d = new Date();
    var month = d.getMonth()+1;
    var day = d.getDate();
    console.log(day+'/'+month+'/'+ d.getFullYear());
    return (day+'/'+month+'/'+ d.getFullYear());
}



    </script>



</head>

<body onload="controlarOpcion()">
<input id="opcion" type="hidden" value="<?=$_GET['opcion']?>">

<div class="wrapper">

    <div class="sidebar" data-color="purple" data-image="/img/sidebar-1.jpg">
        <!--
              Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

              Tip 2: you can also add an image using data-image tag
          -->

        <div class="logo">
            <a href="" class="simple-text">
                Reportes
            </a>
        </div>

        <!-- MENU IZQUIERDO -->
        <div class="sidebar-wrapper">
            <ul class="nav">
                <li class="active">
                    <a href="<?= site_url('CReporte/index') ?>">
                        <i class="material-icons">dashboard</i>
                        <p>Tabla general</p>
                    </a>
                </li>
                <ul class="nav">
                    <h4><i class="material-icons">person</i>Societarios</h4>
                    <li>
                        <a href="<?= site_url('CReporte/reportesSocietarios?opcion=sociosactivos') ?>">

                            <p>Socios Activos</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?= site_url('CReporte/reportesSocietarios?opcion=sociosMorosos') ?>">

                            <p>Socios Morosos</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?= site_url('CReporte/reportesSocietarios?opcion=altasybajas') ?>">

                            <p>Altas y bajas</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?= site_url('CReporte/reportesSocietarios?opcion=sociosconembarcaciones') ?>">

                            <p>Socios con embarcaciones</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?= site_url('CReporte/reportesSocietarios?opcion=sociosadherentes') ?>">

                            <p>Socios Adherentes</p>
                        </a>
                    </li>

                </ul>
                <ul class="nav">
                    <h4><i class="material-icons">content_paste</i>Financieros</h4>
                    <li>
                        <a href="<?= site_url('CReporte/reportesFinancieros?opcion=recaudacion') ?>">
                            <p>Recaudación bruta</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?= site_url('CReporte/reportesFinancieros?opcion=recaudadoxcobrador') ?>">
                            <p>Recaudado por cobrador (bruto)</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?= site_url('CReporte/reportesFinancieros?opcion=recaudacionxdeporte') ?>">
                            <p>Recaudación por deporte (bruta mensual)</p>
                        </a>
                    </li>

                </ul>
                <ul class="nav">
                    <h4><i class="material-icons">directions_boat</i>Embarcaciones</h4>
                    <li>
                        <a href="<?= site_url('CReporte/reportesEmbarcaciones?opcion=reporteParaPrefectura') ?>">
                            <p>Reporte para prefectura</p>
                        </a>
                    </li>

                </ul>
            </ul>
        </div>
    </div>
    <!-- FINALIZA EL MENÚ IZQUIERDO -->


    <div class="main-panel">
        <nav class="navbar navbar-transparent navbar-absolute">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?= base_url();?>">Volver atrás</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">


                        <!--- De la siguiente manera se pueden agregar elementos en el extremo superior derecho
                          <li>
                            <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                              <i class="material-icons">dashboard</i>
                              <p class="hidden-lg hidden-md">Dashboard</p>
                            </a>
                          </li>
                          ---->

                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">



                </div>


                <!-- REPORTE PARA PREFECTURA -->
                <!-- SOCIOS ACTIVOS -->
                <div class="row" id="reporteParaPrefectura">
                    <div class="col-md-3">

                    </div>
                    <div class="col-md-3">

                    </div>
                    <div id="tablaEmbarcaciones" class="col-md-12">
                        <table id="tablaReporteParaPrefectura" class="table display table-responsive">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Matricula</th>
                                <th>Marca</th>
                                <th>Numero</th>
                                <th>Tipo</th>
                                <th>Apellido y nombre</th>
                                <th>Domicilio</th>
                                <th>DNI</th>
                                <th>TEL/CEL</th>
                            </tr>
                            </thead>


                            <tbody class="cursorManito">
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Nombre</th>
                                <th>Matricula</th>
                                <th>Marca</th>
                                <th>Numero</th>
                                <th>Tipo</th>
                                <th>Nombre y Apellido</th>
                                <th>Domicilio</th>
                                <th>DNI</th>
                                <th>TEL/CEL</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>


                    <br><br><br>
                </div>
                    <!------------------ /TABLA DE DATOS -------------------->





                </div>




            </div>





        </div>
    </div>

</body>
</html>