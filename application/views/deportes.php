<html>

<link rel="stylesheet" type="text/css" href="/ParqueSur/css/tablas/css/jquery.dataTables.css">

<script type="text/javascript" src="<?php echo base_url("/js/tablas/js/jquery.dataTables.js");?>"></script>

<script type="text/javascript" src="<?php echo base_url("/js/funDeportes.js");?>"></script>

<body onLoad="inicializarLista(), inicializarListaSocios(), comprobarCampos()">
<div class="container cajas">    

  <div class="row">


              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ventanaAgregarDeporte" onClick="actualizarLista(), vaciarCampos(),seVaAEditar(false)">Agregar</button>&nbsp; 

<button type="button" class="btn btn-warning"  onClick="obtenerDeporte(),seVaAEditar(true)">Editar</button>&nbsp;

              <button type="button" class="btn btn-danger" onClick="darDeBajaDeporte()">Dar de baja</button>&nbsp;
         
              <button type="button" class="btn btn-success" data-toggle="modal" onClick="abrirVentanaInscripcion()">Inscribir un socio</button>&nbsp;

              <button type="button" class="btn btn-info" onClick="window.location.href='CInscripcion'">Ver inscripciones</button>&nbsp;
                        <a  type="button" class="btn btn-info" style="float: right;" href="#myModal" data-toggle="modal">Ayuda</a>


</div>

  <div class="row">

  <div class="col-lg-6"><h1>Lista de deportes</h1></div>

  </div>

  <!------------------ TABLA DE DATOS --------------------> 


<table id="tabladedeportes" class="table display table-responsive">

      <thead>

        <tr>
        <th>Nº Deporte</th>

        <th>Deporte</th>

        <th>Monto</th>

        <th>Estado</th>

       </tr>

      </thead>

    <tbody id="bodyDetabladedeportes" class="cursorManito">

    </tbody>
    <tfoot>
    <tr>
        <th>Nº Deporte</th>

        <th>Deporte</th>

        <th>Monto</th>

        <th>Estado</th>

       </tr>
    </tfoot>
    </table>

    <!------------------ /TABLA DE DATOS --------------------> 

</form>

	</div>  

    <span id="resultado"></span>

</div>

<!------------------ VENTANA AGREGAR --------------------> 

<!-- Modal -->

<div id="ventanaAgregarDeporte"  style="color:black;"class="modal fade" role="dialog">

  <div class="modal-dialog">


    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

<button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Agregar Deporte</h4>

      </div>

      <div class="modal-body">



<ul class="nav nav-tabs">

  <li class="active"><a data-toggle="tab" href="#datosPersonales">Datos deporte</a></li>

 
</ul>



<form id="datos">

<div class="tab-content">

  <!-- Datos Personales-->

  <div id="datosDeporte" class="tab-pane fade in active">

      <h3>Datos Deporte</h3>

  <table style="border-spacing:5px;border-collapse: separate;">

  <tr>

  <td style="text-align:right"><label>Nombre</label></td>

  <td><input id="campoNombre" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="nombre" type="text" /></td>

  </tr>

 

  <tr>

  <td style="text-align:right"><label>Monto</label></td>

  <td><input id="campoMonto" onChange="comprobarCampos();" onKeyUp="this.onchange();" name="monto" type="real" /></td>

  </tr>       
     

  <tr>

  <td style="text-align:right"><label>Estado</label></td>

  <td><select name="estado" id="campoEstado">

  			<option value="0">Habilitado</option>

			<option value="1">Deshabilitado</option>

		</select></td>

  </tr> 

  
</table>

  </div>



  </div>

</div>

		

      

      <div class="modal-footer">
      <span id="advertenciaDatos" style="color:red;"></span>
        <button type="button" id="botonAceptar" class="btn btn-default" data-dismiss="modal" href="javascript:;" onClick="if(!editar){agregarDeporte();}else{editarDeporte()}return false;" >Aceptar</button>

        <button type="button" class="btn btn-default" data-dismiss="modal" href="javascript:;" onClick="vaciarCampos()">Cancelar</button>

      </div>

      </form>

    </div>



  </div>

</div>


  <!------------------ VENTANA INSCRIPCION --------------------> 

<!-- Modal -->

<div id="ventanaAgregarInscripcion" style="color:black;" class="modal fade" role="dialog">

  <div class="modal-dialog">


    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

<button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Agregar Inscripción</h4>

      </div>


<div class="modal-body">
<form id="busquedaSocio">
<div class="row">
<div class="col-lg-6"><h3>Búsqueda Socio</h3></div>

</div>

<table id="tablaBusquedaSocios" class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Nº Socio</th>
        <th>Nombre completo</th>
        <th>Domicilio</th>
        <th>DNI</th>
    </tr>
    </thead>
    <tbody id="bodyTablaSocio">
    </tbody>

</table>

</div>
<div class="modal-footer">
      <span id="advertenciaDatos" style="color:red;"></span>
      <button type="button" id="botonAceptar" class="btn btn-default" data-dismiss="modal" href="javascript:;" onClick="agregarInscripcion()" >Inscribir</button>

      <button type="button" class="btn btn-default" data-dismiss="modal" href="javascript:;" onClick="">Cancelar</button>

</div>

</form>


</div> 
</div>
</div>
<!------------------ TERMINA VENTANA INSCRIPCION --------------------> 

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: black">Módulo de Deportes</h4>
      </div>
      <div class="modal-body" style="color: black">
        <p align="justify">Este es el módulo de deportes o actividades que se realizan en el club. Se muestran en pantalla los que están guardados en el sistema con su informacion, precio y si están disponibles o no. Las funciones disponibles son: </p>
        <p><u>Alta, Modificacion y baja:</u></p>
        <p align="justify">La función agregar permite agregar nuevos deportes o actividades al club (Ejemplo: "Futbol infantil x1"), dandole un monto mensual que se agregan a las cuotas de socios. La modificación permite cambiar los nombres y los valores mensuales de su actividad. A su vez, se puede dar de baja un deporte, dejandóse de cobrar a los socios que están inscriptos, hasta que éste vuelva a estar activo.</p>
        <p><u>Inscribir un socio:</u></p>
        <p align="justify">Esta accion permite inscribir a un socio TITULAR a un deporte/actividad, pasandose a cobrar mensualmente junto a las demas acciones y tipo de socio que tenga. </p>

        <p><u>Ver inscripciones:</u></p>
        <p align="justify">Muestra un listado de las inscripciones a los deportes por parte de los socios</p>


        

       </div>
      <div class="modal-footer">
        <center><button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button></center>
      </div>
    </div>

  </div>
</div>

</body>

</html>