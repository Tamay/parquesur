// JavaScript Document
var editar;

var idSeleccionado=-1;
var agregoServicios;

function seVaAEditar(vof)
{
  editar=vof;
    if(editar)
    {
        $('#tabDeportes').hide();
        $('#tabEmbarcaciones').hide();
        $('#tabGrupoFamiliar').hide();
        $("#campoFechaAlta").prop('enabled', false);
        $("#tituloVentana").html("Editar Socio");

    }
    else
    {
        $('#tabDeportes').show();
        $('#tabEmbarcaciones').show();
        $('#tabGrupoFamiliar').show();
        $("#campoFechaAlta").prop('disabled', false);
        $("#tituloVentana").html("Agregar Socio");
    }

}


//a

function agregarSocio(){
        var parametros = {
			"nombre" : ($('#campoNombre').val()).toUpperCase(),
                "apellidos" : ($('#campoApellidos').val()).toUpperCase(),
                "dni" : $('#campoDNI').val(),
                "domicilio" : ($('#campoDomicilio').val()).toUpperCase(),
            "genero" : $('#campoGenero').val(),
        "correoElectronico" : $('#campoCorreoElectronico').val(),
        "telefonoFijo" : $('#campoTelefonoFijo').val(),
        "telefonoCelular" : $('#campoTelefonoCelular').val(),
        "nacionalidad" : ($('#campoNacionalidad').val()).toUpperCase(),
        "estadoCivil" : ($('#campoEstadoCivil').val()).toUpperCase(),
        "observaciones" : ($('#campoObservaciones').val()).toUpperCase(),
        "fechaNacimiento" : $('#campoFechaNacimiento').val(),
        "profesion" : ($('#campoProfesion').val()).toUpperCase(),
        "domicilioDeCobro": ($('#campoDomicilioDeCobro').val()).toUpperCase(),
        "intervaloDePago": $('#campoIntervaloDePago').val(),
        "tipoSocio": $('#campoTipoSocio').val(),
		"idCobrador": $('#campoCobrador').val(),
        "idDeporte" : $('#campoTipoDeporte').val(),
            "nombreEmbarcacion" : $('#campoNombreBarco').val(),
            "matriculaEmbarcacion" : $('#campoMatricula').val(),
            "tipoEmbarcacion" : $('#campoTipoEmbarcaciones').val(),
            "marcaMotor" : $('#campoMarcaMotor').val(),
            "numeroMotor" : $('#campoNumeroMotor').val(),
            "tipoMotor" : $('#campoTipoMotor').val(),
            "amarre" : $('#campoAmarre').val(),
            "fechaAdquisicion" : $('#campoFechaAdquisicion').val(),
            "anioConstruccion" : $('#campoAnioConstruccion').val(),
            "arboladura" : $('#campoArboladura').val(),
            "eslora" : $('#campoEslora').val(),
            "manga" : $('#campoManga').val(),
            "puntal" : $('#campoPuntal').val(),
            "tonelaje" : $('#campoTonelaje').val(),
            "hp" : $('#campoHP').val(),
            "observacionesBarco" : $('#campoObservacionesBarco').val(),
            "fechaAlta" : $('#campoFechaAlta').val(),
            "nombreAdherente" : $('#campoNombreCompletoSA2').val(),
            "dniAdherente" : $('#campoDNISA2').val(),
            "nacimientoAdherente" : $('#campoFechaNacimientoSA2').val(),
            "paretntezcoAdherente" : $('#campoParentezcoSA2').val()

        };
        $.ajax({
                data:  parametros,
                url:   "CSocio/agregar",
                type:  'post',
                beforeSend: function () {
                        $("#ventanaAgregar").modal('hide');
                        $("#modal_Cargando").modal({backdrop: 'static', keyboard: false});
                        $("#modal_Cargando").modal('show');
                },
                success:  function (response) {
                        actualizarLista();
                        vaciarCampos();
                        $("#modal_Cargando").modal('hide');
                        window.alert(response);

                        var num = String((parseInt($("#campoNroSocio").val())));
                        console.log("el numero es "+num);
                        idSeleccionado=num;
                        numSocioAAgregar();


                    if (confirm('¿Desea agregarle servicios a este socio?')) {
                        agregoServicios=true;
                        abrirVentanaServicios();
                    }
                    else
                    {
                        agregoServicios=false;
                        $("#ventanaAgregar").modal('show');
                    }
                }
        });
}


function irAPosicion(nroSocio)
{
    var table = $('#tabladesocios').dataTable();
    var tabla = $('#tabladesocios').DataTable();
    console.log("Está haciendo la funcion");
    console.log("id seleccionado es "+idSeleccionado);

    tabla.$('tr.selected').removeClass('selected');

    tabla.rows().eq( 0 ).each( function (idx) {
        var row = tabla.row( idx );

        if ( row.data().id === nroSocio ) {
            posicion=idx;
            tabla.row(posicion, {order:'current'}).select();
            tabla.row(posicion).scrollTo();
            console.log("Lo encontró");
        }
    } );
    console.log("Terminó");
}

function copiarDomicilio()
{
    $(document).ready(function () {
        $("#campoDomicilio").keyup(function () {
            var value = $(this).val();
            $("#campoDomicilioDeCobro").val(value);
        });
    });
}


function editarSocio()
{
  var valorRadio = obtenerIdSocioSeleccionado();

    if(valorRadio==-1)
    {
        alert("Debe seleccionar un socio");
    }
    else{

  var parametros = {

	  "nombre" : ($('#campoNombre').val()).toUpperCase(),
                "apellidos" : ($('#campoApellidos').val()).toUpperCase(),
                "dni" : $('#campoDNI').val(),
                "domicilio" : ($('#campoDomicilio').val()).toUpperCase(),
            "genero" : $('#campoGenero').val(),
        "correoElectronico" : $('#campoCorreoElectronico').val(),
        "telefonoFijo" : $('#campoTelefonoFijo').val(),
        "telefonoCelular" : $('#campoTelefonoCelular').val(),
        "nacionalidad" : ($('#campoNacionalidad').val()).toUpperCase(),
        "estadoCivil" : ($('#campoEstadoCivil').val()).toUpperCase(),
        "observaciones" : ($('#campoObservaciones').val()).toUpperCase(),
        "fechaNacimiento" : $('#campoFechaNacimiento').val(),
        "profesion" : ($('#campoProfesion').val()).toUpperCase(),
        "domicilioDeCobro": ($('#campoDomicilioDeCobro').val()).toUpperCase(),
        "intervaloDePago": $('#campoIntervaloDePago').val(),
        "tipoSocio": $('#campoTipoSocio').val(),
		"idCobrador": $('#campoCobrador').val()

        };
        $.ajax({
                data:  parametros,
                url:   "CSocio/Editar/" +valorRadio,
                type:  'post',
                beforeSend: function () {
                        $("#modal_Cargando").modal({backdrop: 'static', keyboard: false});
                        $("#modal_Cargando").modal('show');
                },
                success:  function (response) {
                        $("#modal_Cargando").modal('hide');
                        window.alert(response);
                    $("#ventanaAgregar").modal('hide');
            actualizarLista();
            vaciarCampos();


                }
        });
    }
}

function comprobarCampos()
{
  if($('#campoNombre').val()=='' || $('#campoApellidos').val()=='' || $('#campoDNI').val()==0 ||
    $('#campoDomicilioDeCobro').val()=='' || $('#campoIntervaloDePago').val()==0|| $('#campoFechaNacimiento').val()=='')
  {
    $('#botonAceptar').prop('disabled', true);
    $("#advertenciaDatos").text("Faltan datos obligatorios");
  }

	  else
	  {
		  $('#botonAceptar').prop('disabled', false);
		  $("#advertenciaDatos").text("");
		  }

}


function vaciarCampos()
{
        $('#campoNombre').val("");
        $('#campoApellidos').val("");
        $('#campoDNI').val("");
        $('#campoDomicilio').val("");
        $('#campoCorreoElectronico').val("");
        $('#campoFechaNacimiento').val("");
        $('#campoProfesion').val("");
        //$('#campoGenero').val("");
        $('#campoDomicilioDeCobro').val("");


    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    var yyyy = today.getFullYear();
    if(dd<10){dd='0'+dd}
    if(mm<10){mm='0'+mm}
    today = yyyy+'-'+mm+'-'+dd;

    $("#campoFechaAlta").val(today);

        $('#campoIntervaloDePago').val("1");
        //$('#campoTipoSocio').val("");
        $('#campoNacionalidad').val("Argentino");
        $('#campoTelefonoFijo').val("");
        $('#campoTelefonoCelular').val("");
        $('#campoEstadoCivil').val("");
         $('#campoObservaciones').val("");
		//$('#campoCobrador').val("1");
        $("#campoNroSocio").prop('disabled', true);

    $('#campoTipoDeporte').val(0),
     $('#campoNombreBarco').val(""),
    $('#campoMatricula').val(""),
    $('#campoTipoEmbarcaciones').val(0),
     $('#campoMarcaMotor').val(""),
    $('#campoNumeroMotor').val(""),
     $('#campoTipoMotor').val(""),
     $('#campoAmarre').val(""),
    $('#campoFechaAdquisicion').val(""),
     $('#campoAnioConstruccion').val(""),
     $('#campoArboladura').val(""),
    $('#campoEslora').val(""),
     $('#campoManga').val(""),
     $('#campoPuntal').val(""),
     $('#campoTonelaje').val(""),
   $('#campoHP').val(""),
     $('#campoObservacionesBarco').val(""),
     $('#campoNombreCompletoSA2').val(""),
     $('#campoDNISA2').val(""),
     $('#campoFechaNacimientoSA2').val(""),
     $('#campoParentezcoSA2').val("")


    //Pongo el tab en el primer tab
    $('.nav-tabs a:first').tab('show');
        comprobarCampos();
}

function actualizarTipoSocios()
{
    $.ajax({
                url:   "CTipoSocio/TiposSociosEnSelect",
                type:  'post',
                success:  function (response) {
          $('#tipoSociosEnSelect').html(response);
                }});
}

function actualizarDeportes()
{
    $.ajax({
        url:   "CDeporte/DeportesEnSelect",
        type:  'post',
        success:  function (response) {
            $('#deportesEnSelect').html(response);
            $('#deportesEnSelect2').html(response);
        }});
}

function actualizarTipoEmbarcaciones()
{
    $.ajax({
        url:   "CTipoEmbarcaciones/TiposEmbarcacionesEnSelect2",
        type:  'post',
        success:  function (response) {
            $('#tipoEmbarcacionesEnSelect').html(response);
        }});
}


function actualizarCobradores()
{
    $.ajax({
                url:   "CCobrador/CobradoresSeleccion",
                type:  'post',
                success:  function (response) {
          $('#CobradoresSeleccion').html(response);
                }});
}

function obtenerSocio()
{
  var valorRadio = obtenerIdSocioSeleccionado();

/// Si no hubo ninguno seleccionado
  if(valorRadio==-1)
    {
    window.alert("Debe seleccionar un socio");
    }

    else{
      $("#ventanaAgregar").modal("show");

  $.ajax({
    url: "CSocio/obtenerSocio/" + valorRadio,
    type:  'get',
    dataType: 'json',
    success: function(res) {
        $('#campoNombre').val(res.nombre);
        $('#campoApellidos').val(res.apellidos);
        $('#campoDNI').val(res.dni);
        $('#campoDomicilio').val(res.domicilio);
        $('#campoCorreoElectronico').val(res.correoElectronico);

        $('#campoTelefonoFijo').val(res.telefonoFijo);
        $('#campoTelefonoCelular').val(res.telefonoCelular);
        $('#campoEstadoCivil').val(res.estadoCivil);
        $('#campoNacionalidad').val(res.nacionalidad);
        $('#campoObservaciones').val(res.observaciones);



        $('#campoFechaNacimiento').val(res.fechaNacimiento);
        $('#campoProfesion').val(res.profesion);
        $('#campoGenero').val(res.genero);
        $('#campoDomicilioDeCobro').val(res.domicilioCobro);
        $('#campoIntervaloDePago').val(res.intervaloPago);
        $('#campoTipoSocio').val(res.tiposocio);
		$('#campoCobrador').val(res.cobrador);
        $('#campoFechaAlta').val(res.fechaAlta);
        $("#campoFechaAlta").prop('disabled', true);
        $('#campoNroSocio').val(valorRadio);
        comprobarCampos();
  }});
}
}

function darDeAlta()
{
    var valorRadio = obtenerIdSocioSeleccionado();

/// Si no hubo ninguno seleccionado
    if(valorRadio==-1)
    {
        window.alert("Debe seleccionar un socio");
    }

    else{

        var respuesta = confirm("¿Está seguro que desea volver a darle el alta a ese socio?");
        if(respuesta)
        {
            $.ajax({
                url:"CSocio/DarDeAlta/"+ valorRadio,
                type:  'POST',
                beforeSend: function () {
                    $("#modal_Cargando").modal({backdrop: 'static', keyboard: false});
                    $("#modal_Cargando").modal('show');
                },
                success: function()
                {
                    $("#modal_Cargando").modal('hide');
                    window.alert("El socio ha sido dado de alta con éxito.")
                    actualizarLista();
                }
            });
        }
    }
}

function darDeBaja()
{
  var valorRadio = obtenerIdSocioSeleccionado();

/// Si no hubo ninguno seleccionado
  if(valorRadio==-1)
    {
    window.alert("Debe seleccionar un socio");
    }

    else{

var respuesta = confirm("¿Está seguro que desea dar de baja este socio?");
if(respuesta)
{
   $.ajax({
    url:"CSocio/DarDeBaja/"+ valorRadio,
    type:  'POST',
    beforeSend: function () {
                        $("#modal_Cargando").modal({backdrop: 'static', keyboard: false});
                        $("#modal_Cargando").modal('show');
                },
    success: function()
    {
        $("#modal_Cargando").modal('hide');
      window.alert("El socio ha sido dado de baja con éxito.")
      actualizarLista();
    }
});
}
}
}

function numSocioAAgregar()
{
    $.ajax({
        url:   "CSocio/obtenerMaxIdSocio" ,
        success:  function (response) {
            $("#campoNroSocio").val(response);
                   }
    });
}

function inicializarLista()
{
   $('#tabladesocios').DataTable( {
        "ajax":
        {
            "url": 'CSocio/obtenerSocios',
            "type": 'post',
            "complete": function()
            {

            }
        },
       rowId: 'id',
        "columns": [
            { "data": "id" },
            { "data": "nombreCompleto" },
            { "data": "dni" },
            { "data": "domicilio" },
            { "data": "tipoSocio" },
            { "data": "estado" }
        ],
       aLengthMenu: [
           [-1],
           ["Todos"]
       ],
       iDisplayLength: -1,
       //paging: false,
       scrollY:        500,
       scrollCollapse: true,
       deferRender:    true,
       scroller:       true,
       "fnDrawCallback": function( oSettings ) {

           if(idSeleccionado!=-1)
           {
               var posicion;
               var table = $('#tabladesocios').dataTable();
               posicion = table.fnGetPosition($("#"+idSeleccionado+"")[0]);
               var tabla = $('#tabladesocios').DataTable();
               tabla.row(posicion).nodes().to$().addClass('selected');
           }

       },
       "order": [[1,"asc"]]
   });

   cargarPropiedadesTabla();
}


function actualizarLista()
{
var t = $('#tabladesocios').DataTable();
//t.clear().draw();
 t.ajax.reload(function()
 {
     setTimeout(function(){

         irAPosicion(idSeleccionado);
     }, 500);

 });
}

function seleccionarUltimo()
{
    numSocioAAgregar();
    setTimeout(function(){
        var num = String((parseInt($("#campoNroSocio").val())-1));
        console.log("el numero es "+num);
        irAPosicion(num);
        idSeleccionado=num;
    }, 1000);


}

function cargarPropiedadesTabla()
{
    ///AQUÍ SE CARGAN LAS PROPIEDADES DE LA TABLA
    var table = $('#tabladesocios').DataTable();
                    $('#tabladesocios tbody').on( 'click', 'tr', function () {
                   if ( $(this).hasClass('selected') )
                    {
                        $(this).removeClass('selected');
                        actualizarBotones("");
                        idSeleccionado=-1;
                    }
                    else {
                        table.$('tr.selected').removeClass('selected');
                        $(this).addClass('selected');
                       idSeleccionado = (this.cells[0].innerHTML);
                       actualizarBotones(this.cells[6].innerHTML);
                        }
                    });
}

function actualizarBotones(estadoSocio)
{
    switch (estadoSocio)
    {
        case "Activo":
            $("#botonDarDeAlta").hide("slow");
            $("#botonDarDeBaja").show("slow");
            break;
        case "Inactivo":
            $("#botonDarDeAlta").show("slow");
            $("#botonDarDeBaja").hide("slow");
            break;
        default:
            $("#botonDarDeAlta").hide();
            $("#botonDarDeBaja").hide();
            break;
    }
}


function obtenerIdSocioSeleccionado()
{
    return idSeleccionado;
}




function abrirVentanaServicios()
{
    var valorRadio = obtenerIdSocioSeleccionado();
        if(valorRadio==-1)
    {
        window.alert("Por favor seleccione un socio.")
    }
    else{
            $("#ventanaServicios").modal("show");
            $("#numeroSocio").val(valorRadio);

        actualizarListaSociosAdherentes(valorRadio);
        var scope = angular.element(document.getElementById("controladorAngular")).scope();
        scope.$apply(function () {
        scope.obtenerInscripcionesSocio();
        scope.obtenerEmbarcacionesSocio();
        });

    }
}

function aceptarVentanaServicios()
{
    if(agregoServicios)
    {
        $("#ventanaAgregar").modal('show');
        agregoServicios=false;
    }
}


function actualizarListaSociosAdherentes(nroSocio)
{
    $.ajax({
        url:   "CSocioAdherente/ObtenerSociosAdherentes/" + nroSocio,
        type:  'get',
        dataType: "json",
        success:  function (response)
        {
            // La siguiente línea es para borrar todas las filas excepto la primera (sería la cabecera)
            $("#tablaSociosAdherentes tr:gt(0)").remove();

            $.each(response.sociosAdherentes, function(i, item) {

                // Asigno a la variable socio, cada socio que se recorre del archivo JSON
                var socioAdherente = response.sociosAdherentes[i];

                // Inserto en la tabla cada socio
                insertarElementoEnTablaSociosAdherentes(socioAdherente.id,socioAdherente.nombreCompleto,socioAdherente.dni,socioAdherente.parentezco,socioAdherente.fechaNacimiento);
            });

        }});
}

  function  insertarElementoEnTablaSociosAdherentes(id,nombreCompleto,dni,parentezco,fechaNacimiento)
    {
        var table = document.getElementById('tablaSociosAdherentes');
        var rowCount = table.rows.length;
        var row = table.insertRow(rowCount);
        row.style.textAlign = "center";

        var celdaNombreCompleto = row.insertCell(0);
        var celdaDNI = row.insertCell(1);
        var celdaParentezco = row.insertCell(2)
;        var celdaFechaNacimiento = row.insertCell(3);
        var celdaSeleccion = row.insertCell(4);

        celdaNombreCompleto.innerHTML = nombreCompleto;
        celdaDNI.innerHTML = dni;
        celdaParentezco.innerHTML = parentezco;
        celdaFechaNacimiento.innerHTML = fechaNacimiento;
        celdaSeleccion.innerHTML = '<input type="radio" id="radioAdherente_'+rowCount+'" name="idElegido" value="'+id+'">';
    }


function obtenerIdSocioAdherenteSeleccionado()
{
    var i=1;
    var valorRadio=-1;
    var table = document.getElementById('tablaSociosAdherentes');
    var rowCount = table.rows.length;
    while($("#radioAdherente_"+i).is(":checked")==false && i<=rowCount)
    {
        i++;
    }

/// Si no hubo ninguno seleccionado
    if(i>rowCount)
    {
        valorRadio=-1;
    }

    else{
        (valorRadio = $("#radioAdherente_"+i).val());
    }
    return valorRadio;

}

function abrirVentanaSociosAdherentes()
{
    var valorRadio = obtenerIdSocioSeleccionado();
    vaciarCamposSociosAdherentes();

    if(valorRadio==-1)
    {
        window.alert("Por favor seleccione un socio.")
    }
    else{
        $("#ventanaSociosAdherentes").modal("show");
        actualizarListaSociosAdherentes(valorRadio);
    }

}



function agregarSocioAdherente(){

    var valorRadio = obtenerIdSocioSeleccionado();


    var parametros = {
        "nombreCompleto" : ($('#campoApellidoSA').val()+' '+$('#campoNombreSA').val()).toUpperCase(),
        "dni" : $('#campoDNISA').val(),
        "parentezco": ($('#campoParentezcoSA').val()).toUpperCase(),
        "fechaNacimiento": $('#campoFechaNacimientoSA').val()
}
    $.ajax({
        data:  parametros,
        url:   "CSocioAdherente/Agregar/" + valorRadio,
        type:  'post',
        success:  function (response) {
            if(response=="success")
            {
                actualizarListaSociosAdherentes(valorRadio);
                vaciarCamposSociosAdherentes();
            }
            else{
                window.alert("Ya existe el socio adherente en la base de datos");
            }

        }
    });
}

function vaciarCamposSociosAdherentes()
{
    $('#campoApellidoSA').val("");
    $('#campoNombreSA').val("");
    $('#campoDNISA').val("");
    $('#campoParentezcoSA').val("");
    $('#campoFechaNacimientoSA').val("");
}


function eliminarSocioAdherente()
{
    var valorRadio = obtenerIdSocioSeleccionado();
    var valorRadioAdherente = obtenerIdSocioAdherenteSeleccionado();

        /// Si no hubo ninguno seleccionado
        if(valorRadioAdherente==-1)
        {
            window.alert("Debe seleccionar un socio adherente");
        }

        else{

            var respuesta = confirm("¿Está seguro que desea eliminar este socio adherente?");
            if(respuesta)
            {
                $.ajax({
                    url: "CSocioAdherente/Eliminar/" + valorRadioAdherente,
                    type:  'POST',
                    success: function()
                    {
                        window.alert("El socio adherente ha sido eliminado con éxito.")
                        actualizarListaSociosAdherentes(valorRadio);
                    }
                });
            }
        }
}



var k = new Kibo();

function pasajeTabs() {
  //console.log('last key: ' + k.lastKey());
  if(k.lastKey()=='right')
  {
  $('.nav-tabs a:last').tab('show');
  }
  if(k.lastKey()=='left')
    {
      $('.nav-tabs a:first').tab('show');
    }

}

function crearNuevoSocio()
{
    $("#ventanaAgregar").modal("show");
}

k.down(['shift right','shift left'], pasajeTabs);
k.down(['shift n'], crearNuevoSocio);



function controlarNoNegatividad()
{
    if($("#campoIntervaloDePago").val()<0)
    {
        $("#campoIntervaloDePago").val("");
    }

    if($("#campoDNI").val()<0)
    {
        $("#campoDNI").val("");
    }
}

function comprobarEdad()
{
    var fecha = $("#campoFechaNacimiento").val();

    var edad = calcularEdad(fecha);
	console.log(edad);

//	console.log(is(edad));

    if (isNaN(edad) || (fecha.length>10)){$("#textoFechaNacimiento").html("Formato de fecha incorrecto")}else{
    if(edad<18)
    {
        $("#textoFechaNacimiento").html("Debe ser mayor de 18 años.");
        $('#botonAceptar').prop('disabled', true);
    }
    else{
        $('#botonAceptar').prop('disabled', false);
		$("#textoFechaNacimiento").html("");
		comprobarCampos();
    }
	}

}


function calcularEdad(birthday) {    var year, month, day, age, year_diff, month_diff, day_diff;    var myBirthday = new Date();    var today = new Date();    var array = birthday.split("-");
    year = array[0];    month = array[1];    day = array[2];
    year_diff = today.getFullYear() - year;    month_diff = (today.getMonth() + 1) - month;    day_diff = today.getDate() - day;
    if (month_diff < 0) {        year_diff--;    } else if ((month_diff === 0) && (day_diff < 0)) {        year_diff--;    }    return year_diff;
}




function informeDetallado()
{
  var valorRadio = obtenerIdSocioSeleccionado();

/// Si no hubo ninguno seleccionado
  if(valorRadio==-1)
    {
    window.alert("Debe seleccionar un socio");
    }

    else
    {
      window.open("CSocio/informeDetallado/"+valorRadio);
    }
}