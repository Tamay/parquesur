type = ['','info','success','warning','danger'];


demo = {
    initPickColor: function(){
        $('.pick-class-label').click(function(){
            var new_class = $(this).attr('new-class');
            var old_class = $('#display-buttons').attr('data-class');
            var display_div = $('#display-buttons');
            if(display_div.length) {
            var display_buttons = display_div.find('.btn');
            display_buttons.removeClass(old_class);
            display_buttons.addClass(new_class);
            display_div.attr('data-class', new_class);
            }
        });
    },

    initFormExtendedDatetimepickers: function(){
        $('.datetimepicker').datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
         });
    },

   /*initDocumentationCharts: function(){
        /* ----------==========     Recaudación anual , inicialización para la documentación    ==========---------- *

        datosGraficoRecaudacion = {
            labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            series: [
                [12300, 17500, 14500, 13000, 12800, 12400, 12000, 11900,12000,11900,12000,11000]
            ]
        };

        opcionesGraficoRecaudacion = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: 1000, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0},
        }

        var graficoRecaudacion = new Chartist.Line('#graficoRecaudacion', datosGraficoRecaudacion, opcionesGraficoRecaudacion);

        md.startAnimationForLineChart(graficoRecaudacion);
    },  
    */

    initDashboardPageCharts: function(){

        /* ----------==========    Recaudación anual - inicialización    ==========---------- */

        datosGraficoRecaudacion = {
            labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            series: [
                [12300, 17500, 14500, 13000, 12800, 12400, 12000, 11900,12000,11900,12000,11000]
            ]
        };

        opcionesGraficoRecaudacion = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: 100000, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: { top: 0, right: 0, bottom: 0, left: 10},
        }

        var graficoRecaudacion = new Chartist.Line('#graficoRecaudacion', datosGraficoRecaudacion, opcionesGraficoRecaudacion);

        md.startAnimationForLineChart(graficoRecaudacion);



        /* ----------==========    Actividades que realizan los socios    ==========---------- */

        datosActividadesSocios = {
            labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            series: [
                [230, 750, 450, 300, 280, 240, 200, 190]
            ]
        };

        opcionesGraficoActividadesSocio = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: 1000, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0}
        }

        var graficoActividadesSocio = new Chartist.Line('#graficoActividadesSocio', datosActividadesSocios, opcionesGraficoActividadesSocio);

        // start animation for the Completed Tasks Chart - Line Chart
        md.startAnimationForLineChart(graficoActividadesSocio);



        /* ----------==========     Gráfico de socios activos    ==========---------- */

        var datosParaElGraficoSocios = {
          labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
          series: [
            [542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895]

          ]
        };
        var opcionesParaElGraficoSocios = {
            axisX: {
                showGrid: false
            },
            low: 0,
            high: 1000,
            chartPadding: { top: 0, right: 5, bottom: 0, left: 0}
        };
        var responsiveOptions = [
          ['screen and (max-width: 640px)', {
            seriesBarDistance: 5,
            axisX: {
              labelInterpolationFnc: function (value) {
                return value[0];
              }
            }
          }]
        ];
        var graficodeSocios = Chartist.Bar('#graficoSociosActivos', datosParaElGraficoSocios, opcionesParaElGraficoSocios, responsiveOptions);

        //Comienza la animación para el gráfico de los socios
        md.startAnimationForBarChart(graficodeSocios);

    },


}
