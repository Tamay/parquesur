// JavaScript Document
<!------------------ JAVASCRIPT --------------------> 

var editar=false;
var idSeleccionado=-1;
var idSocioSeleccionado=-1;
function seVaAEditar(vof)
{
  editar=vof;
}

function agregarDeporte(){
        var parametros = {
                "nombre" : ($('#campoNombre').val()).toUpperCase(),
                "monto" : $('#campoMonto').val(),
		        "estado": $('#campoEstado').val()
        };
        $.ajax({
                data:  parametros,
                url:   "CDeporte/agregarDeporte",
                type:  'post',
               	success:  function (response) {

                        if(response=="success")
                        {
                        actualizarLista();
                        vaciarCampos();  
                        }
                        else{
                          window.alert("Ya existe el deporte en la base de datos");

                        }
                }
        });
}

function comprobarCampos()
{
  if($('#campoNombre').val()=='' || $('#campoMonto').val()=='')
  {
    $('#botonAceptar').prop('disabled', true);
    $("#advertenciaDatos").text("Faltan datos obligatorios");
  }
  else
  {
   $('#botonAceptar').prop('disabled', false); 
   $("#advertenciaDatos").text("");
  }   
}

function vaciarCampos()
{
        $('#campoNombre').val("");
        $('#campoMonto').val("");
        $('#campoEstado').val("");
        comprobarCampos();
}

function editarDeporte()
{
  var valorRadio = obtenerIdDeporteSeleccionado();

    if(valorRadio==-1)
    {
        alert("Debe seleccionar una actividad");
    }
    else{


  var parametros = {
      "nombre" : ($('#campoNombre').val()).toUpperCase(),
      "monto" : $('#campoMonto').val(),
      "estado" : $('#campoEstado').val()
        };
        $.ajax({
                data:  parametros,
                url:   "CDeporte/Editar/"+ valorRadio,
                type:  'post',
                beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                },
                success:  function (response) {
                        $("#resultado").html(response);
            actualizarLista();
            vaciarCampos();
                }
        });
    }
}

function darDeBajaDeporte()
{
  var valorRadio = obtenerIdDeporteSeleccionado();

/// Si no hubo ninguno seleccionado
  if(valorRadio==-1)
    {
    window.alert("Debe seleccionar un deporte");
    }
    
    else{

var respuesta = confirm("¿Está seguro que desea dar de baja a este deporte?");
if(respuesta)
{
   $.ajax({
    url:"CDeporte/DarDeBajaDeporte/"+ valorRadio,
    type:  'POST',
    success: function() 
    {
      window.alert("El deporte ha sido dado de baja con éxito.")
      actualizarLista();
    }
});
}
}
}


function inicializarLista()
{
   $('#tabladedeportes').DataTable( {
        "ajax": 'CDeporte/obtenerDeportes',
        "columns": [
            { "data": "id" },
            { "data": "nombre" },
            { "data": "monto" },
            { "data": "estado" }
        ]
    });

   cargarPropiedadesTabla(); 
}


function actualizarLista()
{
var t = $('#tabladedeportes').DataTable();
//t.clear().draw()ñ
 t.ajax.reload();
}

function cargarPropiedadesTabla()
{
    ///AQUÍ SE CARGAN LAS PROPIEDADES DE LA TABLA  
    var table = $('#tabladedeportes').DataTable();
    table.column(3).visible(false);
                    $('#bodyDetabladedeportes').on( 'click', 'tr', function () {
                   if ( $(this).hasClass('selected') ) 
                    {
                        $(this).removeClass('selected');
						idSeleccionado=-1;
						console.log(idSeleccionado);
                    }
                    else {
                        table.$('tr.selected').removeClass('selected');
                        $(this).addClass('selected');
						idSeleccionado = (this.cells[0].innerHTML);
						console.log(idSeleccionado);
						
                        }
                    });

                    $('#button').click( function () {
                   table.row('.selected').remove().draw( false );
                    } );
}

function obtenerIdDeporteSeleccionado()
{
    return idSeleccionado;
}

var k = new Kibo();

function pasajeTabs() {
  //console.log('last key: ' + k.lastKey());
  if(k.lastKey()=='right')
  {
  $('.nav-tabs a:last').tab('show');  
  }
  if(k.lastKey()=='left')
    {
      $('.nav-tabs a:first').tab('show');   
    }
  
}

function obtenerDeporte()
{
	var valorRadio = obtenerIdDeporteSeleccionado();

/// Si no hubo ninguno seleccionado
  if(valorRadio==-1)
    {
    window.alert("Debe seleccionar una actividad");
    }
    else
	{
      $("#ventanaAgregarDeporte").modal("show");
     // var valorRadio = $("#radio_"+i).val();
  $.ajax({
    url: "CDeporte/obtenerDeporte/" + valorRadio,
    type:  'get',
    dataType: 'json',
    success: function(res) {
        $('#campoNombre').val(res.nombre);
        $('#campoMonto').val(res.monto);
        $('#campoEstado').val(res.estado);
        comprobarCampos();

   		}});
	}
  }
  
function inicializarListaSocios()
{
   $('#tablaBusquedaSocios').DataTable( {
        "ajax": 'CSocio/obtenerSocios',
        "columns": [
            { "data": "id" },
            { "data": "nombreCompleto" },
            { "data": "domicilio" },
            { "data": "dni" }
        ]
    });

   cargarPropiedadesTablaSocios(); 
}


function actualizarListaSocios()
{
var t = $('#tablaBusquedaSocios').DataTable();
//t.clear().draw()ñ
 t.ajax.reload();
}

function cargarPropiedadesTablaSocios()
{
    ///AQUÍ SE CARGAN LAS PROPIEDADES DE LA TABLA  
    var table = $('#tablaBusquedaSocios').DataTable();
                    $('#bodyTablaSocio').on( 'click', 'tr', function () {
						
                   if ( $(this).hasClass('selected') ) 
                    {
						
                        $(this).removeClass('selected');
                        idSocioSeleccionado=-1;
						console.log(idSocioSeleccionado);
                    }
                    else {
                        table.$('tr.selected').removeClass('selected');
                        $(this).addClass('selected');
                        idSocioSeleccionado = (this.cells[0].innerHTML);
						console.log(idSocioSeleccionado);
                        }
                    
                    
                    });

                    $('#button').click( function () {
                   table.row('.selected').remove().draw( false );
                    } );
                    /// Aquí termina lo relacionado con las propiedades de la tabla
}
function obtenerIdSocioSeleccionado()
{
    return idSocioSeleccionado;
}

function abrirVentanaInscripcion()
{
    var valorRadio = obtenerIdDeporteSeleccionado();

    if(valorRadio==-1)
    {
        window.alert("Por favor seleccione un deporte.")
    }
    else{
        $("#ventanaAgregarInscripcion").modal("show");

    }

}

function agregarInscripcion(){
	var idDeporte = obtenerIdDeporteSeleccionado();
	var idSocio = obtenerIdSocioSeleccionado();
		var parametros = {
                "socio" : idSocio,
                "deporte" : idDeporte
        };
		if(idSocio==-1){
            window.alert("Debe seleccionar un socio");
        }
        else{
        $.ajax(
		{
                data:  parametros,
                url:   "CInscripcion/agregarInscripcion",
                type:  'post',
                success:  function (response) 
				{
                        if(response!="success")
                        {                  
                          window.alert("Ya existe esa inscripción en la base de datos");
                        }    
						else 
						{
							window.alert("El socio se ha inscripto a la actividad con éxito.")
						} 
                }
        });
		}
}