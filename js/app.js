/**
 * Created by Fer on 18/09/16.
 */


/**
 * Funcion que devuelve true o false dependiendo de si la fecha es correcta.
 * Tiene que recibir el dia, mes y año
 */
function isValidDate(day,month,year)
{
    var dteDate;
 
    // En javascript, el mes empieza en la posicion 0 y termina en la 11 
    //   siendo 0 el mes de enero
    // Por esta razon, tenemos que restar 1 al mes
    month=month-1;
    // Establecemos un objeto Data con los valore recibidos
    // Los parametros son: año, mes, dia, hora, minuto y segundos
    // getDate(); devuelve el dia como un entero entre 1 y 31
    // getDay(); devuelve un num del 0 al 6 indicando siel dia es lunes,
    //   martes, miercoles ...
    // getHours(); Devuelve la hora
    // getMinutes(); Devuelve los minutos
    // getMonth(); devuelve el mes como un numero de 0 a 11
    // getTime(); Devuelve el tiempo transcurrido en milisegundos desde el 1
    //   de enero de 1970 hasta el momento definido en el objeto date
    // setTime(); Establece una fecha pasandole en milisegundos el valor de esta.
    // getYear(); devuelve el año
    // getFullYear(); devuelve el año
    dteDate=new Date(year,month,day);
 
    //Devuelva true o false...
    return ((day==dteDate.getDate()) && (month==dteDate.getMonth()) && (year==dteDate.getFullYear()));
}
 



function calcularEdad(fecha)
{
         window.alert(fecha);// Si la fecha es correcta, calculamos la edad
         fecha=fecha.toString();
        var values=fecha.split("-");
        var dia = values[2];
        var mes = values[1];
        var ano = values[0];
 
        // cogemos los valores actuales
        var fecha_hoy = new Date();
        var ahora_ano = fecha_hoy.getYear();
        var ahora_mes = fecha_hoy.getMonth()+1;
        var ahora_dia = fecha_hoy.getDate();
 
        // realizamos el calculo
        var edad = (ahora_ano + 1900) - ano;
        if ( ahora_mes < mes )
        {
            edad--;
        }
        if ((mes == ahora_mes) && (ahora_dia < dia))
        {
            edad--;
        }
        if (edad > 1900)
        {
            edad -= 1900;
        }
 
        // calculamos los meses
        var meses=0;
        if(ahora_mes>mes)
            meses=ahora_mes-mes;
        if(ahora_mes<mes)
            meses=12-(mes-ahora_mes);
        if(ahora_mes==mes && dia>ahora_dia)
            meses=11;
 
        // calculamos los dias
        var dias=0;
        if(ahora_dia>dia)
            dias=ahora_dia-dia;
        if(ahora_dia<dia)
        {
            ultimoDiaMes=new Date(ahora_ano, ahora_mes, 0);
            dias=ultimoDiaMes.getDate()-(dia-ahora_dia);
        }
 
        return años;
}




var angularTodo = angular.module('sociosApp', []);

angularTodo.controller('mainController',function($scope, $http){
    $scope.deportes = [ ];
    $scope.tipoEmbarcaciones = [ ];
    $scope.inscripcionesSocio = [ ];
    $scope.embarcacionesSocio = [ ];


    //Obtengo los deportes para la lista
    $http.get('CDeporte/obtenerDeportes')
        .success(function(data) {
            $scope.deportes = eval(data.data);
            console.log(data)
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });

    //Obtengo los tipos de embaracaciones para la lista
    $http.get('CTipoEmbarcaciones/obtenerTipoEmbarcaciones')
        .success(function(data) {
            $scope.tipoEmbarcaciones = eval(data.tipoEmbarcaciones);
            console.log(data)
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });


    $scope.obtenerInscripcionesSocio = function()
    {
        $http.post('CInscripcion/ObtenerInscripciones',{socio:parseInt($("#numeroSocio").val())})
            .success(function(data) {
                $scope.inscripcionesSocio = eval(data.data);
                console.log(data)
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    }

    $scope.agregarInscripcion = function() {
        $http.post('CInscripcion/agregarInscripcion', { deporte: parseInt($scope.deporteSeleccionado), socio: parseInt($("#numeroSocio").val())})
            .success(function(data) {
                window.alert(data);
                console.log(data)
                $scope.obtenerInscripcionesSocio();
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    }

    $scope.eliminarInscripcion = function( idInscripcion ) {
        if ( confirm("¿Está seguro que desea eliminar este servicio?") ) {
            $http.post('CInscripcion/DarDeBajaUnaInscripcion/'+idInscripcion)
                .success(function(data) {
                    console.log(data)
                    $scope.obtenerInscripcionesSocio();
                })
                .error(function(data) {
                    window.alert('Error: ' + data);
                    console.log('Error: ' + data);
                });
        }
    }

    $scope.agregarEmbarcacion = function() {
        $http.post('CEmbarcacion/agregar',
            { matricula: ($scope.matriculaEmbarcacion).toUpperCase(),
                nombre: ($scope.nombreEmbarcacion).toUpperCase(),
                tipoEmbarcaciones: $scope.tipoEmbarcacion,
                motorMarca: $scope.marcaMotorEmbarcacion,
                motorNumero:$scope.numeroMotorEmbarcacion,
                motorTipo: $scope.tipoMotorEmbarcacion,
                amarre: $scope.amarreEmbarcacion,
                fechaAdquisicion: $scope.fechaAdquisicionEmbarcacion,
                anioConstruccion: $scope.anioConstruccionEmbarcacion,
                arboladura: $scope.arboladuraEmbarcacion,
                eslora: $scope.esloraEmbarcacion,
                manga: $scope.mangaEmbarcacion,
                puntal:$scope.puntalEmbarcacion,
                tonelaje:$scope.tonelajeEmbarcacion,
                HP:$scope.hpEmbarcacion,
                observaciones: $scope.observacionesEmbarcacion,
                socio_id:parseInt($("#numeroSocio").val())})
            .success(function(data) {
                window.alert(data);
                console.log(data)
                $scope.obtenerEmbarcacionesSocio();


                $scope.matriculaEmbarcacion='';
                $scope.nombreEmbarcacion='';
                $scope.arboladuraEmbarcacion='';
                $scope.marcaMotorEmbarcacion='';
                $scope.numeroMotorEmbarcacion='';
                $scope.tipoMotorEmbarcacion='';
                $scope.amarreEmbarcacion='';
                $scope.fechaAdquisicionEmbarcacion='';
                $scope.anioConstruccionEmbarcacion='';
                $scope.esloraEmbarcacion='';
                $scope.mangaEmbarcacion='';
                $scope.puntalEmbarcacion='';
                $scope.tonelajeEmbarcacion='';
                $scope.hpEmbarcacion='';
                $scope.observacionesEmbarcacion='';

            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    }

    $scope.obtenerEmbarcacionesSocio = function()
    {
        $http.post('CEmbarcacion/obtenerEmbarcaciones',{socio:parseInt($("#numeroSocio").val())})
            .success(function(data) {
                $scope.embarcacionesSocio = eval(data.data);
                console.log(data)
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    }

    $scope.eliminarEmbarcacion = function( idEmbarcacion ) {
        if ( confirm("¿Está seguro que desea eliminar esta embarcacion?") ) {
            $http.post('CEmbarcacion/DarDeBaja/'+idEmbarcacion)
                .success(function(data) {
                    console.log(data)
                    $scope.obtenerEmbarcacionesSocio();
                })
                .error(function(data) {
                    console.log('Error: ' + data);
                });
        }
    }
});
