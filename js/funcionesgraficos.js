type = ['','info','success','warning','danger'];
        
    var vectorValoresMontoAnual=[];  
    var vectorValoresSociosAnual=[];  

    






demo = {
    initPickColor: function(){
        $('.pick-class-label').click(function(){
            var new_class = $(this).attr('new-class');
            var old_class = $('#display-buttons').attr('data-class');
            var display_div = $('#display-buttons');
            if(display_div.length) {
            var display_buttons = display_div.find('.btn');
            display_buttons.removeClass(old_class);
            display_buttons.addClass(new_class);
            display_div.attr('data-class', new_class);
            }
        });
    },




    initFormExtendedDatetimepickers: function(){
        $('.datetimepicker').datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
         });
    },

   /*initDocumentationCharts: function(){
        /* ----------==========     Recaudación anual , inicialización para la documentación    ==========---------- *

        datosGraficoRecaudacion = {
            labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            series: [
                [12300, 17500, 14500, 13000, 12800, 12400, 12000, 11900,12000,11900,12000,11000]
            ]
        };

        opcionesGraficoRecaudacion = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: 1000, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0},
        }

        var graficoRecaudacion = new Chartist.Line('#graficoRecaudacion', datosGraficoRecaudacion, opcionesGraficoRecaudacion);

        md.startAnimationForLineChart(graficoRecaudacion);
    },  
    */


    initDashboardPageCharts: function(){


        obtenerRecaudacionAnual();
        obtenerSociosAnual();



        /* ----------==========    Recaudación anual - inicialización    ==========---------- */
        datosGraficoRecaudacion = {
            labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            series: [
                [vectorValoresMontoAnual[0], vectorValoresMontoAnual[1], vectorValoresMontoAnual[2], vectorValoresMontoAnual[3], vectorValoresMontoAnual[4], vectorValoresMontoAnual[5], vectorValoresMontoAnual[6], vectorValoresMontoAnual[7],vectorValoresMontoAnual[8],vectorValoresMontoAnual[9],vectorValoresMontoAnual[10],vectorValoresMontoAnual[11]]
            ]
        };

        opcionesGraficoRecaudacion = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: 500000, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: { top: 0, right: 0, bottom: 0, left: 15}
        }

        var graficoRecaudacion = new Chartist.Line('#graficoRecaudacion', datosGraficoRecaudacion, opcionesGraficoRecaudacion);

        md.startAnimationForLineChart(graficoRecaudacion);



        /* ----------==========     Gráfico de socios activos    ==========---------- */

        var datosParaElGraficoSocios = {
          labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
          series: [
            [vectorValoresSociosAnual[0], vectorValoresSociosAnual[1], vectorValoresSociosAnual[2], vectorValoresSociosAnual[3], vectorValoresSociosAnual[4], vectorValoresSociosAnual[5], vectorValoresSociosAnual[6], vectorValoresSociosAnual[7], vectorValoresSociosAnual[8], vectorValoresSociosAnual[9], vectorValoresSociosAnual[10], vectorValoresSociosAnual[11]]

          ]
        };
        var opcionesParaElGraficoSocios = {
            axisX: {
                showGrid: false
            },
            low: 0,
            high: 1000,
            chartPadding: { top: 0, right: 5, bottom: 0, left: 0}
        };
        var responsiveOptions = [
          ['screen and (max-width: 640px)', {
            seriesBarDistance: 5,
            axisX: {
              labelInterpolationFnc: function (value) {
                return value[0];
              }
            }
          }]
        ];
        var graficodeSocios = Chartist.Bar('#graficoSociosActivos', datosParaElGraficoSocios, opcionesParaElGraficoSocios, responsiveOptions);

        //Comienza la animación para el gráfico de los socios
        md.startAnimationForBarChart(graficodeSocios);

    }
}


        //Obtiene un vector con la recaudacion producida en los 12 meses del año
        function obtenerRecaudacionAnual()
            {   
                    
                var response=httpGet('CReporte/recaudacionAnual');

                //var array = JSON.parse(response);
                //vectorValoresMontoAnual = array;
                
                vectorValoresMontoAnual = JSON.parse(response);
                console.log("Mes 01 -"+ vectorValoresMontoAnual[0]);

                console.log("El vector de recaudacion anual es: "+vectorValoresMontoAnual);
            }
        
        
        //Obtiene un vector con la cantidad de socios que hay en los 12 meses del año
        function obtenerSociosAnual()
            {   
                    
                var response=httpGet('CReporte/sociosAnual');

                vectorValoresSociosAnual = JSON.parse(response);
            }


            function httpGet(theUrl)
                {
                    var xmlHttp = new XMLHttpRequest();
                    xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
                    xmlHttp.send( null );
                    return xmlHttp.responseText;
                }

